function openFeedbackWindow(ele,upd_ele,id)
{
    Effect.Appear(ele);
    var back1 = document.getElementById ('backgroundpopup-call');
    back1.style.display = "block";
}
function closeFeedbackWindow(ele1){
    var val1=document.getElementById(ele1);    
    var background=document.getElementById('backgroundpopup-call');
    Effect.Fade(val1);
    Effect.Fade(background);
    $$('div.error-massage').each(function(ele){
            ele.hide();
    });
}
function sendCallback(url){
    if(callback_form && callback_form.validate()){
        $('loader').show();
        $('btnsubmit').setAttribute('disabled', true);
        var parameters=$('frm_callback').serialize(true);
        new Ajax.Request(url, {
                method: 'post',
                dataType: 'json',
                parameters: parameters,
                onSuccess: function(transport) {
                    if(transport.status == 200) {
                        var response=transport.responseText.evalJSON();
                        $('success_message_callback').innerHTML=response.message;
                        if(response.result=='success'){
                            $('success_message_callback').removeClassName('callback-error-msg');
                            $('success_message_callback').addClassName('callback-success-msg');
                        }
                        else{
                            $('success_message_callback').removeClassName('callback-success-msg');
                            $('success_message_callback').addClassName('callback-error-msg');
                        }
                        $('loader').hide();
                        $('success_message_callback').show();
                        if(response.recaptcha == 1){
                            Recaptcha.reload();
                        }
                        Effect.toggle('success_message_callback', 'appear',{ duration: 5.0});
                        if(response.result=='success'){
                            setTimeout(function (){
                                    closeFeedbackWindow('callback_information');
                                    $('frm_callback').reset();
                                    $('btnsubmit').removeAttribute('disabled');
                                },6000);
                        }
                        $('btnsubmit').removeAttribute('disabled');
                        return false;
                    }
                }
        });
        return false;
    }
}