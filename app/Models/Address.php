<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{

  protected $table = 'addresses';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'user_id', 'address', 'country', 'city', 'state', 'pin_code', 'phone', 'ship_to_different_address','shipping_address', 'shipping_country', 'shipping_city', 'shipping_state', 'shipping_pin_code', 'shipping_phone'
  ];
}
