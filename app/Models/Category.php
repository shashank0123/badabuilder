<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Category extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'categories';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['category_name','category_id','slug','featured','status','category_description','category_img'];
    // protected $hidden = [];
    // protected $dates = [];

    public function setCategoryImgAttribute($value)
    {
        $attribute_name = "category_img";
        $disk = "public";
        $destination_path = "products";
        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }


    public function uploadFileToDisk($value, $attribute_name, $disk, $destination_path)
    {
      $request = \Request::instance();

      // if a new file is uploaded, delete the file from the disk
      if ($request->hasFile($attribute_name) &&
          $this->{$attribute_name} &&
          $this->{$attribute_name} != null) {
          \Storage::disk($disk)->delete($this->{$attribute_name});
      $this->attributes[$attribute_name] = null;
  }

      // if the file input is empty, delete the file from the disk
  if (is_null($value) && $this->{$attribute_name} != null) {
      \Storage::disk($disk)->delete($this->{$attribute_name});
      $this->attributes[$attribute_name] = null;
  }

      // if a new file is uploaded, store it on disk and its filename in the database
  if ($request->hasFile($attribute_name) && $request->file($attribute_name)->isValid()) {
          // 1. Generate a new file name
      $file = $request->file($attribute_name);
      $new_file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();

          // 2. Move the new file to the correct path
      $file_path = $file->storeAs($destination_path, $new_file_name, $disk);

          // 3. Save the complete path to the database
      $this->attributes[$attribute_name] = 'storage/' . $file_path;
  }
}


    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getCategoryCountAttribute()
    {
        return \DB::table('categories')->where('category_id', $this->category_id)->count('id');
    }

    public function getProductCountAttribute()
    {
        return \DB::table('products')->where('category_id', $this->id)->count('id');
    }

    public function getCategoryIdAttribute($value)
    {
        $category = \DB::table('categories')->where('id', $value)->first();
        if(!empty($category))
            $value = $category->category_name;
        else
            $value="";

        return $value;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
