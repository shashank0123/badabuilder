<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Headline extends Model
{
	use CrudTrait;

    protected $fillable = ['headline'];
}
