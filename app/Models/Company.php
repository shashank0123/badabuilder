<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Company extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'companies';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['name', 'email', 'address', 'contact_no', 'support_email', 'sort_discription', 'about_us', 'privacy_policy', 'return_policy'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    // public function getAddressAttribute($value)
    // {
    //     return $this->attributes['address'] = strip_tags($value);
    // }

    // public function getSortDiscriptionAttribute($value)
    // {
    //     return  $this->attributes['sort_discription'] =  strip_tags($value);
    // }

    // public function getAboutUsAttribute($value)
    // {
    //     return $this->attributes['about_us'] = strip_tags($value);
    // }

    // public function getPrivacyPolicyAttribute($value)
    // {
    //     return  $this->attributes['privacy_policy'] =  strip_tags($value);
    // }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
