<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Address;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Review;
use App\Wishlist;
use App\User;
use App\Models\Company;
use App\Models\Query;
use App\Models\Faq;
use App\Models\Newsletter;
use App\Models\Help;
use Log;
use DB;
use Auth;
use Mail;
use StdClass;

class SiteController extends Controller
{
  public function getIndex(){
    $companyInfo = Company::first();
    return view('fyc-web.index', compact('companyInfo'));
  }

  public function getProductDetail(Request $request,$slug){
    $product = Product::where('slug',$slug)->first();
    return view('fyc-web.product-detail',compact('product'));
  }

    // get category wise product
  public function getCategoryWiseProduct($slug, Request $request)
  {
      // code...
  }
    //Category Wise Products Selection
  public function getCategoryProducts($id,Request $request){
    $checkId = Category::where('slug',$id)->first();
    $subcat = $checkId->id;

    $keyword = $request->product_keyword;
    $max_price = Product::where('status','Active')->max("sell_price");

    $cartproducts = array();
    $color = array();

    $brand = array();
    $material = array();
    $size = array();
    $quantity = array();
    $cnt = 0;
    if(session()->get('cart') != null){
      foreach(session()->get('cart') as $cart){
        $cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
        $quantity[$cnt] = $cart->quantity;
        $cnt++;
      }
    }
    else{
      $cartproducts = null;
      $quantity = 0;
    }

    $lastproduct = Product::where('status','Active')->orderBy('created_at','desc')->first();


    $maincategory = Category::where('category_id',0)->where('status','Active')->get();

    foreach($maincategory as $row){
      $count=0;
      $count=Product::where('category_id',$row->id)->where('status','Active')->count();
      $category = Category::where('category_id',$row->id)->where('status','Active')->get();
      if($category){
        foreach($category as $col){
          $count+=Product::where('category_id',$col->id)->where('status','Active')->count();
          $subcategory = Category::where('category_id',$col->id)->where('status','Active')->get();
          if($subcategory){
            foreach($subcategory as $set){
              $count+=Product::where('category_id',$set->id)->where('status','Active')->count();
            }
          }
        }

      }
    }

    $searched_products = array();
    $cat_ids = array();

    $total = array();
    $i=0;
    $sec = Category::where('slug',$id)->where('status','Active')->first();
    $cat_ids[0] = Category::where('id',$sec->id)->where('status','Active')->first();

    $cid = 1;
    $cnt_pro = 0;
    $count_catpro = 0;
    $idss = Category::where('category_id',$sec->id)->where('status','Active')->get();
    if($idss != null){
      foreach($idss as $ids){
        $count_catpro = 0;
        $cat_ids[$cid++] = $ids;
        $idsss = Category::where('category_id',$ids->id)->where('status','Active')->get();
        $count_catpro+=Product::where('category_id',$ids->id)->where('status','Active')->count();
        if($idsss != null){
          foreach($idsss as $ds){
            $cat_ids[$cid++] = $ds;
            $count_catpro+=Product::where('category_id',$ds->id)->where('status','Active')->count();
          }
          if($count==0)
            $total[$i++] = 0;
          else
            $total[$i++] = $count_catpro;
        }
      }
    }

    // foreach ($total as $t){
    //   echo $t." / ";
    // }

    for($j=0 ; $j<$cid ; $j++){
      $products = Product::where('category_id',$cat_ids[$j]->id)->where('status','Active')->get();
      if($products != null){
        foreach($products as $pro){
          $searched_products[$cnt_pro] = $pro;
          $color[$cnt_pro] = $pro->product_color;
          $brand[$cnt_pro] = $pro->product_brand;
          $material[$cnt_pro] = $pro->product_material;
          $cnt_pro++;
        }
      }
    }


    $color = array_filter($color);
    $color = array_unique($color);

    $brand = array_filter($brand);
    $brand = array_unique($brand);

    $material = array_filter($material);
    $material = array_unique($material);

    shuffle($searched_products);

    return view('fyc-web.categorywise-products',compact('maincategory','searched_products','cat_ids','total','lastproduct','max_price','cartproducts','quantity','id','color','brand','material'));
  }

  public function getSearchedProducts(){
   return view('fyc-web.searched-products');
 }

 public function getWishlist($id){
  $wishlist = Wishlist::where('user_id',$id)->get();

  return view('fyc-web.wishlist',compact('wishlist'));
}

public function getCheckoutPage(){
  $productDetails = array();
  $totalAmount = 0;
  if (session()->get('cart')) {
    foreach (session()->get('cart') as $key => $value) {
      $product = Product::where('id', $value->product_id)->first();
      $product['quantity'] = $value->quantity;
      $product['total_price'] = $product->sell_price * $value->quantity;
      $totalAmount += $product['total_price'];
      array_push($productDetails, $product);
    }
  } else {
    return redirect('/');
  }

  $success ='';
  $address = Address::where('user_id', Auth::user()->id)->first();
  $allAddress = Address::where('user_id', Auth::user()->id)->where('id','!=',$address->id)->get();

      // Log::info($address);
  return view('fyc-web.checkout', compact('productDetails', 'totalAmount', 'success', 'address', 'allAddress'));
}

public function orderProduct(Request $request)
{
  $validatedData = $request->validate([
        // 'country' => 'required',
        // 'address' => 'required',
        // 'city' => 'required',
        // 'state' => 'required',
        // 'pin_code' => 'required',
        // 'phone' => 'required',
        // 'payment_type' => 'required',
  ]);

  $data = $request->all();

//   var_dump($data);
//   die;

  DB::beginTransaction();

  $phone = $request->phone;
  $amount = $request->totalAmount;
  
  // try {
  if(!empty(Auth::user()) ){
    $userId = Auth::user()->id;

            // $productDetails = array();
    $totalAmount = $request['totalAmount'];
    $request['user_id'] = $userId;
    $add_id = $request->add_id;
    $address="";
    if($add_id!=NULL)
    $address = Address::where('user_id', $userId)->where('id',$add_id)->first();
  else
    $address = Address::where('user_id', $userId)->first();
    $address2 = new Address;
    $key = $request->ship_to_different_address;
    $sub_key = $request->shipped; 
    // die;
    $shipping = 0;

    if(!empty($address)){
      $address->address = $request->address;
      $address->country = $request->country;
      $address->state = $request->state;
      $address->city = $request->city;
      $address->pin_code = $request->pin_code;
      $address->phone = $request->phone;
      $address->update();
      $phoneno = $request->phone1;
    }
    else{
      $address = new Address;
      $address->address = $request->address;
      $address->user_id = $userId;
      $address->country = $request->country;
      $address->state = $request->state;
      $address->city = $request->city;
      $address->pin_code = $request->pin_code;
      $address->phone = $request->phone;
      $address->save();
      $phoneno = $request->phone1;
    }

    if(isset($key) && $key == 'different'){
      if($sub_key != NULL){
        $shipping = $sub_key;
      }
      else{
        $address2->user_id = $userId;
        $address2->address = $request->shipping_address;
        $address2->city = $request->shipping_city;
        $address2->state = $request->shipping_state;
        $address2->country = $request->shipping_country;
        $address2->pin_code = $request->shipping_pin_code;
        $address2->phone = $request->shipping_phone;

        $address2->save();
        $shipping = $address2->id;
      }
    }
    else{
      $shipping = $address->id;
    }

    if($request->payment_method == 'cash_on_delivery')
      $order_status = "order_booked";
    else
      $order_status = "in_process";
      
    $orders = new Order;

    $orders->user_id = $userId;
    $orders->address =  $address->id;
    $orders->shipping_address =  $shipping;
    $orders->price =   $totalAmount;
    $orders->payment_method =  $request['payment_method'];
    $orders->date = date('Y-m-d H:m:s');
    $orders->status = $order_status;
    $orders->save();

        // Log::info($address);
        // Log::info($request);
        // if($address){
        //   Address::find($address->id)->update($request->toArray());
        // }else {
        //   $address = Address::create($request->toArray());
        // }
        // if($request['ship_to_different_address']){
        //   $transactionData = array('user_id' => $userId, 'address' => $address->shipping_address.' '.$address->shipping_city.' '.$address->shipping_state.' '.$address->shipping_pin_code.' '.$address->shipping_country,
        //                      'price' => $totalAmount, 'payment_method' => $request['payment_method'], 'status' => 'order_booked');
        // } else {
        //   $transactionData = array('user_id' => $userId, 'address' => $address->address.' '.$address->city.' '.$address->state.' '.$address->pin_code.' '.$address->country,
        //                      'price' => $totalAmount, 'payment_method' => $request['payment_method'], 'status' => 'order_booked');
        // }
        // $order = Order::create($transactionData);

    if (session()->get('cart')) {
     $i = 1;

     foreach(session()->get('cart') as $cart){
      $order_item = new OrderItem;
      $order_item->order_id = $orders->id;
      $order_item->item_id = $cart->product_id;
      $price = Product::where('id',$cart->product_id)->first();
      $order_item->quantity = $cart->quantity;
      $order_item->price = $cart->quantity * $price->sell_price;
      $order_item->save();
      $i++;
    }

            // $product = Product::where('id', $value->product_id)->first();
            // $product['quantity'] = $value->quantity;
            // $product['price'] = $product->sell_price * $value->quantity;
            // // $totalAmount += $product['total_price'];
            // // $product['user_id'] = $userId;
            // $product['order_id'] = $orders->id;
            // $product['item_id'] = $product->id;
            // $orderedItem = OrderItem::create($product->toArray());
            // array_push($productDetails, $orderedItem);
  }
}
// else {
//   return redirect('/');
// }
        // update total amount
DB::commit();
          // send mail after purchase
try {
  $user = Auth::user();
  $productDetails = DB::table('order_items')
  ->join('products', 'products.id', '=', 'order_items.item_id')
  ->where('order_items.order_id', $orders->id)
  ->select('products.name', 'order_items.quantity', 'order_items.price')
  ->get();
                                  // Log::info($productDetails);
                //  Mail::send('mails.orderInvoice', ['user' => $user, 'order' => $order, 'productDetails' => $productDetails],
                //  function ($m) use ($user) {
                //      $m->from( env('MAIL_USERNAME'), env('APP_NAME') );

                //      $m->to($user->email, $user->name)->subject('Order Invoice');
                //  });
}
catch(Exception $e)
{
                 // return redirect()->back()->with('error', 'something went wrong');
 return view('mails.orderInvoice', compact('user', 'order', 'productDetails'));
}
          // all good
//  catch (\Exception $e) {
//   DB::rollback();
//   return redirect()->back()->with('error', 'something went wrong');
// }

if($request->payment_method == 'cash_on_delivery'){
session()->forget('cart');
return redirect('order-booked')->with('success', 'Order Booked Successfully');}
else{
  $price = $data['totalAmount'];
return view('payment.payment',compact('phone','user','amount','price'));
// return view('fyc-web.event',compact('phone','user','amount'));
}
}


//Payumoney Form Submit
public function checkoutForm1(Request $request,$id){
        $success = "";
        if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0){
    //Request hash
            $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';   
            if(strcasecmp($contentType, 'application/json') == 0){
                $data = json_decode(file_get_contents('php://input'));
                $hash=hash('sha512', $data->key.'|'.$data->txnid.'|'.$data->amount.'|'.$data->pinfo.'|'.$data->fname.'|'.$data->email.'|||||'.$data->udf5.'||||||'.$data->salt);
                $json=array();          
                $json['success'] = $hash;
                echo json_encode($json);        
            }
            exit(0);
        }
    } 

    public function getSuccess(Request $request){      

        $email = $_POST['email'];
        $txnid = $_POST['txnid'];
        $name = $_POST['firstname'];
        $amount = $_POST['amount'];
        $phone = $_POST['phone'];
        $status = 'Success';

        $order = Order::where('user_id',\Auth::user()->id)->orderBy('created_at','DESC')->where('status','in_process')->first();

        $order->status = 'order_booked';
        $order->transaction_id = $txnid;
        $order->update();
        
        session()->forget('cart');

   return view('payment.success',compact('name','phone','txnid','amount'));
}   

public function getAboutUs(){
  $aboutCompany = Company::first();
  return view('fyc-web.about-us', compact('aboutCompany'));
}

public function getContactUs(){
  $companyInfo = Company::first();
  return view('fyc-web.contact', compact('companyInfo'));
}

public function getMyAccount(){
  $user = Auth::user();
  $addresses = "";
  $orderDetails = Order::where('user_id', $user->id)->get();
  $address = Address::where('user_id', $user->id)->first();
  if(!empty($address))
  $addresses = Address::where('user_id', $user->id)->where('id','!=',$address->id)->get();
  $help = Help::where('user_id', $user->id)->get();
  return view('fyc-web.my-account', compact('user', 'orderDetails', 'address', 'help','addresses'));
}

public function getFaq(){
  $faq = Faq::where('status', 'active')->orderBy('created_at', 'desc')->get();
  return view('fyc-web.faq', compact('faq'));
}

public function getTermsandCondition(){
 return view('badabuilder.terms');
}

public function getPrivacyPolicy(){
  $companyInfo = Company::first();
  return view('badabuilder.privacy_policy', compact('companyInfo'));
}

public function getReturnPolicy(){
  $companyInfo = Company::first();
  return view('badabuilder.return-policy', compact('companyInfo'));
}
    // giveProductRating
public function giveProductRating(Request $request)
{
  Review::create($request->toArray());

  return redirect()->back()->with('success', 'Thanks for your feedback');
}

    // orderedItemDetails
public function orderedItemDetails($orderId)
{
      // $orderItemDetails = OrderItem::where('order_id', $orderId)->get();
  $orderItemDetails = DB::table('products')
  ->join('order_items', 'order_items.item_id', '=', 'products.id')
  ->where('order_items.order_id', $orderId)
  ->get();
  $order_tran = Order::where('id',$orderId)->first();
  return view('fyc-web.ordered-item-details', compact('orderItemDetails','order_tran'));
}


    // orderedItemDetails
public function orderedItemTrack($orderId)
{
  $orderItemTrack = array('order_placed' => false, 'processing' => false, 'preparing' => false, 'shiped' => false, 'delivered' => false);
      // $orderItemDetails = OrderItem::where('order_id', $orderId)->get();
      // $orderItemDetails = DB::table('products')
      //                     ->join('order_items', 'order_items.item_id', '=', 'products.id')
      //                     ->where('order_items.order_id', $orderId)
      //                     ->get();
  $orderDetails = Order::where('id', $orderId)->first();
  if ($orderDetails) {
        // Log::info($orderDetails);
    switch ($orderDetails->status) {
      case 'order booked':
      $orderItemTrack['order_placed'] =true;
      break;
      case 'processing':
      $orderItemTrack['order_placed'] =true;
      $orderItemTrack['processing'] =true;
      break;
      case 'preparing':
      $orderItemTrack['order_placed'] =true;
      $orderItemTrack['processing'] =true;
      $orderItemTrack['preparing'] =true;
      break;
      case 'shiped':
      $orderItemTrack['order_placed'] =true;
      $orderItemTrack['processing'] =true;
      $orderItemTrack['preparing'] =true;
      $orderItemTrack['shiped'] =true;
      break;
      case 'delivered':
      $orderItemTrack['order_placed'] =true;
      $orderItemTrack['processing'] =true;
      $orderItemTrack['preparing'] =true;
      $orderItemTrack['shiped'] =true;
      $orderItemTrack['delivered'] =true;
      break;
      default:
            // code...
      break;
    }

  }
      // Log::info($orderItemTrack);
  return view('fyc-web.ordered-item-track', compact('orderItemTrack'));
}
    // globalSearchProduct
public function globalSearchProduct(Request $request)
{
      // Log::info($request);
      // if category selected then find product under specific category otherwise all
  $query = (new Product)->newQuery();
  

  if($request->has('product_keyword') && $request->input('product_keyword') != ''){
    $query->where('name', 'Like', '%' . $request->input('product_keyword') . '%');
  }

  $product = $query->orderBy('created_at', "desc")->get();
  return view('fyc-web.searched-products', compact('product'));
}


    /*
     * share snap on social site
     */
    public function shareLinkOnSocialSite($siteKey , $slug_url, $product_id = null, $user_badge = null, Request $request)
    {
     $productDetails = Product::where('slug', $slug_url)->first();
     if ($productDetails) {

       $shareUrl = env('APP_URL').'/product-detail/'.$productDetails->slug;
       $redirectBackurl = env('APP_URL').'/SharedLink/callback/fb?product_id='.$productDetails->id;
       $imageUrl =  env('APP_URL').'/'.$productDetails->image2;
       $text = $productDetails->long_descriptions;
       switch($siteKey)
       {
         case 'fb':
            // $this->increamentSocialShare($review_badge,"facebook");
             // $fbAppId = 745670775803120;
         $fbUrl = 'https://www.facebook.com/dialog/share?app_id='.env('FACEBOOK_APP_ID').'&display=page'.'&href='.urlencode($shareUrl).'&redirect_uri='.urlencode($redirectBackurl);
         return redirect()->intended($fbUrl);

         break;

         case 'tw':
                 // $this->increamentSocialShare($review_badge,"twitter");
         $twUrl="https://twitter.com/intent/tweet?url=".urlencode($shareUrl)."&text=".urlencode($text);
         return redirect()->intended($twUrl);
         break;
         case 'pi':
              // $this->increamentSocialShare($review_badge,"pinterest");
         $piUrl ="https://www.pinterest.com/pin/create/button/?url=".urlencode($shareUrl)."&media=".$imageUrl."&description=".urlencode($text);
         return redirect()->intended($piUrl);
         break;
       }
     }else {
       return redirect('/');
     }
   }

       /*
        * callback share review on social site
        */
       public function SharedLinkCallback($siteKey,Request $request)
       {
          // Log::info($request);
          // Log::info($request->input('snap_badge'));
        $productDetails = Product::where('id', $request->input('product_id'))->first();
        if ($productDetails) {
          switch($siteKey)
          {
            case 'fb':
            $redirectBackurl = $_ENV['APP_URL'].'/product-detail/'.$productDetails->slug;
            return redirect()->intended($redirectBackurl) ;
            break;
          }
        }else {
          return redirect('/');
        }
      }
      // contactUs
      public function contactUs(Request $request)
      {
        $company = Company::first();

        if(empty($company)){
          $company = new StdClass;
          $company->email = 'samriddhicreators@gmail.com';
          $company->name = 'BADABUILDER';

        }
        $request['response_status'] = 'awating';
        if(Query::create($request->toArray())){
          try {
           Mail::send('mails.contact', ['request' => $request], function ($m) use ($company, $request) {
             $m->from( env('MAIL_USERNAME'), env('APP_NAME') );

             $m->to($company->email, $company->name)->subject('Welcome to '.env('APP_NAME'));
           });
         }
         catch(Exception $e)
         {

         }
       }
       return redirect()->back()->with('successStatus', 'Thanks for contacting us!');
     }
      // Newsletter
     public function createNewsletter(Request $request)
     {
      if ($request->has('email')) {
        $newsletter = Newsletter::where('email', $request->input('email'))->first();
        if($newsletter){
          return redirect()->back()->with('NewslatterMessege', 'Already Registered. Thank You"');
        }else {
          $newsCreate = array('email' => $request->input('email'), 'status' => 'Active');
          Newsletter::create($newsCreate);
          return redirect()->back()->with('NewslatterMessege', 'Thank you .We will reply you soon');
        }
      }else {
        return redirect()->back()->with('NewslatterMessege', 'First Enter Your Email');
      }
    }

    public function orderProceed(){
    $user = Auth::user();
    $totalAmount = 0;

      // $lastOrder = Order::latest('id')->first();
      // $lastOrder = DB::table('orders')->first();
      // var_dump($lastOrder); die;
      // print_r($lastOrder->id); die;

    if (session()->get('cart')) {

       foreach(session()->get('cart') as $cart){
          $price = Product::where('id',$cart->product_id)->first();
          $totalAmount = $totalAmount+($cart->quantity * $price->sell_price);
      }

      $orders = new Order;

      $orders->user_id = $user->id;
      $orders->address =  0;
      $orders->shipping_address =  0;
      $orders->price =   $totalAmount;
      $orders->payment_method =  "";
      $orders->date = date('Y-m-d H:m:s');
      $orders->status = 'pending';
      $orders->save();

      $order = DB::table('orders')->where('user_id',$user->id)->get()->last();

    //   print_r($order);
      // print_r($orders);
      // die;

      $i = 1;

      foreach(session()->get('cart') as $cart){
          $order_item = new OrderItem;
          $order_item->order_id = $order->id ?? '0';
          $order_item->item_id = $cart->product_id;
          $price = Product::where('id',$cart->product_id)->first();
          $order_item->quantity = $cart->quantity;
          $order_item->price = $cart->quantity * $price->sell_price;
          $order_item->save();
          $i++;
      }

      $productDetails = DB::table('order_items')
      ->join('products', 'products.id', '=', 'order_items.item_id')
      ->where('order_items.order_id', $order->id)
      ->select('products.name', 'order_items.quantity', 'order_items.price')
      ->get();

    //   $user->admin = 'samriddhicreators@gmail.com';
      $user->admin = 'jyotikasethi3007@gmail.com';
      $user->site = 'BadaBuilder';
      $action = 'admin';

    //   var_dump($productDetails); die;

    // Log::info($productDetails);
                 Mail::send('mails.orderInvoice', ['user' => $user, 'order' => $order, 'productDetails' => $productDetails, 'action' => $action],
                 function ($m) use ($user) {
                     $m->from( env('MAIL_USERNAME'), env('APP_NAME') );

                     $m->to($user->admin, $user->site)->subject('Products Request');
                 });
                 
                 $action = 'User';
                 Mail::send('mails.orderInvoice', ['user' => $user, 'order' => $order, 'productDetails' => $productDetails, 'action' => $action],
                 function ($m) use ($user) {
                     $m->from( env('MAIL_USERNAME'), env('APP_NAME') );

                     $m->to($user->email, $user->name)->subject('Products Request');
                 });

      session()->forget('cart');

      return redirect()->back()->with('requestSuccess','1');


  }

  else{
   return redirect()->back()->with('requesttFail','1');

}

}


public function getDesign(){
  return view('badabuilder.design');
}

public function getCBT(){
  return view('badabuilder.cbt');
}

public function getConstruction(){
  return view('badabuilder.construction');
}

public function getOpinion($term){
  return view('badabuilder.opinion',compact('term'));
}

  }
