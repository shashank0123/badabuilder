<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Testimonial;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Category;
use App\Models\Address;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Review;
use App\Wishlist;
use App\Models\Banner;
use App\Models\Company;
use App\Models\Brand;
use App\Models\Query;
use App\Models\Faq;
use App\Models\Newsletter;
use App\Models\Help;
use Log,DB,Auth,Mail,StdClass;

class FrontController extends Controller
{
    public function index(){
        $allProducts = array();
        $banners = Banner::where('status','Active')
        ->where('image_type','slider')
        ->orderBy('created_at','DESC')
        ->get();

        $brands = Brand::where('status','Active')
        ->get();

        $allCategories = Category::where('featured','yes')
        ->where('category_id',0)
        ->get();

        $topCategories = Category::where('category_id',0)
        ->where('featured','yes')
        ->where('status','Active')
        ->orderBy('id','DESC')
        ->limit(4)
        ->get();


        $recently_viewed_products = Product::where('status','Active')->orderBy('click_counts','DESC')->limit(6)->get();


        if(!empty($topCategories)){

            foreach($topCategories as $category){
                $i=0;
                $cat = array();
                $cat[$i] = $category->id;
                $item = array();
                $cat2 = Category::where('category_id',$category->id)->get();
                if(!empty($cat2)){
                    foreach($cat2 as $cate2){
                        $cat[] = $cate2->id;
                        $cat3 = Category::where('category_id',$cate2->id)->get();
                        if(!empty($cat3)){
                            foreach($cat3 as $cate3){
                                $cat[] = $cate3->id;

                            }
                        }
                    }
                }
                $product = Product::where('status','Active')->whereIn('category_id',$cat)->orderBy('created_at','DESC')->limit(8)->get();

                if(count($product)>0){
                    $item['products']= $product;
                    $item['category']= $category;
                    $allProducts[] = $item;
                    $i++;
                }
            }
        }

        // var_dump($allProducts);die;


        $featured_products = Product::where('status','Active')
        ->where('trending','yes')
        ->orderBy('created_at','DESC')
        ->limit(6)
        ->get();

        $trending_products = Product::where('status','Active')
        ->orderBy('created_at','DESC')
        ->limit(6)
        ->get();

        $top_products = Product::where('status','Active')
        ->orderBy('created_at','DESC')
        ->limit(6)
        ->get();

        $full_banner = Banner::where('status','Active')
        ->where('image_type','full')
        ->orderBy('created_at','DESC')
        ->get()
        ->last();
        
        $best_sellers_produts = Product::where('status','Active')
        ->orderBy('created_at','DESC')
        ->orderBy('sell_price','ASC')
        ->limit(6)
        ->get();

        $page = 'home';

        return view('badabuilder.index',compact('banners','full_banner','allCategories','brands','featured_products','trending_products','top_products','allProducts','recently_viewed_products','best_sellers_produts','topCategories','page'));
        // return view('fyc.index',compact('products','best_products','banners','medium_banner','testimonials','full_banners'));
    }


    

    public function searchedProducts(Request $request){
        $data = $request->all();
        $keyword = $data['keyword'];
        
        $searched_products = Product::where('name','LIKE',$keyword.'%')
        ->where('status','Active')
        ->get();

        $categories = Category::where('featured','yes')
        ->where('category_id',0)
        ->get();
        $brands = Brand::where('status','Active')
        ->get();
              $min=0;

              $maxPrice = Product::orderBy('sell_price','DESC')->first();
        $max = $maxPrice->sell_price;

        return view('badabuilder.products', compact('searched_products','keyword','categories','brands','min','max'));
     // return view('fyc-web.home',compact('companyInfo'));
    }


    public function getProducts(Request $request, $slug){
        $category = "";
        $categories = "";
        $searched_products = "";
        $min=0;
        $brands = Brand::where('status','Active')
        ->get();

        if($slug=='all'){
            $categories = Category::where('featured','yes')
            ->where('category_id',0)
            ->get();

            $searched_products = Product::where('status','Active')
            ->orderBy('created_at','DESC')
            ->get();
            $category = 'All';
        }
        else{
            $cat = array();
            $Category = Category::where('slug',$slug)->first();
            $category = ucfirst($Category->category_name);
            $categories = Category::where('category_id',$Category->id)->get();
            $cat[0] = $Category->id;
            if(!empty($categories)){

                $i=1;
                foreach($categories as $cate){
                    $cat[$i++] = $cate->id;

                    $cat2 = Category::where('category_id',$cate->id)->get();
                    if(!empty($cat2)){
                        foreach($cat2 as $cate2){
                            $cat[$i++] = $cate2->id;
                            $cat3 = Category::where('category_id',$cate2->id)->get();
                            if(!empty($cat3)){
                                foreach($cat3 as $cate3){
                                    $cat[$i++] = $cate3->id;
                                }
                            }
                        }
                    }
                }
            }

            // var_dump($cat); die;

            $searched_products = Product::where('status','Active')->whereIn('category_id',$cat)->orderBy('created_at','DESC')->get();

        }   

        $maxPrice = Product::orderBy('sell_price','DESC')->first();
        $max = $maxPrice->sell_price;

        return view('badabuilder.products',compact('searched_products','categories','slug','max','category','brands','min'));
    }

    public function getOffers(){
        $offers = Offer::where('status','Active')
        ->get();
        return view('fyc-web.offers',compact('offers'));
    }




    public function getAbout(){
        $maincategories = Category::where('category_id',0)->get();
        return view('badabuilder.about',compact('maincategories'));
    }

    public function getCart(){

        $products = array(); 
        $total = 0;
        if(!empty(session()->get('cart'))){
            $carts = session()->get('cart');
            foreach($carts as $cart){
                $product = Product::where('id',$cart->product_id)->first();
                $product->quantity = $cart->quantity;
                $product->subtotal = $cart->total_price;
                $products[] = $product;
                $total = $total + $cart->total_price;

            }

        }

        // if(count($products) == 0){
        //     return redirect('/')->with('cartMessage','Cart is empty. First add products in cart.');
        // }
        // var_dump($products); die;
        return view('badabuilder.cart',compact('products','total'));
    }

    public function emptyCart(){
        session()->forget('cart');
        return redirect('/')->with('cartMessage','Cart items deleted successfully.');
    }
    

    public function getCheckout(){
        $productDetails = array();
        $totalAmount = 0;
        if (session()->get('cart')) {
            foreach (session()->get('cart') as $key => $value) {
              $product = Product::where('id', $value->product_id)->first();
              $product['quantity'] = $value->quantity;
              $product['total_price'] = $product->sell_price * $value->quantity;
              $totalAmount += $product['total_price'];
              array_push($productDetails, $product);
          }
      } else {
        return redirect('/');
    }

    $success ='';
    $address = Address::where('user_id', Auth::user()->id)->first();
    if(isset($address))
      $allAddress = Address::where('user_id', Auth::user()->id)->where('id','!=',$address->id)->get();
  else
    $allAddress="";


return view('fyc-web.checkout', compact('productDetails', 'totalAmount', 'success', 'address', 'allAddress'));

}

public function getContact(){
   $companyInfo = Company::first();

   return view('badabuilder.contact-us',compact('companyInfo'));
}

public function getFaq(){
    return view('fyc-web.faq');
}

public function getLogin(){
    return view('fyc-web.login');
}

public function getAccount(){
    $user = Auth::user();
    $addresses = "";
    $orderDetails = Order::where('user_id', $user->id)->where('status','order_booked')->get();
    $address = Address::where('user_id', $user->id)->first();
    $upd_add = Address::where('user_id', $user->id)->first();

    if(!empty($address))
      $addresses = Address::where('user_id', $user->id)->where('id','!=',$address->id)->get();
  $help = Help::where('user_id', $user->id)->get();
  
  return view('fyc-web.my-account', compact('user', 'orderDetails', 'address', 'help','addresses','upd_add'));
}

public function getProductDetails(Request $request,$slug){
    $productDetail = Product::where('slug',$slug)->first();
    
    $productCategory = Category::where('category_name',$productDetail->category_id)->first();
    $related_products = Product::where('category_id',$productDetail->category_id)->get();
    $productReview = Review::where('product_id', $productDetail->id)->avg('rating');
    $countReview = Review::where('product_id', $productDetail->id)->count();

    $productReview = floor($productReview);
    $reviews = Review::where('product_id',$productDetail->id)->get();

    $discount = $productDetail->mrp-$productDetail->sell_price;
    $discount = ($discount*100)/$productDetail->mrp;
    $discount = number_format($discount, 2);

    return view('badabuilder.productDetail',compact('productDetail','productCategory','related_products','productReview','countReview','reviews','discount'));
}



public function getRegister(){
    return view('fyc-web.register');
}


public function getWishlist(){
    $id=Auth::user()->id;
    $products = array();
    $wishlist = Wishlist::where('user_id',$id)->get();

    if(!empty($wishlist)){
        foreach($wishlist as $list){
            $product = Product::where('id',$list->product_id)->first();
            if(!empty($product)){
                $products[] = $product;
            }
        }
    }

    return view('badabuilder.wishlist',compact('products'));

}

public function emptyWishlist(){
    $id=Auth::user()->id;
    $products = array();
    $wishlist = Wishlist::where('user_id',$id)->delete();


    return redirect()->back()->with('successWishlist','Wishlist deleted successfully.');

}

public function getSeller(){
    $offers = Offer::where('status','Active')->orderBy('created_at','DESC')->get();
    return view('fyc-web.seller',compact('offers'));
}

public function getOfferDetail(Request $request){
    $id = $request->id;
    $offer = Offer::find($id);
    return response()->json(['offer'=>$offer]);
}





}


