<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wishlist;
use App\Cart;
use App\Models\Product;
use App\Models\Category;
use App\Models\Newsletter;
use App\User;
use App\Models\Brand;
use App\Models\Address;
use DB;
use Auth;
use Hash;
use StdClass;
use Log;


class AjaxController extends Controller
{
    public function addToCart(Request $request){

        $cart = new Cart;

        $cartItem = 0;

        $product_id = $request->id;
        $quantity = $request->quantity ?? '1';

        $product = Product::where('id',$product_id)->first();
        if (session()->get('cart') != null){
            $cart = session()->get('cart');
        }
        else {
            $cart = array();
        }

        if (isset($cart[$product_id]->quantity)){
            $cart[$product_id]->quantity = $cart[$product_id]->quantity + $quantity;
            $cart[$product_id]->total_price = $cart[$product_id]->quantity*$cart[$product_id]->unit_price;
        }
        else{
            $item = new StdClass;
            $item->product_id = $product_id;
            $item->quantity = $quantity;
            $item->unit_price = $product->sell_price;
            $item->total_price = $quantity*$product->sell_price;
            $cart[$product_id] = $item;
        }

        $request->session()->put('cart',$cart);

        foreach(session()->get('cart') as $getCart){
            $cartItem++;
        }

        if(!$cart){
            echo "Something Went Worng";
        }

        return response()->json(['status' => 200, 'message' => 'data retrieved','cartItem' => $cartItem]);
    }

    public function addToWishlist(Request $request){
    	$product_id = $request->id;
        $user_id = $request->userId;
        $response = new StdCLass;
        $response->wishItem = 0;
        $check = Wishlist::where('user_id',$user_id)->where('product_id',$product_id)->first();
        if($check){

        }
        else{
            $new_user = new  Wishlist;
            $new_user->product_id = $product_id;
            $new_user->user_id = $user_id;

            $new_user->save();
        }

        $response->wishItem = Wishlist::where('user_id',$user_id)->count();

        return response()->json($response);

    }

    public function submitNewsletter(Request $request)
    {
        $response = new StdClass;
        $response->status = 200;
        $response->message = 'Something went wrong';
        $newsletter = new Newsletter;
        $checkemail = Newsletter::where('email',$request->email)->first();

        if($checkemail){
            $response->message = "Already Registered. Thank You";
            return response()->json($response);
        }
        else{
            if($request->email == null){
                $response->message = 'First Enter Your Email';
                $response->email = $request->email;
                return response()->json($response);
            }
            else{
                $newsletter->email = $request->email;
                $newsletter->save();
                if($newsletter){
                    $response->message ="Thank you .We will reply you soon";
                    return response()->json($response);
                }
                else{
                    return response()->json($response);
                }
            }
        }
    }

    public function deleteWishlistItem(Request $request){
        $flag = 0;
        $response = new StdClass;
        $response->flag = 0;
        $response->wishItem=0;
        $wishlist = Wishlist::where('product_id',$request->id)
        ->where('user_id',Auth::user()->id)
        ->first();
        $wishlist->delete();
        $cartItem = 0;

        $response->wishItem = Wishlist::where('user_id',$request->user_id)->count();

        return response()->json($response);
    }

    //To show session data on Mini Cart
    public function showMiniCart(){
        $cartproducts = array();
        $quantity = array();
        $i = 0;
        $bill = 0;
        if(session()->get('cart') != null){
            foreach(session()->get('cart') as $cart){
                $cartproducts[$i] = Product::where('id',$cart->product_id)->first();
                $quantity[$i] = $cart->quantity;
                $i++;
            }
            $j=0;
            foreach(session()->get('cart') as $cart){
                $quantity[$j];
                $j++;
            }

            foreach(session()->get('cart') as $pro){
                $product = Product::where('id',$pro->product_id)->first();
                $price = $product->sell_price;
                $bill = $bill + $pro->quantity*$price;
            }
        }

        return view('fyc-web.minicart',compact('cartproducts','quantity','bill'))->render();
        
    }

    public function deleteSessionData(Request $request)
    {
        $flag = 1 ;
        $bill = 0;
        $cart = session()->get('cart');
        $cartItem = 0;
        $cart[$request->id] = null;
        $cart = array_filter($cart);
        $request->session()->put('cart',$cart);

        foreach(session()->get('cart') as $pro){
            $cartItem++;
            $product = Product::where('id',$pro->product_id)->first();
            $price = $product->sell_price;
            $bill = $bill + $pro->quantity*$price;
        }
        if($flag == 1)
            return response()->json(['status' => 200, 'message' => 'Data Deleted Successfully','bill' => $bill, 'cartItem' => $cartItem]);
        else
            return response()->json(['status' => 201, 'message' => 'Something Went Wrong']);
    }

    public function updateCart(Request $request)
    {
        $flag = 1 ;
        $bill = 0;
        $total = 0;
        $action = $request->action;
        $item = Product::where('id',$request->id)->first();
        $cart = session()->get('cart');
        if($action == 'plus'){
         $cart[$request->id]->quantity = $cart[$request->id]->quantity+1;
     }
     else{
         $cart[$request->id]->quantity = $cart[$request->id]->quantity-1;
     }

     $cart[$request->id]->total_price = $cart[$request->id]->quantity*$cart[$request->id]->unit_price;

     $total = $cart[$request->id]->quantity * $item->sell_price;
     $cart = array_filter($cart);
     $request->session()->put('cart',$cart);
     $cartItem = 0;
     foreach(session()->get('cart') as $pro){
        $product = Product::where('id',$pro->product_id)->first();
        $price = $product->sell_price;
        $bill = $bill + $pro->quantity*$price;
        $cartItem++;
    }
    $bill;

    if($flag == 1)
        return response()->json(['status' => 200, 'message' => 'Data Deleted Successfully', 'bill' => $bill, 'total' => $total, 'cartItem' => $cartItem]);
    else
        return response()->json(['status' => 201, 'message' => 'Something Went Wrong']);
}


public function filterProducts(Request $request){

    $data = $request->all();

    $sort = "";
    $field = "";
    $keyword = "";
    $brand = "";
    $min = explode('₹',$data['min'])[1];
    $max = explode('₹',$data['max'])[1];

    $categories = "";

    if(!empty($data['keyword'])){
        $keyword = $data['keyword'];
    }

    if(!empty($data['brand'])){
        $checkbrand = Brand::where('slug',$data['brand'])->first();
        $brand = $checkbrand->id;
    }

    if(!empty($data['sort'])){
        if($data['sort'] == 'low-high'){
            $sort = 'ASC';
            $field = 'name';
        }
        elseif($data['sort'] == 'high-low'){
            $sort = 'DESC';
            $field = 'name';
        }
        elseif($data['sort'] == 'priceAsc'){
            $sort = 'ASC';
            $field = 'sell_price';
        }
        elseif($data['sort'] == 'priceDesc'){
            $sort = 'DESC';
            $field = 'sell_price';
        }
        elseif($data['sort'] == 'new'){
            $sort = 'DESC';
            $field = 'created_at';
        }
    }
    else{
        $sort = 'DESC';
        $field = 'created_at';
    }

    if($data['category'] == 'all'){
        $categories = Category::where('featured','yes')
            ->where('category_id',0)
            ->get();
        if(!empty($keyword) && !empty($brand) && !empty($sort)){
            $searched_products = Product::where('product_brand',$brand)
            ->where('name','LIKE',$keyword."%")
            ->where('sell_price','>=',$min)
            ->where('sell_price','<=',$max)
            ->orderBy($field,$sort)
            ->get();
        }
        else if(!empty($keyword) && !empty($brand)){
            $searched_products = Product::where('product_brand',$brand)
            ->where('name','LIKE',$keyword."%")
            ->where('sell_price','>=',$min)
            ->where('sell_price','<=',$max)
            ->orderBy($field,$sort)
            ->get();
        }
        else if(!empty($keyword) && !empty($sort)){
            $searched_products = Product::where('name','LIKE',$keyword."%")
            ->where('sell_price','>=',$min)
            ->where('sell_price','<=',$max)
            ->orderBy($field,$sort)
            ->get();
        }
        else if(!empty($brand) && !empty($sort)){
            $searched_products = Product::where('product_brand',$brand)
            ->where('sell_price','>=',$min)
            ->where('sell_price','<=',$max)
            ->orderBy($field,$sort)
            ->get();
        }
        else if(!empty($keyword)){
            $searched_products = Product::where('name','LIKE',$keyword."%")
            ->where('sell_price','>=',$min)
            ->where('sell_price','<=',$max)
            ->orderBy($field,$sort)
            ->get();
        }
        else if(!empty($brand)){
            $searched_products = Product::where('product_brand',$brand)
            ->where('sell_price','>=',$min)
            ->where('sell_price','<=',$max)
            ->orderBy($field,$sort)
            ->get();
        }
        else if(!empty($sort)){
            $searched_products = Product::orderBy($field,$sort)
            ->where('sell_price','>=',$min)
            ->where('sell_price','<=',$max)
            ->get();
        }
        else{
            $searched_products = Product::orderBy($field,$sort)
            ->where('sell_price','>=',$min)
            ->where('sell_price','<=',$max)
            ->get();
        }
    }

    else{

        $cat = array();
            $Category = Category::where('slug',$data['category'])->first();
            $category = ucfirst($Category->category_name);
            $categories = Category::where('category_id',$Category->id)->get();
            $cat[0] = $Category->id;
            if(!empty($categories)){

                $i=1;
                foreach($categories as $cate){
                    $cat[$i++] = $cate->id;

                    $cat2 = Category::where('category_id',$cate->id)->get();
                    if(!empty($cat2)){
                        foreach($cat2 as $cate2){
                            $cat[$i++] = $cate2->id;
                            $cat3 = Category::where('category_id',$cate2->id)->get();
                            if(!empty($cat3)){
                                foreach($cat3 as $cate3){
                                    $cat[$i++] = $cate3->id;
                                }
                            }
                        }
                    }
                }
            }

        if(!empty($keyword) && !empty($brand) && !empty($sort)){
            $searched_products = Product::where('product_brand',$brand)
            ->whereIn('category_id',$cat)
            ->where('name','LIKE',$keyword."%")
            ->where('sell_price','>=',$min)
            ->where('sell_price','<=',$max)
            ->orderBy($field,$sort)
            ->get();
        }
        else if(!empty($keyword) && !empty($brand)){
            $searched_products = Product::where('product_brand',$brand)
            ->whereIn('category_id',$cat)
            ->where('name','LIKE',$keyword."%")
            ->where('sell_price','>=',$min)
            ->where('sell_price','<=',$max)
            ->orderBy($field,$sort)
            ->get();
        }
        else if(!empty($keyword) && !empty($sort)){
            $searched_products = Product::where('name','LIKE',$keyword."%")
            ->whereIn('category_id',$cat)
            ->where('sell_price','>=',$min)
            ->where('sell_price','<=',$max)
            ->orderBy($field,$sort)
            ->get();
        }
        else if(!empty($brand) && !empty($sort)){
            $searched_products = Product::where('product_brand',$brand)
            ->whereIn('category_id',$cat)
            ->where('sell_price','>=',$min)
            ->where('sell_price','<=',$max)
            ->orderBy($field,$sort)
            ->get();
        }
        else if(!empty($keyword)){
            $searched_products = Product::where('name','LIKE',$keyword."%")
            ->whereIn('category_id',$cat)
            ->where('sell_price','>=',$min)
            ->where('sell_price','<=',$max)
            ->orderBy($field,$sort)
            ->get();
        }
        else if(!empty($brand)){
            $searched_products = Product::where('product_brand',$brand)
            ->whereIn('category_id',$cat)
            ->where('sell_price','>=',$min)
            ->where('sell_price','<=',$max)
            ->orderBy($field,$sort)
            ->get();
        }
        else if(!empty($sort)){
            $searched_products = Product::whereIn('category_id',$cat)
            ->where('sell_price','>=',$min)
            ->where('sell_price','<=',$max)
            ->orderBy($field,$sort)
            ->get();
        }
        else{
            $searched_products = Product::whereIn('category_id',$cat)
            ->where('sell_price','>=',$min)
            ->where('sell_price','<=',$max)
            ->orderBy($field,$sort)
            ->get();
        }
    }
    
    return view('badabuilder.productCat',compact('searched_products','categories'));
}

    //Show Sorted Products on Search Category Page
public function sorting(Request $request){
    $searched_products = array();

    $i=0;
    $sort_type = $request->sort_type;
    $checkId = Category::where('slug',$request->cat_id)->first();
    $send_id = $checkId->id;
    $min = $request['min_cost'];
    $max = $request['max_cost'];        
    $color = $request['color'];

    $category_ids = array();
    $j = 1;
    $category_ids[0] = $send_id;

    $cat = Category::where('category_id',$send_id)->get();

    if($cat != null){
        foreach($cat as $cid){
            $category_ids[$j++] = $cid->id;
            $subcat = Category::where('category_id',$cid->id)->get();
            if($subcat != null){
                foreach($subcat as $sid){
                    $category_ids[$j++] = $sid->id;
                }
            }
        }
    }

    if($color==""){
        $i=0;
        for($k=0 ; $k<$j ; $k++){
           $products = Product::where('category_id',$category_ids[$k])
           ->where('status','Active')
           ->get();

           if($products != null){
            foreach($products as $product){
                if($product->sell_price>=$min && $product->sell_price<=$max){
                    $searched_products[] = $product;
                }
            }
        }
    }
}        

else{
    $i=0;
    for($k=0 ; $k<$j ; $k++){
        $products = Product::where('category_id',$category_ids[$k])
        ->where('product_color','LIKE','%'.$color.'%')
        ->where('status','Active')
        ->get();
        if($products != null){
            foreach($products as $product){
                if($product->sell_price>=$min && $product->sell_price<=$max){
                    $searched_products[] = $product;
                }
            }
        }
    }
}

if($sort_type == 'name-asc'){
    $name = array_column($searched_products, 'name');
    array_multisort($name, SORT_ASC, $searched_products);
}

else if($sort_type == 'name-desc'){
    $created_at = array_column($searched_products, 'name');
    array_multisort($created_at, SORT_DESC, $searched_products);
}

else if($sort_type == 'created'){
    $created_at = array_column($searched_products, 'created_at');
    array_multisort($created_at, SORT_ASC, $searched_products);
}

else if($sort_type == 'low-high'){
    $sell_price = array_column($searched_products, 'sell_price');
    array_multisort($sell_price, SORT_ASC, $searched_products);
}

else if($sort_type == 'high-low'){
    $sell_price = array_column($searched_products, 'sell_price');
    array_multisort($sell_price, SORT_DESC, $searched_products);
}
return view('fyc-web.product_cat',compact('searched_products','send_id'));
}
    // getProductDetailsData
public function getProductDetailsData($productId)
{
 $productDetails = Product::where('id', $productId)->first();
 $category = Category::where('id',$productDetails->category_id)->first(); 

 $discount = $productDetails->mrp - $productDetails->sell_price;
 if($discount == 0){
 }
 else{
    $discount = ($discount*100)/$productDetails->mrp; 
}


return response()->json(['status' => 200, 'data' => $productDetails, 'category' => $category, 'discount' => $discount]);
}
    // filter record by colored
public function colored(Request $request)
{
    $category_ids = array();
    $category_id = $request->cat_id;
    $color = $request->color;
    $checkId = Category::where('slug',$request->cat_id)->first();
    $send_id = $checkId->id;

    if($checkId != null){
        $midcat = Category::where('category_id',$checkId->id)->get();
        if($midcat != null){

            foreach($midcat as $cid){
                $category_ids[] = $cid->id;
                $subcat = Category::where('category_id',$cid->id)->get();
                if($subcat != null){
                    foreach($subcat as $sid){
                        $category_ids[] = $sid->id;
                    }
                }
            }
        }
    }

    $searched_products = array();

    foreach($category_ids as $fin_id){
       $products = Product::where('category_id',$fin_id)
       ->where('product_color','LIKE','%'.$color.'%')
       ->get();

       if(!empty($products)){
        foreach ($products as $product) {
            if($product->sell_price>=$request['min_cost']  && $product->sell_price<=$request['max_cost']){
                $searched_products[] = $product;
            }
        }
    }   
}

return view('fyc-web.product_cat', compact('searched_products', 'category_id', 'color_code', 'send_id'));
}

    // filter record by price
public function price(Request $request)
{
  $category_ids = array();
  $category_id = $request->cat_id;
  $color_code = $request->color;
  $searched_products = $arrayName = array();


  if($category_id == 'all'){
    $searched_products = Product::where('status','Active')
    ->where('sell_price','>=',$request->min_cost)
    ->where('sell_price','<=',$request->max_cost)
    ->get();
}
else if($category_id == 'trending'){
    $searched_products = Product::where('status','Active')
    ->where('trending','yes')
    ->where('sell_price','>=',$request->min_cost)
    ->where('sell_price','<=',$request->max_cost)
    ->get();
}
else{


  $checkId = Category::where('slug',$request->cat_id)->first();
  $send_id = $checkId->id;

  if($checkId != null){
    $midcat = Category::where('category_id',$checkId->id)->get();
    if($midcat != null){

        foreach($midcat as $cid){
            $category_ids[] = $cid->id;
            $subcat = Category::where('category_id',$cid->id)->get();
            if($subcat != null){
                foreach($subcat as $sid){
                    $category_ids[] = $sid->id;
                }
            }
        }
    }
}

foreach($category_ids as $fin_id){
    $products= Product::where('category_id',$fin_id)
    ->where('status','Active')
    ->get();
    if(!empty($products)){
        foreach($products as $pro){
            if($pro->sell_price>=$request['min_cost'] && $pro->sell_price<=$request['max_cost']){
                $searched_products[]=$pro;
            }
        }
    }
}
}

return view('fyc-web.product_cat', compact('searched_products', 'category_id', 'color_code', 'send_id'));
}


    // filter record by colored
public function rating(Request $request)
{
  $send_id = $request->cat_id;
  $searched_products = DB::table('categories')
  ->join('products', 'products.category_id', '=', 'categories.id')
  ->where('categories.slug', $request->cat_id)
  ->where('products.product_color', $request->color)
  ->get();
  return view('fyc-web.product_cat', compact('searched_products', 'send_id'));
}



public function updatePassword (Request $request ){
    $data = $request->all();
    if($data['new-password'] == $data['confirm-password']){
        $user = Auth::user()->email;
        User::where('email',$user)->update(['password'=> Hash::make($data['new-password'])]);
        return redirect()->back()->with('message','Password Updated Successfully');
        
    }
    else{
        return redirect()->back()->with('message','Password did not match');

    }
}

public function updateProfile (Request $request ){
    $data = $request->all();
    $user = Auth::user();

    if($data['add_id']>0){
        $address=Address::find($data['add_id']);

        $user->email = $data['email'];
        $user->name = $data['name'];
        $user->update();

        $address->address = $data['address'];
        $address->state = $data['state'];
        $address->city = $data['city'];
        $address->pin_code = $data['pin_code'];
        $address->phone = $data['phone'];

        $address->update();
    }
    else{
        $address = new Address;
        $user->email = $data['email'];
        $user->name = $data['name'];
        $user->update();

        $address->address = $data['address'];
        $address->user_id = $user->id;
        $address->state = $data['state'];
        $address->city = $data['city'];
        $address->pin_code = $data['pin_code'];
        $address->phone = $data['phone'];

        $address->save();
    }


    return redirect()->back()->with('message','Profile  Updated Successfully');

}

public function checkEmail(Request $request){
    $data = $request->all();
    $success = 0;

    $check = User::where('email',$data['email'])->first();

    if(!empty($check)){
        $success = '1';
    }
    else{
        $success = '0';
    }

    echo $success;
}


}
