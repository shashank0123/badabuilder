<?php

namespace App\Http\Controllers;

use App\Models\Help;
use App\Models\Reply;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class HelpSupportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $allOrders = Order::where('user_id', Auth::user()->id)->get();
      return view('fyc-web.help-support', compact('allOrders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $validator = Validator::make($request->toArray(),[
        'subject' => 'required',
        'message_content' => 'required'
      ]);

      if ((count($validator->messages()) >0) || ($validator->fails()) ) {
          return redirect()->back()->withErrors($validator->errors())->withInput($request->toArray);
      }
      $user = Auth::user();
      $subject = $request->subject;
      if ($request->subject == 'other_subject') {
        $validator = Validator::make($request->toArray(),[
          'other_subject' => 'required',
        ]);

        if ((count($validator->messages()) >0) || ($validator->fails()) ) {
            return redirect()->back()->withErrors($validator->errors())->withInput($request->toArray);
        }

          $subject = $request->other_subject;
      }

      // create unique Token
      $token = md5(rand(1, 10) . microtime());
      $document = null;
      if ($request->file('document')) {
             // Get image file
             $image = $request->file('document');
             $document =$this->imageUpload($image);
         }

      $helpSupport = Help::create([
        'user_id' => $user->id,
        'order_id' => $request->order_id,
        'token' => $token,
        'name' => $user->name,
        'email' => $user->email,
        'document' => $document,
        'subject' => $subject,
        'message' => $request->message_content,
        'status' => 'active'
      ]);

      $reply = Reply::create(['token' => $token, 'message' => $request->message_content, 'reply_from' => 'user']);

      return redirect('reply?token='.$token)->with('success', 'Sucessfully update the data')->with('token' , $token);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HelpSupport  $helpSupport
     * @return \Illuminate\Http\Response
     */
    public function show(HelpSupport $helpSupport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HelpSupport  $helpSupport
     * @return \Illuminate\Http\Response
     */
    public function edit(HelpSupport $helpSupport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HelpSupport  $helpSupport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HelpSupport $helpSupport)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HelpSupport  $helpSupport
     * @return \Illuminate\Http\Response
     */
    public function destroy(HelpSupport $helpSupport)
    {
        //
    }
    /**
     * save image
     *
     * @param image file
     * @return uploaded image name
     */
     public function imageUpload($image)
     {
       // Make a image name based on user name and current timestamp
       $name = time();
       // Define folder path
       $folder = '/images/';
       // Make a file path where image will be stored [ folder path + file name + file extension]
       $filePath = $name. '.' . $image->getClientOriginalExtension();
       // Upload image
       $file = $image->storeAs($folder, $name.'.'.$image->getClientOriginalExtension(), 'public');
       // $this->uploadOne($image, $folder, 'public', $name);
       // Set user profile image path in database to filePath
       return $filePath;
     }
}
