<?php
 namespace App\Http\Controllers;
 use Illuminate\Http\Request;
 use Validator,Redirect,Response,File;
 use Socialite;
 use App\User;
 use Log;
 class SocialController extends Controller
 {
 public function redirect($provider = 'facebook')
 {
     return Socialite::driver($provider)->redirect();
 }
 public function callback($provider = 'facebook')
 {
   $getInfo = Socialite::driver($provider)->user();
   $user = $this->createUser($getInfo, $provider);
   auth()->login($user);
   return redirect()->to('/');
 }
 function createUser($getInfo, $provider){
 $user = User::where('provider_id', $getInfo->id)->first();
 Log::info($getInfo);
 if (!$user) {
       // check for email exist
                $userExist = User::where('email', $user->email)->first();
                if($userExist){
                    User::find($userExist->id)->update(['provider' => 'google',
                                                        'provider_id' => $user->id]);
                    return $userExist;
                } else{
                    $user = User::create([
         'name'     => $getInfo->name,
         'email'    => $getInfo->email,
         'provider' => $provider,
         'provider_id' => $getInfo->id,
         'access_type' => 'customer',
         'status' => 'active',
     ]);


   return $user;
                }

   }
 }
 }
