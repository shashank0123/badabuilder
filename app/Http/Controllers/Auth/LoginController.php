<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Log;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
      // This section is the only change
      $user = User::where('email', $request->input('email'))->first();
      Log::info($user);
      if ($user) {
          if ($user->status == 'active') {
              Auth::login($user, $request->has('remember'));
               $user=Auth::user();
               $val = session();
              return redirect()->intended($this->redirectTo);
          } else {
              return redirect('login') // Change this to redirect elsewhere
                  ->withInput($request->only('email', 'remember'))
                  ->withErrors([
                      'active' => 'You must be active to login.'
                  ]);
          }
      }

      return redirect('login')
          ->withInput($request->only('email', 'remember'))
          ->withErrors([
              'email' => 'You must be active to login.',
          ]);
  }
}
