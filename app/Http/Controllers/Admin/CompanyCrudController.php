<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CompanyRequest as StoreRequest;
use App\Http\Requests\CompanyRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class CompanyCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CompanyCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Company');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/company-info');
        $this->crud->setEntityNameStrings('company', 'companies');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        $this->crud->addFields([
            ['name' => 'address',  'label' => "Company Address",'type' => 'ckeditor' ],
            ['name' => 'email', 'label' => "Email",'type' => 'text'],
            ['name' => 'contact_no',  'label' => "Company Contact No.",'type' => 'text' ],
            ['name' => 'support_email','label' => "Support Email",'type' => 'text'],
            ['name' => 'sort_discription','label' => "Sort Discription",'type' => 'ckeditor'],
            ['name' => 'about_us','label' => "About Us",'type' => 'ckeditor'],
            ['name' => 'privacy_policy','label' => "Privacy Policy",'type' => 'ckeditor'],
            ['name' => 'return_policy','label' => "Return Policy",'type' => 'ckeditor'],

        ]);
        // add asterisk for fields that are required in CompanyRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->setColumns(['name', 'email', 'support_email', 'contact_no', 'address', 'sort_discription', 'about_us', 'privacy_policy', 'privacy_policy']);

        // $this->crud->addColumns(['address', 'email', 'contact_no', 'support_email']);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
