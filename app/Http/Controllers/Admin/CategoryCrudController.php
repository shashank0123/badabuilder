<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CategoryRequest as StoreRequest;
use App\Http\Requests\CategoryRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
// use App\Models\Category;

/**
 * Class CategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CategoryCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Category');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/category');
        $this->crud->setEntityNameStrings('category', 'categories');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();
        $categories = array('0'=>'None');
        $categorieslist = \App\Models\Category::get();
        foreach ($categorieslist as $key => $value) {
             $categories[$value->id] = $value->category_name;
        }

        $this->crud->addFields([
            ['name' => 'category_name', 'label' => "Category Name", 'type' => 'text', 'allows_null' => false ],
            ['name' => 'category_description', 'label' => "Category Description", 'type' => 'text', 'allows_null' => false ],
            ['name' => 'category_img',    'label' => 'Category Image ','type' => 'upload','upload' => true,'disk' => 'uploads' ],
            // ['name' => 'category_type', 'label' => "Category Type", 'type' => 'select_from_array', 'options' => ['category' => 'Category', 'sub_category' => 'Sub Category']],
            ['name' => 'category_id','label' => "Parent Category", 'type' => 'select_from_array','options' => $categories,'allows_null' => false ],

            ['name' => 'featured','label' => "Featured",'type' => 'select_from_array','options' => ['yes' => 'Yes', 'no' => 'No'],'allows_null' => false ],

            ['name' => 'slug', 'label' => "Slug", 'type' => 'text', 'allows_null' => false ],

            ['name' => 'status', 'label' => "Status", 'type' => 'select_from_array', 'options' => ['Active' => 'Active', 'Deactive' => 'Deactive'], 'allows_null' => false ]
        ]);

        // add asterisk for fields that are required in CategoryRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        // Custom fields
        $this->crud->setColumns([
            'category_name',
             'slug',
             ['name' => 'category_id','label' => "Parent Category",'type' => 'text'],
            
             ['name' => 'category_img','label' => "Image",'type' => 'image','height' => '50px',
                 'width' => '50px'],
             'status'
         ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
