<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ProductRequest as StoreRequest;
use App\Http\Requests\ProductRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class ProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ProductCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Product');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/product');
        $this->crud->setEntityNameStrings('product', 'products');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();
        $categories = array('0'=>'None');
        $categorieslist = \App\Models\Category::get();
        foreach ($categorieslist as $key => $value) {
            $categories[$value->id] = $value->category_name;
        }

$brands = array('0'=>'None');
        $brandslist = \App\Models\Brand::get();
        foreach ($brandslist as $key => $value) {
            $brands[$value->id] = $value->brand_name;
        }


        $this->crud->addFields([
            ['name' => 'name',  'label' => "Product Name",'type' => 'text' ],
            ['name' => 'category_id',   'label' => "Category",'type' => 'select_from_array','options' => $categories ,'allows_null' => true],
            ['name' => 'image1',    'label' => 'Image1 ','type' => 'upload','upload' => true,'disk' => 'uploads' ],
            ['name' => 'image2','label' => 'Image 2 ','type' => 'upload','upload' => true,'disk' => 'uploads'  ],
            ['name' => 'image3','label' => 'Image 3','type' => 'upload','upload' => true,'disk' => 'uploads'  ],
            ['name' => 'slug', 'label' => "Slug", 'type' => 'text'],
            ['name' => 'mrp','label' => "MRP",'type' => 'number' ],
            ['name' => 'sell_price','label' => "Selling Price",'type' => 'number' ],
            ['name' => 'product_brand',   'label' => "Product Brand",'type' => 'select_from_array','options' => $brands ,'allows_null' => true],
            ['name' => 'product_weight','label' => "Product Weight",'type' => 'text'],
            // ['name' => 'product_size','label' => "Product Size",'type' => 'checkbox'  ],
            ['name' => 'product_color','label' => "Product Color",'type' => 'text'],
            ['name' => 'short_descriptions','label' => "Short Description",'type' => 'text'],
            ['name' => 'long_descriptions','label' => "Long Description",'type' => 'text'],
            ['name' => 'trending','label' => "Featured",'type' => 'select_from_array','options' => ['yes' => 'Yes', 'no' => 'No'],'allows_null' => true ],
            ['name' => 'availability','label' => "Availability",'type' => 'select_from_array','options' => ['yes' => 'In Stock', 'no' => 'Out of Stock'],'allows_null' => true  ],
            ['name' => 'page_title','label' => "Page Title",'type' => 'text' ],
            ['name' => 'page_description','label' => "Page Description",'type' => 'text'],
            ['name' => 'page_keywords','label' => "Page Keywords",'type' => 'text'],
            ['name' => 'status', 'label' => "Status", 'type' => 'select_from_array', 'options' => ['Active' => 'Active', 'Deactive' => 'Deactive']],
            ['name' => 'min_quantity','label' => "Minimum Quantity",'type' => 'number'],


        ]);

        // add asterisk for fields that are required in ProductRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        // add export button
        $this->crud->enableExportButtons();


        $this->crud->setColumns([
            'name',
            ['name' => 'category_id','label' => "Category",'type' => 'text'],
            'mrp',
            'sell_price',
            'image1',
            'availability',
            'status'
        ]);
        
        $this->crud->addColumns([
            [
               'name' => 'image1', // The db column name
               'label' => "Image 1", // Table column heading
               'type' => 'image',
                // 'prefix' => 'folder/subfolder/',
                // optional width/height if 25px is not ok with you
                'height' => '50px',
                 'width' => '50px',
            ]
        ]);
    }

    // public function index()
    // {
    //     $this->crud->hasAccessOrFail('list');

    //     $this->data['crud'] = $this->crud;
    //     $this->data['title'] = ucfirst(($this->course ? $this->course->title . ' --> ' : '') . $this->crud->entity_name_plural);

    //     // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
    //     return view($this->crud->getListView(), $this->data);
    // }





public function store(StoreRequest $request)
{
        // your additional operations before save here
    $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
    return $redirect_location;
}

public function update(UpdateRequest $request)
{
        // your additional operations before save here
    $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
    return $redirect_location;
}
}
