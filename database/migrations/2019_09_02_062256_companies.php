<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Companies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('companies', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('name')->nullable();
          $table->string('email')->nullable();
          $table->string('address')->nullable();
          $table->string('contact_no')->nullable();
          $table->string('support_email')->nullable();
          $table->string('sort_discription')->nullable();
          $table->string('about_us')->nullable();
          $table->string('privacy_policy')->nullable();
          $table->string('return_policy')->nullable();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
