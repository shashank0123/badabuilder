<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','FrontController@index');
Route::get('/products/{slug}','FrontController@getProducts');
Route::get('/cart', 'FrontController@getCart');
Route::get('/searchedProducts', 'FrontController@searchedProducts');
Route::post('/add-to-cart','AjaxController@addToCart');
Route::post('/productFilter/filterProducts','AjaxController@filterProducts');
Route::post('/add-to-wishlist','AjaxController@addToWishlist');
Route::get('/wishlist', 'FrontController@getWishlist');
Route::get('/emptyWishlist', 'FrontController@emptyWishlist');
Route::get('/emptyCart', 'FrontController@emptyCart');
Route::get('/product-detail/{slug}', 'FrontController@getProductDetails');
Route::post('/give-product-rating', 'SiteController@giveProductRating')->name('give-product-rating');
Route::post('/newsletter', 'SiteController@createNewsletter')->name('newsletter');
Route::get('/checkout', 'SiteController@orderProceed');


Route::get('/offers', 'FrontController@getOffers');
Route::post('checkEmail', 'AjaxController@checkEmail');
// Route::get('/{slug}','FrontController@getProducts');



Route::get('/home', 'FrontController@getHome');
Route::get('/about', 'FrontController@getAbout');
Route::get('/sell-on-homeglare', 'FrontController@getSeller');
Route::post('/offer-detail', 'FrontController@getOfferDetail');

Route::get('/opinion/{type}', 'SiteController@getOpinion');
Route::get('/design', 'SiteController@getDesign');
Route::get('/cbt', 'SiteController@getCBT');
Route::get('/construction', 'SiteController@getConstruction');

Route::get('/contact', 'FrontController@getContact');
Route::post('/contact',  'SiteController@contactUs');
Route::get('/faq', 'FrontController@getFaq');
Route::get('/account/{id}', 'FrontController@getAccount');
// Route::get('/product/{slug}', 'FrontController@getProducts');
// Route::get('/register', 'FrontController@getRegister');

Route::post('pay', 'PayController@pay');
Route::get('payment-success', 'PayController@success');

Route::get('/auth/redirect/facebook', 'SocialController@redirect');
Route::get('/callback/facebook', 'SocialController@callback');

Route::get('/redirect/google', 'SocialAuthGoogleController@redirect');
Route::get('/callback/google', 'SocialAuthGoogleController@callback');

Route::post('/delete-wishlist-item','AjaxController@deleteWishlistItem');
Route::post('/cart/delete-product/{id}','AjaxController@deleteSessionData');
Route::post('/update-cart','AjaxController@updateCart');

Auth::routes();

// Route::get('/home', 'FrontController@getHome');
// Route::get('/', 'FrontController@getHome');
Route::post('/update-password', 'AjaxController@updatePassword');
Route::post('/update-profile', 'AjaxController@updateProfile');

Route::post('/order-product', 'SiteController@orderProduct')->name('order-product');
Route::get('/order-booked', function()
{
  return view('fyc-web.ordersuccess');
});

// Route::get('/about-us','SiteController@getAboutUs');
// Route::get('/contact','SiteController@getContactUs');
// Route::post('/contact-us', 'SiteController@contactUs')->name('contact-us');
Route::get('/faq','SiteController@getFaq')->name('faq');
Route::get('/terms_n_conditions','SiteController@getTermsandCondition');
Route::get('/privacy_policy','SiteController@getPrivacyPolicy');
Route::get('/return-policy','SiteController@getReturnPolicy');
Route::get('/my-account/{id}','SiteController@getMyAccount');
Route::get('/ordered-item-details/{order_id}', 'SiteController@orderedItemDetails')->name('ordered-item-details');
Route::get('/ordered-item-track/{order_id}', 'SiteController@orderedItemTrack')->name('ordered-item-track');
Route::resource('reply', 'ReplyController');

Route::get('/show-minicart','AjaxController@showMiniCart');
Route::get('/search-product/productsCat','AjaxController@showProductCat');
Route::get('/search-product/sort', 'AjaxController@sorting');
Route::get('/search-product/price-filter', 'AjaxController@price');
Route::get('/search-product/colored', 'AjaxController@colored');
Route::get('/search-product/rating', 'AjaxController@rating');
Route::resource('help-support', 'HelpSupportController');

Route::get('/get-product_details-data/{productId}', 'AjaxController@getProductDetailsData');
Route::post('/global-search-product', 'SiteController@globalSearchProduct')->name('global-search-product');

// snapSocialShare
Route::get('/shareLinkOnSocialSite/{siteKey}/{slug_url}/{product_id}', 'SiteController@shareLinkOnSocialSite');
//fb snap Social Share callback
Route::get('SharedLink/callback/{siteKey}', 'SiteController@SharedLinkCallback');
