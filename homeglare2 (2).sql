-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 30, 2019 at 10:08 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `homeglare2`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
CREATE TABLE IF NOT EXISTS `addresses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pin_code` bigint(20) DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ship_to_different_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_pin_code` bigint(20) DEFAULT NULL,
  `shipping_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `user_id`, `address`, `country`, `city`, `state`, `pin_code`, `phone`, `created_at`, `updated_at`, `ship_to_different_address`, `shipping_address`, `shipping_country`, `shipping_city`, `shipping_state`, `shipping_pin_code`, `shipping_phone`) VALUES
(1, 2, 'hello', NULL, 'world', 'delhi', 110000, 9078563413, '2019-09-05 06:09:18', '2019-10-28 15:05:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 5, 'RZ-10 billu building material', NULL, 'uttam nagar', 'delhi', 110059, 9895529567, '2019-09-06 15:10:15', '2019-09-06 17:22:31', NULL, 'dh', NULL, 'gjh', 'gj', 565656, '8989898989'),
(3, 12, 'RZ-10 billu building material', NULL, NULL, NULL, NULL, 9895529567, '2019-09-06 17:32:51', '2019-09-06 17:32:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 13, 'Q-79 Manas Kunj Road Uttam Nagar East', NULL, 'Delhi', 'Delhi', 110059, 6396787929, '2019-09-09 15:37:42', '2019-09-09 15:39:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 14, 'A-1/63\r\nBIRLA FARM', NULL, NULL, NULL, NULL, 8595243306, '2019-09-09 17:39:47', '2019-09-09 17:39:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 15, 'No: 2/2, 1st main, vinayaka nagar, near vinayaka temple  nagasandra', NULL, NULL, NULL, NULL, 9611192954, '2019-09-24 18:40:10', '2019-09-24 18:40:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 16, 'dfghjkl;\'lkjh', NULL, NULL, NULL, NULL, 5585541848, '2019-09-27 14:20:05', '2019-09-27 14:20:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 17, 'fdkslj', NULL, NULL, NULL, NULL, 5585541848, '2019-10-07 16:26:00', '2019-10-07 16:26:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 19, 'hbfeibv ijhfi', NULL, 'delhi', 'delhi', 110006, 99999966, '2019-10-15 22:42:54', '2019-10-15 22:49:15', NULL, 'hhtri ji f i fij fid', NULL, 'delhi', 'delhi', 110007, '784575579'),
(10, 20, 'c-143,, jeevan park, uttamnagar east', NULL, 'delhi', 'delhi', 110059, 9560755823, '2019-10-16 15:39:41', '2019-10-16 15:46:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 21, 'Uttam Nagar Delhi', NULL, NULL, NULL, NULL, 7060936569, '2019-10-24 02:10:50', '2019-10-24 02:10:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 22, 'Delhi', NULL, NULL, NULL, NULL, 9056560413, '2019-10-24 02:12:19', '2019-10-24 02:12:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 23, '...', NULL, NULL, NULL, NULL, 88726494781, '2019-10-24 08:45:07', '2019-10-24 08:45:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 2, 'Shalimar Bagh', 'India', 'delhi', 'new delhi', 110029, 9090909090, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 2, 'hello', NULL, 'world', 'delhi', 110000, 9078563413, '2019-10-28 15:15:19', '2019-10-28 15:16:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 24, 'New Delhi', NULL, 'delhi', 'delhi', 110099, 9056560413, '2019-11-21 03:42:24', '2019-11-29 07:50:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 24, 'Keshav Puram', NULL, 'Delhi', 'New Delhi', 110099, 9056560412, '2019-11-22 23:14:32', '2019-11-29 07:36:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `image_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyword` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Deactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `image_url`, `image_type`, `keyword`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'storage/banners/c6dfe9a5e8675524ecd627457b3d9457.png', 'slider', 'homeglare slider banner', 'slider_1', 'Active', '2019-08-29 16:42:38', '2019-11-19 04:01:54'),
(2, 'storage/banners/22f63cccddce0b34dcfa035fd5f62591.png', 'slider', 'homeglare slider banner 2', 'slider_2', 'Active', '2019-08-29 16:53:11', '2019-11-19 04:02:03'),
(3, 'storage/banners/be5f90e91893ef7afb921fcb638e40ba.png', 'slider', 'slider banner 3', 'slider_banner_3', 'Active', '2019-08-29 19:52:06', '2019-08-29 19:52:06'),
(4, 'storage/banners/2627801e63b669af06dc42d18441f1e1.png', 'portrait', 'portrait banner 1', 'portrait_banner_1', 'Active', '2019-08-29 19:53:09', '2019-08-29 19:53:09'),
(8, 'storage/banners/713b1edb1f157abee88b6078b7fc749c.jpg', 'medium', 'medium2', 'medium2', 'Active', '2019-11-19 04:09:15', '2019-11-19 04:09:15'),
(7, 'storage/banners/f8a5c880d1fc3e5d54eb780ef86e8eb9.jpg', 'medium', 'medium1', 'medium1', 'Active', '2019-11-19 04:07:43', '2019-11-19 04:07:43'),
(9, 'storage/banners/74870dd586a9038c49a33f67621edc83.jpg', 'medium', 'medium3', 'medium3', 'Active', '2019-11-19 04:09:52', '2019-11-19 04:09:52'),
(10, 'storage/banners/b32e8b004cbf1b2008015a476cc54d55.jpg', 'medium', 'medium4', 'medium4', 'Active', '2019-11-19 04:10:36', '2019-11-19 04:10:36'),
(11, 'storage/banners/15c3aa827d3ce7cba3fef94bddaff175.jpg', 'medium', 'medium5', 'medium5', 'Active', '2019-11-19 04:11:05', '2019-11-19 04:11:05');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE IF NOT EXISTS `blogs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `heading` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `blogs_slug_unique` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `brands_slug_unique` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

DROP TABLE IF EXISTS `carts`;
CREATE TABLE IF NOT EXISTS `carts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cart` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_description` text COLLATE utf8mb4_unicode_ci,
  `featured` char(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Deactive',
  `slug` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `category_description`, `featured`, `status`, `slug`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Kitchen Appliances', 'Kitchen Appliances', 'yes', 'Active', 'Kitchen_Appliances', '0', '2019-08-28 19:29:31', '2019-09-09 18:47:12'),
(15, 'Home Lighting', 'Home Lighting', 'yes', 'Active', 'Home_Lighting', '14', '2019-09-09 18:50:55', '2019-09-09 18:50:55'),
(14, 'Home', 'Home', 'yes', 'Active', 'Home_', '0', '2019-09-09 18:49:44', '2019-09-09 18:49:44'),
(8, 'Kitchen Cookware & Serveware', 'Kitchen Cookware & Serveware', 'yes', 'Active', 'Kitchen _Cookware', '1', '2019-09-09 18:15:21', '2019-09-09 18:15:21'),
(9, 'Home Decor', 'Home Decor', 'yes', 'Active', 'Home_Decor', '14', '2019-09-09 18:16:50', '2019-09-09 19:04:02'),
(10, 'Table & Dinnerware', 'Table & Dinnerware', 'yes', 'Active', 'Table_Dinnerware', '8', '2019-09-09 18:19:14', '2019-09-09 19:21:58'),
(11, 'Kitchen Tools', 'Kitchen Tools', 'yes', 'Active', 'Kitchen_Tools', '8', '2019-09-09 18:23:21', '2019-09-09 18:23:21'),
(12, 'Utility Lighting', 'Utility Lighting', 'yes', 'Active', 'Utility_Lighting', '7', '2019-09-09 18:26:41', '2019-09-09 18:26:41'),
(13, 'Decor  Lighting & Access', 'Decor  lighting & Access', 'yes', 'Active', 'Decor_lighting', '7', '2019-09-09 18:29:02', '2019-09-09 18:29:46');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `support_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_discription` longtext COLLATE utf8mb4_unicode_ci,
  `about_us` longtext COLLATE utf8mb4_unicode_ci,
  `privacy_policy` longtext COLLATE utf8mb4_unicode_ci,
  `return_policy` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `email`, `address`, `contact_no`, `support_email`, `sort_discription`, `about_us`, `privacy_policy`, `return_policy`, `created_at`, `updated_at`) VALUES
(1, 'HomeGlare', 'noreply@homeglare.com', '<p>C-191 sector -1 Bawana industrial area Delhi</p>', '8826361066,8802030809', 'support@homeglare.com', '<p>The administration we offer you is everything to us. Our expert site makes picking your apparatus as simple as could be expected under the circumstances. Putting in your request online is sheltered with our 100% verified site, so you can feel loose and certain your subtleties are completely secured</p>', '<p>Here at Homglare, we pride ourselves on being kitchen apparatus authorities; from the choice of machines, we offer to our apparatus counsel and conveyance choices. These are customized around you, our clients, so we can offer you the most ideal administration with each request you place. The products you purchase from this site will be acquired from Homeglare PVT LTD.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>All through each phase of your adventure, from choosing your new apparatus to conveying it and setting it in your kitchen, we are here next to you to guarantee you get a smooth and loosened up procedure &ndash; all things considered, for what reason should you stress over anything separated from which machine from our immense range will look fabulous in your home.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>There are four primary reasons our clients adore us;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Service</strong><strong>:</strong>The administration we offer you is everything to us. Our expert site makes picking your apparatus as simple as could be expected under the circumstances. Putting in your request online is sheltered with our 100% verified site, so you can feel loose and certain your subtleties are completely secured</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Price: </strong>We checks each value, every day to ensure we can bring our clients the most recent offers and extraordinary low costs.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Conveyance: </strong>&nbsp;We pride ourselves on our conveyance administration where our group will convey your machine into a room of your decision. We guarantee clarifies our whole conveyance process and what makes us stand apart from the group.</p>', '<p>We esteem the trust you place in us. That is the reason we demand the most elevated models for secure exchanges and client data protection. It would be ideal if you perused the accompanying articulation to find out about our data social affair and spread practices.</p>\r\n\r\n<p>Note:</p>\r\n\r\n<p>Our protection approach is liable to change whenever without notice. To ensure you know about any changes, it would be ideal if you survey this approach occasionally.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>By visiting this Website you consent to be bound by the terms and states of this Privacy Policy. On the off chance that you don&#39;t concur kindly don&#39;t utilize or get to our Website.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>By unimportant utilization of the Website, you explicitly agree to our utilization and exposure of individual data furnished by you as per this Privacy Policy. This Privacy Policy is fused into and subject to the Terms of Use.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>1. Gathering of Personally Identifiable Information and other Information</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>When you utilize our Website, we gather and store your data which is given by you every once in a while. Our essential objective in doing as such is to give you a sheltered, productive, smooth and tweaked understanding. This enables us to give administrations and highlights that probably address your issues and to tweak our Website to make your experience more secure and simpler. All the more significantly, at the same time, we gather individual data from you that we think about essential for accomplishing this reason.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>As a rule, you can peruse the Website without disclosing to us what your identity is or uncovering any close to home data about yourself. When you give us your data, you are not mysterious to us. Where conceivable, we demonstrate which fields are required and which fields are discretionary. You generally have the alternative to not give data by deciding not to utilize a specific administration or highlight on the Website. We may naturally follow certain data about you dependent on your conduct on our Website. We utilize this data to do inward look into our clients&#39; socioeconomics, interests, and conduct to all the more likely comprehend, secure and serve our clients. This data is accumulated and investigated on a collected premise. This data may incorporate the URL that you just originated from (regardless of whether this URL is on our Website or not), which URL you next go to (whether this URL is on our Website or not), your PC program data, and your IP address.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We use information accumulation gadgets, for example, &quot;treats&quot; on specific pages of the Website to help investigate our site page stream, measure limited time adequacy, and advance trust and security. &quot;Treats&quot; are little documents set on your hard drive that help us in giving our administrations. We offer certain highlights that are just accessible using a &quot;treat&quot;. We additionally use treats to enable you to enter your secret phrase less as often as possible during a session. Treats can likewise enable us to give data that is focused on your interests. Most treats are &quot;session treats,&quot; implying that they are consequently erased from your hard drive toward the part of the bargain. You are in every case allowed to decay our treats if your program grants, even though all things considered you will most likely be unable to utilize certain highlights on the Website and you might be required to reemerge your secret word all the more often during a session.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Also, you may experience &quot;treats&quot; or other comparative gadgets on specific pages of the Website that are set by outsiders. We don&#39;t control the utilization of treats by outsiders.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>If you purchase on the Website, we gather data about your purchasing conduct, inclinations, and other such data that you give.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>If you execute with us, we gather some extra data, for example, a charging address, a credit/plastic number and a credit/platinum card lapse date as well as other installment instrument subtleties and following data from checks or cash orders.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>If you post messages on our message loads up, visit rooms or other message territories or leave input or on the off chance that you use voice directions to shop on the Website, we will gather that data you give to us. We hold this data as important to determine debates, give client support and investigate issues as allowed by law.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>If you send us individual correspondence, for example, messages or letters, or if different clients or outsiders send us correspondence about your exercises or postings on the Website, we may gather such data into a record explicit to you.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We gather by and by recognizable data (email address, name, telephone number, charge card/check card/other installment instrument subtleties, and so forth.) from you when you set up a free account with us. While you can peruse a few segments of our Website without being an enlisted part, certain exercises, (for example, putting in a request) do require enrollment. We do utilize your contact data to send you offers dependent on your past requests and your interests.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>2. Utilization of Demographic/Profile Data/Your Information</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We utilize individual data to give the administrations you demand. To the degree we utilize your data to market to you, we will give you the capacity to quit such employments. We utilize your data to help dealers in taking care of and satisfying requests, improving client experience, resolve questions; investigate issues; help advance a sheltered administration; gather cash; measure shopper enthusiasm for our items and administrations, illuminate you about on the web and disconnected offers, items, administrations, and updates; redo and upgrade your experience; recognize and ensure us against blunder, misrepresentation and other crime; uphold our terms and conditions; and as generally depicted to you at the hour of accumulation.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>With your assent, we will approach your SMS, contacts in your registry, area, and gadget data and we may demand you to give your PAN and Aadhaar subtleties to check your qualification for specific items/administrations including yet not constrained to giving credit) being offered by us, our offshoots of our loaning accomplices. We may impart this information to our subsidiaries or our loaning accomplices for indistinguishable purposes from referenced above, in any case, we won&#39;t store any Aadhaar information ourselves. On the occasion that agrees to this such utilization of information is pulled back, later on, we will stop gathering of such information however keep on putting away the information (put something aside for Aadhaar information) and use it for inward purposes to further improve our administrations.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>In our endeavors to ceaselessly improve our item and administration contributions, we gather and investigate statistic and profile information about our clients&#39; movement on our Website.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We distinguish and utilize your IP address to help determine issues to have our server, and to oversee our Website. Your IP address is likewise used to help recognize you and to assemble expansive statistic data.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We will once in a while request that you complete discretionary online overviews. These overviews may approach you for individual data, contact data, date of birth, statistic data (like postal district, age, or salary level), properties, for example, your interests, family or way of life data, your obtaining conduct or history, inclinations, and other such data that you may give.. We utilize this information to tailor your involvement with our Website, giving you content that we figure you may be keen on and to show substance as indicated by your inclinations.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Treats</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>A &quot;treat&quot; is a little snippet of data put away by a web server on an internet browser so it tends to be later perused once again from that program. Treats help empower the program to recall data explicit to a given client. We place both lasting and brief treats in your PC&#39;s hard drive. The treats don&#39;t contain any of your by and by recognizable data.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>3. Sharing of individual data</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We may impart individual data to our other corporate substances and associates. These substances and associates may market to you because of such sharing except if you expressly quit.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We may reveal individual data to outsiders. This divulgence might be required for us to give you access to our Services, for satisfaction of your requests, or for improving your experience, or to conform to our legitimate commitments, to implement our User Agreement, to encourage our showcasing and promoting exercises, or to avoid, recognize, alleviate, and explore fake or criminal operations identified with our Services. We don&#39;t unveil your data to outsiders for their showcasing and publicizing purposes without your express assent.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We may uncover individual data whenever required to do as such by law or in the great confidence conviction that such divulgence is sensibly important to react to subpoenas, court orders, or other lawful procedures. We may uncover individual data to law authorization workplaces, outsider rights proprietors, or others in the great confidence conviction that such revelation is sensibly important to uphold our Terms or Privacy Policy; react to claims that a notice, posting or other substance disregards the privileges of an outsider; or ensure the rights, property or individual security of our clients or the overall population.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We and our associates will share/sell a few or the majority of your data with another business substance should we (or our benefits) plan to converge with, or be procured by that business element, or re-association, amalgamation, rebuilding of the business. Should such an exchange happen, that different business substance (or the new joined element) will be required to pursue this security approach as for your data?</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>4. Connections to Other Sites</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Our Website connects to different sites that may gather recognizable data about you. homeglare.com isn&#39;t in charge of the protection rehearses or the substance of those connected sites.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>5. Security Precautions</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Our Website has stringent safety efforts set up to ensure the misfortune, abuse, and change of the data under our influence. At whatever point you change</p>', '<p>Return is a plan given by individual venders straightforwardly under this arrangement as far as which the choice of trade, substitution as well as the discount is offered by the separate dealers to you. All items recorded under a specific classification might not have a similar returns strategy. For all items, the arrangement on the item page will beat the general returns strategy.</p>\r\n\r\n<p><strong>General Rules for an effective Return</strong></p>\r\n\r\n<ol>\r\n	<li>&nbsp; &nbsp;In certain situations where the vendor can&#39;t process a substitution under any circumstances, a discount will be given.</li>\r\n	<li>&nbsp;During open box conveyances, while tolerating your request, on the off chance that you got an alternate or a harmed item, you will be given a discount (on the spot discounts for money down requests). When you have acknowledged an open box conveyance, no arrival solicitation will be prepared, aside from assembling abandons. In such cases, this classification of explicit substitution/return general conditions will be appropriate.</li>\r\n	<li>&nbsp; &nbsp;For items where the establishment is given by Flipkart&#39;s administration accomplices, don&#39;t open the item bundling without anyone else&#39;s input. Flipkart approved staff will help in unpacking and establishment of the item.</li>\r\n	<li>&nbsp; &nbsp;For Furniture, any item related issues will be checked by an approved administration staff (free of expense) and endeavored to be settled by supplanting the broken/faulty piece of the item. The full substitution will be given uniquely in situations where the administration faculty opines that supplanting the broken/imperfect part won&#39;t resolve the issue.</li>\r\n</ol>', '2019-09-02 01:36:36', '2019-10-24 03:03:56');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
CREATE TABLE IF NOT EXISTS `faqs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `status`, `created_at`, `updated_at`) VALUES
(1, 'can we take cash on delivery', '<p>Yes!</p>', 'active', '2019-09-02 23:41:50', '2019-09-05 13:03:32'),
(2, 'can i have multiple product at a time', '<p>Yes You can buy multiple prooduct at a time</p>', 'active', '2019-09-02 23:56:06', '2019-09-02 23:58:11');

-- --------------------------------------------------------

--
-- Table structure for table `help_supports`
--

DROP TABLE IF EXISTS `help_supports`;
CREATE TABLE IF NOT EXISTS `help_supports` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `order_id` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `help_supports`
--

INSERT INTO `help_supports` (`id`, `user_id`, `order_id`, `token`, `name`, `email`, `document`, `subject`, `message`, `status`, `created_at`, `updated_at`) VALUES
(1, 16, NULL, '907f886b4eeaf3bfc4c567a100cde24b', 'navin', 'raman5665@gmail.com', NULL, 'regarding_order', 'yte4gh6543', 'active', '2019-09-27 14:22:26', '2019-09-27 14:22:26'),
(2, 16, NULL, '5b2f2ecbfd1627a61c38ff1ea2131a5c', 'navin', 'raman5665@gmail.com', NULL, 'regarding_order', 'yte4gh6543', 'active', '2019-09-27 14:23:31', '2019-09-27 14:23:31'),
(3, 2, NULL, '1b71503b6c20c3600caf4fa31966bca6', 'Sonu', 'sonubackstage@gmail.com', NULL, 'regarding_payment', 'Testing', 'active', '2019-11-20 12:38:32', '2019-11-20 12:38:32');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

DROP TABLE IF EXISTS `newsletters`;
CREATE TABLE IF NOT EXISTS `newsletters` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`id`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'ss6969688@gmail.com', 'Active', '2019-09-09 19:30:13', '2019-09-09 19:30:13'),
(2, 'aaa@gmail.com', 'Active', '2019-09-09 19:48:03', '2019-09-09 19:48:03'),
(3, 'kerriegilmore7439@gmail.com', 'Active', '2019-10-07 22:05:11', '2019-10-07 22:05:11'),
(4, 'valerie.kucher@mail.ru', 'Active', '2019-10-09 06:45:52', '2019-10-09 06:45:52'),
(5, 'jyotikasethi3007@gmail.com', 'Active', '2019-11-20 12:50:57', '2019-11-20 12:50:57'),
(6, 'riya@gmail.com', 'Active', '2019-11-21 01:23:55', '2019-11-21 01:23:55'),
(7, 'asd@gmail.com', 'Active', '2019-11-21 01:24:32', '2019-11-21 01:24:32');

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

DROP TABLE IF EXISTS `offers`;
CREATE TABLE IF NOT EXISTS `offers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `offer_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `offer_sub_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `offer_short_description` text COLLATE utf8mb4_unicode_ci,
  `offer_long_description` text COLLATE utf8mb4_unicode_ci,
  `image_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `offer_title`, `offer_sub_title`, `offer_short_description`, `offer_long_description`, `image_url`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Offer', 'Ofeer', 'oFeer', 'Offer', 'storage/offers/30a0086b07c3cabcef28d951a11ec20d.jpg', 'Active', '2019-09-05 12:51:10', '2019-11-19 06:13:31'),
(2, 'offer2', 'Offer2', 'Offer2', 'Offer2', 'storage/offers/ae240714e32101a88e24b80e7bbe4f51.jpeg', 'Active', '2019-11-19 06:14:55', '2019-11-19 06:14:55'),
(3, 'offer3', 'Offer3', 'Offer3', 'Offer3', 'storage/offers/7faadcd0c5f8a5e67d59c9905e85981b.jpg', 'Active', '2019-11-19 06:15:50', '2019-11-19 06:15:50');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `date` date DEFAULT NULL,
  `price` int(11) NOT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `encrypted_payment_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `shipping_address` int(125) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `address`, `date`, `price`, `payment_method`, `transaction_id`, `encrypted_payment_id`, `status`, `created_at`, `updated_at`, `shipping_address`) VALUES
(15, 5, 'RZ-10 billu building material uttam nagar delhi 110059 ', NULL, 50, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-09-26 14:37:29', '2019-09-26 14:37:29', 0),
(6, 8, 'RZ-10', NULL, 1200, 'cash on delivery', NULL, NULL, 'order booked', '2019-09-05 12:19:33', '2019-09-23 12:16:33', 0),
(3, 5, 'uttam nagar delhi uttam nagar delhi 11455 ', NULL, 550, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-09-03 19:45:46', '2019-09-03 19:45:46', 0),
(4, 5, 'uttam nagar delhi uttam nagar delhi 11455 ', NULL, 200, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-09-03 19:51:32', '2019-09-03 19:51:32', 0),
(5, 5, 'uttam nagar delhi uttam nagar delhi 11455 ', NULL, 200, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-09-03 19:54:34', '2019-09-03 19:54:34', 0),
(7, 5, 'uttam nagar delhi uttam nagar delhi 11455 ', NULL, 0, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-09-05 14:20:39', '2019-09-05 14:20:39', 0),
(8, 11, 'sadasd    ', NULL, 700, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-09-05 16:09:54', '2019-09-05 16:09:54', 0),
(9, 5, 'RZ-10 uttam nagar delhi 110059 ', NULL, 325, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-09-06 15:10:15', '2019-09-06 15:10:15', 0),
(10, 5, 'RZ-10 uttam nagar delhi 110059 ', NULL, 450, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-09-06 17:13:23', '2019-09-06 17:13:23', 0),
(11, 5, 'RZ-10 uttam nagar delhi 110059 ', NULL, 800, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-09-06 17:22:31', '2019-09-06 17:22:31', 0),
(12, 13, 'Q-79 Manas Kunj Road Uttam Nagar East    ', NULL, 599, 'pay_by_card', NULL, NULL, 'order_booked', '2019-09-09 15:39:31', '2019-09-09 15:39:31', 0),
(13, 2, 'uttam nagar delhi uttam nagar delhi 11455 ', NULL, 2000, 'pay_by_card', NULL, NULL, 'order_booked', '2019-09-09 19:29:02', '2019-09-09 19:29:02', 0),
(14, 2, 'uttam nagar delhi uttam nagar delhi 11455 ', NULL, 2450, 'pay_by_card', NULL, NULL, 'order_booked', '2019-09-09 19:51:05', '2019-09-09 19:51:05', 0),
(16, 19, 'hbfeibv ijhfi    ', NULL, 200, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-10-15 22:49:15', '2019-10-15 22:49:15', 0),
(17, 19, 'hbfeibv ijhfi delhi delhi 110006 ', NULL, 1100, 'pay_by_card', NULL, NULL, 'order_booked', '2019-10-15 22:58:19', '2019-10-15 22:58:19', 0),
(18, 20, 'c-142,, jeevan park, uttamnagar east    ', NULL, 900, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-10-16 15:46:48', '2019-10-16 15:46:48', 0),
(19, 2, 'uttam nagar delhi uttam nagar delhi 11455 ', NULL, 200, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-10-28 14:26:56', '2019-10-28 14:26:56', 0),
(20, 2, 'uttam nagar delhi uttam nagar delhi 11455 ', NULL, 200, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-10-28 14:30:24', '2019-10-28 14:30:24', 0),
(21, 2, 'uttam nagar delhi uttam nagar delhi 11455 ', NULL, 200, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-10-28 14:32:04', '2019-10-28 14:32:04', 0),
(22, 2, '1', '2019-10-28', 200, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-10-28 15:00:24', '2019-10-28 15:00:24', 14),
(23, 2, '1', '2019-10-28', 200, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-10-28 15:01:17', '2019-10-28 15:01:17', 14),
(24, 2, '1', '2019-10-28', 200, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-10-28 15:05:48', '2019-10-28 15:05:48', 14),
(25, 2, '1', '2019-10-28', 400, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-10-28 15:12:54', '2019-10-28 15:12:54', 14),
(26, 2, '1', '2019-10-28', 400, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-10-28 15:13:28', '2019-10-28 15:13:28', 14),
(27, 2, '1', '2019-10-28', 400, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-10-28 15:13:49', '2019-10-28 15:13:49', 14),
(28, 2, '1', '2019-10-28', 100, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-10-28 15:15:19', '2019-10-28 15:15:19', 15),
(29, 2, '15', '2019-10-28', 350, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-10-28 15:16:48', '2019-10-28 15:16:48', 15),
(30, 2, '1', '2019-10-29', 50, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-10-29 00:46:36', '2019-10-29 00:46:36', 15),
(31, 2, '1', '2019-11-21', 550, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-11-21 00:32:12', '2019-11-21 00:32:12', 0),
(32, 24, '16', '2019-11-22', 1000, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 04:44:29', '2019-11-22 04:44:29', 0),
(33, 24, '16', '2019-11-22', 350, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-11-22 04:47:10', '2019-11-22 04:47:10', 0),
(34, 24, '16', '2019-11-22', 350, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-11-22 04:54:04', '2019-11-22 04:54:04', 0),
(35, 24, '16', '2019-11-22', 350, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-11-22 04:54:53', '2019-11-22 04:54:53', 0),
(36, 24, '16', '2019-11-22', 350, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-11-22 04:56:41', '2019-11-22 04:56:41', 0),
(37, 24, '16', '2019-11-22', 350, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-11-22 04:57:38', '2019-11-22 04:57:38', 0),
(38, 24, '16', '2019-11-22', 350, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-11-22 04:58:10', '2019-11-22 04:58:10', 0),
(39, 24, '16', '2019-11-22', 350, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-11-22 05:06:17', '2019-11-22 05:06:17', 0),
(40, 24, '16', '2019-11-22', 350, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-11-22 05:07:28', '2019-11-22 05:07:28', 0),
(41, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:08:35', '2019-11-22 05:08:35', 0),
(42, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:09:33', '2019-11-22 05:09:33', 0),
(43, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:09:52', '2019-11-22 05:09:52', 0),
(44, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:09:53', '2019-11-22 05:09:53', 0),
(45, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:11:39', '2019-11-22 05:11:39', 0),
(46, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:12:25', '2019-11-22 05:12:25', 0),
(47, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:13:49', '2019-11-22 05:13:49', 0),
(48, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:15:15', '2019-11-22 05:15:15', 0),
(49, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:22:25', '2019-11-22 05:22:25', 0),
(50, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:24:09', '2019-11-22 05:24:09', 0),
(51, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:25:59', '2019-11-22 05:25:59', 0),
(52, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:27:07', '2019-11-22 05:27:07', 0),
(53, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:28:08', '2019-11-22 05:28:08', 0),
(54, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:31:22', '2019-11-22 05:31:22', 0),
(55, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:32:07', '2019-11-22 05:32:07', 0),
(56, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:35:27', '2019-11-22 05:35:27', 0),
(57, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:36:16', '2019-11-22 05:36:16', 0),
(58, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:36:56', '2019-11-22 05:36:56', 0),
(59, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:38:20', '2019-11-22 05:38:20', 0),
(60, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:38:46', '2019-11-22 05:38:46', 0),
(61, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:40:37', '2019-11-22 05:40:37', 0),
(62, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:41:19', '2019-11-22 05:41:19', 0),
(63, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:43:19', '2019-11-22 05:43:19', 0),
(64, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:46:07', '2019-11-22 05:46:07', 0),
(65, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:47:22', '2019-11-22 05:47:22', 0),
(66, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:56:45', '2019-11-22 05:56:45', 0),
(67, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:57:01', '2019-11-22 05:57:01', 0),
(68, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 05:57:38', '2019-11-22 05:57:38', 0),
(69, 24, '16', '2019-11-22', 350, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-11-22 06:04:53', '2019-11-22 06:04:53', 0),
(70, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 06:05:07', '2019-11-22 06:05:07', 0),
(71, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 06:05:58', '2019-11-22 06:05:58', 0),
(72, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 06:07:21', '2019-11-22 06:07:21', 0),
(73, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 06:08:45', '2019-11-22 06:08:45', 0),
(74, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 06:08:58', '2019-11-22 06:08:58', 0),
(75, 24, '16', '2019-11-22', 350, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-11-22 06:09:13', '2019-11-22 06:09:13', 0),
(76, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 06:09:25', '2019-11-22 06:09:25', 0),
(77, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 06:09:47', '2019-11-22 06:09:47', 0),
(78, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 06:10:28', '2019-11-22 06:10:28', 0),
(79, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 06:11:01', '2019-11-22 06:11:01', 0),
(80, 24, '16', '2019-11-22', 350, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 06:11:13', '2019-11-22 06:11:13', 0),
(81, 24, '16', '2019-11-23', 1149, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 22:53:16', '2019-11-22 22:53:16', 0),
(82, 24, '16', '2019-11-23', 1149, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-22 23:14:32', '2019-11-22 23:14:32', 17),
(83, 24, '16', '2019-11-28', 200, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-28 07:07:56', '2019-11-28 07:07:56', 0),
(84, 24, '16', '2019-11-28', 200, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-28 07:15:02', '2019-11-28 07:15:02', 0),
(85, 24, '16', '2019-11-28', 200, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-28 07:42:42', '2019-11-28 07:42:42', 0),
(86, 24, '16', '2019-11-29', 200, 'pay_by_card', NULL, NULL, 'order_booked', '2019-11-29 07:52:58', '2019-11-29 07:52:58', 0),
(87, 24, '16', '2019-11-29', 200, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-11-29 07:53:10', '2019-11-29 07:53:10', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `price` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `item_id`, `quantity`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 1, 400, '2019-08-29 19:47:10', '2019-08-29 19:47:10'),
(2, 2, 3, 2, 450, '2019-08-29 20:37:41', '2019-08-29 20:37:41'),
(3, 3, 10, 1, 200, '2019-09-03 19:45:46', '2019-09-03 19:45:46'),
(4, 3, 11, 1, 350, '2019-09-03 19:45:46', '2019-09-03 19:45:46'),
(5, 4, 8, 1, 200, '2019-09-03 19:51:32', '2019-09-03 19:51:32'),
(6, 5, 8, 1, 200, '2019-09-03 19:54:34', '2019-09-03 19:54:34'),
(7, 6, 7, 4, 1200, '2019-09-05 12:19:33', '2019-09-05 12:19:33'),
(8, 8, 11, 2, 700, '2019-09-05 16:09:54', '2019-09-05 16:09:54'),
(9, 9, 1, 1, 100, '2019-09-06 15:10:15', '2019-09-06 15:10:15'),
(10, 9, 3, 1, 225, '2019-09-06 15:10:15', '2019-09-06 15:10:15'),
(11, 10, 11, 1, 350, '2019-09-06 17:13:23', '2019-09-06 17:13:23'),
(12, 10, 9, 2, 100, '2019-09-06 17:13:23', '2019-09-06 17:13:23'),
(13, 11, 12, 4, 800, '2019-09-06 17:22:31', '2019-09-06 17:22:31'),
(14, 12, 8, 1, 200, '2019-09-09 15:39:31', '2019-09-09 15:39:31'),
(15, 12, 13, 1, 399, '2019-09-09 15:39:31', '2019-09-09 15:39:31'),
(16, 13, 2, 20, 2000, '2019-09-09 19:29:02', '2019-09-09 19:29:02'),
(17, 14, 3, 6, 1350, '2019-09-09 19:51:05', '2019-09-09 19:51:05'),
(18, 14, 2, 11, 1100, '2019-09-09 19:51:05', '2019-09-09 19:51:05'),
(19, 15, 9, 1, 50, '2019-09-26 14:37:29', '2019-09-26 14:37:29'),
(20, 16, 10, 1, 200, '2019-10-15 22:49:15', '2019-10-15 22:49:15'),
(21, 17, 9, 1, 50, '2019-10-15 22:58:19', '2019-10-15 22:58:19'),
(22, 17, 11, 3, 1050, '2019-10-15 22:58:19', '2019-10-15 22:58:19'),
(23, 18, 4, 2, 500, '2019-10-16 15:46:49', '2019-10-16 15:46:49'),
(24, 18, 5, 1, 400, '2019-10-16 15:46:49', '2019-10-16 15:46:49'),
(25, 19, 10, 1, 200, '2019-10-28 14:26:56', '2019-10-28 14:26:56'),
(26, 20, 12, 1, 200, '2019-10-28 14:30:24', '2019-10-28 14:30:24'),
(27, 21, 12, 1, 200, '2019-10-28 14:32:04', '2019-10-28 14:32:04'),
(28, 22, 12, 1, 200, '2019-10-28 15:00:24', '2019-10-28 15:00:24'),
(29, 23, 10, 1, 200, '2019-10-28 15:01:17', '2019-10-28 15:01:17'),
(30, 24, 12, 1, 200, '2019-10-28 15:05:48', '2019-10-28 15:05:48'),
(31, 26, 5, 1, 400, '2019-10-28 15:13:28', '2019-10-28 15:13:28'),
(32, 28, 2, 1, 100, '2019-10-28 15:15:19', '2019-10-28 15:15:19'),
(33, 29, 11, 1, 350, '2019-10-28 15:16:48', '2019-10-28 15:16:48'),
(34, 30, 9, 1, 50, '2019-10-29 00:46:36', '2019-10-29 00:46:36'),
(35, 31, 11, 1, 350, '2019-11-21 00:32:13', '2019-11-21 00:32:13'),
(36, 31, 12, 1, 200, '2019-11-21 00:32:13', '2019-11-21 00:32:13'),
(37, 32, 12, 5, 1000, '2019-11-22 04:44:29', '2019-11-22 04:44:29'),
(38, 33, 11, 1, 350, '2019-11-22 04:47:10', '2019-11-22 04:47:10'),
(39, 34, 11, 1, 350, '2019-11-22 04:54:04', '2019-11-22 04:54:04'),
(40, 35, 11, 1, 350, '2019-11-22 04:54:53', '2019-11-22 04:54:53'),
(41, 36, 11, 1, 350, '2019-11-22 04:56:41', '2019-11-22 04:56:41'),
(42, 37, 11, 1, 350, '2019-11-22 04:57:38', '2019-11-22 04:57:38'),
(43, 38, 11, 1, 350, '2019-11-22 04:58:10', '2019-11-22 04:58:10'),
(44, 39, 11, 1, 350, '2019-11-22 05:06:17', '2019-11-22 05:06:17'),
(45, 40, 11, 1, 350, '2019-11-22 05:07:28', '2019-11-22 05:07:28'),
(46, 41, 11, 1, 350, '2019-11-22 05:08:35', '2019-11-22 05:08:35'),
(47, 42, 11, 1, 350, '2019-11-22 05:09:33', '2019-11-22 05:09:33'),
(48, 43, 11, 1, 350, '2019-11-22 05:09:52', '2019-11-22 05:09:52'),
(49, 44, 11, 1, 350, '2019-11-22 05:09:53', '2019-11-22 05:09:53'),
(50, 45, 11, 1, 350, '2019-11-22 05:11:39', '2019-11-22 05:11:39'),
(51, 46, 11, 1, 350, '2019-11-22 05:12:25', '2019-11-22 05:12:25'),
(52, 47, 11, 1, 350, '2019-11-22 05:13:49', '2019-11-22 05:13:49'),
(53, 48, 11, 1, 350, '2019-11-22 05:15:15', '2019-11-22 05:15:15'),
(54, 49, 11, 1, 350, '2019-11-22 05:22:25', '2019-11-22 05:22:25'),
(55, 50, 11, 1, 350, '2019-11-22 05:24:09', '2019-11-22 05:24:09'),
(56, 51, 11, 1, 350, '2019-11-22 05:25:59', '2019-11-22 05:25:59'),
(57, 52, 11, 1, 350, '2019-11-22 05:27:07', '2019-11-22 05:27:07'),
(58, 53, 11, 1, 350, '2019-11-22 05:28:08', '2019-11-22 05:28:08'),
(59, 54, 11, 1, 350, '2019-11-22 05:31:22', '2019-11-22 05:31:22'),
(60, 55, 11, 1, 350, '2019-11-22 05:32:07', '2019-11-22 05:32:07'),
(61, 56, 11, 1, 350, '2019-11-22 05:35:27', '2019-11-22 05:35:27'),
(62, 57, 11, 1, 350, '2019-11-22 05:36:16', '2019-11-22 05:36:16'),
(63, 58, 11, 1, 350, '2019-11-22 05:36:56', '2019-11-22 05:36:56'),
(64, 59, 11, 1, 350, '2019-11-22 05:38:20', '2019-11-22 05:38:20'),
(65, 60, 11, 1, 350, '2019-11-22 05:38:46', '2019-11-22 05:38:46'),
(66, 61, 11, 1, 350, '2019-11-22 05:40:37', '2019-11-22 05:40:37'),
(67, 62, 11, 1, 350, '2019-11-22 05:41:19', '2019-11-22 05:41:19'),
(68, 63, 11, 1, 350, '2019-11-22 05:43:19', '2019-11-22 05:43:19'),
(69, 64, 11, 1, 350, '2019-11-22 05:46:07', '2019-11-22 05:46:07'),
(70, 65, 11, 1, 350, '2019-11-22 05:47:22', '2019-11-22 05:47:22'),
(71, 66, 11, 1, 350, '2019-11-22 05:56:45', '2019-11-22 05:56:45'),
(72, 67, 11, 1, 350, '2019-11-22 05:57:01', '2019-11-22 05:57:01'),
(73, 68, 11, 1, 350, '2019-11-22 05:57:38', '2019-11-22 05:57:38'),
(74, 69, 11, 1, 350, '2019-11-22 06:04:53', '2019-11-22 06:04:53'),
(75, 70, 11, 1, 350, '2019-11-22 06:05:07', '2019-11-22 06:05:07'),
(76, 71, 11, 1, 350, '2019-11-22 06:05:58', '2019-11-22 06:05:58'),
(77, 72, 11, 1, 350, '2019-11-22 06:07:21', '2019-11-22 06:07:21'),
(78, 73, 11, 1, 350, '2019-11-22 06:08:45', '2019-11-22 06:08:45'),
(79, 74, 11, 1, 350, '2019-11-22 06:08:58', '2019-11-22 06:08:58'),
(80, 75, 11, 1, 350, '2019-11-22 06:09:13', '2019-11-22 06:09:13'),
(81, 76, 11, 1, 350, '2019-11-22 06:09:25', '2019-11-22 06:09:25'),
(82, 77, 11, 1, 350, '2019-11-22 06:09:47', '2019-11-22 06:09:47'),
(83, 78, 11, 1, 350, '2019-11-22 06:10:28', '2019-11-22 06:10:28'),
(84, 79, 11, 1, 350, '2019-11-22 06:11:01', '2019-11-22 06:11:01'),
(85, 80, 11, 1, 350, '2019-11-22 06:11:13', '2019-11-22 06:11:13'),
(86, 81, 11, 1, 350, '2019-11-22 22:53:16', '2019-11-22 22:53:16'),
(87, 81, 12, 1, 200, '2019-11-22 22:53:16', '2019-11-22 22:53:16'),
(88, 81, 13, 1, 399, '2019-11-22 22:53:16', '2019-11-22 22:53:16'),
(89, 81, 10, 1, 200, '2019-11-22 22:53:16', '2019-11-22 22:53:16'),
(90, 82, 11, 1, 350, '2019-11-22 23:14:32', '2019-11-22 23:14:32'),
(91, 82, 12, 1, 200, '2019-11-22 23:14:32', '2019-11-22 23:14:32'),
(92, 82, 13, 1, 399, '2019-11-22 23:14:32', '2019-11-22 23:14:32'),
(93, 82, 10, 1, 200, '2019-11-22 23:14:32', '2019-11-22 23:14:32'),
(94, 83, 12, 1, 200, '2019-11-28 07:07:57', '2019-11-28 07:07:57'),
(95, 84, 12, 1, 200, '2019-11-28 07:15:02', '2019-11-28 07:15:02'),
(96, 85, 12, 1, 200, '2019-11-28 07:42:42', '2019-11-28 07:42:42'),
(97, 86, 12, 1, 200, '2019-11-29 07:52:58', '2019-11-29 07:52:58'),
(98, 87, 12, 1, 200, '2019-11-29 07:53:10', '2019-11-29 07:53:10');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `extras` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('raman@gmail.com', '$2y$10$VQr7erqWg4b8.9ZRKF.pnOGFk5scddaCH8p5w.waJv8xVc3/BZPta', '2019-09-06 16:51:00'),
('kerriegilmore7439@gmail.com', '$2y$10$GVTIk8WGDAY4kW5iVi/1aufZSSfAUEiG1p2BxT/E7JpX4V4Cf58/C', '2019-10-07 22:05:55');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_descriptions` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `long_descriptions` text COLLATE utf8mb4_unicode_ci,
  `mrp` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sell_price` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `availability` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  `product_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_description` text COLLATE utf8mb4_unicode_ci,
  `page_keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trending` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `category_id` bigint(20) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_material` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `products_slug_unique` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `short_descriptions`, `long_descriptions`, `mrp`, `sell_price`, `slug`, `image1`, `image2`, `image3`, `product_weight`, `availability`, `product_size`, `product_color`, `page_title`, `page_description`, `page_keywords`, `trending`, `category_id`, `status`, `created_at`, `updated_at`, `product_brand`, `product_material`, `min_quantity`) VALUES
(1, 'Cutter Green', 'Green Cutter', 'Green Cutter', '200', '100', 'Cutter_green', 'storage/products/0090a158c1fe19bf5e91ff3300da5b69.png', 'storage/products/b9c67090c25eeaad21fd86cacda0ed9b.png', 'storage/products/cc1f617935e6bbfe07e38012ac33dba7.png', NULL, 'yes', NULL, 'green', 'Green Cutter', 'Green Cutter', 'Green Cutter', 'yes', 11, 'Active', '2019-08-28 19:38:32', '2019-09-09 18:52:03', NULL, NULL, NULL),
(2, 'Container', 'container', 'Colourful Container', '150', '100', 'container_pink', 'storage/products/a37f52cc45ba47a08d5d2c48b775bbe5.png', 'storage/products/26b3e234963131fb85616b190793fad5.png', 'storage/products/965ddd873e3260efe24a6c6f769aaf4b.png', NULL, 'yes', NULL, 'pink', 'container', 'Colourful container', 'container', 'yes', 10, 'Active', '2019-08-29 13:09:52', '2019-09-09 18:53:37', NULL, NULL, NULL),
(3, 'Shaker', 'Shaker', 'My Shaker', '250', '225', 'shaker_my', 'storage/products/a137a2c67d1b593d71df2388cb9ae8ce.png', 'storage/products/834c6bc8ddfea903f64662c4b886e435.png', 'storage/products/e7a7475505df784ca8ced6af6181a66a.png', NULL, 'yes', NULL, 'blue', 'My Shaker', 'My Shaker', 'My Shaker', 'yes', 10, 'Active', '2019-08-29 13:13:47', '2019-09-09 18:59:54', NULL, NULL, NULL),
(4, 'Sprinkler Set', 'Sprinkler', 'Sprinkler Set', '400', '250', 'Sprinkler_powder', 'storage/products/6f6355ae6bfedf371caec394bfb07bdc.png', 'storage/products/33c65fc5c68ca2aa2dad3cd02591882c.png', 'storage/products/183e9163e4262a484f61aa717614ba30.png', NULL, 'yes', NULL, 'transparent', 'Sprinkler Set', 'Sprinkler Set', 'Sprinkler Set', 'yes', 10, 'Active', '2019-08-29 13:20:17', '2019-09-09 18:54:43', NULL, NULL, NULL),
(5, 'CUP PLATES', 'CUP_PLATES', 'CUP_PLATES', '450', '400', 'CUP_PLATES', 'storage/products/2791c6702cdc663bc1e8983b407434f4.png', 'storage/products/f8dd81fcda86fbd947d969812bdbde9e.png', 'storage/products/b3cf1b2f2a6f0383cab2f24c4719b1fc.png', NULL, 'yes', NULL, 'Golden', 'CUP_PLATES', 'CUP_PLATES', 'CUP_PLATES', 'yes', 10, 'Active', '2019-08-29 13:24:11', '2019-09-09 18:55:10', NULL, NULL, NULL),
(6, 'Serving Spoon', 'Serving Spoon', 'Serving Spoon', '200', '150', 'Serving_Spoon', 'storage/products/0ed436159bfc5272424159e476cad350.png', 'storage/products/a71459e2fea4cfd77357251364cb0a1f.png', 'storage/products/c6d5b324efaa961e2f52f8f8217eb4fe.png', NULL, 'yes', NULL, 'green', 'Serving Spoon', 'Serving Spoon', 'Serving Spoon', 'yes', 11, 'Active', '2019-08-29 13:30:14', '2019-09-09 18:55:43', NULL, NULL, NULL),
(7, 'Coffee Jar', 'Coffee Jar', 'Coffee Jar', '400', '300', 'Coffee_Mug', 'storage/products/e688f8cc7f66ad17ad31d63e73b8b6c1.png', 'storage/products/e688f8cc7f66ad17ad31d63e73b8b6c1.png', 'storage/products/e688f8cc7f66ad17ad31d63e73b8b6c1.png', NULL, 'yes', NULL, 'All Color', 'Coffee Jar', 'Coffee Jar', 'Coffee Jar', 'yes', 10, 'Active', '2019-08-29 13:34:35', '2019-09-09 18:56:41', NULL, NULL, NULL),
(8, 'Designer Cup', 'Designer Cup', 'Designer Cup', '250', '200', 'Designer_Cup', 'storage/products/75252211842e181828189382c00fffd4.png', 'storage/products/7fbd339298e0a95d1c1c8445023f259d.png', 'storage/products/4898f39bdc5811fb2fce2b006886600d.png', NULL, 'yes', NULL, 'Pink', 'Designer Cup', 'Designer Cup', 'Designer Cup', 'yes', 10, 'Active', '2019-08-29 13:38:21', '2019-09-09 18:57:42', NULL, NULL, NULL),
(9, 'Paper Glass', 'Paper Glass', 'Paper Glass', '100', '70', 'Paper _Glass', 'storage/products/d1e3e47cdcda797afa7bf6c8e6e1391b.png', 'storage/products/608a214d6ab6af51322cb09b7303807a.png', 'storage/products/608a214d6ab6af51322cb09b7303807a.png', NULL, 'yes', NULL, 'white', 'Paper Glass', 'Paper Glass', 'Paper Glass', 'yes', 10, 'Active', '2019-08-29 13:48:35', '2019-09-09 18:59:32', NULL, NULL, NULL),
(10, 'Fruit Cutter', 'Fruit Cutter', 'Fruit Cutter', '300', '200', 'Fruit_Cutter', 'storage/products/d5cf5418356927fb60fa4a24cb4d426e.png', 'storage/products/9d5992771ca4ec85e7f9264666d36368.png', 'storage/products/bd346e23c71f19d7fa4ee9c7b0eb2390.png', NULL, 'yes', NULL, 'red', 'Fruit Cutter', 'Fruit Cutter', 'Fruit Cutter', 'yes', 11, 'Active', '2019-08-29 13:51:10', '2019-09-09 19:00:21', NULL, NULL, NULL),
(11, 'Vegetable Cutter', 'Vegetable Cutter', 'Vegetable Cutter', '400', '350', 'Vegetable_Cutter', 'storage/products/ce02f5e04a83f96464e8210c4c163d9f.png', 'storage/products/6933d3128f47f4b10c788e65ae6139e3.png', 'storage/products/355dc359ebf58b88294ac2556198f3f7.png', NULL, 'yes', NULL, 'red', 'Vegetable Cutter', 'Vegetable Cutter', 'Vegetable Cutter', 'yes', 11, 'Active', '2019-08-29 13:58:06', '2019-09-09 19:01:13', NULL, NULL, NULL),
(12, 'Spoon Stand', 'Spoon Stand', 'Spoon Stand', '250', '200', 'Spoon_Stand', 'storage/products/c6052e3c7ea82eaaf55397ec2a4db9f1.png', 'storage/products/e89e501f58177d2815bd0b115b12d188.png', 'storage/products/1cba25399a73f390624edfb86089f047.png', NULL, 'yes', NULL, 'green', 'Spoon Stand', 'Spoon Stand', 'Spoon Stand', 'yes', 10, 'Active', '2019-08-29 14:12:42', '2019-09-09 19:01:38', NULL, NULL, NULL),
(13, 'Glass Pot', 'Glass Pot', 'Glass Pot', '400', '399', 'Glass_Pot', 'storage/products/e4aadff901af9bb442476c8266a94c9c.png', 'storage/products/e980b86df0617856eb8f5ee2cfd02129.png', 'storage/products/43ebe4d3cc68e3f9065ce4ffc4aca06c.png', NULL, 'yes', NULL, 'red', 'Glass Pot', 'Glass Pot', 'Glass Pot', 'yes', 9, 'Active', '2019-08-29 14:21:44', '2019-09-09 19:02:22', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `queries`
--

DROP TABLE IF EXISTS `queries`;
CREATE TABLE IF NOT EXISTS `queries` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message_content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `response_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `queries`
--

INSERT INTO `queries` (`id`, `name`, `email`, `subject`, `message_content`, `response_status`, `created_at`, `updated_at`) VALUES
(1, 'ravi', 'ravi@gmail.com', 'avenyee', 'hi', 'awating', '2019-09-03 20:00:24', '2019-09-03 20:00:24'),
(2, 'ravi', 'ravi@gmail.com', 'avenyee', 'hi', 'awating', '2019-09-03 20:02:50', '2019-09-03 20:02:50'),
(3, 'qwrqqkiev https://google.com', 'nedifomea1989@mail.ru', 'qwrqqkiev https://google.com', 'qwrqqkiev https://google.com', 'awating', '2019-09-08 13:32:38', '2019-09-08 13:32:38'),
(4, 'SANJEEV KUMAR', 'nik143itasharma@gmail.com', 'business proposals', 'kmedk', 'awating', '2019-09-09 19:30:51', '2019-09-09 19:30:51'),
(5, 'SANJEEV KUMAR', 'nik143itasharma@gmail.com', 'business proposals', 'hlo', 'awating', '2019-09-09 19:31:08', '2019-09-09 19:31:08'),
(6, 'fjlk222k https://google.com', 'untewhitle1978@mail.ru', 'fjlk222k https://google.com', 'fjlk222k https://google.com', 'awating', '2019-10-05 18:28:01', '2019-10-05 18:28:01'),
(7, 'fdsssk222k https://google.com', 'wildnecilitt1978@mail.ru', 'fdsssk222k https://google.com', 'fdsssk222k https://google.com', 'awating', '2019-10-21 22:39:36', '2019-10-21 22:39:36'),
(8, 'jyotika', 'jyotika@gmail.com', 'Test', 'Testing', 'awating', '2019-11-21 13:59:48', '2019-11-21 13:59:48'),
(9, 'riya', 'riya@gmail.com', 'test', 'test', 'awating', '2019-11-21 14:00:59', '2019-11-21 14:00:59'),
(10, 'riya', 'rya@gmail.com', 'Feedback Suggestion', 'Testing', 'awating', '2019-11-25 04:27:01', '2019-11-25 04:27:01');

-- --------------------------------------------------------

--
-- Table structure for table `replies`
--

DROP TABLE IF EXISTS `replies`;
CREATE TABLE IF NOT EXISTS `replies` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `reply_from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `replies`
--

INSERT INTO `replies` (`id`, `token`, `message`, `reply_from`, `created_at`, `updated_at`) VALUES
(1, '5b2f2ecbfd1627a61c38ff1ea2131a5c', 'yte4gh6543', 'user', '2019-09-27 14:23:31', '2019-09-27 14:23:31'),
(2, '5b2f2ecbfd1627a61c38ff1ea2131a5c', 'fast reply', 'user', '2019-09-27 14:23:56', '2019-09-27 14:23:56'),
(3, '5b2f2ecbfd1627a61c38ff1ea2131a5c', 'hi user how can help you', 'admin', '2019-09-27 14:24:18', '2019-09-27 14:24:18'),
(4, '5b2f2ecbfd1627a61c38ff1ea2131a5c', '10000 rupye chahiye', 'user', '2019-09-27 14:24:42', '2019-09-27 14:24:42'),
(5, '5b2f2ecbfd1627a61c38ff1ea2131a5c', 'brother i am also wating for sallary!', 'admin', '2019-09-27 14:25:17', '2019-09-27 14:25:17'),
(6, '5b2f2ecbfd1627a61c38ff1ea2131a5c', '.d,mcl;dsmvldsmvcwdjCLMQSKJNXHICWQSMCKJWWQHCJKSAMKHDXWNDHCUQWHCKASNHCYSAHCIUWYCSQJKNXYQWGXCJWQCNXGYQWGCJSANBCXTCSGCWQJDU', 'user', '2019-09-27 14:25:23', '2019-09-27 14:25:23'),
(7, '1b71503b6c20c3600caf4fa31966bca6', 'Testing', 'user', '2019-11-20 12:38:32', '2019-11-20 12:38:32'),
(8, '1b71503b6c20c3600caf4fa31966bca6', 'Testing', 'user', '2019-11-20 12:38:49', '2019-11-20 12:38:49'),
(9, '1b71503b6c20c3600caf4fa31966bca6', 'Hello', 'user', '2019-11-20 12:49:00', '2019-11-20 12:49:00');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review` text COLLATE utf8mb4_unicode_ci,
  `rating` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `product_id`, `name`, `email`, `review`, `rating`, `status`, `created_at`, `updated_at`) VALUES
(1, '2', 'mjnhjb', 'nik143itasharma@gmail.com', 'jguyjfvmh', '1', 'Active', '2019-09-09 19:26:55', '2019-09-09 19:26:55'),
(2, '3', 'SANJEEV KUMAR', 'nik143itasharma@gmail.com', 'huehue', '1', 'Active', '2019-09-09 19:49:34', '2019-09-09 19:49:34'),
(3, '8', 'NntiaCDXTwgL', 'kerriegilmore7439@gmail.com', NULL, NULL, 'Active', '2019-10-07 22:06:01', '2019-10-07 22:06:01'),
(4, '10', 'riya', 'riyasetji3007@gmail.com', 'awsm', '4', 'Active', '2019-11-06 13:17:33', '2019-11-06 13:17:33'),
(5, '12', 'jyotika', 'jyotika@gmail.com', 'Testing', '4', 'Active', '2019-11-20 00:39:04', '2019-11-20 00:39:04'),
(7, '12', 'riya', 'riya@gmail.com', 'Testing', '4', 'Active', '2019-11-20 06:18:20', '2019-11-20 06:18:20'),
(8, '12', 'zafar', 'zafar@gmail.com', 'Hello Wrld', '2', 'Active', '2019-11-20 06:18:49', '2019-11-20 06:18:49'),
(9, '4', 'bibhu', 'bibhu@gmail.com', 'Test', '4', 'Active', '2019-11-20 12:52:23', '2019-11-20 12:52:23');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
CREATE TABLE IF NOT EXISTS `states` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
CREATE TABLE IF NOT EXISTS `testimonials` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `client_name`, `rating`, `review`, `city`, `image_url`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(2, 'sonu', '5', 'this is super one', 'delhi', 'storage/testimonial/8a7675e3e6c1393390de75b23c4956bb.png', NULL, 'Active', '2019-09-03 20:04:14', '2019-09-03 20:04:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `provider`, `provider_id`, `access_type`, `status`) VALUES
(1, 'admin', 'somphpdeveloper@gmail.com', NULL, '$2y$10$mLHpfVV/5eyudckfNyprd.bKXIn1rY5EAwYymuCPpElWK5LGaaPjO', NULL, '2019-08-28 19:28:01', '2019-08-30 16:38:10', 'google', '116599215729127144581', 'admin', 'inactive'),
(2, 'Sonu', 'sonubackstage@gmail.com', NULL, '$2y$10$1.GdAr6BLmYDAg1xYZbEVO45fWx.0QInOcHgQTAfec3d0rn.55nyO', NULL, '2019-08-28 20:24:52', '2019-09-05 12:30:59', 'google', '108199582828179969920', 'admin', 'active'),
(4, 'som', 'som@backstagesupporters.com', NULL, '$2y$10$2ygTPMLs9ciHPaNrgx0V0eSBzjlzkJt4KcwpTKV8q2SRjBwi9OC2O', NULL, '2019-08-29 16:26:55', '2019-08-29 16:26:55', NULL, NULL, 'customer', 'active'),
(5, 'raman', 'raman@gmail.com', NULL, '$2y$10$uwBvNhUaqEWsV8J5VbjSgOd20tPo1YfzXJC4rTPHn4dteEoFr5xEW', NULL, '2019-08-29 19:42:25', '2019-08-29 19:42:25', NULL, NULL, 'customer', 'active'),
(7, 'jyotika setthi', 'jyotika@backstagesupporters.com', NULL, NULL, NULL, '2019-08-30 14:08:07', '2019-08-30 14:08:07', 'google', '113595589106945456232', 'customer', 'active'),
(8, 'vicky', 'v@b', NULL, '$2y$10$S.HgqX.CRe.T/l43aJyDZ.UO06irq07k5ndjeLlV6OEK/Y9ePB/Ne', NULL, '2019-09-05 12:13:01', '2019-09-05 12:13:01', NULL, NULL, 'customer', 'active'),
(9, 'ravi', 'ravi@gmail.com', NULL, '$2y$10$1.GdAr6BLmYDAg1xYZbEVO45fWx.0QInOcHgQTAfec3d0rn.55nyO', NULL, '2019-09-05 12:47:53', '2019-09-05 12:47:53', NULL, NULL, 'customer', 'active'),
(10, 'mohan', 'mohan@gmail.com', NULL, '$2y$10$/Ny7Z2S1NH.y1xj7q3seD.Lc3OrRpbDXiMR.fRZYp6b40Nn5uR3aW', NULL, '2019-09-05 15:14:57', '2019-09-05 15:14:57', NULL, NULL, 'customer', 'active'),
(11, 'radha mohan', 'rmp@gmail.com', NULL, '$2y$10$48H7j1cq8FRWWvkD0mxBVeBr9lMk7OG.o70G7jZXZ/uFVLCPzVd6K', NULL, '2019-09-05 15:15:43', '2019-09-05 15:15:43', NULL, NULL, 'customer', 'active'),
(12, 'priya', 'priya@gmail.com', NULL, '$2y$10$Iv9T0mGJEyGjuF16IkeQGOwrlM4MBMw6dMRRlTkrwt/9KlWQqrNmO', NULL, '2019-09-06 17:32:51', '2019-09-06 17:32:51', NULL, NULL, 'customer', 'active'),
(13, 'Palak Gupta', 'palakgupta19999@gmail.com', NULL, '$2y$10$R1UpTVOsEuQGzds/Im7Wv.vjtu6ZmEUViSDoCYXp9ns96MwL7G0ni', NULL, '2019-09-09 15:37:42', '2019-09-09 15:37:42', NULL, NULL, 'customer', 'active'),
(14, 'Rahul Sharma', 'ss6969688@gmail.com', NULL, '$2y$10$I3.crr5NN73yfWlKJKnwHOz.ur3lbDeZtK/.OHE9QajqfdZ3Szzxq', NULL, '2019-09-09 17:39:47', '2019-09-09 17:39:47', NULL, NULL, 'customer', 'active'),
(15, 'Priya', 'deebapriyaa@gmail.com', NULL, '$2y$10$rGe4WbJBXEn4fGy2MvFu.OH.nNdPULgXJ7JQ/0h39MEKO1cp9Rw.i', NULL, '2019-09-24 18:40:10', '2019-09-24 18:40:10', NULL, NULL, 'customer', 'active'),
(16, 'navin', 'raman5665@gmail.com', NULL, '$2y$10$nbW9/vzLpGQq5HZchthkl.6bcozg2hk07RcHoV6v9pt0P/iXdb4se', NULL, '2019-09-27 14:20:05', '2019-09-27 14:20:05', NULL, NULL, 'customer', 'active'),
(17, 'Black Colored Coffee Mug', 'raman1@gmail.com', NULL, '$2y$10$5z2RiX7TSvlW0TlUlw2D5ef7CSbqHntV5WvAR4d29WNPnJzQ0ZlhG', NULL, '2019-10-07 16:26:00', '2019-10-07 16:26:00', NULL, NULL, 'customer', 'active'),
(18, 'mEPJrHeaKdkQ', 'kerriegilmore7439@gmail.com', NULL, '$2y$10$DtyAlwdJEuq7qMAqzYIBFesoljijTosNn2APKZi8NF9lLiEQIk/xm', NULL, '2019-10-07 22:05:51', '2019-10-07 22:05:51', NULL, NULL, 'customer', 'active'),
(19, 'kuldeep', 'hdkd@gnsm.com', NULL, '$2y$10$0GXQZq/req4vFpcK9YRjbekgqQ00hq/S.k2O8zqhCSm4h1r9tJywa', NULL, '2019-10-15 22:42:54', '2019-10-15 22:42:54', NULL, NULL, 'customer', 'active'),
(20, 'Shashank Shekhar', 'shashank@backstagesupporters.com', NULL, '$2y$10$2qb5.GZ5negQ6ZDWAp.6tui50OtQ4UIN1O/Mex1jE5N508aoS6RaC', NULL, '2019-10-16 15:39:41', '2019-10-16 15:39:41', NULL, NULL, 'customer', 'active'),
(21, 'Bibhu', 'dreaminvader910@gmail.com', NULL, '$2y$10$/1Sz7nhRi.77IUKOMWrbreuz68ygc18C2fQPWsYCBRMPEbdszzjGu', NULL, '2019-10-24 02:10:50', '2019-10-24 02:10:50', NULL, NULL, 'customer', 'active'),
(22, 'jyotika', 'jyotikas@backstagesupporters.com', NULL, '$2y$10$wKmkx2NbzmscDSPAMWdVY.QOWA.UU92b7PD3So0ZJJhWx5WWsbM2a', NULL, '2019-10-24 02:12:19', '2019-10-24 02:12:19', NULL, NULL, 'customer', 'active'),
(23, 'How to get 0,986 BTC per day: https://hideuri.com/qPo69w?JaHILaPJG', 'breezegeag@gmail.com', NULL, '$2y$10$ejaQurVVNT49mgAq3Mo52.1QfL2JUVjW9NX47cKghk3KiYyAsAyn.', NULL, '2019-10-24 08:45:07', '2019-10-24 08:45:07', NULL, NULL, 'customer', 'active'),
(24, 'riya sethi ji', 'riya2@gmail.com', NULL, '$2y$10$TsENiXkVluRA1amDxX9JCetBBjB0qWTtG.Xe6Elm29DXbgCaVJxQ2', NULL, '2019-11-21 03:42:23', '2019-11-29 07:51:00', NULL, NULL, 'customer', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

DROP TABLE IF EXISTS `wishlists`;
CREATE TABLE IF NOT EXISTS `wishlists` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wishlists`
--

INSERT INTO `wishlists` (`id`, `user_id`, `product_id`, `created_at`, `updated_at`) VALUES
(1, '5', '12', '2019-09-06 17:14:29', '2019-09-06 17:14:29'),
(2, '12', '4', '2019-09-06 17:33:05', '2019-09-06 17:33:05'),
(3, '12', '6', '2019-09-06 17:33:07', '2019-09-06 17:33:07'),
(5, '2', '3', '2019-09-09 19:31:31', '2019-09-09 19:31:31'),
(6, '15', '4', '2019-09-24 18:41:39', '2019-09-24 18:41:39'),
(7, '19', '9', '2019-10-15 22:50:11', '2019-10-15 22:50:11'),
(8, '19', '11', '2019-10-15 22:51:22', '2019-10-15 22:51:22'),
(9, '19', '10', '2019-10-15 22:51:36', '2019-10-15 22:51:36'),
(10, '19', '1', '2019-10-15 22:51:39', '2019-10-15 22:51:39');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
