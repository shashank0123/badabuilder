
<?php
use App\Models\Category;
use App\Models\Product;
use App\Wishlist;
use App\Models\Company;

$company = Company::first();
$contact = explode(',',$company->contact_no);
$products = Product::all();
$count = 0;
$maincategory = Category::where('status','Active')->where('category_id',0)->get();
$allcat = Category::where('status','Active')->where('category_id','!=',0)->get();

if(!empty(session()->get('cart'))){
  foreach(session()->get('cart') as $cart){
    $count++;
  }
}

$total = 0;
$cartproducts = array();
    $quantity = array();
    $i = 0;
    $bill = 0;
    if(!empty(session()->get('cart'))){
      foreach(session()->get('cart') as $cart){
        $cartproducts[$i] = Product::where('id',$cart->product_id)->first();
        $quantity[$i] = $cart->quantity;
        $i++;
      }
      $j=0;
      foreach(session()->get('cart') as $cart){
        $quantity[$j++];
      }

      foreach(session()->get('cart') as $pro){
        $product = Product::where('id',$pro->product_id)->first();
        $price = $product->sell_price;
        $bill = $bill + $pro->quantity*$price;
      }
    }


?>


<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="robots" content="noindex, follow" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- meta tag for seo -->
  <title>Home Glare | @yield('page_title')</title>
  <meta name="description" content="@yield('page_discription')">
  <link rel="canonical" href="@yield('page_URL')" />
  <!-- meta tags for social share -->
  <meta property="og:image" content="@yield('social_image')">
  <meta property="og:title" content="@yield('social_title')">
  <meta property="og:description" content="@yield('social_description')">
  <!-- Favicon -->
  <link rel="shortcut icon" type="image/x-icon" href="{{asset('asset/images/favicon.ico')}}">

    <!-- CSS
     ============================================ -->

     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="/asset/css/vendor/bootstrap.min.css">
     <!-- Fontawesome -->
     <link rel="stylesheet" href="/asset/css/vendor/font-awesome.css">
     <!-- Fontawesome Star -->
     <link rel="stylesheet" href="/asset/css/vendor/fontawesome-stars.css">
     <!-- Ion Icon -->
     <link rel="stylesheet" href="/asset/css/vendor/ion-fonts.css">
     <!-- Slick CSS -->
     <link rel="stylesheet" href="/asset/css/plugins/slick.css">
     <!-- Animation -->
     <link rel="stylesheet" href="/asset/css/plugins/animate.css">
     <!-- jQuery Ui -->
     <link rel="stylesheet" href="/asset/css/plugins/jquery-ui.min.css">
     <!-- Lightgallery -->
     <link rel="stylesheet" href="/asset/css/plugins/lightgallery.min.css">
     <!-- Nice Select -->
     <link rel="stylesheet" href="/asset/css/plugins/nice-select.css">

     <!-- Vendor & Plugins CSS (Please remove the comment from below vendor.min.css & plugins.min.css for better website load performance and remove css files from the above) -->
    <!--
    <script src="/asset/js/vendor/vendor.min.js"></script>
    <script src="/asset/js/plugins/plugins.min.js"></script>
  -->

  <!-- Main Style CSS (Please use minify version for better website load performance) -->
  <link rel="stylesheet" href="/asset/css/style.css">
  <!--<link rel="stylesheet" href="/asset/css/style.min.css">-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <style media="screen">
    .star-checked {
      color: #00BB00 !important;
    }
  </style>

</head>

<body class="template-color-1" onload="checkCart()">

  <div class="main-wrapper">
<div class="fixed-bottom">
  <a class="btn m-2" style="background-color: #00e676;border-radius: 25px;" target="_blank" href="https://api.whatsapp.com/send?phone=8802030809" role="button">
    <i class="fa fa-2x fa-whatsapp text-white"></i>
  </a>
</div>
    <!-- Begin Loading Area -->
    <div class="loading">
      <div class="text-center middle">
        <div class="lds-ellipsis">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    </div>
    <!-- Loading Area End Here -->
    <!-- Begin Homeglare Newsletter Popup Area -->
    {{-- <div class="popup_wrapper">
      <div class="test">
        <span class="popup_off"><i class="ion-android-close"></i></span>
        <div class="subscribe_area text-center">
          <h2>Sign up for send newsletter?</h2>
          <p>Subscribe to our newsletters now and stay up-to-date with new collections, the latest lookbooks and exclusive offers.</p>
          <div class="subscribe-form-group">
            <form action="#">
              <input autocomplete="off" type="email" name="email" id="email" placeholder="Enter your email address">
              <button type="submit">subscribe</button>
            </form>
          </div>
          <div class="subscribe-bottom">
            <input type="checkbox" id="newsletter-permission">
            <label for="newsletter-permission">Don't show this popup again</label>
          </div>
        </div>
      </div>
    </div> --}}
    <!-- Homeglare Newsletter Popup Area Here -->

    <!-- Begin Homeglare Header Main Area -->
    <header class="header-main_area">
      <div class="header-top_area">
        <div class="container">
          <div class="row">
            <div class="col-lg-5">
              <div class="ht-left_area">
                <div class="header-shipping_area">
                  <ul>
                    <li>
                      @if($company)
                      <span>Telephone Enquiry:</span>
                      @if(isset($contact))
                      @foreach($contact as $con)
                      <a href="callto://+91{{$con}}">(+91) {{$con}}</a>
                      @endforeach
                      @endif
                      @endif
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-7">
              <div class="ht-right_area">
                <div class="ht-menu">
                  <ul>

                    @if(!empty(Auth::user()->id))
                    <li><a>My Account<i class="fa fa-chevron-down"></i></a>
                      <ul class="ht-dropdown ht-my_account">
                        <li><a href="/my-account/{{Auth::user()->id}}">Dashboard</a></li>
                        <li class="active">
                          <a href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                          {{'Logout'}}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                        </form>

                      </li>
                    </ul>
                  </li>
                  @else
                  <li><a>Account<i class="fa fa-chevron-down"></i></a>
                    <ul class="ht-dropdown ht-my_account">
                      <!-- <li><a href="/register">Register</a></li> -->
                      <li class="active"><a href="/login">Login</a></li>
                    </ul>
                  </li>
                  @endif
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-middle_area d-none d-lg-block">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="header-logo">
              <a href="/">
                <img src="/logo/Home-Glare-logo.png" alt="HomeGlare Logo" style="width: 20%; height: auto;"><span style="font-size: 30px; color: #005EC9; font-weight: 600; font-family: 'Lato', sans-serif">HOME GLARE</span>
              </a>
            </div>
          </div>
          <div class="col-lg-8">
            <div class="hm-form_area">
              <form action="/global-search-product" method="POST"  class="hm-searchbox">
                <select class="nice-select select-search-category" value="{{ request('subcategory') }}" autocomplete="off" name="subcategory">
                  <option value="0">All</option>;
                  @foreach($allcat as $row)
                  <option value="{{ $row->id }}"><b>{{ $row->category_name }}</b></option>
                  <!-- get all subcategory -->
                  @endforeach
                </select>
                <input type="text" value="{{ request('product_keyword') }}" autocomplete="off" placeholder="Enter your search key ..." name="product_keyword">
                <button type="submit" class="li-btn" name="submit"><i class="fa fa-search"></i></button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-bottom_area header-sticky stick">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-4 d-lg-none d-block">
            <div class="header-logo">
              <a href="/">
                <img src="/logo/Home-Glare-logo.png" alt="Homeglare Header Logo" style="width: 20%; height: auto;"><span style="font-size: 30px; color: #005EC9; font-weight: 600; font-family: 'Lato', sans-serif">HOME GLARE</span>
              </a>
            </div>
          </div>
          <div class="col-lg-9 d-none d-lg-block position-static">
            <div class="main-menu_area">
              <nav>
                <ul>
                  {{-- <li class="active"><a href="/index">Home</a></li>      --}}
                  @foreach($maincategory as $row)
                  <li class="megamenu-holder"><a href="/products/{{$row->slug}}">{{$row->category_name}}</a>
                    <ul class="hm-megamenu">
                      <?php $category=Category::where('category_id',$row->id)->where('status','Active')->get(); ?>
                      @foreach($category as $col)
                      <li><span class="megamenu-title">{{$col->category_name}}</span>
                        <?php $subproduct=Category::where('category_id',$col->id)->where('status','Active')->get(); ?>
                        <ul>
                          @foreach($subproduct as $cols)
                          <li><a href="/products/{{$cols->slug}}">{{$cols->category_name}}</a>
                          </li>
                          @endforeach
                        </ul>
                      </li>
                      @endforeach
                    </ul>
                  </li>
                  @endforeach
                      {{-- <li><a href="/about">About Us</a></li>
                      <li><a href="/contact-us">Contact</a></li> --}}
                      @if (!empty(Auth::user()->id))
                      {{-- <li><a href="/en/member">Panel</a></li>
                      <li><a href="/en/logout">Logout</a></li> --}}
                      @else
                      {{-- <li><a href="/en/login">Login</a></li> --}}
                      @endif
                    </ul>
                  </nav>
                </div>
              </div>
              <div class="col-lg-3 col-md-8 col-sm-8">
                <div class="header-right_area">
                  <ul>
                    <li>
                      <a href="<?php if(!empty(Auth::user()->id)){echo '/wishlist/'.Auth::user()->id ;} else {echo '/login';} ?>" class="wishlist-btn">
                        <i class="ion-android-favorite-outline"></i>(<span id="wishItem"><?php
                          if(!empty(Auth::user()->id))
                            {$wish_count = Wishlist::where('user_id',Auth::user()->id)->count();
                          echo $wish_count;
                        }
                        else { echo 0; }
                        ?></span>)
                      </a>
                    </li>
                    <li>
                      <a href="#mobileMenu" class="mobile-menu_btn toolbar-btn color--white d-lg-none d-block">
                        <i class="ion-navicon"></i>
                      </a>
                    </li>
                    <li>
                      <a href="#miniCart" class="minicart-btn toolbar-btn" onclick="showMiniCart()">
                        <i class="ion-bag"></i>(<span id="cartItem">0</span>)
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="offcanvas-minicart_wrapper" id="miniCart">
          <div class="offcanvas-menu-inner">
            <a href="#" class="btn-close"><i class="ion-android-close"></i></a>
            <div class="minicart-content">
              <div class="minicart-heading">
                <h4>Shopping Cart</h4>
              </div>
              @if($cartproducts != null)
                <?php $i=0;  ?>
              <ul class="minicart-list">
                @foreach($cartproducts as $product)

                <li class="minicart-product" id="listhide{{$product->id}}">
                  <a class="product-item_remove" href="javascript:void(0)"  onclick="deleteProduct({{$product->id}})"><i class="ion-android-close"></i></a>
                  <div class="product-item_img">
                    <img src="{{url('/')}}/{{$product->image1}}" alt="Homeglare Product Image">
                  </div>
                  <div class="product-item_content">
                    <a class="product-item_title" href="/product-detail/{{$product->slug}}">{{strtoupper($product->name)}}</a>
                    <span class="product-item_quantity">{{$quantity[$i]}} x Rs. {{$product->sell_price}}</span>
                  </div>
                </li>
                <?php
                      $total  =$total + $product->sell_price * $quantity[$i];
                      $i++;
                      ?>
                @endforeach
              </ul>
              @endif
            </div>
            <div class="minicart-item_total">
              <span>Subtotal</span>
              <span class="ammount">Rs. <p id="total-bill">{{$total}}</p></span>
            </div>
            <div class="minicart-btn_area">
              <a href="<?php if(!empty(Auth::user()->id)){echo '/cart/'.Auth::user()->id;} else{ echo '/login'; } ?>" class="hiraola-btn hiraola-btn_dark hiraola-btn_fullwidth">Cart</a>
            </div>
            <div class="minicart-btn_area">
              <a href="<?php if(!empty(Auth::user()->id)){echo '/checkout/'.Auth::user()->id;} else{ echo '/login'; } ?>" class="hiraola-btn hiraola-btn_dark hiraola-btn_fullwidth">Checkout</a>
            </div>
          </div>
        </div>
        <div class="mobile-menu_wrapper" id="mobileMenu">
          <div class="offcanvas-menu-inner">
            <div class="container">
              <a href="#" class="btn-close"><i class="ion-android-close"></i></a>
              <div class="offcanvas-inner_search">
                <form action="#" class="hm-searchbox">
                  <input type="text" placeholder="Search for item...">
                  <button class="search_btn" type="submit"><i class="ion-ios-search-strong"></i></button>
                </form>
              </div>
              <nav class="offcanvas-navigation">
                <ul class="mobile-menu">

                  <li>
                    <a href="/">
                      <span class="mm-text">Home</span>
                    </a>
                  </li>

                  @foreach($maincategory as $row)
                  <li class="menu-item-has-children">
                    <a href="/products/{{$row->slug}}">

                      <span class="mm-text">{{strtoupper($row->category_name)}}</span>
                    </a>
                    <ul class="sub-menu">
                      <?php $category=Category::where('category_id',$row->id)->get(); ?>
                      @foreach($category as $col)
                      <li class="menu-item-has-children">
                        <a href="/products/{{$col->slug}}">

                          <span class="mm-text">{{strtoupper($col->category_name)}}</span>
                        </a>

                        <ul class="sub-menu">
                          <?php $subproduct=Category::where('category_id',$col->id)->get(); ?>
                          @foreach($subproduct as $cols)
                          <li>
                            <a href="/products/{{$cols->slug}}">
                              <span class="mm-text">{{ strtoupper($cols->category_name)}}</span>
                            </a>
                          </li>
                          @endforeach

                        </ul>
                      </li>
                      @endforeach
                    </ul>
                  </li>
                  @endforeach

                </ul>
              </nav>
              <nav class="offcanvas-navigation user-setting_area">
                <ul class="mobile-menu">
                  @if(!empty(Auth::user()->id))
                  <li>
                    <a href="/my-account">
                      <span class="mm-text">My Account</span>
                    </a>
                  </li>
                  <li>
                    <a href="/cart">
                      <span class="mm-text">Cart</span>
                    </a>
                  </li>
                  <li>
                    <a href="/checkout">
                      <span class="mm-text">Checkout</span>
                    </a>
                  </li>
                  @else
                  <li>
                    <a href="/login">
                      <span class="mm-text">Login</span>
                    </a>
                  </li>
                  <li>
                    <a href="/register">
                      <span class="mm-text">Register</span>
                    </a>
                  </li>

                  <li>
                    <a href="/login">
                      <span class="mm-text">Cart</span>
                    </a>
                  </li>
                  @endif

                  <li>
                    <a href="/faq">
                      <span class="mm-text">FAQ</span>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </header>
      <!-- Homeglare Header Main Area End Here -->



      {{-- Main Section --}}

      @yield('content')

      {{-- End Of Main Section --}}




      <!-- Begin Homeglare Shipping Area -->
      <div class="hiraola-shipping_area">
        <div class="container">
          <div class="shipping-nav">
            <div class="row">
              <div class="col-lg-3 col-md-6">
                <div class="shipping-item">
                  <div class="shipping-icon">
                    <img src="/asset/images/shipping-icon/1.png" alt="Homeglare Shipping Icon">
                  </div>
                  <div class="shipping-content">
                    <h6>Free India Standard Delivery</h6>
                    <p>Designated day delivery</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-6">
                <div class="shipping-item">
                  <div class="shipping-icon">
                    <img src="/asset/images/shipping-icon/2.png" alt="Homeglare Shipping Icon">
                  </div>
                  <div class="shipping-content">
                    <h6>Freshly Prepared Ingredients</h6>
                    <p>Made for your delivery date</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-6">
                <div class="shipping-item">
                  <div class="shipping-icon">
                    <img src="/asset/images/shipping-icon/3.png" alt="Homeglare Shipping Icon">
                  </div>
                  <div class="shipping-content">
                    <h6>98% Of Anta Clients</h6>
                    <p>Reach their personal goals set</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-6">
                <div class="shipping-item">
                  <div class="shipping-icon">
                    <img src="/asset/images/shipping-icon/4.png" alt="Homeglare Shipping Icon">
                  </div>
                  <div class="shipping-content">
                    <h6>Winner Of 15 Awards</h6>
                    <p>Healthy food and drink 2019</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Homeglare Shipping Area End Here -->


      <!-- Begin Homeglare Footer Area -->
      <div class="hiraola-footer_area">
        <div class="footer-top_area">
          <div class="container">
            <div class="row">
              <div class="col-lg-4">
                <div class="footer-widgets_info">

                  <div class="footer-widgets_logo">
                    <a href="/">

                      <img src="/logo/Home-Glare-logo.png" alt="Homeglare Header Logo" style="width: 25%; height: auto;"><span style="font-size: 30px; color: #005EC9; font-weight: 600; font-family: 'Lato', sans-serif">HOME GLARE</span>

                      {{-- <img src="/asset/images/footer/logo/1.png" alt="Homeglare Footer Logo"> --}}
                    </a>
                  </div>


                  <div class="widget-short_desc">
                    <p>
                      @if($company)
                      <?php echo $company->sort_discription ?>
                      @else
                      We are a team of designers and developers that create high quality website.
                      @endif
                    </p>
                  </div>
                  <div class="hiraola-social_link">
                    <ul>
                      <li class="facebook">
                        <a href="https://www.facebook.com/homeglareonline" data-toggle="tooltip" target="_blank" title="Facebook">
                          <i class="fab fa-facebook"></i>
                        </a>
                      </li>
                      <li class="twitter">
                        <a href="https://www.twitter.com" data-toggle="tooltip" target="_blank" title="Twitter">
                          <i class="fab fa-twitter-square"></i>
                        </a>
                      </li>
                      <li class="google-plus">
                        <a href="https://www.plus.google.com/discover" data-toggle="tooltip" target="_blank" title="Google Plus">
                          <i class="fab fa-google-plus"></i>
                        </a>
                      </li>
                      <li class="instagram">
                        <a href="https://www.instagram.com" data-toggle="tooltip" target="_blank" title="Instagram">
                          <i class="fab fa-instagram"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-lg-8">
                <div class="footer-widgets_area">
                  <div class="row">
                    <div class="col-lg-3">
                      <div class="footer-widgets_title">
                        <h6>Product</h6>
                      </div>
                      <div class="footer-widgets">
                        <ul>
                          <li><a href="{{url('/about-us')}}">About Us</a></li>
                          <li><a href="{{url('/privacy-policy')}}">Privacy Policy</a></li>
                          <li><a href="{{url('/return-policy')}}">Return Policy</a></li>
                          <li><a href="{{url('/contact')}}">Contact us</a></li>
                          <li><a href="{{url('/faq')}}">FAQ</a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-lg-5">
                      <div class="footer-widgets_info">
                        <div class="footer-widgets_title">
                          <h6>About Us</h6>
                        </div>
                        <div class="widgets-essential_stuff">
                          @if($company)
                          <ul>
                            <li class="hiraola-address"><i class="ion-ios-location"></i><span>Address:</span> <?php echo $company->address ?> </li>
                            <li class="hiraola-phone"><i class="ion-ios-telephone"></i><span>Call Us:</span>
                               @if(isset($contact))
                      @foreach($contact as $con)
                              <a href="tel://+91{{$con}}">+91 {{$con}}</a> <br>
                              @endforeach
                              @endif

                            </li>
                            <li class="hiraola-email"><i class="ion-android-mail"></i><span>Email:</span> <a href="mailto://{{$company->support_email}}">{{$company->support_email}}</a></li>
                          </ul>
                          @endif
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="instagram-container footer-widgets_area">
                        <div class="footer-widgets_title">
                          <h6>Sign Up For Newslatter</h6>
                        </div>
                        <div class="widget-short_desc">
                          <p>Subscribe to our newsletters now and stay up-to-date with new collections</p>
                        </div>
                        <div class="newsletter-form_wrap">
                          <form id="newsletter" method="post" action="{{ route('newsletter') }}">
                            {{ csrf_field() }}
                            <div id="mc_embed_signup_scroll">
                              <div id="mc-form" class="mc-form subscribe-form">
                                <input id="newsletterEmail" class="newsletter-input" type="email" name="email" autocomplete="off" placeholder="Enter your email" required />
                                <button class="newsletter-btn">
                                  <i class="ion-android-mail" aria-hidden="true"></i>
                                </button>
                              </div>
                              @if(session('NewslatterMessege'))
                                  <div class="alert alert-success" id="NewslatterMessege">
                                      {{ session('NewslatterMessege') }}
                                  </div>
                              @endif
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Homeglare Footer Area End Here -->
      <!-- Begin Homeglare Modal Area -->
      <div class="modal fade modal-wrapper" id="exampleModalCenter">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div class="modal-inner-area sp-area row">
                <div class="col-lg-5 col-md-5">
                  <div class="sp-img_area">
                    <div class="sp-img_slider-2 slick-img-slider hiraola-slick-slider arrow-type-two" data-slick-options='{
                    "slidesToShow": 1,
                    "arrows": false,
                    "fade": true,
                    "draggable": false,
                    "swipe": false,
                    "asNavFor": ".sp-img_slider-nav"
                  }'>
                  <div class="single-slide red">
                    <img id="pdImage1" src="/asset/images/single-product/large-size/1.jpg" alt="Homeglare Product Image">
                  </div>
                  <div class="single-slide orange">
                    <img id="pdImage2" src="/asset/images/single-product/large-size/2.jpg" alt="Homeglare Product Image">
                  </div>
                  <div class="single-slide brown">
                    <img id="pdImage3" src="/asset/images/single-product/large-size/3.jpg" alt="Homeglare Product Image">
                  </div>
                  <!-- <div class="single-slide umber">
                    <img src="/asset/images/single-product/large-size/4.jpg" alt="Homeglare Product Image">
                  </div> -->
                </div>
                <div class="sp-img_slider-nav slick-slider-nav hiraola-slick-slider arrow-type-two" data-slick-options='{
                "slidesToShow": 4,
                "asNavFor": ".sp-img_slider-2",
                "focusOnSelect": true
              }' data-slick-responsive='[
              {"breakpoint":768, "settings": {"slidesToShow": 3}},
              {"breakpoint":577, "settings": {"slidesToShow": 3}},
              {"breakpoint":481, "settings": {"slidesToShow": 2}},
              {"breakpoint":321, "settings": {"slidesToShow": 2}}
              ]'>
              <div class="single-slide red">
                <img id="pdImage4" src="/asset/images/single-product/small-size/1.jpg" alt="Homeglare Product Thumnail">
              </div>
              <div class="single-slide orange">
                <img id="pdImage5" src="/asset/images/single-product/small-size/2.jpg" alt="Homeglare Product Thumnail">
              </div>
              <div class="single-slide brown">
                <img id="pdImage6" src="/asset/images/single-product/small-size/3.jpg" alt="Homeglare Product Thumnail">
              </div>
              <!-- <div class="single-slide umber">
                <img src="/asset/images/single-product/small-size/4.jpg" alt="Homeglare Product Thumnail">
              </div> -->
            </div>
          </div>
        </div>
        <div class="col-xl-7 col-lg-6 col-md-6">
          <div class="sp-content">
            <div class="sp-heading">
              <h5><a id="pdName" href="/"></a></h5>
            </div>
            <!-- <div class="rating-box">
              <ul>
                <li><i class="fa fa-star-of-david"></i></li>
                <li><i class="fa fa-star-of-david"></i></li>
                <li><i class="fa fa-star-of-david"></i></li>
                <li><i class="fa fa-star-of-david"></i></li>
                <li><i class="fa fa-star-of-david"></i></li>
              </ul>
            </div> -->
            <div class="price-box">
              <span id="pdPrice" class="new-price"></span>
              <!-- <span class="old-price">£93.68</span> -->
            </div>
            <!-- <div class="essential_stuff">
              <ul>
                <li>EX Tax:<span>£453.35</span></li>
                <li>Price in reward points:<span>400</span></li>
              </ul>
            </div> -->
            <!-- <div class="list-item">
              <ul>
                <li>10 or more £81.03</li>
                <li>20 or more £71.09</li>
                <li>30 or more £61.15</li>
              </ul>
            </div> -->
            <div class="list-item last-child">
              <ul>
                <li id="pdBrand"></li>
                <li id="pdProductCode"></li>
                <li id="pdReward"></li>
                <li id="pdAvailability"></li>
              </ul>
            </div>
            <!-- <div class="color-list_area">
              <div class="color-list_heading">
                <h4>Available Options</h4>
              </div>
              <span class="sub-title">Color</span>
              <div class="color-list">
                <a href="javascript:void(0)" class="single-color active" data-swatch-color="red">
                  <span class="bg-red_color"></span>
                  <span class="color-text">Red (+£3.61)</span>
                </a>
                <a href="javascript:void(0)" class="single-color" data-swatch-color="orange">
                  <span class="burnt-orange_color"></span>
                  <span class="color-text">Orange (+£2.71)</span>
                </a>
                <a href="javascript:void(0)" class="single-color" data-swatch-color="brown">
                  <span class="brown_color"></span>
                  <span class="color-text">Brown (+£0.90)</span>
                </a>
                <a href="javascript:void(0)" class="single-color" data-swatch-color="umber">
                  <span class="raw-umber_color"></span>
                  <span class="color-text">Umber (+£1.81)</span>
                </a>
              </div>
            </div> -->
            <!-- <div class="quantity">
              <label>Quantity</label>
              <div class="cart-plus-minus">
                <input class="cart-plus-minus-box" value="1" type="text">
                <div class="dec qtybutton"><i class="fa fa-angle-down"></i></div>
                <div class="inc qtybutton"><i class="fa fa-angle-up"></i></div>
              </div>
            </div> -->
            <div class="hiraola-group_btn">
              <ul>
                <li><a id="pdAddToCart" href="javascript:void(0);" class="add-to_cart">Add To Cart</a></li>
                <li><a id="pdaddToWishlist" href="javascript:void(0);"><i class="ion-android-favorite-outline"></i></a></li>
                <!-- <li><a href=""><i class="ion-ios-shuffle-strong"></i></a></li> -->
              </ul>
            </div>
            <!-- <div class="hiraola-tag-line">
              <h6>Tags:</h6>
              <a href="javascript:void(0)">Ring</a>,
              <a href="javascript:void(0)">Necklaces</a>,
              <a href="javascript:void(0)">Braid</a>
            </div> -->
            <!-- <div class="hiraola-social_link">
              <ul>
                <li class="facebook">
                  <a href="https://www.facebook.com" data-toggle="tooltip" target="_blank" title="Facebook">
                    <i class="fab fa-facebook"></i>
                  </a>
                </li>
                <li class="twitter">
                  <a href="https://twitter.com" data-toggle="tooltip" target="_blank" title="Twitter">
                    <i class="fab fa-twitter-square"></i>
                  </a>
                </li>
                <li class="youtube">
                  <a href="https://www.youtube.com" data-toggle="tooltip" target="_blank" title="Youtube">
                    <i class="fab fa-youtube"></i>
                  </a>
                </li>
                <li class="google-plus">
                  <a href="https://www.plus.google.com/discover" data-toggle="tooltip" target="_blank" title="Google Plus">
                    <i class="fab fa-google-plus"></i>
                  </a>
                </li>
                <li class="instagram">
                  <a href="https://www.instagram.com" data-toggle="tooltip" target="_blank" title="Instagram">
                    <i class="fab fa-instagram"></i>
                  </a>
                </li>
              </ul>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Homeglare Modal Area End Here -->

</div>

    <!-- JS
      ============================================ -->

      <!-- jQuery JS -->
      <script src="/asset/js/vendor/jquery-1.12.4.min.js"></script>
      <!-- Modernizer JS -->
      <script src="/asset/js/vendor/modernizr-2.8.3.min.js"></script>
      <!-- Popper JS -->
      <script src="/asset/js/vendor/popper.min.js"></script>
      <!-- Bootstrap JS -->
      <script src="/asset/js/vendor/bootstrap.min.js"></script>

      <!-- Slick Slider JS -->
      <script src="/asset/js/plugins/slick.min.js"></script>
      <!-- Countdown JS -->
      <script src="/asset/js/plugins/countdown.js"></script>
      <!-- Barrating JS -->
      <script src="/asset/js/plugins/jquery.barrating.min.js"></script>
      <!-- Counterup JS -->
      <script src="/asset/js/plugins/jquery.counterup.js"></script>
      <!-- Nice Select JS -->
      <script src="/asset/js/plugins/jquery.nice-select.js"></script>
      <!-- Sticky Sidebar JS -->
      <script src="/asset/js/plugins/jquery.sticky-sidebar.js"></script>
      <!-- Jquery-ui JS -->
      <script src="/asset/js/plugins/jquery-ui.min.js"></script>
      <script src="/asset/js/plugins/jquery.ui.touch-punch.min.js"></script>
      <!-- Lightgallery JS -->
      <script src="/asset/js/plugins/lightgallery.min.js"></script>
      <!-- Scroll Top JS -->
      <script src="/asset/js/plugins/scroll-top.js"></script>
      <!-- Theia Sticky Sidebar JS -->
      <script src="/asset/js/plugins/theia-sticky-sidebar.min.js"></script>
      <!-- Waypoints JS -->
      <script src="/asset/js/plugins/waypoints.min.js"></script>
      <!-- Instafeed JS -->
      <script src="/asset/js/plugins/instafeed.min.js"></script>
      <!-- Instafeed JS -->
      <script src="/asset/js/plugins/jquery.elevateZoom-3.0.8.min.js"></script>

      <!-- Vendor & Plugins JS (Please remove the comment from below vendor.min.js & plugins.min.js for better website load performance and remove js files from avobe) -->
    <!--
<script src="/asset/js/vendor/vendor.min.js"></script>
<script src="/asset/js/plugins/plugins.min.js"></script>
-->

<!-- Main JS -->
<script src="/asset/js/main.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js"></script>

<script>
$('#mc-submit').on('click', function(){
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  var email = $('#newsletterEmail').val();
  var request = $.ajax({
    type: "POST",
    url: "/newsletter",
    // data: $('#newsletter').serialize(),
    data: {_token: CSRF_TOKEN, email: email},
     success: function (data) {
       console.log(data);
       if (data.message == 'Thank you .We will reply you soon'){
         alert(data.message);
       }
       else
         alert(data.message);
     },
     failure: function (data) {
       alert(data.message);
     }
  });
});

  function checkCart(){
    var count = <?php echo $count;?>;
    // alert(count);
    $('#cartItem').html(count);
  }

  function showMiniCart(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
     url: '/show-minicart',
     type: 'GET',
     data: {_token: CSRF_TOKEN},
          success: function (data) {
            $('#miniCart').html(data);
            $("#total-bill").text(data.bill);
          }
   });
  }

  function deleteProduct(id){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        // var std = $("#show-total").html();
        // count_product--;
        // alert(id);
        $('#listhide'+id).hide();
        $('#cart'+id).hide();
        $.ajax({
          /* the route pointing to the post function */
          url: '/cart/delete-product/',
          type: 'POST',
          /* send the csrf-token and the input to the controller */
          data: {_token: CSRF_TOKEN, id: id},
          success: function (data) {
            $("#show-total").html(data.showcount);
            $("#bill").html(data.bill);
          }
        });
      }

      $('#exampleModalCenter').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var productId = button.data('myid')
        var modal = $(this)
        // get product details data
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
         url: '/get-product_details-data/'+productId,
         type: 'GET',
         data: {_token: CSRF_TOKEN},
              success: function (response) {
                // var responseObj = JSON.parse(data);
                data = response.data;

                $("#pdName").html(data.name);
                $("#pdPrice").html('Price: Rs.'+data.sell_price);
                $("#pdName").attr('href', '/product-detail/'+data.slug);

                $("#pdImage1").attr('src', '/'+data.image1);
                $("#pdImage2").attr('src', '/'+data.image2);
                $("#pdImage3").attr('src', '/'+data.image3);

                $("#pdImage4").attr('src', '/'+data.image1);
                $("#pdImage5").attr('src', '/'+data.image2);
                $("#pdImage6").attr('src', '/'+data.image3);
                if(data.product_brand)
                $("#pdBrand").html('Brand: '+ data.product_brand);
                // $("#pdProductCode").html('Product Code: Product '+ data.id);
                // $("#pdReward").html('Reward Points: '+ data);
                if(data.availability == 'yes')
                 $("#pdAvailability").html("Availability: In Stock");
                else
                 $("#pdAvailability").html("Availability: Out of Stock");

                $("#pdAddToCart").attr('onclick', 'addToCart('+data.id+')');
                $("#pdaddToWishlist").attr('onclick', 'addToWishlist('+data.id+')');
              }
       });
      })

      @if(session('NewslatterMessege'))
      $('html, body').animate({
       scrollTop: $("#NewslatterMessege").offset().top
       }, 2000);
      @endif

      // for reply
      $('#subject').on('change', function(e){
        if ($('#subject').val() ==  'other_subject') {
          $('#other').removeClass('d-none');
        }else {
          $('#other').addClass('d-none')
        }
      })

</script>



@yield('js-script')

</body>

</html>
