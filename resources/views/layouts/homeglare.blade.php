<?php 
  use App\Wishlist;
  use App\Models\Company;
  
  $company = Company::first();
  $contact = explode(',',$company->contact_no);
  $count = 0;
  
  if(!empty(session()->get('cart'))){
    foreach(session()->get('cart') as $cart){
      $count++;
  }
  }
  
  ?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="keywords" content="HomeGlare-Ecommerce">
    {{-- 
    <meta name="">
    --}}
    <meta content="Navin Kumar Yadav (navinkumaryadav1997@gmail.com) / Frontend Developer " name="description">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Home Glare | @yield('page_title')</title>
    <meta name="description" content="@yield('page_discription')">
    <link rel="canonical" href="@yield('page_URL')" />
    <!-- meta tags for social share -->
    <meta property="og:image" content="@yield('social_image')">
    <meta property="og:title" content="@yield('social_title')">
    <meta property="og:description" content="@yield('social_description')">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="/fyc-new/images/logo/favicon.png">
    <!-- CSS
      ============================================ -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/fyc-new/css/vendor/bootstrap.min.css">
    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="/fyc-new/css/vendor/line-awesome.css">
    <link rel="stylesheet" href="/fyc-new/css/vendor/themify.css">
    <!-- othres CSS -->
    <link rel="stylesheet" href="/fyc-new/css/plugins/animate.css">
    <link rel="stylesheet" href="/fyc-new/css/plugins/owl-carousel.css">
    <link rel="stylesheet" href="/fyc-new/css/plugins/slick.css">
    <link rel="stylesheet" href="/fyc-new/css/plugins/magnific-popup.css">
    <link rel="stylesheet" href="/fyc-new/css/plugins/jquery-ui.css">
    <link rel="stylesheet" href="/fyc-new/css/style.css">
    <style>
      
      .swal2-popup .swal2-styled.swal2-confirm { background-color: #000033 !important }
    </style>
  </head>
  <body onload="checkCart()">
    <div class="main-wrapper">
      <header class="header-area bg-black sticky-bar header-padding-1">
        
        <div class="main-header-wrap">

<div class="top-head" style="background-color: #ddd">
  <a href="https://api.whatsapp.com/send?phone=7289800098&text=Hey">
          <!-- <a href="whatsapp://send?abid=7289800098&text=Hey%2C%20User!"> -->
          <img src="{{asset('/logo/icon.png')}}" style="width: 25px; height: auto">
          7289800098</a>
        </div>

          <div class="container-fluid">
            <div class="row align-items-center">
              <div class="col-xl-2 col-lg-3 logo-border">
                <div class="logo ">
                  <a href="/"><img src="/fyc-new/images/logo/logo.png" alt="logo"></a>
                </div>
              </div>
              <div class="col-xl-8 col-lg-6 d-flex justify-content-center">
                <div class="main-menu menu-common-style menu-lh-2 menu-margin-2 menu-white">
                  <nav>
                    <ul>
                      <li><a href="/">Home</a></li>
                      <li><a href="/about">About us </a></li>
                      {{-- 
                      <li><a href="/productw">Products</a></li>
                      --}}
                      <li><a href="/contact">Contact Us</a></li>
                      <li><a href="/sell-on-homeglare">Sell on Homeglare</a></li>
                      @if(!empty(Auth::user()->id))
                      <li class="angle-shape">
                        <a href="#">My Account</a>
                        <ul class="submenu">
                          <li><a href="/account/{{Auth::user()->id}}">Dashboard</a></li>
                          <li class="active">
                            <a href="{{ route('logout') }}"
                              onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                            {{'Logout'}}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                            </form>
                          </li>
                        </ul>
                      </li>
                      @else
                      <li><a href="/login">Login</a></li>
                      {{-- 
                      <li><a href="/login">Sign Up</a></li>
                      --}}
                      @endif
                      {{-- 
                      <li class="angle-shape">
                        <a href="shop.html">Shop </a>
                        <ul class="mega-menu">
                          <li>
                            <a class="menu-title" href="#">Shop Layout</a>
                            <ul>
                              <li><a href="shop.html">standard style</a></li>
                              <li><a href="shop-2.html">standard style 2</a></li>
                              <li><a href="shop-2-col.html">shop 2 column</a></li>
                              <li><a href="shop-no-sidebar.html">shop no sidebar</a></li>
                              <li><a href="shop-fullwide.html">shop fullwide</a></li>
                            </ul>
                          </li>
                          <li>
                            <a class="menu-title" href="#">Shop Layout</a>
                            <ul>
                              <li><a href="shop-fullwide-no-sidebar.html">fullwide no sidebar </a></li>
                              <li><a href="shop-list.html">list style</a></li>
                              <li><a href="shop-list-2col.html">list 2 column</a></li>
                              <li><a href="shop-list-no-sidebar.html">list no sidebar</a></li>
                            </ul>
                          </li>
                          <li>
                            <a class="menu-title" href="#">Product Details</a>
                            <ul>
                              <li><a href="product-details.html">standard style</a></li>
                              <li><a href="product-details-2.html">standard style 2</a></li>
                              <li><a href="product-details-tab1.html">tab style 1</a></li>
                              <li><a href="product-details-tab2.html">tab style 2</a></li>
                              <li><a href="product-details-tab3.html">tab style 3 </a></li>
                            </ul>
                          </li>
                          <li>
                            <a class="menu-title" href="#">Product Details</a>
                            <ul>
                              <li><a href="product-details-gallery.html">gallery style </a></li>
                              <li><a href="product-details-sticky.html">sticky style</a></li>
                              <li><a href="product-details-slider.html">slider style</a></li>
                              <li><a href="product-details-affiliate.html">Affiliate style</a></li>
                            </ul>
                          </li>
                        </ul>
                      </li>
                      --}}
                    </ul>
                  </nav>
                </div>
              </div>
              <div class="col-xl-2 col-lg-3 header-right-border">
                <div class="header-right-wrap header-right-white ">
                  <div class="search-wrap common-style mr-45">
                    <button class="search-active">
                    <i class="la la-search"></i>
                    </button>
                  </div>
                  <div class="cart-wrap common-style mr-15">
                    <button class="cart-active" onclick="getWishlist()">
                      <i class="la la-heart-o"></i>
                      <sup>
                        <p class="cartsup" id="wishItem"><?php
                          if(!empty(Auth::user()->id))
                              {$wish_count = Wishlist::where('user_id',Auth::user()->id)->count();
                          echo $wish_count;
                          }
                          else { echo 0; }
                          ?></p>
                      </sup>
                    </button>
                  </div>
                  <div class="cart-wrap common-style">
                    <button class="cart-active" onclick="showMiniCart()">
                      <i class="la la-shopping-cart"></i>
                      <sup>
                        <p class="cartsup" id="cartItem">0</p>
                      </sup>
                      <!--  <span class="count-style count-theme-color">2 Items</span> -->
                    </button>
                    <div class="shopping-cart-content" id="miniCart">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- main-search start -->
          <div class="main-search-active">
            <div class="sidebar-search-icon">
              <button class="search-close"><span class="la la-close"></span></button>
            </div>
            <div class="sidebar-search-input">
              <form action="/global-search-product" method="POST">
                <div class="form-search">
                  <input id="search" class="input-text" value="" placeholder="Search Now" type="search" name="product_keyword">
                  <button>
                  <i class="la la-search"></i>
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="top-head2" style="background-color: #ddd">
          <a href="whatsapp://send?abid=7289800098&text=Hey%2C%20User!">
          <img src="{{asset('/logo/icon.png')}}" style="width: 25px; height: auto">
          7289800098</a>
        </div>
        <div class="header-small-mobile header-mobile-white">

          <div class="container-fluid">
            <div class="row align-items-center">
              <div class="col-6">
                <div class="mobile-logo">
                  <a href="/homew">
                  <img  height="30px" alt="" src="/fyc-new/images/logo/logo.png">
                  </a>
                </div>
              </div>
              <div class="col-6">
                <div class="header-right-wrap mr-10">
                  <div class="cart-wrap common-style">
                    <button class="cart-active">
                      <i class="la la-heart-o"></i>
                      <!--  <span class="count-style">2 Items</span> -->
                      <sup>
                        <p class="cartsup" id="wishItem2">0</p>
                      </sup>
                    </button>
                  </div>
                  <div class="cart-wrap common-style">
                    <button class="cart-active" onclick="showMiniCart()">
                      <i class="la la-shopping-cart"></i>
                      <!--  <span class="count-style">2 Items</span> -->
                      <sup>
                        <p class="cartsup" id="cartItem2">0</p>
                      </sup>
                    </button>
                    <div class="shopping-cart-content">
                      <div class="shopping-cart-top">
                        <h4>Your Cart</h4>
                        <a class="cart-close" href="#"><i class="la la-close"></i></a>
                      </div>
                      <ul>
                        <li class="single-shopping-cart">
                          <div class="shopping-cart-img">
                            <a href="#"><img alt="" src="/fyc-new/images/cart/cart-1.jpg"></a>
                            <div class="item-close">
                              <a href="#"><i class="sli sli-close"></i></a>
                            </div>
                          </div>
                          <div class="shopping-cart-title">
                            <h4><a href="#">Golden Easy Spot Chair.</a></h4>
                            <span>₹99.00</span>
                          </div>
                          <div class="shopping-cart-delete">
                            <a href="#"><i class="la la-trash"></i></a>
                          </div>
                        </li>
                        <li class="single-shopping-cart">
                          <div class="shopping-cart-img">
                            <a href="#"><img alt="" src="/fyc-new/images/cart/cart-2.jpg"></a>
                            <div class="item-close">
                              <a href="#"><i class="sli sli-close"></i></a>
                            </div>
                          </div>
                          <div class="shopping-cart-title">
                            <h4><a href="#">Golden Easy Spot Chair.</a></h4>
                            <span>₹99.00</span>
                          </div>
                          <div class="shopping-cart-delete">
                            <a href="#"><i class="la la-trash"></i></a>
                          </div>
                        </li>
                        <li class="single-shopping-cart">
                          <div class="shopping-cart-img">
                            <a href="#"><img alt="" src="/fyc-new/images/cart/cart-3.jpg"></a>
                            <div class="item-close">
                              <a href="#"><i class="sli sli-close"></i></a>
                            </div>
                          </div>
                          <div class="shopping-cart-title">
                            <h4><a href="#">Golden Easy Spot Chair.</a></h4>
                            <span>₹99.00</span>
                          </div>
                          <div class="shopping-cart-delete">
                            <a href="#"><i class="la la-trash"></i></a>
                          </div>
                        </li>
                      </ul>
                      <div class="shopping-cart-bottom">
                        <div class="shopping-cart-total">
                          <h4>Subtotal <span class="shop-total">₹290.00</span></h4>
                        </div>
                        <div class="shopping-cart-btn btn-hover default-btn text-center">
                          <a class="black-color" href="/checkoutw">Continue to Chackout</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="mobile-off-canvas">
                    <a class="mobile-aside-button" href="#"><i class="la la-navicon la-2x"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <div class="mobile-off-canvas-active">
        <a class="mobile-aside-close"><i class="la la-close"></i></a>
        <div class="header-mobile-aside-wrap">
          <div class="mobile-search">
            <form class="search-form" action="#">
              <input type="text" placeholder="Search entire store…">
              <button class="button-search"><i class="la la-search"></i></button>
            </form>
          </div>
          <div class="mobile-menu-wrap">
            <!-- mobile menu start -->
            <div class="mobile-navigation">
              <!-- mobile menu navigation start -->
              <nav>
                <ul class="mobile-menu">
                  <li><a href="/homew">Home</a></li>
                  <li><a href="/aboutw">About us </a></li>
                  {{-- 
                  <li><a href="/productw">Products</a></li>
                  --}}
                  <li><a href="/contactw">Contact Us</a></li>
                  <li><a href="/offersw">Offers & Service</a></li>
                  <!-- <li class="menu-item-has-children "><a href="#">shop</a>
                    <ul class="dropdown">
                        <li class="menu-item-has-children"><a href="#">shop layout</a>
                            <ul class="dropdown">
                                <li><a href="shop.html">standard grid style</a></li>
                                <li><a href="shop-2.html">standard style 2</a></li>
                                <li><a href="shop-2-col.html">shop 2 column</a></li>
                                <li><a href="shop-no-sidebar.html">shop no sidebar</a></li>
                                <li><a href="shop-fullwide.html">shop fullwide</a></li>
                                <li><a href="shop-fullwide-no-sidebar.html">fullwide no sidebar </a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children"><a href="#">shop list layout</a>
                            <ul class="dropdown">
                                <li><a href="shop-list.html">list style</a></li>
                                <li><a href="shop-list-2col.html">list 2 column</a></li>
                                <li><a href="shop-list-no-sidebar.html">list no sidebar</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children"><a href="#">product details</a>
                            <ul class="dropdown">
                                <li><a href="product-details.html">standard style</a></li>
                                <li><a href="product-details-2.html">standard style 2</a></li>
                                <li><a href="product-details-tab1.html">tab style 1</a></li>
                                <li><a href="product-details-tab2.html">tab style 2</a></li>
                                <li><a href="product-details-tab3.html">tab style 3 </a></li>
                                <li><a href="product-details-gallery.html">gallery style </a></li>
                                <li><a href="product-details-sticky.html">sticky style</a></li>
                                <li><a href="product-details-slider.html">slider style</a></li>
                                <li><a href="product-details-affiliate.html">Affiliate style</a></li>
                            </ul>
                        </li>
                    </ul>
                    </li>
                    <li class="menu-item-has-children"><a href="#">Pages</a>
                    <ul class="dropdown">
                        <li><a href="about-us.html">about us </a></li>
                        <li><a href="cart.html">cart page </a></li>
                        <li><a href="checkout.html">checkout </a></li>
                        <li><a href="compare.html">compare </a></li>
                        <li><a href="wishlist.html">wishlist </a></li>
                        <li><a href="my-account.html">my account </a></li>
                        <li><a href="contact.html">contact us </a></li>
                        <li><a href="login-register.html">login/register </a></li>
                    </ul>
                    </li> -->
                </ul>
              </nav>
              <!-- mobile menu navigation end -->
            </div>
            <!-- mobile menu end -->
          </div>
          <div class="mobile-curr-lang-wrap">
            <div class="single-mobile-curr-lang">
              <a class="mobile-account-active" href="/accountw">My Account <i class="la la-angle-down"></i></a>
              <div class="lang-curr-dropdown account-dropdown-active">
                <ul>
                  <li><a href="/login">Login</a></li>
                  <li><a href="/login">Sign Up</a></li>
                  <li><a href="/account">My Account</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="mobile-social-wrap">
            <a href="https://www.facebook.com/homeglareonline" class="facebook" data-toggle="tooltip" target="_blank" title="Facebook"><i class=" ti-facebook"></i></a>
            <a class="twitter" href="https://www.twitter.com" data-toggle="tooltip" target="_blank" title="Twitter"><i class="ti-twitter-alt"></i></a>
            {{-- <a class="pinterest" href="#"><i class="ti-pinterest"></i></a> --}}
            {{-- <a class="google" href="https://www.plus.google.com/discover" data-toggle="tooltip" target="_blank" title="Google Plus"><i class="ti-google"></i></a> --}}
            <a class="instagram" href="https://www.instagram.com" data-toggle="tooltip" target="_blank" title="Instagram"><i class="ti-instagram"></i></a>
          </div>
        </div>
      </div>
      @yield('content')
      {{-- 
      <div class="subscribe-area pb-100">
        <div class="container">
          <div class="subscribe-bg bg-img pt-45 pb-50 pl-20 pr-20" style="background-image:url(/fyc-new/images/bg/bg-3.jpg);">
            <div class="row">
              <div class="col-lg-6 ml-auto mr-auto">
                <div class="subscribe-content-3 text-center">
                  <h2>Sign up &amp; Get all updates.</h2>
                  <div id="mc_embed_signup" class="subscribe-form-3 mt-20">
                    <form id="mc-embedded-subscribe-form" class="validate subscribe-form-style" novalidate="" target="_blank" name="mc-embedded-subscribe-form" method="post" action="http://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef">
                      <div id="mc_embed_signup_scroll" class="mc-form">
                        <input class="email" type="email" required="" placeholder="Enter Your E-mail" name="EMAIL" value="">
                        <div class="mc-news" aria-hidden="true">
                          <input type="text" value="" tabindex="-1" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef">
                        </div>
                        <div class="clear">
                          <input id="mc-embedded-subscribe" class="button" type="submit" name="subscribe" value="">
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      --}}
      <footer class="footer-area">
        {{-- <div class="feature-area ">
          <div class="container-fluid">
            <div class="feature-border feature-border-about">
              <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                  <div class="feature-wrap mb-30 text-center">
                    <img src="/fyc-new/images/icon-img/feature-icon-1.png" alt="">
                    <h5>Best Product</h5>
                    <span>Best Queality Products</span>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                  <div class="feature-wrap mb-30 text-center">
                    <img src="/fyc-new/images/icon-img/feature-icon-2.png" alt="">
                    <h5>100% fresh</h5>
                    <span>Best Queality Products</span>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                  <div class="feature-wrap mb-30 text-center">
                    <img src="/fyc-new/images/icon-img/feature-icon-3.png" alt="">
                    <h5>Secure Payment</h5>
                    <span>Best Queality Products</span>
                  </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                  <div class="feature-wrap mb-30 text-center">
                    <img src="/fyc-new/images/icon-img/feature-icon-4.png" alt="">
                    <h5>Winner Of 15 Awards</h5>
                    <span>Healthy food and drink 2019</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> --}}
        <div class="footer-top pt-105 pb-75 default-overlay footer-overlay">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-3 col-md-6 col-12 col-sm-6">
                <div class="footer-widget footer-contact-wrap-2 mb-40">
                  <a href="#"><img src="/fyc-new/images/logo/logo.png" alt="logo"></a>
                  <div class="footer-contact-content-2">
                    <p>The administration we offer you is everything to us. Our expert site makes picking your apparatus as simple as could be expected under the circumstances. Putting in your request online is sheltered with our 100% verified site, so you can feel loose and certain your subtleties are completely secured</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-2 col-md-6 col-12 col-sm-6">
                <div class="footer-widget mb-30">
                  <div class="footer-title-2">
                    <h3>Help Center</h3>
                  </div>
                  <div class="footer-list-2">
                    <ul>
                      <li><a href="{{url('/contact')}}">Contact us</a></li>
                      <li><a href="{{url('/')}}">Payments</a></li>
                      <li><a href="{{url('/')}}">Shipping</a></li>
                      <li><a href="{{url('/')}}">Cancellation & Return</a></li>
                      <li><a href="{{url('/')}}">Career</a></li>
                      <li><a href="{{url('/')}}">Sell with Homeglare</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-lg-2 col-md-6 col-12 col-sm-6">
                <div class="footer-widget mb-30">
                  <div class="footer-title-2">
                    <h3>Information</h3>
                  </div>
                  <div class="footer-list-2">
                    <ul>
                      <li><a href="{{url('/about')}}">About Us</a></li>
                      <li><a href="{{url('/terms&conditions')}}">Terms & Conditions</a></li>
                      <li><a href="{{url('/privacy-policy')}}">Privacy Policy</a></li>
                      <li><a href="{{url('/return-policy')}}">Return Policy</a></li>
                      <li><a href="{{url('/faq')}}">FAQ</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-lg-2 col-md-6 col-12 col-sm-6">
                <div class="footer-widget mb-30">
                  <div class="footer-title-2">
                    <h3>Logistic Partner</h3>
                  </div>
                  <div class="footer-list-2 text-center mx-auto" >
                    <ul>
                      <li><a href="#"><img src="/logo/blue_dart.png" style="width: 50px; height: auto"></a></li>
                      <li><a href="#"><img src="/logo/xpressbees.jpg" style="width: 50px; height: auto"></a></li>
                      <li><a href="#"><img src="/logo/delhivery.png" style="width: 50px; height: auto"></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-6 col-12 col-sm-6">
                <div class="footer-widget mb-10">
                  <div class="footer-title-2">
                    <h3>Buy On Homeglare</h3>
                  </div>
                  <div class="footer-contact-wrap">
                    <ul class="web-dic">
                      <li>100% Purchase Product  </li>
                      <li>Easy Return</li>
                      <li>Genuine Product </li>
                      <li>Secure Payment & Safe Ordering</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="row ">
              <div class="col-lg-3 col-md-6 col-12 col-sm-6">
                <div class="footer-contact-content ">
                  <h4><b>Address:-</b></h4>
                  @if($company)
                  <span > <?php echo $company->address ?> </span>
                  <p><i class=" ti-headphone-alt "></i>&nbsp;&nbsp;Call Us: @if(isset($contact))
                    @foreach($contact as $con)
                    <a href="tel://+91{{$con}}">+91 {{$con}}.&nbsp;&nbsp;</a> 
                    <br>
                    @endforeach
                    @endif
                  </p>
                  <p><i class=" ti-email"></i>&nbsp;&nbsp;Email: <a href="mailto://{{$company->support_email}}">{{$company->support_email}}</a> </p>
                  @endif
                </div>
              </div>
              <div class="col-lg-3 col-md-6 col-12 col-sm-6">
                <ul class="app-link-img">
                  <li> <img src="/app-code/playstore.png"> </li>
                  <li> <img src="/app-code/appstore.png"> </li>
                </ul>
              </div>
              <div class="col-lg-3 col-md-6 col-12 col-sm-6">
                <div class=" qr-img text-center">
                <img  src="/app-code/qr.webp">
                </div> 
              </div>
              <div class="col-lg-3 col-md-6 col-12 col-sm-6">
                <div class="footer-widget mb-30">
                  <div class="footer-contact-wrap">
                    <p class="mt-4">Subscribe to our newsletters now and stay up-to-date with new collections.</p>
                    <div id="mc_embed_signup" class="subscribe-form-2 mt-20">
                      <form id="mc-embedded-subscribe-form" class="validate subscribe-form-style" novalidate="" target="_blank" name="mc-embedded-subscribe-form" method="post" action="{{ route('newsletter') }}">
                        <div id="newsletterEmail" class="mc-form">
                          <input class="email" type="email" required="" placeholder="Enter Your E-mail" name="email" value="">
                          <div class="mc-news" aria-hidden="true">
                            <input type="text" value="" tabindex="-1" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef">
                          </div>
                          <div class="clear">
                            <input id="mc-embedded-subscribe" class="button" type="submit" name="subscribe" value="">
                          </div>
                        </div>
                        @if(session('NewslatterMessege'))
                        <div class="alert alert-success" id="NewslatterMessege" style="margin-top: 10px">
                          {{ session('NewslatterMessege') }}
                        </div>
                        @endif
                      </form>
                    </div>
                    <div class="footer-social-hm5">
                      <span>Follow US</span>
                      <ul >
                        <li><a href="https://www.facebook.com/homeglareonline" data-toggle="tooltip" target="_blank" title="Facebook" class="facebook"><i class=" ti-facebook"></i></a></li>
                        <li><a class="twitter" href="https://www.twitter.com" data-toggle="tooltip" target="_blank" title="Twitter"><i class="ti-twitter-alt"></i></a></li>
                        {{-- 
                        <li><a class="pinterest" href="#"><i class="ti-pinterest"></i></a></li>
                        --}}
                        {{-- 
                        <li><a class="google" href="https://www.plus.google.com/discover" data-toggle="tooltip" target="_blank" title="Google Plus"><i class="ti-google"></i></a></li>
                        --}}
                        <li><a class="instagram" href="https://www.instagram.com" data-toggle="tooltip" target="_blank" title="Instagram"><i class="ti-instagram"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-bottom bg-black-2 ptb-10">
          <div class="container">
            <div class="copyright copyright-2 text-center">
              <p>Copyright © <a href="/">HomeGlare</a>. All Right Reserved</p>
            </div>
          </div>
        </div>
      </footer>
               <!-- {{ TawkTo::widgetCode() }} -->

               <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5df0e63d43be710e1d219bfd/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
      <!-- Modal -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-5 col-sm-6 col-xs-12">
                  <div class="tab-content quickview-big-img">
                    <div id="pro-1" class="tab-pane fade show active">
                      <img src="" alt="" id="pro1">
                    </div>
                    <div id="pro-2" class="tab-pane fade">
                      <img src="" alt="" id="pro2">
                    </div>
                    <div id="pro-3" class="tab-pane fade">
                      <img src="" alt="" id="pro3">
                    </div>
                    <div id="pro-4" class="tab-pane fade">
                      <img src="" alt="" id="pro4">
                    </div>
                  </div>
                  <!-- Thumbnail Large Image End -->
                  <!-- Thumbnail Image End -->
                  <div class="quickview-wrap mt-15">
                    <div class="quickview-slide-active owl-carousel nav nav-style-2" role="tablist">
                      <a class="active" data-toggle="tab" href="#pro-1"><img src="" alt="" id="pro5"></a>
                      <a data-toggle="tab" href="#pro-2"><img src="" alt="" id="pro6"></a>
                      <a data-toggle="tab" href="#pro-3"><img src="" alt="" id="pro7"></a>
                      <a data-toggle="tab" href="#pro-4"><img src="" alt="" id="pro8"></a>
                    </div>
                  </div>
                </div>
                <div class="col-md-7 col-sm-6 col-xs-12">
                  <div class="product-details-content quickview-content">
                    <span id="pdCategory">Life Style</span>
                    <h2 id="pdName">LaaVista Depro, FX 829 v1</h2>
                    {{-- 
                    <div class="product-ratting-review">
                      <div class="product-ratting">
                        <i class="la la-star"></i>
                        <i class="la la-star"></i>
                        <i class="la la-star"></i>
                        <i class="la la-star"></i>
                        <i class="la la-star-half-o"></i>
                      </div>
                      <div class="product-review">
                        <span>40+ Reviews</span>
                      </div>
                    </div>
                    --}}
                    <!--  <div class="pro-details-color-wrap">
                      <span>Color:</span>
                      <div class="pro-details-color-content">
                          <ul>
                              <li class="green"></li>
                              <li class="yellow"></li>
                              <li class="red"></li>
                              <li class="blue"></li>
                          </ul>
                      </div>
                      </div>
                      <div class="pro-details-size">
                      <span>Size:</span>
                      <div class="pro-details-size-content">
                          <ul>
                              <li><a href="#">s</a></li>
                              <li><a href="#">m</a></li>
                              <li><a href="#">xl</a></li>
                              <li><a href="#">xxl</a></li>
                          </ul>
                      </div>
                      </div> -->
                    <div class="pro-details-price-wrap">
                      <div class="product-price">
                        <span id="pdPrice">₹210.00</span>
                        <span class="old" id="pdMrp">₹230.00</span>
                      </div>
                      <div class="dec-rang" ><span id="pdDiscount">- 30%</span></div>
                    </div>
                    {{-- 
                    <div class="pro-details-quality">
                      <div class="cart-plus-minus">
                        <input class="cart-plus-minus-box" type="text" name="qtybutton" value="1">
                      </div>
                    </div>
                    --}}
                    <div class="pro-details-compare-wishlist">
                      <div class="pro-details-buy-now btn-hover btn-hover-radious">
                        <a id="pdAddToCart">Add To Cart</a>
                      </div>
                      <br>
                      <div class="ml-3 mb-3 pro-details-buy-now btn-hover btn-hover-radious">
                        <a title="Add To Wishlist" id="pdaddToWishlist"><i class="la la-heart-o"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal end -->
    </div>
    <!-- JS
      ============================================ -->
    <!-- Modernizer JS -->
    <script src="/fyc-new/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- Modernizer JS -->
    <script src="/fyc-new/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="/fyc-new/js/vendor/popper.js"></script>
    <!-- Bootstrap JS -->
    <script src="/fyc-new/js/vendor/bootstrap.min.js"></script>
    <!-- Slick Slider JS -->
    <script src="/fyc-new/js/plugins/countdown.js"></script>
    <script src="/fyc-new/js/plugins/counterup.js"></script>
    <script src="/fyc-new/js/plugins/images-loaded.js"></script>
    <script src="/fyc-new/js/plugins/isotope.js"></script>
    <script src="/fyc-new/js/plugins/instafeed.js"></script>
    <script src="/fyc-new/js/plugins/jquery-ui.js"></script>
    <script src="/fyc-new/js/plugins/jquery-ui-touch-punch.js"></script>
    <script src="/fyc-new/js/plugins/magnific-popup.js"></script>
    <script src="/fyc-new/js/plugins/owl-carousel.js"></script>
    <script src="/fyc-new/js/plugins/scrollup.js"></script>
    <script src="/fyc-new/js/plugins/waypoints.js"></script>
    <script src="/fyc-new/js/plugins/slick.js"></script>
    <script src="/fyc-new/js/plugins/wow.js"></script>
    <script src="/fyc-new/js/plugins/textillate.js"></script>
    <script src="/fyc-new/js/plugins/elevatezoom.js"></script>
    <script src="/fyc-new/js/plugins/sticky-sidebar.js"></script>
    <script src="/fyc-new/js/plugins/smoothscroll.js"></script>
    <!-- Main JS -->
    <script src="/fyc-new/js/main.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js"></script>
    @yield('script')
    <script>
      function getWishlist(){
         var data = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id ;} else {echo '0';}?>;
         if(data > 0){
          window.location.href = '/wishlist/'+data;
      }
      else{
          window.location.href = '/login';
      
      }
      }
      
      function showMiniCart(){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      
      $.ajax({
         url: '/show-minicart',
         type: 'GET',
         data: {_token: CSRF_TOKEN},
         success: function (data) {
          $('#miniCart').html(data);
          $("#total-bill").text(data.bill);
      }
      });
      }
      
      function deleteProduct(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      // var std = $("#show-total").html();
      // count_product--;
      // alert(id);
      
      $.ajax({
      /* the route pointing to the post function */
      url: '/cart/delete-product/'+id,
      type: 'POST',
      /* send the csrf-token and the input to the controller */
      data: {_token: CSRF_TOKEN, id: id},
      success: function (data) {
      $('#listhide'+id).hide();
      $('#cart'+id).hide();
      $("#show-total").html(data.showcount);
      $("#total-bill").html(data.bill+".00");
      
      $('#cartItem').html(data.cartItem);
      $('#cartItem2').html(data.cartItem);            
      }
      });
      }
      
      function checkCart(){
      var count = <?php echo $count;?>;
      // alert(count);
      $('#cartItem').html(count);
      $('#cartItem2').html(count);
      }
      
      
      $('#exampleModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var productId = button.data('myid')
      var modal = $(this)
      // get product details data
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      
      $.ajax({
      url: '/get-product_details-data/'+productId,
      type: 'GET',
      data: {_token: CSRF_TOKEN},
      success: function (response) {
          // var responseObj = JSON.parse(data);
          data = response.data;
          category = response.category;
          
      
          $("#pdName").html(data.name);
          $("#pdDiscount").text('-'+response.discount+'%');
          $("#pdCategory").text(category.category_name);
          $("#pdPrice").html('Price: ₹'+data.sell_price);
          $("#pdMrp").html('MRP: ₹'+data.mrp);
          $("#pdName").attr('href', '/product-detail/'+data.slug);
      
          $("#pro1").attr('src', '/'+data.image1);
          $("#pro2").attr('src', '/'+data.image2);
          $("#pro3").attr('src', '/'+data.image1);
          $("#pro4").attr('src', '/'+data.image3);
      
          $("#pro5").attr('src', '/'+data.image1);
          $("#pro6").attr('src', '/'+data.image2);
          $("#pro7").attr('src', '/'+data.image1);
          $("#pro8").attr('src', '/'+data.image3);
          if(data.product_brand)
              $("#pdBrand").html('Brand: '+ data.product_brand);
          // $("#pdProductCode").html('Product Code: Product '+ data.id);
          // $("#pdReward").html('Reward Points: '+ data);
          if(data.availability == 'yes')
             $("#pdAvailability").html("Availability: In Stock");
         else
             $("#pdAvailability").html("Availability: Out of Stock");
      
         $("#pdAddToCart").attr('onclick', 'addToCart('+data.id+')');
         $("#pdaddToWishlist").attr('onclick', 'addToWishlist('+data.id+')');
      }
      });
      })
      
    </script>
  </body>
</html>