<?php
use App\Models\Category;
use App\Models\Product;
use App\Wishlist;
use App\Models\Company;
$categories = Category::where('status','Active')->where('category_id','0')->get();
$countcart=0 ; $countwishlist=0;
if(!empty(session()->get('cart'))){
    $countcart=count(session()->get('cart'));
}
if(!empty(Auth::user())){
    $countwishlist = Wishlist::where('user_id',Auth::user()->id)->count();
}
$min=0;
$maxPrice = Product::orderBy('sell_price','DESC')->first();
$max = $maxPrice->sell_price ?? '0';

$contact = [];
$company = Company::first();
if(!empty($company))
  $contact = explode(',',$company->contact_no);
$count = 0;
?>


<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from code-theme.com/html/buildpower/{{ url('/') }} by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 May 2020 17:37:01 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="">
    <title>BADA BUILDER</title>
    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i%7CRaleway:600,800" rel="stylesheet">
    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="{{ asset('front/css/font-awesome.min.css') }}">
    <!-- Slider Revolution CSS Files -->
    <link rel="stylesheet" href="{{ asset('front/revolution/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('front/revolution/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('front/revolution/css/navigation.css') }}">
    <!-- ARCHIVES CSS -->
    <link rel="stylesheet" href="{{ asset('front/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/lightcase.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('front/css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('barfiller-master/css/style.css') }}">
    <style type="text/css">
        .dropdown-menu{
            min-width: 225px !important;
        }
        .dropdown-menu1{
        border-top: 1px solid rgb(204, 204, 204);
    border-bottom: 1px solid rgb(204, 204, 204);
    display: block;
    position: absolute;
    left: 102%;
    top: 0%;
    background: #fff;
    border: 1px solid #ccc;
}
    </style>
</head>

<body onload="checkSuccess()">

    @if(!empty($page) && $page=='home')
    <!-- START SECTION HEADINGS -->
    <div class="header head-tr">
        <div class="header-top head-tr">
            <div class="container-fluid">
                <div class="top-info hidden-sm-down">
                    <div class="call-header">
                        <p><a href="tel:@if($contact)+91 {{$contact[0]}}@endif" style="color: #fff"><i class="fa fa-phone" aria-hidden="true"></i> @if($contact)+91 {{$contact[0]}}@endif</a></p>
                    </div>

                    <div class="mail-header">
                        <p><a href="mailto:@if($company && $company->email != ''){{$company->email ?? ''}}@endif" style="color:#fff"><i class="fa fa-envelope" aria-hidden="true"></i> @if($company && $company->email != ''){{$company->email ?? ''}}@endif</a></p>
                    </div>
                </div>
                <div class="top-social hidden-sm-down">
                    <div class="login-wrap">

                        <ul class="d-flex">
                            @if(!empty(Auth::user()))
                            <li class="dropdown hidden-md-down">
                                <a class="active dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class=""></i>{{ucfirst(Auth::user()->name ?? '')}}</a>

                                <div class="dropdown-menu">
                                    <ul class="myAccount">
                               <!--  <li>
                            <a href="{{ url('myaccount') }}">My Dashboard</a>
                                    
                                </li>
                                <li>
                                    
                            <a href="{{ url('myorders') }}">My Oders</a>
                        </li> -->
                        <li class=" last" > <a href="#" title="Log Out" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();" >Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                  </li>
              </ul>
          </div>
      </li>




      @else
      <li><a href="{{ url('login') }}"><i class="fa fa-user"></i> Login</a></li>
      <li><a href="{{ url('register') }}"><i class="fa fa-sign-in"></i> Register</a></li>
      @endif
  </ul>
</div>
<div class="social-icons-header">
    <div class="social-icons">
        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
    </div>
</div>

</div>

<div class="top-social" id="mobileResponse">
    <div class="login-wrap">
        <ul class="d-flex">
                        @if(!empty(Auth::user()))
                        <li class="dropdown hidden-md-down">
                            <a class="active dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class=""></i>{{ucfirst(Auth::user()->name ?? '')}}</a>

                            <div class="dropdown-menu">
                                <ul class="myAccount">

                                    <li class=" last" > <a href="#" title="Log Out" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();" >Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                      @csrf
                                  </form>
                              </li>
                          </ul>
                      </div>
                  </li>

                  @else
                  <li><a href="{{ url('login') }}"><i class="fa fa-user"></i> Login</a></li>
                  <li><a href="{{ url('register') }}"><i class="fa fa-sign-in"></i> Register</a></li>
                  @endif
              </ul>
    </div>


</div>
</div>
</div>
<div class="header-bottom heading head-tr sticky-header" id="heading">
    <div class="container-fluid">
        <a href="{{ url('/') }}" class="logo">
            <!-- <img src="images/logo.svg" alt="realhome"> -->
            <h1 style="font-size: 32px; color: #fff">BADABUILDER</h1>
        </a>
        <button type="button" class="search-button" data-toggle="collapse" data-target="#bloq-search" aria-expanded="false">
            <i class="fa fa-search" aria-hidden="true"></i>
        </button>
        <div class="get-quote hidden-lg-down">

            <a href="@if(!empty(Auth::user())){{ url('wishlist') }}@else{{ url('login') }}@endif" title="Wishlist">
                <i class="fa fa-heart-o"></i>(<span id="wishItem">{{$countwishlist}}</span>)
            </a>
        </div>
        <div class="get-quote2 hidden-lg-down">
         <a  href="{{ url('cart') }}" title="Cart">
            <i class="fa fa-cart-plus"></i>(<span id="cartItem">{{$countcart}}</span>)
        </a>
    </div>
    <button type="button" class="button-menu hidden-lg-up" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </button>

    <form action="{{ url('searchedProducts') }}" id="bloq-search" class="collapse" method="get">
        <div class="bloq-search">
            <input type="text" placeholder="search..." name="keyword">
            <input type="submit" value="Search">
        </div>
    </form>
<nav id="main-menu" class="collapse">
        <ul>
            <li><a href="{{ url('/') }}">Home</a></li>
            <!-- STAR COLLAPSE MOBILE MENU -->
            <li class="hidden-lg-up">
                <div class="po">
                    <a data-toggle="collapse" href="#home" aria-expanded="false">Sub Category</a>
                </div>
                <div class="collapse" id="home">
                    @if(count($categories)>0)
                    @php $countc = 0 @endphp

                    <div class="card card-block">
                         @foreach($categories as $category)
                    <a class="dropdown-item" href="{{ url('products/'.$category->slug ?? '') }}">{{ucfirst($category->category_name ?? '')}}</a>




                    <?php
                    $countc=1;
                    $subcategories = Category::where('category_id',$category->id)->get();
                    $counts=0;
                    ?>


                    @if(count($subcategories)>0)
                    <div class="dropdown-menu1">

                        @foreach($subcategories as $subcat)
                        <a class="dropdown-item" href="{{ url('products/'.$subcat->slug ?? '') }}">{{ucfirst($subcat->category_name ?? '')}}</a>
                        <?php
                        $counts=1;
                        $superSubCategories = Category::where('category_id',$subcat->id)->get();
                        ?>
                        @if(count($superSubCategories)>0)
                        <div class="dropdown-menu2">

                            @foreach($superSubCategories as $superSub)
                            <a class="dropdown-item" href="{{ url('products/'.$superSub->slug ?? '') }}">{{ucfirst($superSub->category_name ?? '')}}</a>
                            @endforeach

                        </div>
                        @endif

                        @endforeach

                    </div>
                    @endif


                    @endforeach
                    </div>
                    @endif
                </div>
            </li>
            <!-- END COLLAPSE MOBILE MENU -->
            <li class="dropdown hidden-md-down">
                <a class="active dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" onmouseover="hideSubNav()">Sub Category</a>
                @if(count($categories)>0)
                @php $countc = 0 @endphp
                <div class="dropdown-menu">
                    @foreach($categories as $category)
                    <a class="dropdown-item" href="{{ url('products/'.$category->slug ?? '') }}" onmouseover="showSubCat({{$category->id ?? ''}})">{{ucfirst($category->category_name ?? '')}} <i class="fa fa-angle-right pull-right"></i></a>




                    <?php
                    $countc=1;
                    $subcategories = Category::where('category_id',$category->id)->get();
                    $counts=0;
                    ?>


                    @if(count($subcategories)>0)
                    <div class="dropdown-menu1" style="display: none; border-top:1px solid #ccc; border-bottom: 1px solid #ccc" id="subCat{{$category->id ?? ''}}">

                        @foreach($subcategories as $subcat)
                        <a class="dropdown-item" href="{{ url('products/'.$subcat->slug ?? '') }}">&nbsp;&nbsp; &nbsp;{{ucfirst($subcat->category_name ?? '')}}</a>
                        <?php
                        $counts=1;
                        $superSubCategories = Category::where('category_id',$subcat->id)->get();
                        ?>
                        @if(count($superSubCategories)>0)
                        <div class="dropdown-menu2">

                            @foreach($superSubCategories as $superSub)
                            <a class="dropdown-item" href="{{ url('products/'.$superSub->slug ?? '') }}">{{ucfirst($superSub->category_name ?? '')}}</a>
                            @endforeach

                        </div>
                        @endif

                        @endforeach

                    </div>
                    @endif


                    @endforeach
                </div>

                @endif
            </li>


            <li><a href="#">Design</a></li>
            <li><a href="#">CBT</a></li>
            <li><a href="#">Construction</a></li>
            <li><a href="{{ url('contact') }}">Contact</a></li>
        </ul>
    </nav>
</div>
</div>
</div>




@else

<div class="header">
    <div class="header-top">
        <div class="container-fluid">
            <div class="top-info hidden-sm-down">
                <div class="call-header">
                    <p><a href="tel:@if($contact)+91 {{$contact[0]}}@endif" style="color: #fff"><i class="fa fa-phone" aria-hidden="true"></i> @if($contact)+91 {{$contact[0]}}@endif</a></p>
                </div>

                <div class="mail-header">
                    <p><a href="mailto:@if($company && $company->email != ''){{$company->email ?? ''}}@endif" style="color:#fff"><i class="fa fa-envelope" aria-hidden="true"></i> @if($company && $company->email != ''){{$company->email ?? ''}}@endif</a></p>
                </div>
            </div>
            <div class="top-social hidden-sm-down">
                <div class="login-wrap">

                    <ul class="d-flex">
                        @if(!empty(Auth::user()))
                        <li class="dropdown hidden-md-down">
                            <a class="active dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class=""></i>{{ucfirst(Auth::user()->name ?? '')}}</a>

                            <div class="dropdown-menu">
                                <ul class="myAccount">

                                    <li class=" last" > <a href="#" title="Log Out" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();" >Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                      @csrf
                                  </form>
                              </li>
                          </ul>
                      </div>
                  </li>

                  @else
                  <li><a href="{{ url('login') }}"><i class="fa fa-user"></i> Login</a></li>
                  <li><a href="{{ url('register') }}"><i class="fa fa-sign-in"></i> Register</a></li>
                  @endif
              </ul>
          </div>
          <div class="social-icons-header">
            <div class="social-icons">
                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
            </div>
        </div>

    </div>

    <div class="top-social" id="mobileResponse">
        <div class="login-wrap">
            <ul class="d-flex">
                        @if(!empty(Auth::user()))
                        <li class="dropdown hidden-md-down">
                            <a class="active dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><i class=""></i>{{ucfirst(Auth::user()->name ?? '')}}</a>

                            <div class="dropdown-menu">
                                <ul class="myAccount">

                                    <li class=" last" > <a href="#" title="Log Out" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();" >Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                      @csrf
                                  </form>
                              </li>
                          </ul>
                      </div>
                  </li>

                  @else
                  <li><a href="{{ url('login') }}"><i class="fa fa-user"></i> Login</a></li>
                  <li><a href="{{ url('register') }}"><i class="fa fa-sign-in"></i> Register</a></li>
                  @endif
              </ul>
        </div>


    </div>
</div>
</div>
<div class="header-bottom heading sticky-header" id="heading">
    <div class="container-fluid">
        <a href="{{ url('/') }}" class="logo">
            <!-- <img src="images/logo.svg" alt="realhome"> -->
            <h1 style="font-size: 32px; color: #fff">BADABUILDER</h1>
        </a>
        <button type="button" class="search-button" data-toggle="collapse" data-target="#bloq-search" aria-expanded="false">
            <i class="fa fa-search" aria-hidden="true"></i>
        </button>
        <div class="get-quote hidden-lg-down">

            <a href="@if(!empty(Auth::user())){{ url('wishlist') }}@else{{ url('login') }}@endif" title="Wishlist">
                <i class="fa fa-heart-o"></i>(<span id="wishItem">{{$countwishlist}}</span>)
            </a>
        </div>
        <div class="get-quote2 hidden-lg-down">
         <a  href="{{ url('cart') }}" title="Cart">
            <i class="fa fa-cart-plus"></i>(<span id="cartItem">{{$countcart}}</span>)
        </a>
    </div>
    <button type="button" class="button-menu hidden-lg-up" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </button>

    <form action="{{ url('searchedProducts') }}" id="bloq-search" class="collapse" method="get">
        <div class="bloq-search">
            <input type="text" placeholder="search..." name="keyword">
            <input type="submit" value="Search">
        </div>
    </form>

    <nav id="main-menu" class="collapse">
        <ul>
            <li><a href="{{ url('/') }}">Home</a></li>
            <!-- STAR COLLAPSE MOBILE MENU -->
            <li class="hidden-lg-up">
                <div class="po">
                    <a data-toggle="collapse" href="#home" aria-expanded="false">Sub Category</a>
                </div>
                <div class="collapse" id="home">
                    @if(count($categories)>0)
                    @php $countc = 0 @endphp

                    <div class="card card-block">
                         @foreach($categories as $category)
                    <a class="dropdown-item" href="{{ url('products/'.$category->slug ?? '') }}">{{ucfirst($category->category_name ?? '')}}</a>




                    <?php
                    $countc=1;
                    $subcategories = Category::where('category_id',$category->id)->get();
                    $counts=0;
                    ?>


                    @if(count($subcategories)>0)
                    <div class="dropdown-menu1">

                        @foreach($subcategories as $subcat)
                        <a class="dropdown-item" href="{{ url('products/'.$subcat->slug ?? '') }}">{{ucfirst($subcat->category_name ?? '')}}</a>
                        <?php
                        $counts=1;
                        $superSubCategories = Category::where('category_id',$subcat->id)->get();
                        ?>
                        @if(count($superSubCategories)>0)
                        <div class="dropdown-menu2">

                            @foreach($superSubCategories as $superSub)
                            <a class="dropdown-item" href="{{ url('products/'.$superSub->slug ?? '') }}">{{ucfirst($superSub->category_name ?? '')}}</a>
                            @endforeach

                        </div>
                        @endif

                        @endforeach

                    </div>
                    @endif


                    @endforeach
                    </div>
                    @endif
                </div>
            </li>
            <!-- END COLLAPSE MOBILE MENU -->
            <li class="dropdown hidden-md-down">
                <a class="active dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" onmouseover="hideSubNav()">Sub Category</a>
                @if(count($categories)>0)
                @php $countc = 0 @endphp
                <div class="dropdown-menu">
                    @foreach($categories as $category)
                    <a class="dropdown-item" href="{{ url('products/'.$category->slug ?? '') }}" onmouseover="showSubCat({{$category->id ?? ''}})">{{ucfirst($category->category_name ?? '')}} <i class="fa fa-angle-right pull-right"></i></a>




                    <?php
                    $countc=1;
                    $subcategories = Category::where('category_id',$category->id)->get();
                    $counts=0;
                    ?>


                    @if(count($subcategories)>0)
                    <div class="dropdown-menu1" style="display: none; border-top:1px solid #ccc; border-bottom: 1px solid #ccc" id="subCat{{$category->id ?? ''}}">

                        @foreach($subcategories as $subcat)
                        <a class="dropdown-item" href="{{ url('products/'.$subcat->slug ?? '') }}">&nbsp;&nbsp; &nbsp;{{ucfirst($subcat->category_name ?? '')}}</a>
                        <?php
                        $counts=1;
                        $superSubCategories = Category::where('category_id',$subcat->id)->get();
                        ?>
                        @if(count($superSubCategories)>0)
                        <div class="dropdown-menu2">

                            @foreach($superSubCategories as $superSub)
                            <a class="dropdown-item" href="{{ url('products/'.$superSub->slug ?? '') }}">{{ucfirst($superSub->category_name ?? '')}}</a>
                            @endforeach

                        </div>
                        @endif

                        @endforeach

                    </div>
                    @endif


                    @endforeach
                </div>

                @endif
            </li>


            <li><a href="#">Design</a></li>
            <li><a href="#">CBT</a></li>
            <li><a href="#">Construction</a></li>
            <li><a href="{{ url('contact') }}">Contact</a></li>
        </ul>
    </nav>
</div>
</div>
</div>


@endif
















@yield('content')





<!-- START FOOTER -->
<footer class="first-footer">
    <div class="top-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="netabout">
                        <a href="{{ url('/') }}" class="logo">
                            <!-- <img src="images/logo.svg" alt="netcom"> -->
                            <h1 style="font-size: 32px; color: #fff">BADABUILDER</h1>
                        </a>
                        <p>@if($company && $company->sort_discription)
                          <?php echo $company->sort_discription; ?>
                          @endif
                      </p>
                  </div>
                  <div class="contactus">
                    <ul>
                        <li>
                            <div class="info">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <p class="in-p">@if($company && $company->address)
                                    <?php echo $company->address; ?><br>
                                @endif</p>
                            </div>
                        </li>
                        <li>
                            <div class="info">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <p class="in-p">

                                    @if(!empty($contact))
                                    @foreach($contact as $con)
                                    <a href="{{$con}}" style="color: #fff">+91-{{$con}}</a><br>
                                    @endforeach
                                    @endif

                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="info">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <p class="in-p ti">
                                    @if($company && $company->email != '')<a href="mailto:{{$company->email ?? ''}}" style="color: #fff">{{$company->email ?? ''}}</a>@endif
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="widget quick-link clearfix">
                    <h3 class="widget-title">Quick Links</h3>
                    <div class="quick-links">
                        <ul class="one-half">
                            <!-- <ul class="one-half mr-5"> -->
                                <li><a href="{{ url('/') }}">Home</a></li>
                                <li><a href="{{ url('about') }}">About Us</a></li>
                                <li><a href="{{ url('contact') }}">Contact Us</a></li>
                                <li><a href="#">Our Services</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Privacy Policy</a></li>

                            </ul>
                                <!-- <ul class="one-half">
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Pricing</a></li>
                                    <li><a href="contact-us.html">Contact</a></li>
                                    <li><a href="#">Support</a></li>
                                    <li><a href="faq.html">Our Faq</a></li>
                                    <li class="no-pb"><a href="#">Case Detail</a></li>
                                </ul> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="widget">
                            @if(count($categories)>0)
                            <h3>Top Categories</h3>
                            <ul class="photo">

                                @foreach($categories as $category)
                                <li class="hover-effect">
                                    <figure>
                                        <a href="{{ url('products/'.$category->slug ?? '') }}"><img src="{{ asset($category->category_img ?? '') }}" alt=""></a>
                                    </figure>
                                </li>
                                @endforeach
                            </ul>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="newsletters">
                            <h3>Newsletters</h3>
                            <p>Sign Up for Our Newsletter to get Latest Updates and Offers. Subscribe to receive news in your inbox.</p>
                        </div>
                        <form class="bloq-email mailchimp form-inline" method="post" action="{{ route('newsletter') }}">
                            <label for="subscribeEmail" class="error"></label>
                            <div class="email"> 
                                <input type="email" id="subscribeEmail" name="EMAIL" placeholder="Enter Your Email" required="required">
                                <input type="submit" value="Subscribe">
                                <p class="subscription-success"></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="second-footer">
            <div class="container">
                <p>Copyright ©2020 All rights reserved. </p>
                <ul class="netsocials">
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </footer>

    <a data-scroll href="#heading" class="go-up"><i class="fa fa-angle-double-up" aria-hidden="true"></i></a>
    <!-- END FOOTER -->

    <!-- START PRELOADER -->
    <div id="preloader">
        <div id="status">
            <div class="status-mes"></div>
        </div>
    </div>
    <!-- END PRELOADER -->

    <!-- ARCHIVES JS -->
    <script src="{{ asset('front/js/jquery.min.js') }}"></script>
    <script src="{{ asset('front/js/tether.min.js') }}"></script>
    <script src="{{ asset('front/js/moment.js') }}"></script>
    <script src="{{ asset('front/js/transition.min.js') }}"></script>
    <script src="{{ asset('front/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('front/js/fitvids.js') }}"></script>
    <script src="{{ asset('front/js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('front/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('front/js/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('front/js/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('front/js/smooth-scroll.min.js') }}"></script>
    <script src="{{ asset('front/js/jquery.barfiller.js') }}"></script>
    <script src="{{ asset('front/js/barfiller.js') }}"></script>
    <script src="{{ asset('front/js/lightcase.js') }}"></script>
    <script src="{{ asset('front/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('front/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('front/js/ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('front/js/newsletter.js') }}"></script>
    <script src="{{ asset('front/js/jquery.form.js') }}"></script>
    <script src="{{ asset('front/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('front/js/forms-2.js') }}"></script>

    <script>
        var tpj = jQuery;
        var revapi34;
        tpj(document).ready(function() {
            if (tpj("#rev_slider_home").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_home");
            } else {
                revapi34 = tpj("#rev_slider_home").show().revolution({
                    sliderType: "standard",
                    jsFileLocation: "js/revolution-slider/js/",
                    sliderLayout: "fullscreen",
                    dottedOverlay: "none",
                    delay: 9000,
                    navigation: {
                        keyboardNavigation: "on",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        onHoverStop: "on",
                        touch: {
                            touchenabled: "on",
                            swipe_threshold: 75,
                            swipe_min_touches: 1,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        },
                        arrows: {
                            style: "zeus",
                            enable: true,
                            hide_onmobile: true,
                            hide_under: 600,
                            hide_onleave: true,
                            hide_delay: 200,
                            hide_delay_mobile: 1200,
                            tmp: '<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div> </div>',
                            left: {
                                h_align: "left",
                                v_align: "center",
                                h_offset: 30,
                                v_offset: 0
                            },
                            right: {
                                h_align: "right",
                                v_align: "center",
                                h_offset: 30,
                                v_offset: 0
                            }
                        },
                        bullets: {
                            enable: true,
                            hide_onmobile: true,
                            hide_under: 600,
                            style: "metis",
                            hide_onleave: true,
                            hide_delay: 200,
                            hide_delay_mobile: 1200,
                            direction: "horizontal",
                            h_align: "center",
                            v_align: "bottom",
                            h_offset: 0,
                            v_offset: 30,
                            space: 5,
                            tmp: '<span class="tp-bullet-img-wrap"><span class="tp-bullet-image"></span></span>'
                        }
                    },
                    viewPort: {
                        enable: true,
                        outof: "pause",
                        visible_area: "80%"
                    },
                    responsiveLevels: [1240, 1024, 778, 480],
                    gridwidth: [1240, 1024, 778, 480],
                    gridheight: [600, 550, 500, 450],
                    lazyType: "none",
                    parallax: {
                        type: "scroll",
                        origo: "enterpoint",
                        speed: 400,
                        levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50],
                    },
                    shadow: 0,
                    spinner: "off",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "off",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
            }
        }); /*ready*/

    </script>

    <!-- Slider Revolution scripts -->
    <script src="{{ asset('front/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('front/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('front/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
    <script src="{{ asset('front/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
    <script src="{{ asset('front/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script src="{{ asset('front/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script src="{{ asset('front/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
    <script src="{{ asset('front/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script src="{{ asset('front/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
    <script src="{{ asset('front/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script src="{{ asset('front/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
    <script src="{{ asset('barfiller-master/js/jquery.barfiller.js') }}"></script>

    <!-- MAIN JS -->
    <script src="{{ asset('front/js/script.js') }}"></script>

    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js"></script>

    @yield('script')

    @include('badabuilder.include.scripts')

</body>


<!-- Mirrored from code-theme.com/html/buildpower/{{ url('/') }} by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 May 2020 17:37:46 GMT -->
</html>
