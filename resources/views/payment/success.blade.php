@extends('layouts.fyc')

@section('content')

<div class="container">
	<div style="text-align: center;">

		<h1 style="text-align: center; padding: 5% 0 0; font-size: 42px">
			Sucessfully Donated
		</h1>
		<p>With</p>
		<h4>Transaction ID : {{$txnid}}</h4>
		<p>of</p>
		<h4>Amount : {{$amount}}</h4>
		<p>Mobile : {{$phone}}</p>
	</div>
</div>

@endsection