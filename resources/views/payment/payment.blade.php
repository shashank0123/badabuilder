<?php

use App\User;

$user = Auth::user();

$key = 'hopUqBCB';
$salt = '8iveMuBZix';
$txnid = 'TXN'.rand(10000,99999999);
$email = $user->email;
$price = $price;
$name = $user->name;
$phone = $phone;
$product_info = 'Fyc Professional Products';
$udf5 = "BOLT_KIT_PHP7";
$hash=hash('sha512', $key.'|'.$txnid.'|'.$price.'|'.$product_info.'|'.$name.'|'.$email.'|||||'.$udf5.'||||||'.$salt);

function getCallbackUrl()
{
	$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	return $protocol . $_SERVER['HTTP_HOST']  . '/api/payment-success';
}

?>
<input type="hidden" name="user_id" value="{{$user->id}}" id="user_id">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PayUmoney</title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<!-- this meta viewport is required for BOLT //-->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" >
<!-- BOLT Sandbox/test //-->
<script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-
color="e34524" bolt-logo="http://boltiswatching.com/wp-content/uploads/2015/09/Bolt-Logo-e14421724859591.png"></script>
<!-- BOLT Production/Live //-->
<!--// script id="bolt" src="https://checkout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="http://boltiswatching.com/wp-content/uploads/2015/09/Bolt-Logo-e14421724859591.png"></script //-->

</head>
<style type="text/css">
	.main {
		margin-left:30px;
		font-family:Verdana, Geneva, sans-serif, serif;
	}
	.text {
		float:left;
		width:180px;
	}
	.dv {
		margin-bottom:5px;
	}
</style>
<body style="text-align: center;" onload="launchBOLT()">
<div class="main">
	<div>
    	<img src="/asset/images/payUmoney/payumoney.jpg"  style="height: 60px; width: auto;"/>
    </div>
    <div>
    	<h3>FYC Professional Beauty Products</h3>
    </div>
	<form action="" id="payment_form">
		@csrf
    <input type="hidden" id="udf5" name="udf5" value="BOLT_KIT_PHP7" />
    <input type="hidden" id="surl" name="surl" value="<?php echo getCallbackUrl(); ?>" />
    <div class="dv">
    <span><input type="hidden" id="key" name="key" placeholder="Merchant Key" value="{{$key}}" /></span>
    </div>
    
    <div class="dv">
    <span><input type="hidden" id="salt" name="salt" placeholder="Merchant Salt" value="{{$salt}}" /></span>
    </div>
    
    <div class="dv">
    <span><input type="hidden" id="txnid" name="txnid" placeholder="Transaction ID" value="{{$txnid}}" /></span>
    </div>
    
    <div class="dv">
    <span><input type="hidden" id="amount" name="amount" placeholder="Amount" value="{{$price}}" /></span>    
    </div>
    
    <div class="dv">
    <span><input type="hidden" id="pinfo" name="pinfo" placeholder="Product Info" value="{{$product_info}}" /></span>
    </div>
    
    <div class="dv">
    <span><input type="hidden" id="fname" name="fname" placeholder="First Name" value="{{$name}}" /></span>
    </div>
    
    <div class="dv">
    <span><input type="hidden" id="email" name="email" placeholder="Email ID" value="{{$email}}" /></span>
    </div>
    
    <div class="dv">
    <span><input type="hidden" id="mobile" name="mobile" placeholder="Mobile/Cell Number" value="{{$phone}}" /></span>
    </div>
    
    <div class="dv">
    <span><input type="hidden" id="hash" name="hash" placeholder="Hash" value="{{$hash}}" /></span>
    </div>
    
    
    <div><input type="submit" value="Pay" onclick="launchBOLT(); return false;" /></div>
	</form>
</div>
<script type="text/javascript"><!--

//-->
</script>
<script type="text/javascript"><!--
function launchBOLT()
{
	bolt.launch({
	key: $('#key').val(),
	txnid: $('#txnid').val(), 
	hash: $('#hash').val(),
	amount: $('#amount').val(),
	firstname: $('#fname').val(),
	email: $('#email').val(),
	phone: $('#mobile').val(),
	productinfo: $('#pinfo').val(),
	udf5: $('#udf5').val(),
	surl : $('#surl').val(),
	furl: $('#surl').val(),
	mode: 'dropout'	
},{ responseHandler: function(BOLT){
	console.log( BOLT.response.txnStatus );		
	if(BOLT.response.txnStatus != 'CANCEL')
	{
		//Salt is passd here for demo purpose only. For practical use keep salt at server side only.
		var fr = '<form action=\"'+$('#surl').val()+'\" method=\"post\">' +
		'<input type=\"hidden\" name=\"key\" value=\"'+BOLT.response.key+'\" />' +
		'<input type=\"hidden\" name=\"salt\" value=\"'+$('#salt').val()+'\" />' +
		'<input type=\"hidden\" name=\"txnid\" value=\"'+BOLT.response.txnid+'\" />' +
		'<input type=\"hidden\" name=\"amount\" value=\"'+BOLT.response.amount+'\" />' +
		'<input type=\"hidden\" name=\"productinfo\" value=\"'+BOLT.response.productinfo+'\" />' +
		'<input type=\"hidden\" name=\"firstname\" value=\"'+BOLT.response.firstname+'\" />' +
		'<input type=\"hidden\" name=\"email\" value=\"'+BOLT.response.email+'\" />' +
		'<input type=\"hidden\" name=\"udf5\" value=\"'+BOLT.response.udf5+'\" />' +
		'<input type=\"hidden\" name=\"mihpayid\" value=\"'+BOLT.response.mihpayid+'\" />' +
		'<input type=\"hidden\" name=\"status\" value=\"'+BOLT.response.status+'\" />' +
		'<input type=\"hidden\" name=\"hash\" value=\"'+BOLT.response.hash+'\" />' +
		'</form>';
		var form = jQuery(fr);
		jQuery('body').append(form);								
		form.submit();
	}
	// else { 
	// 	var id = $('#donar_id').val();
	// 	window.location.href = "/"; }
},
	catchException: function(BOLT){
 		alert( BOLT.message );
	}
});
}
//--
</script>	

</body>
</html>
	
