
@extends('layouts.badabuilder')

@section('content')



    <section class="headings">
        <div class="text-heading text-center">
            <div class="container">
                <h1>Login</h1>
            </div>
        </div>
    </section>

    <div class="road">
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="{{ url('/') }}">Home</a><span>»</span><span>Login</span>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION HEADINGS -->

    <!-- START SECTION LOGIN -->
    <div id="login">
        <div class="login">
            <form method="POST" action="{{ route('login') }}">
                    @csrf
                <div class="access_social">
                    <!-- <a href="#0" class="social_bt facebook">Login with Facebook</a> -->
                    <!-- <a href="#0" class="social_bt google">Login with Google</a> -->
                    <!-- <a href="#0" class="social_bt linkedin">Login with Linkedin</a> -->
                </div>
                <!-- <div class="divider"><span>Or</span></div> -->
                <div class="form-group">
                    <label>Email</label>


                    <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}" required>
                    <i class="icon_mail_alt" ></i>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password" id="password" value="" required="request">
                    <i class="icon_lock_alt"></i>
                </div>
                <div class="fl-wrap filter-tags clearfix add_bottom_30">
                    <div class="checkboxes float-left">
                        <div class="filter-tags-wrap">
                            <input id="check-b" type="checkbox" name="check">
                            <label for="check-b">Remember me</label>
                        </div>
                    </div>
                    <div class="float-right mt-1"><a id="forgot" href="javascript:void(0);">Forgot Password?</a></div>
                </div>
                <button type="submit" class="btn_1 rounded full-width">Login</button>
                <div class="text-center add_top_10">New to BADABUILDER? <strong><a href="{{ url('register') }}">Sign up!</a></strong></div>
            </form>
        </div>
    </div>
    <!-- END SECTION LOGIN -->
@endsection


  @section('script')
  <script type="text/javascript">

    /* Start input check regex */
    function checkInputValue(field) {
      var value = field.value;
      var fieldId = field.id;
      var pattern = field.pattern;
      var pattern = new RegExp(field.pattern);
      var re = pattern.test(value);
    // var re = value.match(pattern);
    if(!re) {
      $('#' + fieldId + '_error').removeClass('d-none');
      if (document.getElementById(fieldId + '_error')) {
        $('#' + fieldId + '_error').removeClass('d-none');
        $('.btn-submit').prop('disabled', true);
      } else {
        $('#' + fieldId).after('<small id="' +fieldId+'_error" class="text-danger">This value is not valid</small>');
        $('.btn-submit').prop('disabled', true);
      }
    } else {
      $('#' + fieldId + '_error').addClass('d-none');
      $('.btn-submit').prop('disabled', false)
    }
  };
  /* End input check regex*/
</script>
@endsection
