@extends('layouts.badabuilder')

@section('content')


<section class="headings">
    <div class="text-heading text-center">
        <div class="container">
            <h1>Register</h1>
        </div>
    </div>
</section>

<div class="road">
    <div class="container">
        <div class="row">
            <div class="col">
                <a href="{{ url('/') }}">Home</a><span>»</span><span>Register</span>
            </div>
        </div>
    </div>
</div>
<!-- END SECTION HEADINGS -->

<div id="login">
    <div class="login">
        <form method="POST" action="{{ route('register') }}">
            @csrf



            <div class="form-group">
                <label for="name" >{{ __('Name') }}</label>


                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"
                pattern="^[a-zA-Z\s]*$" onkeyup="checkInputValue(this)" required autocomplete="name" autofocus>
                <i class="ti-user"></i>

                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror

            </div>

            <div class="form-group">
                <label for="phone">{{ __('Phone') }}</label>


                <input id="phone" type="text" maxlength="10" class="form-control @error('phone') is-invalid @enderror" name="phone"
                value="{{ old('phone') }}" pattern="^\d+$" onkeyup="checkInputValue(this)" required autocomplete="phone" autofocus>
                <i class="ti-user"></i>
                @error('phone')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror

            </div>

            <div class="form-group">
                <label for="email">{{ __('E-Mail Address') }}</label>


                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"  name="email"
                value="{{ old('email') }}" pattern="^.+@[^\.].*\.[a-z]{2,}$" onkeyup="checkInputValue(this)" required autocomplete="email" onchange="checkEmail()">
                <i class="ti-user"></i>
                <span class="invalid-feedback" role="alert">
                    <strong id="checkEmail" style="color: red; font-size: 12px;"></strong>
                </span>
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror

            </div>

            <div class="form-group">
                <label for="address">{{ __('Address') }}</label>


                <textarea id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address"
                value="{{ old('address') }}" required autocomplete="address">
            {{ old('address') }}</textarea>
            <i class="ti-user"></i>

            @error('address') 
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror

        </div>

        <div class="form-group">
            <label for="password">{{ __('Password') }}</label>


            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
            <i class="ti-user"></i>

            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror

        </div>

        <div class="form-group">
            <label for="password-confirm">{{ __('Confirm Password') }}</label>


            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
        </div>
    </div>

    <div id="pass-info" class="clearfix"></div>
    <button type="submit" class="btn_1 rounded add_top_30" id="register">Register Now!</button>
    <div class="text-center add_top_10">Already have an acccount? <strong><a href="{{ url('login') }}">Sign In</a></strong></div>
</form>
</div>
</div>





























<script type="text/javascript">

    /* Start input check regex */
    function checkInputValue(field) {
        var value = field.value;
        var fieldId = field.id;
        var pattern = field.pattern;
        var pattern = new RegExp(field.pattern);
        var re = pattern.test(value);
    // var re = value.match(pattern);
    if(!re) {
        $('#' + fieldId + '_error').removeClass('d-none');
        if (document.getElementById(fieldId + '_error')) {
          $('#' + fieldId + '_error').removeClass('d-none');
          $('.btn-submit').prop('disabled', true);
      } else {
        $('#' + fieldId).after('<small id="' +fieldId+'_error" class="text-danger">This value is not valid</small>');
        $('.btn-submit').prop('disabled', true);
    }
} else {
    $('#' + fieldId + '_error').addClass('d-none');
    $('.btn-submit').prop('disabled', false)
}
};
/* End input check regex*/


function checkEmail(){
    var CSRF_TOKEN = jQuery('meta[name="csrf-token"]').attr('content');
    var email = $('#email').val();

    jQuery.ajax({
     url: '/checkEmail',
     type: 'POST',
     data: {_token: CSRF_TOKEN, email: email},
     success: function (data) {
               if(data==1){
                jQuery('#checkEmail').show();
                jQuery('#checkEmail').html('Email Already Exists. Try another');
                $('#register').attr('disabled',true)
            }
            else{
                jQuery('#checkEmail').hide();
                $('#register').prop('disabled',false)
            }
        },
        failure: function (data) {
           }
       });
}
</script>

@endsection
