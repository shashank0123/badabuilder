

@extends('layouts.front')

@section('content')

<div class="container" style="margin-top: 25px">
    <div class="cart display-single-price">
        <div class="page-title title-buttons">
            <h1><span>Shopping</span> 
            Cart</h1>
            <ul class="checkout-types top">
                <li>    <a hre="#" title="Proceed to Checkout" class="button btn-proceed-checkout btn-checkout no-checkout"><span><span>Proceed to Checkout</span></span></a>
                </li>
            </ul>
        </div>

        <!-- <ul class="messages">
            <li class="notice-msg">
                <ul>
                    <li>
                        <span> Minimum cart amount should be more than ₹ 2000</span>
                    </li>
                </ul>
            </li>
        </ul> -->  

        
        <table id="shopping-cart-table" class="cart-table data-table" style="margin-top: 25px">
            <col width="1" />
            <col width="1" />
            <col width="1" />
            <col width="1" />
            <col width="1" />
            <col width="1" />

            <thead>
                <tr>
                    <th rowspan="1"><span class="nobr">Product</span></th>
                    <th rowspan="1" style="width: 200px">&nbsp;</th>
                    <th class="a-center cart-price-head" colspan="1"><span class="nobr">Price</span></th>
                    <th rowspan="1" class="a-center"  style="width: 225px">Qty</th>
                    <!-- <th class="a-center"  rowspan="1">In Stock</td> -->
                        <th class="a-center cart-total-head" colspan="1">Subtotal</th>
                        <th class="a-center" rowspan="1">Action</th>

                    </tr>

                    </tr>
                </thead>
                

                <tbody>
     @if(count($products)>0)
                    @foreach($products as $product)
                    <tr id="div-list{{$product->id}}">
                        <td class="product-cart-image">
                            <a href="{{ url('product-detail/'.$product->slug) }}">
                                <img src="{{ asset($product->image1) }}" style="height: 75px; width: auto "/>
                            </a>
                        </td>

                        <td class="product-cart-info">


                            <h2 class="product-name">
                                <a href="{{ url('product-detail/'.$product->slug) }}">
                                    {{ ucfirst($product->name ?? '') }}</a>
                                </h2>

                   <!--  <div class="product-cart-sku">
                        <span class="label">SKU:</span> 
                    FNCFR001GR        </div> -->
<!-- 
                    <dl class="item-options">
                        <dt>CGST@</dt>
                        <dd>
                        9%                            </dd>
                        <dt>SGST@</dt>
                        <dd>
                        9%                            </dd>
                    </dl> -->



                </td>


                <td class="product-cart-price">
                    <span class="cart-price  cart-row-item-price-id-7372">
                        <span class="price">
                            ₹{{$product->sell_price ?? '0'}}
                        </span>                       </span>
                    </td>
                    <!-- inclusive price starts here -->
                    <td class="product-cart-actions" data-rwd-label="Qty">
                        <input type="text" pattern="\d*" name="cart" 
                        value="{{$product->quantity ?? '1'}}" size="4" 
                        title="Qty" class="input-text qty"  />

                        <button type="submit" 
                        name="update_cart_action" value="update_qty" title="Update" class="button btn-update">
                        <span><span>Update</span></span>
                    </button>
                    <ul class="cart-links">
                           <!-- <li>
                    <a href="" 
                    title="">
                    </a>
                </li> -->

            </ul>

        </td>

        <!--Sub total starts here -->
        <td class="product-cart-total cart-row-item-id-7372" data-rwd-label="Subtotal">

          <span class="price">₹{{$product->subtotal ?? '0'}}</span>                            
      </span>
  </td>
  <td class="a-center product-cart-remove">
    <a onclick="deleteCartItem({{$product->id}})"  
        title="Remove Item" class="btn-remove btn-remove2">
    Remove Item</a>
</td>
</tr>
@endforeach
@endif


</tbody>

<tfoot>
                    <tr  style="margin-top: 25px">
                        <td colspan="50" class="a-right cart-footer-actions">


                            <a href="{{url('/')}}" name="update_cart_action"  
                            title="Empty Cart" class="button2 btn-empty" id="empty_cart_button">
                            <span><span>Continue Shopping</span></span></a>


                            <a title="Continue Shopping" class="button2 btn-continue" 
                            href="{{ url('emptyCart') }}">
                            <span><span>Empty Cart</span></span></a>

                        </td>
                    </tr>
                </tfoot>




</table>
<script type="text/javascript">decorateTable('shopping-cart-table')</script>
</form>

<div class="cart-forms">

    <form id="discount-coupon-form" action="https://www.ripplemart.com/checkout/cart/couponPost/" method="post">
        <div class="discount">
            <h2>Apply Coupon</h2>
            <div class="discount-form">
                <label for="coupon_code">Apply Coupon</label>
                <input type="hidden" name="remove" id="remove-coupone" value="0" />
                <div class="field-wrapper">
                    <input class="input-text" type="text" id="coupon_code" name="coupon_code" value="" />
                    <div class="button-wrapper">
                        <button type="button" title="Apply" class="button2" onclick="discountForm.submit(false)" 
                        value="Apply">
                        <span><span>Apply</span></span></button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- <div class="shipping">
        <h2>Estimate Shipping and Tax</h2>
        <div class="shipping-form">
           <form action="https://www.ripplemart.com/checkout/cart/estimatePost/" method="post" id="shipping-zip-form">
            <p class="shipping-desc">Enter your destination to get a shipping estimate.</p>
            <ul class="form-list">
                <li class="shipping-country">
                    <label for="country" class="required"><em>*</em>Country</label>
                    <div class="input-box">
                            </div>
                    </li>          
                    <li class="shipping-region">          
                        <label for="region_id" class="required"><em>*</em>State/Province</label>
                        <div class="input-box">
                            <select id="region_id" name="region_id" title="State/Province" style="display:none;"
                            >
                            <option value="">Please select region, state or province</option>
                        </select>
                        <script type="text/javascript">
                       
                       $('region_id').setAttribute('defaultValue',  "");
                    
                   </script>
                   <input type="text" id="region" name="region" value=""  
                   title="State/Province" class="input-text" style="display:none;" />
               </div>
           </li>         
           <li class="shipping-postcode">                            
            <label for="postcode" class="required"><em>*</em>Zip</label>
            <div class="input-box">
                <input class="input-text validate-postcode required-entry validate-length maximum-length-6 minimum-length-6 validate-digits" type="text" id="postcode" name="estimate_postcode" 
                value="" />
            </div>
        </li>
    </ul>
    <div class="buttons-set">
        <button type="button" title="Estimate" onclick="coShippingMethodForm.submit()" class="button2">
            <span><span>Estimate</span></span>
        </button>
    </div>
</form>


</div>
</div> -->


</div>
<div class="cart-totals-wrapper">
    <div class="cart-totals">
        <table id="shopping-cart-totals-table">
            <col />
            <col width="1" />
            <tfoot>
                <tr>
                    <td style="" class="a-right" colspan="1">
                        <strong>Grand Total</strong>
                    </td>
                    <td style="" class="a-right">
                        <strong><span class="price">₹{{$total}}.00</span></strong>
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <tr>
                    <td style="" class="a-right" colspan="1">
                    Subtotal    </td>
                    <td style="" class="a-right">
                        <span class="price">₹{{$total}}.00</span>    </td>
                    </tr>
                    <tr >
                        <td style="" class="a-right" colspan="1">
                        GST            </td>
                        <td style="" class="a-right"><span class="price">₹0.00</span></td>
                    </tr>
                </tbody>
            </table>
            <ul class="checkout-types bottom">
                <li class="method-checkout-cart-methods-onepage-bottom">
                    <button type="button" title="Proceed to Checkout" class="button btn-proceed-checkout btn-checkout no-checkout" disabled="disabled" onclick="Swal('Please Login First');"><span><span>Proceed to Checkout</span></span></button>
                </li>
            </ul>
        </div>
    </div>
</div>

</div>
</div>
</div>

</div>


@endsection