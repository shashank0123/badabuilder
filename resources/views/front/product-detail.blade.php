
@extends('layouts.front')

@section('content')

<div class="container-fluid">
	<div class="breadcrumbs">
		<ul style="margin-top: 25px">
			<li class="home">
				<a href="{{ url('/') }}" title="Go to Home Page">Home</a>
				<span>/ </span>
			</li>
			<li class="category3965">
				<a href="{{ url('products/'.$productCategory->slug ?? '') }}" title="">{{ ucfirst($productCategory->category_name) }}</a>
				<span>/ </span>
			</li>
			<li class="product">
				<strong>{{ ucfirst($productDetail->name ?? '') }}</strong>
			</li>
		</ul>
	</div>
	<div class="col-main">
		<div id="amfpc-global_messages"></div> <script type="text/javascript">
			var optionsPrice = new Product.OptionsPrice([]);
		</script>
		<div id="messages_product_view"><div id="amfpc-messages"></div></div>
		<div class="product-view">
			<div class="product-essential">

				<div class="product-img-box">
					<div class="product-name">
						<h1>{{ ucfirst($productDetail->name ?? '') }}</h1>
					</div>
					<div class="product-image product-image-zoom">
						<div class="product-image-gallery" id="product-image">
							<img id="image-main"
							class="gallery-image visible"
							src="{{ asset($productDetail->image1 ?? '') }}"
							alt="{{ ucfirst($productDetail->name ?? '') }}"
							title="{{ ucfirst($productDetail->name ?? '') }}" />
							<img id="image-0"
							class="gallery-image"
							src="{{ asset($productDetail->image1 ?? '') }}"
							data-zoom-image="{{ asset($productDetail->image1 ?? '') }}" />
						</div>
					</div>
					<div class="more-views">
						<h2>More Views</h2>
						<ul class="product-image-thumbs">
							<li>
								<a class="thumb-link" onclick="hideProductVideo('image-0')" href="#" title="{{ ucfirst($productDetail->name ?? '') }}" data-image-index="0">
									<img src="{{ asset($productDetail->image1 ?? '') }}"
									width="75" height="75" alt="{{ ucfirst($productDetail->name ?? '') }}" />
								</a>
							</li>
						</ul>
					</div>
					<script type="text/javascript">
						function showProductVideo(image) {
							$$('.product-image .product-image-gallery img').invoke('removeClassName', 'visible');
							if ($$('.zoomContainer')) {
								$$('.zoomContainer').invoke('hide');
							}
							if ($('product-image').down("iframe"))
								$('product-image').down("iframe").remove();
							var iframe = '<iframe width="100%" height="315" src="' + image.up('li').down('input').value + '?autoplay=0&showinfo=1&controls=1' + '" frameborder="0" allowfullscreen></iframe>';
							$('product-image').insert(iframe);
						}
						function hideProductVideo(imageid) {
							$$('.product-image .product-image-gallery img').invoke('removeClassName', 'visible');
							$(imageid).addClassName('visible');
							if ($('product-image').down("iframe")) {
								$('product-image').down("iframe").remove();
							}
						}
						$mp = jQuery.noConflict();
						$mp(document).ready(function () {
							$mp('.product-image-thumbs').bxSlider({
								infiniteLoop: false,
								hideControlOnEnd: true,
								slideWidth: 300,
								minSlides: 1,
								maxSlides: 4,
								moveSlides: 1,
								slideMargin: 10
							});
						});
					</script>
					

				</div>
				<div class="product-shop">
					<div class="product-name">
						<span class="h1">{{ ucfirst($productDetail->name ?? '') }}</span>
					</div>
					<div class="clear"></div>
					<p class="no-rating">
						<i class="fa fa-star-o"></i> &nbsp;
						<i class="fa fa-star-o"></i> &nbsp;
						<i class="fa fa-star-o"></i>&nbsp;
						<i class="fa fa-star-o"></i>&nbsp;
						<i class="fa fa-star-o"></i>&nbsp;&nbsp;
						<span> ({{$countReview}}) Rivews </span>
						<a class="be-the-review" href="#review-form">
							Give your Fedback.
						</a>
					</p>
					<div class="clear"></div>
					<div class="product-info-wr ">
						<div class="price-info">
							<div style="clear: both;"></div>
			<!-- <div class="product-sold-count">
				More than 10 available
			</div> -->
			@if($productDetail->availability == 'yes')
			<p class="availability in-stock">
				<span class="label">
				Availability:</span>
				<span class="value">In Stock</span>
			</p>
			@else
			<p class="availability out-stock">
				<span class="label">
				Availability:</span>
				<span class="value">Out Of Stock</span>
			</p>
			@endif
			<div class="price-box">
				<p class="old-price">
					<span class="price-label">Regular Price:</span>
					<span class="price" id="old-price-39694">
						₹{{$productDetail->mrp}} </span>
					</p>
					<p class="special-price">
						<span class="price-label">Special Price</span>
						<span class="price" id="product-price-39694">
							₹{{$productDetail->sell_price}} </span>
						</p>
						<span class="saving_options_discount">{{$discount}}% Off</span>
					</div>
					<!--for GST Breakup module-->
			<!-- <div>
				<span style="color: rgb(165, 167, 167);"><span class="price-with-gst">₹938.10</span>&nbsp;&nbsp;with <span class="gst-percentage">18</span>% GST.</span>
			</div> -->
			<div class="clear saving_options">
				<span> Save: <span><b class="saving_options_price">₹{{$productDetail->mrp-$productDetail->sell_price}}.00</b>
				</span> </span>
			</div>
			<b>Specifications</b>
			<ui>
				@if(!empty($productDetail->product_brand))
				<li>
					<span class="custom-label">Brand</span>:
					<span class="custom-data">{{$productDetail->product_brand ?? ''}}</span>
				</li>
				@endif
				@if(!empty($productDetail->product_color))
				<li>
					<span class="custom-label">Color</span>:
					<span class="custom-data">{{$productDetail->product_color ?? ''}}</span>
				</li>
				@endif
				@if(!empty($productDetail->product_model))
				<li>
					<span class="custom-label">Model</span>:
					<span class="custom-data">{{$productDetail->product_model ?? ''}}</span>
				</li>
				@endif
				@if(!empty($productDetail->product_model_number))
				<li>
					<span class="custom-label">Model Number</span>:
					<span class="custom-data">{{$productDetail->product_model_number ?? ''}}</span>
				</li>
				@endif
			</ui>
			<div style="margin-left: 20px;color: steelblue;font-weight: 600;font-size: 12px;"><a class="specifications-more" href="#more-tab-info">See more</a>
			</div>
			
			<!-- <div>
				<span class="cashOnDel emi">EMI</span>
				<span class="cashOnDel">Cash on Delivery Available</span>
				<span id="change_pincode_block">
					<div id="UpdatePincode">
						<input style="width: 49%;" placeholder="Enter Delivery Pincode" type="text" name="pincode" id="pincode" maxlength="6" value="" onkeypress="return this.value.length <= 6" />
						<button style="margin-top: 5px;margin-bottom:9px;" type="button" title="Update" class="pincode-buttonss" onclick="updatePincodeForm();">Update</button>
					</div>
					<div class="please-wait" id="pincode-please-wait" style="width: 37%;display:none;">
						<img src="https://www.ripplemart.com/skin/frontend/apptha/superstore/images/opc-ajax-loader.gif" alt="Checking..." title="Checking..." class="v-middle" /> Checking... </div>
						<div class="custom-shipping-amount">
								
							</div>
						<div class="validation-advice" id="advice-required-entry-pincode" style="display:none;margin-bottom:9px;">This is a required field.</div>
						<div class="validation-advice" id="advice-required-entry-pincode-six" style="display:none;margin-bottom:9px;">Please enter 6 digits valid number in this field.</div>
					</span>
				</div> -->
			</div>
			<div class="product-options-block">
			</div>
			<div class="add-to-cart-wrapper">
				<div class="add-to-box">
					<div class="add-to-cart">
						<div class="qty-wrapper">
							<label for="qty">Qty:</label>
							<a class="decrement_qty" href="javascript:void(0)" style="float: left; font-size: 28px; font-weight: bolder; margin-right: 7px;">
								<div class="middle_icon_color pull-left">
									<i class="icon-minus"> - </i>
								</div>
							</a>
							<input type="text"
							pattern="\d*"
							name="qty"
							id="qty{{$productDetail->id}}"
							value="1"
							title="Qty" class="input-text qty custom-qty-upadte" />
							<a class="increment_qty" href="javascript:void(0)" style="float: left; font-size: 28px; margin-left: 7px; font-weight: bolder;">
								<div class="middle_icon_color pull-left">
									<i class="icon-plus"> + </i>
								</div>
							</a>
							<span class="unit-pice"> X 1 Unit</span>
						</div>
						<div id="mobile-cart-buttons">
							<div class="add-to-cart-buttons">
								<button type="button"
								title="Add to Cart" class="btn" onclick="addToCart({{$productDetail->id}})">
								<span><span id="add-to-cart-title">Add to Cart</span></span></button>
							</div>
							<div class="add-to-cart-buttons buy-now-button">
								
								<button type="submit"
								title="Buy Now" class="btn" style="background-color: #46b64f">
								<span><span>Buy Now</span></span></button>
							</div>

							<div class="add-to-cart-buttons buy-now-button">
								
								<button type="submit"
								title="Add To Wishlist" class="btn2" onclick="addToWishlist({{$productDetail->id}})">
								<span><span><i class="fa fa-heart-o"></i></span></span></button>
							</div>

						</div>

					</div>
					<div id="cartSuccess"></div>
					
					<span class="or">OR</span>

					<ul class="sharing-links">
						<li><a target="_blank" href="#" class="link-email-friend"
							title="Email to a Friend">
						</a></li>
						<li>
							<a href="#" target="_blank"
							title="Share on Facebook" class="link-facebook">
						</a>
					</li>
					<li>
						<a href="#" target="_blank"
						title="Share on Twitter" class="link-twitter">
					</a>
				</li>
				<li>
					<a href="#" target="_blank"
					title="Share on Twitter" class="link-instagram">
				</a>
			</li>
<!-- <li>
	<a href="#" title="Share with Google+">
</a>
</li> -->
</ul>
</div>
</div>
</div>
<div class="extra-info">
<!-- <dl class="sellerOtherInfo" style="background: none repeat scroll 0 0 #ffffff;">
<dt>Seller</dt><dd><strong>RCP</strong></dd>
<dt>Features</dt>
<dd><div class="std"></div>
</dd>
</dl>-->


</div>
<div class="clear"></div>
<div class="short-description">
<!--<span class="lOff">Limited<br/>
Quantity</span>-->
<div> <!--class="sdCont"--> <h3>Short Description</h3> {{ ucfirst($productDetail->short_descriptions ?? '') }} </div> </div>
</div>
<div class="clearer"></div>

</div>
<div class="related-tabs" id="more-tab-info">
	<div class="product-collateral toggle-content tabs">
		<dl id="collateral-tabs" class="collateral-tabs">
			<dt class="tab Description-tab"><span>Description</span></dt>
			<dd class="tab-container Description-tab">
				<div class="tab-content"> <h2>Details</h2>
					<div class="std">
						<p align="justify">{{$productDetail->long_descriptions ?? ''}}</p> </div>
					</div>
				</dd>
				<dt class="tab Specifications-tab"><span>Specifications</span></dt>
				<dd class="tab-container Specifications-tab">
					<div class="tab-content"> <h2>Additional Information</h2>
						<table class="data-table" id="product-attribute-specs-table">
							<col width="25%" />
							<col />
							<tbody>
								@if(!empty($productDetail->product_brand))
								<tr>
									<th class="label">Brand</th>
									<td class="data">{{$productDetail->product_brand}}</td>
								</tr>
								@endif
								@if(!empty($productDetail->product_color))
								<tr>
									<th class="label">Color</th>
									<td class="data">{{$productDetail->product_color ?? ''}}</td>
								</tr>
								@endif
								@if(!empty($productDetail->product_weight))
								<tr>
									<th class="label">Weight</th>
									<td class="data">
										{{$productDetail->product_weight ?? ''}}</td>
									</tr>
									@endif
									@if(!empty($productDetail->product_material))
									<tr>
										<th class="label">Material</th>
										<td class="data">{{$productDetail->product_material ?? ''}}</td>
									</tr>
									@endif
									@if(!empty($productDetail->product_model))
									<tr>
										<th class="label">Model</th>
										<td class="data">{{$productDetail->product_model ?? ''}}</td>
									</tr>
									@endif
									@if(!empty($productDetail->product_model_number))
									<tr>
										<th class="label">Model No</th>
										<td class="data">{{$productDetail->product_model_number ?? ''}}</td>
									</tr>
									@endif
								</tbody>
							</table>
							<script type="text/javascript">decorateTable('product-attribute-specs-table')</script>
						</div>
					</dd>

					<dt class="tab Reviews-tab"><span>Reviews</span></dt>
					<dd class="tab-container Reviews-tab">
						<div class="tab-content">
							<div class="box-collateral box-reviews" id="customer-reviews">
								@if($countReview == 0)
								<p class="no-rating"><a href="#review-form">Be the first to review this product</a></p>
								@endif
								<div class="form-add">
									<div class="container-fluid">
										<div class="row">
											<div class="col-sm-7">

												<h2>Write Your Own Review</h2>
												<form action="{{route('give-product-rating')}}" method="post">
													@csrf
													<input type="text" name="product_id" hidden value="{{$productDetail->id}}">

													<h3>You're reviewing: <span>{{ ucfirst($productDetail->name ?? '') }}</span>
													</h3>
													<div class="fieldset">
														<h4>How do you rate this product? <em class="required">*</em></h4>
														<span id="input-message-box"></span>

														<ul class="form-list">
															<li class="inline-label">
																<label for="nickname_field" class="required"><em>*</em>Your Name?</label>
																<div class="input-box">
																	<input type="text" name="name" id="nickname_field" class="input-text required-entry" value="" required="re" />
																</div>
															</li>
															<li class="inline-label">
																<label for="nickname_field" class="required"><em>*</em>Your Email?</label>
																<div class="input-box">
																	<input type="text" name="email" id="nickname_field" class="input-text required-entry" value="" required="required" />
																</div>
															</li>
															<li>
																<label for="review_field" class="required"><em>*</em>Share Your Opinion</label>
																<div class="input-box">
																	<textarea name="review" id="review_field" cols="5" rows="3" class="required-entry" required="required"></textarea>
																</div>
															</li>

														</ul>

														<table class="data-table review-summary-table ratings" id="product-review-table">
															<col width="1" />
															<col />
															<col />
															<col />
															<col />
															<col />
															<thead>
																<tr>
																	<th>&nbsp;</th>
																	<th>
																		<div class="rating-box">
																			<span class="rating-number">1</span>
																			<span class="rating nobr"
																			style="width:20%;">1 star </span>
																		</div>
																	</th>
																	<th>
																		<div class="rating-box">
																			<span class="rating-number">2
																			</span>
																			<span class="rating nobr"
																			style="width:40%;">
																		2 star</span>
																	</div>
																</th>
																<th>
																	<div class="rating-box">
																		<span class="rating-number">3</span>
																		<span
																		class="rating nobr"
																		style="width:60%;">3 star </span>
																	</div>
																</th>
																<th>
																	<div class="rating-box">
																		<span class="rating-number">4</span>
																		<span
																		class="rating nobr"
																		style="width:80%;">
																	4 star</span>
																</div>
															</th>
															<th>
																<div class="rating-box">
																	<span class="rating-number">5</span>
																	<span class="rating nobr" style="width:100%;">5 star</span>
																</div>
															</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<th>Quality</th>
															<td class="value"><label for="Quality_1"><input type="radio" name="ratings[1]" id="Quality_1" value="1" class="radio" checked="checked" /></label></td>
															<td class="value"><label for="Quality_2"><input type="radio" name="ratings[1]" id="Quality_2" value="2" class="radio" /></label></td>
															<td class="value"><label for="Quality_3"><input type="radio" name="ratings[1]" id="Quality_3" value="3" class="radio" /></label></td>
															<td class="value"><label for="Quality_4"><input type="radio" name="ratings[1]" id="Quality_4" value="4" class="radio" /></label></td>
															<td class="value"><label for="Quality_5"><input type="radio" name="ratings[1]" id="Quality_5" value="5" class="radio" /></label></td>
														</tr>
													</tbody>
												</table>
												<input type="hidden" name="rating" class="validate-rating" value="" />
												<script type="text/javascript">decorateTable('product-review-table')</script>

											</div>
											<div class="buttons-set">
												<button type="submit" title="Submit Review" class="button">
													<span><span>Submit Review</span></span></button>
												</div>
											</form>
										</div>
										<div class="col-sm-7">
											Reviews List
										</div>

									</div>
								</div>
							</div>
						</div>

					</div>
				</dd>
			</dl>
		</div>
	</div>

</div>

</div>
</div>
</div>

</div>





</div>
@endsection