

@extends('layouts.front')

@section('content')

<div class="container " style="margin-top: 30px; ">
    <div class="cart display-single-price">
        <div class="page-title title-buttons">
            <h4><span><a href="{{url('/')}}">Home</a> > </span> 
            Wishlist</h4>
            
        </div>

        <div  style="margin-top: 25px; text-align: center;">
            <h1 style="text-align: center; font-size: 42px">Wishlist</h1>
            
        </div>


        <table id="shopping-cart-table" class="cart-table data-table" style="margin-top: 25px">
            <col width="1" />
            <col width="1" />
            <col width="1" />
            <col width="1" />
            <col width="1" />
            <col width="1" />

            <thead>
                <tr>
                    <th rowspan="1"><span class="nobr">Product</span></th>
                    <th rowspan="1" style="width: 200px">&nbsp;</th>
                    <th class="a-center cart-price-head" colspan="1"><span class="nobr">Price</span></th>
                    <th rowspan="1" class="a-center"  style="width: 225px">Qty</th>
                    <th class="a-center"  rowspan="1">In Stock</td>
                        <th class="a-center cart-total-head" colspan="1">Subtotal</th>
                        <th class="a-center" rowspan="1">Action</th>

                    </tr>
                </thead>
                
                <tbody>
                    @if(count($products)>0)
                    @foreach($products as $product)
                    <tr id="div-list{{$product->id}}">
                        <td class="product-cart-image">
                            <a href="{{ url('product-detail/'.$product->slug) }}">
                                <img src="{{ asset($product->image1) }}" style="height: 75px; width: auto "/>
                            </a>
                        </td>

                        <td class="product-cart-info">


                            <h2 class="product-name">
                                <a href="{{ url('product-detail/'.$product->slug) }}">
                                    {{ ucfirst($product->name ?? '') }}</a>
                                </h2>

                  


                </td>


                <td class="product-cart-price">
                    <span class="cart-price  cart-row-item-price-id-7372">
                        <span class="price">
                            ₹{{$product->sell_price ?? '0'}}
                        </span>                       </span>
                    </td>
                    <!-- inclusive price starts here -->
                    <td class="product-cart-actions" data-rwd-label="Qty">
                        <input type="text" pattern="\d*" name="cart" 
                        value="1" size="4" 
                        title="Qty" class="input-text qty"  />

                        <button type="submit" 
                        name="update_cart_action" value="update_qty" title="Update" class="button btn-update">
                        <span><span>Update</span></span>
                    </button>
                    <ul class="cart-links">
                           

            </ul>

        </td>
        <td>{{$product->availability}}</td>

        <!--Sub total starts here -->
        <td class="product-cart-total cart-row-item-id-7372" data-rwd-label="Subtotal">

          <span class="price">₹608.00</span>                            
      </span>
  </td>
  <td class="a-center product-cart-remove">
    <a onclick="deleteWishlistItem({{$product->id}})"  
        title="Remove Item" class="btn-remove btn-remove2">
    Remove Item</a>
</td>
</tr>
@endforeach
@endif

@if(count($products)==0)
<tr colspan='7'  class="product-cart-price">
   No product in your wishlist 
</tr>
@endif

</tbody>


<tfoot>
    <tr  style="margin-top: 25px">
        <td colspan="50" class="a-right cart-footer-actions">


            <a href="{{url('/')}}" name="update_cart_action"  
            title="Empty Cart" class="button2 btn-empty" id="empty_cart_button">
            <span><span>Continue Shopping</span></span></a>


            <a title="Continue Shopping" class="button2 btn-continue" 
            href="{{ url('emptyWishlist') }}">
            <span><span>Empty Wishlist</span></span></a>

        </td>
    </tr>
</tfoot>

</table>




</div>

</div>
</div>
</div>

</div>


@endsection