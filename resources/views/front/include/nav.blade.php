<?php
use App\Models\Category;
use App\Wishlist;
$categories = Category::where('status','Active')->where('category_id','0')->get();
$countcart=0 ; $countwishlist=0;
if(!empty(session()->get('cart'))){
	$countcart=count(session()->get('cart'));
}
if(!empty(Auth::user())){
$countwishlist = Wishlist::where('user_id',Auth::user()->id)->count();
}
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<header id="header" class="page-header">
	<div id="rmtPromo">
		<div class="header-coupon">
			<div id="ImageScissor" style="text-align: right; width: 200px">

				<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Welcome To BadaBuilder</h5>
			</div>

		</div>
		<div id="headerDesign">
			<span id="headCustomer">
				<a href="#" id="aHeader">
					<i class="fa fa-envelope"></i>&nbsp;
					<label id="headerLabel">customercare@badabuilder.com</label>
				</a>
			</span>
			<span id="headCustomer">
				<a href="#" id="aHeader">

					<i class="fa fa-cart-arrow-down"></i>&nbsp;
					<label id="headerLabel">Sell on BadaBuilder</label>
				</a>
			</span>
			<span id="headCustomer">
				<a href="#" id="aHeader">

					<i class="fa fa-map-marker"></i>&nbsp;
					<label id="headerLabel">Track Order</label>
				</a>
			</span>
			<span id="headCustomer">
				<a href="#" id="aHeader">

					<i class="fa fa-file-text"></i>&nbsp;
					<label id="headerLabel">Bulk Enquiries</label>
				</a>
			</span>
		</div>
	</div>


	<div class="page-header-container">
		<a class="logo" href="{{ url('/') }}"> 
			<h1>&nbsp;&nbsp;&nbsp;<b>BADABUILDER</b></h1>
		</a>
		<!-- Skip Links -->
		<div class="skip-links">
			<ul>



				@if(!empty(Auth::user()))

				<li>
					<a href="javascript:void('0');" class="skip-link skip-account" title="My Account"> 
						<!-- <span class="icon"></span> -->
						<i class="fa fa-user-o"></i>
						

					</a>
					<span>{{ucfirst(Auth::user()->name ?? 'My Account')}}</span>
					<div id="header-account" class="skip-content">
						<!-- <div class="signSignup">
							<a class="btn" href="javascript:void('0');" onclick="apptha_sociallogin();">Sign In</a>
							<span>New Customer?
								<a href="javascript:void('0');" onclick="apptha_sociallogin();show_hide_socialforms('2');">Start here</a>.
							</span>
						</div> -->
						<div class="links">
							<ul>
								<li class="first" > <a href="#" title="My Account Dashboard" >My Dashboard</a>
								</li>
								<li > <a href="#" title="My Orders" >My Orders</a>
								</li>
								<!-- <li > <a href="#" >Track Order</a>
								</li> -->								
 <li ><a href="{{ url('wishlist') }}" >My Wishlist</a></li>
								
								<li class=" last" > <a href="#" title="Log Out" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();" >Logout</a>
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                        </form>
								</li>
							</ul>
						</div>
					</div>
				</li>

				@else


				<li>
					<a href="{{ url('login') }}" class="skip-link skip-contact" title="Login"> 
						<i class="fa fa-user-o"></i>
					</a>
					<span>Login</span>

				</li>


				@endif

				<li>
					<a href="{{ url('wishlist') }}" class="skip-link skip-contact" title="Wishlist"> 
						<i class="fa fa-heart-o"></i>
						(<span id="wishItem">{{$countwishlist}}</span>)
					</a>
					<span>Wishlist</span>

				</li>

				<!-- Cart -->

				<li>
					<!-- <a href="#" onclick="showMiniCart()" class="skip-link skip-contact" title="Cart" onclick="" >  -->
					<a href="{{ url('cart') }}" class="skip-link skip-contact" title="Cart" onclick="" > 
						<i class="fa fa-cart-plus"></i>
						(<span id="cartItem">{{$countcart}}</span>)
					</a>
					<span>Cart</span>

				</li>

					<!-- <li class="header-minicart" id="header-minicart">
						<a href="/checkout/cart/" title="Shopping Cart" class="skip-link skip-cart no-count">
							<i class="fa fa-cart-plus"></i>
							
							
							<span class="count" id="cartcount">0</span>
						</a>
							<span class="label">Cart</span>
						<div id="header-cart" class="block block-cart skip-content">
						</div>
					</li> -->
				</ul>

			</div>
			<!-- Search -->
			<div id="header-search" class="skip-content">
				<a href="#" class="mobileNav" title="Mobile Nav"> <span
					class="icon"></span>
				</a>
				<form id="search_mini_form" action="{{ url('searchedProducts') }}" method="get" class="searchautocomplete UI-SEARCHAUTOCOMPLETE" >
					<label for="search">Search:</label>
					<!-- <div class="catSearchDD">
						<select name="cat" id="category_search_field" class="category UI-CATEGORY">
							<option value="0">All Categories</option>
						</select>
					</div> -->
					<div class="input-box">
						<input id="search" type="text" autocomplete="off" name="keyword" value="" class="input-text UI-SEARCH UI-NAV-INPUT" placeholder="Search entire store here..." maxlength="128" />
						<button type="submit" title="Search" class="button search-button">
							<span><span>Search</span></span>
						</button>
						<!-- <a href="/catalogsearch/advanced/" title="Advanced Search">Advanced</a> -->
					</div>
					<div class="form-search">
						<div class="searchautocomplete-loader UI-LOADER" style="display:none;"></div>
						<div style="display:none" id="search_autocomplete" class="UI-PLACEHOLDER search-autocomplete searchautocomplete-placeholder"></div>
					</div>
				</form>
			</div>

		</div>
		<!--</div>-->
		<div id="header-nav">
			<div class="em-header-bottom">
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<div class="top-menu-left menu-wrapper">
								<div class="all_categories">
									<div class="menuleftText-title">
										<div class="menuleftText"><span><a href="/category-list/">Shop by Category</a></span></div>
									</div>

									<div class="menuleft em_nav" style="display:none;">
										<div class="megaNav">
											<ul>
												@if(count($categories)>0)
												@php $countc = 0 @endphp
												@foreach($categories as $category)

												<li class="@if($countc == 0){{'active1'}}@endif" >
													<a href="{{ url('products/'.$category->slug ?? '') }}">
														<!-- <span class="nIcon">
															<img alt="" src="https://www.ripplemart.com/media/catalog/category/building_materials_1.png" width="16px">
														</span> -->
														{{ucfirst($category->category_name ?? '')}}
													</a>

													<?php
													$countc=1;
													$subcategories = Category::where('category_id',$category->id)->get();
													$counts=0;
													?>

													@if(count($subcategories)>0)
													<div class="fullCont">
														<div class="subNav">
															<ul>
																@foreach($subcategories as $subcat)
																<li class="@if($counts == 0){{'active2'}}@endif" >
																	<a href="{{ url('products/'.$subcat->slug) }}">{{ ucfirst($subcat->category_name ?? '')}}</a>
																	<?php
																	$counts=1;
																	$superSubCategories = Category::where('category_id',$subcat->id)->get();
																	?>
																	@if(count($superSubCategories)>0)
																	<div class="subNav2">
																		<ul>
																			@foreach($superSubCategories as $superSub)
																			<li>
																				<a href="{{ url('products/'.$superSub->slug) }}">{{ucfirst($superSub->category_name ?? '')}}</a>
																			</li>
																			@endforeach
																		</ul>
																	</div>
																	@endif
																</li>
																@endforeach
															</ul>
														</div>
													</div>
													@endif
												</li>
												@endforeach
												@endif
				
				
			<li class="viewAllCatLink">
				<a href="{{ url('products/all') }}">View All</a>
				<!-- <div class="fullCont">
					<div class="imgBlockall">
						<a href="https://www.ripplemart.com/">
							<img src="https://www.ripplemart.com/media/theme/default/test11.jpg">
						</a>
					</div>
				</div> -->
			</li>
		</ul>
	</div>
</div>
</div>
</div>
</div>
<div class="col-sm-18">
	<div class="em-menu-hoz">
		<div id="em-main-megamenu">
			<div class="mainstore-topmenu-horizontalmenu">
				<div class="megamenu-wrapper wrapper-5_1430">
					<div id="toogle_menu_5_1430" class="em_nav">
						<ul class="hnav em_hoz_menu effect-menu">
							<li class="menu-item-link menu-item-depth-0 menu-item-parent"><a class="em-menu-link" href="#"> <span> <b>Design</b> </span> </a></li>
							<li class="menu-item-link menu-item-depth-0 menu-item-parent"><a class="em-menu-link" href="#"> <span><b>CBT</b></span></a></li>

							<li class="menu-item-link menu-item-depth-0 menu-item-parent"><a class="em-menu-link" href="#"> <span><b> Construction</b> </span> </a></li>
							
							<li class="menu-item-link menu-item-depth-0 menu-item-parent"><a class="em-menu-link" href="{{ url('contact') }}"> <span> <b>Contact Us</b> </span> </a></li>
							<!-- <li class="menu-item-link menu-item-depth-0 menu-item-parent"><a class="em-menu-link" href="https://www.ripplemart.com/blog"> <span> Blog</span> </a></li> -->
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>

</div>
</header>