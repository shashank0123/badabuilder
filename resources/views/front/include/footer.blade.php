


<div class="container-fluid footer-container">

   <div class="containers2 newsletter">
      <div class="itemed">
         <div class="socialIcons">
            
            <a href="#" title="Facebook" ><i class="fa fa-facebook"></i></a>
             <a href="#" title="Instagram" ><i class="fa fa-instagram"></i></a>
             <a href="#" title="Linked In" ><i class="fa fa-linkedin"></i></a>
             <a href="#" title="Twitter" ><i class="fa fa-twitter"></i></a>
         </div>
      </div>
      <div class="itemed">
         <div class="NewsLetterForm">
            <form>
               <input type="search" name="keyword" id="keyword" required="required" placeholder="&nbsp;&nbsp;&nbsp;Enter Email Here">
               <button type="submit" name="search" class="newsSubmit">@</button>
            </form>

         </div>
      </div>
   </div>

   <div class="containers  footer-menu">
      <div class="itemedL">
         <div class="badabulider">
            <h2>BADABUILDER</h2>
            <p class="badaInfo">
               <i class="fa fa-map-marker"></i> <span>Address :</span>
               Address, City
               State, Pincode<br>

               <i class="fa fa-phone"></i> <span>Contact :</span>
               +91 99999999<br>
               

               <i class="fa fa-envelope"></i> <span>Email :</span>
               <a href="mailto:info@gmail.com">mailto:info@gmail.com</a>
              

            </p>
         </div>
      </div>
      <div class="itemed">
         <div class="aboutUs">
            <h4>About Us</h4>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
         </div>
      </div>
      <div class="itemed">
         <div class="usefulLinks">
            <h4>Useful Links</h4>
            <ul>
               <li><a href="{{ url('/') }}"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;Home</a></li>
               <li><a href="{{ url('aboutUs') }}"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;About Us</a></li>
               <li><a href="{{ url('contact') }}"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;Contact Us</a></li>
               <li><a href="{{ url('tnc') }}"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;Terms & Conditions</a></li>
               <li><a href="{{ url('privacy-policy') }}"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;Privacy Policy</li>
            </ul>
         </div>
      </div>
   </div>

   <div class="footer-bottom">
      <p>Copyright BADABUILDER, Design and developed by Backstage Supporters Pvt. Ltd.</p>
   </div>
