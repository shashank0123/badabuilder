<!-- Login & Register Modal -->

<div id="header_logo_Div" class="apptha_header_logo_Div" style="display: none; margin-top: -42px;">
	<a class="closeLink apptha_social_popup_sprite" href="javascript:apptha_socialloginclose();"></a>
	<div id="social_popup_main" class="apptha_social_popup_main">
		<input type="hidden" name="main_hidden" id="main_hidden" value=""/>
		<div id="socialpopup_main_div" class="apptha_socialpopup_main_div">
			<div class="left_login">
				<div id="login_block">
					<form id="form_login"
					action="https://www.ripplemart.com/sociallogin/index/customerloginpost/"
					class="socialpopup_form apptha_form_login">
					<h4>Login</h4>
					<input type="hidden" name="login_hidden" id="login_hidden" value=""/>
					<label for="email" class="required">Mobile/Email <em>*</em></label>
					<div class="socialpopup-input-box">
						<input type="text" name="email" value="" id="email" onfocus="document.getElementById('formSuccess').style.display = 'none';" class="input-text required-entry" title="Email Address" />
					</div>
					<label for="password" class="required">
						Password<em>*</em>
					</label>
					<div class="socialpopup-input-box">
						<input type="password" name="password" value="" id="password" onfocus="document.getElementById('formSuccess').style.display = 'none';" class="input-text required-entry validate-password" title="Password" />
					</div>
					<div id="formSuccess" class="apptha_formSuccess" style="display: none;">&nbsp;</div>
					<div class="social_login_btn">
						<a href="javascript:void(0);" id="show_password" class="apptha_show_password" onclick="show_hide_socialforms('3');">Forgot Your Password?</a>
						<span id="progress_image_login" class="apptha_progress_image_login" style="display: none">
							<img
							src="https://www.ripplemart.com/skin/frontend/apptha/superstore/sociallogin/images/ajax-loader.gif"
							alt="loading please wait" /> </span>
							<button type="button" onclick="socialLoginFrm.submit(this)"
							class="button popup_click_btn" title="Login"
							name="send" id="slogin" onSubmit="test();"><span><span> Login</span></span></button>
						</div>
					</form>
					<div id="all_social_iconbtn" class="apptha_all_social_iconbtn social-login-all">
						<h4>Sign in with</h4>
						<span class="divider-or apptha_social_popup_sprite social_popup_sprite"><b>OR</b></span>
						<ul>
							<li class="sl_clearfix">
								<div onclick="googlepost();"
								title="Google+">
								<span class="gplus_icon_left apptha_social_login_sprite icon_left_grid"></span>
								<button type="submit" class="google_login inner_social_grid"
								name="send"> </button>
								<span class="gplus_icon_right apptha_social_login_sprite icon_right_grid"></span>
							</div>
						</li>
					</ul>
				</div>
				<div class="new_account_create">Don't have an account?
					<a href="javascript:void(0);" onclick="show_hide_socialforms('2');">Create One!</a>
				</div>
				<div class="sl_clear"></div>
				<div id="forget_password_div" style="display: none">
					<form id="forget_password_form"
					action="https://www.ripplemart.com/sociallogin/index/forgotPasswordPost/"
					class="socialpopup_form apptha_forget_password_form">
					<h4>Forgot Your Password?</h4>
					<span class="small_txt">Enter your Mobile Email Address here to receive a link to change password.</span>
					<label for="forget_password" class="required">Enter Your Mobile / Email <em>*</em></label>
					<div class="socialpopup-input-box">
						<input type="text" onfocus="document.getElementById('forget_password_error').style.display = 'none';" value=""
						name="forget_password" class="input-text required-entry"
						id="forget_password" />
						<div class="social_login_btn f-right">
							<div id="progress_image_forgot" class="apptha_progress_image_forgot" style="display: none;"><img
								src="https://www.ripplemart.com/skin/frontend/apptha/superstore/sociallogin/images/ajax-loader.gif"
								alt="loading please wait" /></div>
								<button type="submit" onclick="socialforgetFrm.submit(this); return false;"
								class="button"><span><span>Submit</span></span></button>
							</div>
						</div>
						<div id="forget_password_error" class="apptha_forget_password_error" style="color:red;"></div>
					</form>
				</div>
				<div class="sl_clear"></div>
			</div>
			<div id="register_block" class="apptha_register_block" style="display: none;">
				<div id="register_error" class="popup_error_msg apptha_register_error" style="display: none;"></div>
				<form id="social_frm_register"
				action="https://www.ripplemart.com/sociallogin/index/createPost/"
				class="socialpopup_form">
				<h4>New User? Sign Up</h4>
				<input type="hidden" name="register_hidden" id="register_hidden" value=""/>
				<input type="hidden" name="register_type" id="register_type" value=""/>
				<label for="first_name" class="required">First Name <em>*</em></label>
				<div class="socialpopup-input-box">
					<input type="text" name="firstname"
					value="" id="first_name" class="input-text required-entry"
					title="First Name" />
				</div>
				<label for="last_name" class="required">Last Name <em>*</em></label>
				<div class="socialpopup-input-box">
					<input type="text" name="lastname"
					value="" id="last_name" class="input-text required-entry"
					title="Last Name" />
				</div>
				<label for="email_register" class="required">Email Address <em>*</em></label>
				<div class="socialpopup-input-box">
					<input type="text" name="email"
					value="" id="email_register"
					class="input-text validate-email required-entry"
					title="Email Address" />
				</div>
				<label for="mobile_register" class="required">Mobile <em>*</em></label>
				<div class="socialpopup-input-box">
					<input type="text" name="mobile"
					value="" id="mobile"
					class="input-text validate-number required-entry"
					title="Mobile" />
				</div>
				<div class="sme-company" style="display:none">
					<label for="sme_company_register" class="required">Company <em>*</em></label>
					<div class="socialpopup-input-box">
						<input type="text" name="sme_company"
						value="" id="sme_company"
						class="input-text required-entry"
						title="Company" />
					</div>
				</div>
				<label for="password_register" class="required">Password <em>*</em></label>
				<div class="socialpopup-input-box">
					<input type="password"
					name="password" class="input-text required-entry validate-password"
					id="password_register" title="Password" />
				</div>
				<label for="confirm_password" class="required">Confirm Password <em>*</em></label>
				<div class="socialpopup-input-box">
					<input type="password"
					name="confirmation" value="" id="confirm_password"
					class="input-text required-entry validate-cpassword"
					title="Confirm Password" />
				</div>
				<label
				for="social_gender"
				>Gender</label>
				<div class="socialpopup-input-box">
					<select name="gender"
					id="social_gender">
					<option value="1">Male</option>
					<option value="2">Female</option>
				</select>
			</div>
			<label for="taxvat"
			>Tax/GST number </label>
			<div class="socialpopup-input-box"><input type="text" name="social_vat"
				title="Tax/GST number"
				id="social_vat" /></div>
				<div class="sl_clear"></div>
				<div class="socialpopup-submit-box">
					<div class="social_login_btn">
						<div id="progress_image_register" class="apptha_progress_image_register" style="display: none">
							<img
							src="https://www.ripplemart.com/skin/frontend/apptha/superstore/sociallogin/images/ajax-loader.gif"
							alt="loading please wait" />
						</div>
						<button type="button" onclick="socialRegisFrm.submit(this)"
						title="Sign Up Now!" class="button"
						id="sregister"><span><span>Sign Up Now!</span></span></button>
					</div>
				</div>
			</form>
			<div id="all_social_iconbtn" class="apptha_all_social_iconbtn">
				<h4>Sign in with</h4>
				<span class="divider-or apptha_social_popup_sprite social_popup_sprite"><b>OR</b></span>
				<ul>
					<li class="sl_clearfix">
						<div onclick="googlepost();"
						title="Google+">
						<span class="gplus_icon_left apptha_social_login_sprite icon_left_grid"></span>
						<button type="submit" class="google_login inner_social_grid"
						name="send"> </button>
						<span class="gplus_icon_right apptha_social_login_sprite icon_right_grid"></span>
					</div>
				</li>
			</ul>
		</div>
		<div class="new_account_create">Already have an account?
			<a href="javascript:void(0);" onclick="show_hide_socialforms('1');">Login!</a>
		</div>
	</div>
	<div id="twitter_block" style="display: none;">
		<form id="social_tiw_login"
		action="https://www.ripplemart.com/sociallogin/index/twitterpost/"
		class="socialpopup_form apptha_social_tiw_login">
		<h4>Please Enter your twitter email</h4>
		<input type="hidden" name="twitter_hidden" id="twitter_hidden" value=""/>
		<label for="tw_email" class="required">Enter Your Email <em>*</em></label>
		<div class="socialpopup-input-box">
			<input type="text" id="tw_email"
			name="email_value" value=""
			class="input-text apptha_tw_email validate-email required-entry" />
			<div id="twitter_error" class="apptha_twitter_error popup_error_msg"></div>
			<div class="social_login_btn">
				<div id="progress_image_twitter" class="apptha_progress_image_twitter" style="display: none">
					<img
					src="https://www.ripplemart.com/skin/frontend/apptha/superstore/sociallogin/images/ajax-loader.gif"
					alt="loading please wait" />
				</div>
				<button type="button" onclick="socialTwitFrm.submit(this)"
				class="button twitter_popup_btn"><span><span>Submit</span></span></button>
			</div>
		</div>
	</form>
	<div class="new_account_create">Already have an account?
		<a href="javascript:void(0);" onclick="show_hide_socialforms('1');">Login!</a>
	</div>
</div>
</div>
<div class="SMEImageDis sme-banner-image" style="display:none;">
	<div id="sme-customer-login">
		<img style="height: auto; width: auto;" alt="" src="https://www.ripplemart.com/media/wysiwyg/SMEImg.jpg" />
	</div>
</div>
<div class="SMEImageDis login-banner-image">
	<div id="login-img-cust">
		<p>
			<img src="https://www.ripplemart.com/media/wysiwyg/customer-signin-ui.jpg" alt="" />
		</p>
	</div>
	<style>
	<!--
	.login-img-cust {
		display: none;
	}
	-->
</style>
</div>
</div>
</div>
<div class="sl_clear"></div>
</div>




<!-- Notice Board Modal -->

<div id="ajaxnotice" style="display:none">
	<div class="ajaxnotice_inner">
		<div id="ajaxnotice_working">
			<img src="https://www.ripplemart.com/skin/frontend/apptha/superstore/images/ajaxcartpopup/loading.gif" alt="" />
		</div>
		<div id="ajaxnotice_result"></div>
		<div class="ajaxnotice_clearer"></div>
	</div>
</div>





<!-- Cart Models -->
<div id="cartpopup" style="display:none">
	<div id="cartpopup_slidecontainer">
		<div class="cartpopup_header">
			<span>Shopping Cart</span>
			<a class="cartpopup_close" href="javascript:void(null)" onclick="thiscartpopup.hidePopup()">Close</a>
			<div class="cartpopup_clear"></div>
		</div>
		<form id="cartpopup_form" action="javascript:thiscartpopup.updateQuantityAction()" method="post">
			<table>
				<thead>
					<th class="lefttext" colspan="2">Product</th>
					<th class="lefttext">Price</th>
					<th class="centertext">Quantity</th>
					<th class="centertext">Total</th>
				</thead>
				<tbody>
				</tbody>
			</table>
			<input name="form_key" type="hidden" value="bfoMK4kbRxPCc4v5" />
		</form>
		<div class="cartpopup_footer">
			<div class="cartpopup_subtotal">
				<div class="cartpopup_producttotal">
					Total: <span class="price">₹0.00</span> 
				</div>
				<a href="https://www.ripplemart.com/checkout/cart/">
					<img src="https://www.ripplemart.com/skin/frontend/apptha/superstore/images/ajaxcartpopup/button.gif" alt="View Cart" />
					<div class="button" >View Cart</div>
				</a>
				<a href="https://www.ripplemart.com/checkout/onepage/">
					<img src="https://www.ripplemart.com/skin/frontend/apptha/superstore/images/ajaxcartpopup/button.gif" alt="Checkout" />
					<div class="button">Checkout</div>
				</a>
				<div class="cartpopup_clear"></div>
			</div>
		</div>
		<div id="cartpopup_overlay">
			<div class="cartpopup_overlay_center">
				<img src="https://www.ripplemart.com/skin/frontend/apptha/superstore/images/ajaxcartpopup/loading.gif" alt="" />
			</div>
		</div>
	</div>
</div>


<div style="display: none;" id="callback_information" class="callback-container ">
	<a class="callback_close" title="close" href="#" onclick="closeFeedbackWindow('callback_information'); return false;"></a>
	<div class="callback-popup-content">
		<div class="callback-title"> Request Callback </div>
		<div class="callback-content">
			<form action="https://www.ripplemart.com/callback/index/post/" method="post" id="frm_callback">
				<div id="success_message_callback" style="display: none;" class="callback-success-msg">Your request has been sent</div>
				<div id="loader" class="loader-callback" style="text-align: center;display: none;">
					<p><img src='https://www.ripplemart.com/skin/frontend/apptha/superstore/callback/images/ajax-loader-onestep.gif' alt='' title=''/></p>
				</div>
				<br/>
				<ul class="form-list" id="callback_options">
					<li>
						<label for="contactNumber" class="required">
							Your Contact Number : <!-- <span class="required">*</span> --> 
						</label>
						<input name="contactNumber" value="" title="contact Number" id="fname" type="text" class="input-text required-entry validate-number" maxlength="10" />
					</li>
					<li>
						<span id="customer-care-contact"> Our customer support executive will call you </span><br/>
						<span id="customer-care-soon"> back shortly. </span>
					</li>
					<li>
						<button id="btnsubmit" name="btnsubmit" type="button" class="button-callback" onclick="sendCallback('https://www.ripplemart.com/callback/index/post/')">
							<span><span>Call Me Back</span></span></button>
						</li>
					</ul>
				</form>
				<script type="text/javascript">
					var callback_form = new Validation($('frm_callback'));
				</script>
			</div>
		</div>
	</div>





	<div id="quickViewPopUp" class="overlay">
		<div class="popup">

			<a class="close" href="#">&times;</a>
			<div class="content">





				<div class="col-main">
					<div id="amfpc-global_messages"></div> <script type="text/javascript">
						var optionsPrice = new Product.OptionsPrice([]);
					</script>
					<div id="messages_product_view"><div id="amfpc-messages"></div></div>
					<div class="product-view">
						<div class="product-essential">

							<div class="product-img-box">
								<div class="product-name">
									<h1 class="pdName"></h1>
								</div>
								<div class="product-image product-image-zoom">
									<div class="product-image-gallery" id="product-image">
										<img id="image-main"
										class="gallery-image visible"
										src=""
										alt=""
										title="" />
										<img id="image-0"
										class="gallery-image"
										src=""
										data-zoom-image="" />
									</div>
								</div>
								<div class="more-views">
									<h2>More Views</h2>
									<ul class="product-image-thumbs">
										<li>
											<a class="thumb-link" onclick="hideProductVideo('image-0')" href="#" title="" data-image-index="0">
												<img src=""
												width="75" height="75" alt="" />
											</a>
										</li>
									</ul>
								</div>
								<script type="text/javascript">
									function showProductVideo(image) {
										$$('.product-image .product-image-gallery img').invoke('removeClassName', 'visible');
										if ($$('.zoomContainer')) {
											$$('.zoomContainer').invoke('hide');
										}
										if ($('product-image').down("iframe"))
											$('product-image').down("iframe").remove();
										var iframe = '<iframe width="100%" height="315" src="' + image.up('li').down('input').value + '?autoplay=0&showinfo=1&controls=1' + '" frameborder="0" allowfullscreen></iframe>';
										$('product-image').insert(iframe);
									}
									function hideProductVideo(imageid) {
										$$('.product-image .product-image-gallery img').invoke('removeClassName', 'visible');
										$(imageid).addClassName('visible');
										if ($('product-image').down("iframe")) {
											$('product-image').down("iframe").remove();
										}
									}
									$mp = jQuery.noConflict();
									$mp(document).ready(function () {
										$mp('.product-image-thumbs').bxSlider({
											infiniteLoop: false,
											hideControlOnEnd: true,
											slideWidth: 300,
											minSlides: 1,
											maxSlides: 4,
											moveSlides: 1,
											slideMargin: 10
										});
									});
								</script>


							</div>
							<div class="product-shop">
								<div class="product-name">
									<span class="h1 pdName" ></span>
								</div>
								<div class="clear"></div>
								<p class="no-rating">
									<i class="fa fa-star-o"></i> &nbsp;
									<i class="fa fa-star-o"></i> &nbsp;
									<i class="fa fa-star-o"></i>&nbsp;
									<i class="fa fa-star-o"></i>&nbsp;
									<i class="fa fa-star-o"></i>&nbsp;&nbsp;
									<span idpdReviews> () Rivews </span>

								</p>
								<div class="clear"></div>
								<div class="product-info-wr ">
									<div class="price-info">
										<div style="clear: both;"></div>

										<p class="availability in-stock">
											<span class="label">
											Availability:</span>
											<span class="value">In Stock</span>
										</p>

										<p class="availability out-stock">
											<span class="label">
											Availability:</span>
											<span class="value">Out Of Stock</span>
										</p>

										<div class="price-box">
											<p class="old-price">
												<span class="price-label">Regular Price:</span>
												<span class="price" id="old-price">
												₹0 </span>
											</p>
											<p class="special-price">
												<span class="price-label">Special Price</span>
												<span class="price" id="product-price">
												₹0 </span>
											</p>
											<span class="saving_options_discount" id="pdDiscount">0% Off</span>
										</div>

										<div class="clear saving_options">
											<span> Save: <span><b class="saving_options_price">₹<span id="pdSave"></span></b>
										</span> </span>
									</div>
									<b>Specifications</b>
									<ui>

										<li id="pdBrand">
											<span class="custom-label">Brand</span>:
											<span class="custom-data"></span>
										</li>
										<li id="pdColor">
											<span class="custom-label">Color</span>:
											<span class="custom-data"></span>
										</li>
										<li id="pdModel">
											<span class="custom-label">Model</span>:
											<span class="custom-data"></span>
										</li>
										<li id="pdModelNo">
											<span class="custom-label">Model Number</span>:
											<span class="custom-data"></span>
										</li>

									</ui>
									<div style="margin-left: 20px;color: steelblue;font-weight: 600;font-size: 12px;"><a class="specifications-more" href="#more-tab-info">See more</a>
									</div>


								</div>
								<div class="product-options-block">
								</div>
								<div class="add-to-cart-wrapper">
									<div class="add-to-box">
										<div class="add-to-cart">
											<div class="qty-wrapper">
												<label for="qty">Qty:</label>
												<a class="decrement_qty" href="javascript:void(0)" style="float: left; font-size: 28px; font-weight: bolder; margin-right: 7px;">
													<div class="middle_icon_color pull-left">
														<i class="icon-minus"> - </i>
													</div>
												</a>
												<input type="text"
												pattern="\d*"
												name="qty"
												id="qtyMain"
												value="1"
												title="Qty" class="input-text qty custom-qty-upadte" />
												<a class="increment_qty" href="javascript:void(0)" style="float: left; font-size: 28px; margin-left: 7px; font-weight: bolder;">
													<div class="middle_icon_color pull-left">
														<i class="icon-plus"> + </i>
													</div>
												</a>
												<span class="unit-pice"> X 1 Unit</span>
											</div>
											<div id="mobile-cart-buttons">
												<div class="add-to-cart-buttons">
													<button type="button"
													title="Add to Cart" class="btn"  id="pdAddToCart">
													<span><span id="add-to-cart-title">Add to Cart</span></span></button>
												</div>
												<div class="add-to-cart-buttons buy-now-button">

													<button type="submit"
													title="Buy Now" class="btn" style="background-color: #46b64f" id="pdBuyNow">
													<span><span>Buy Now</span></span></button>
												</div>

												<div class="add-to-cart-buttons buy-now-button">

													<button type="submit"
													title="Add To Wishlist" class="btn2"  id="pdWishlist">
													<span><span><i class="fa fa-heart-o"></i></span></span></button>
												</div>

											</div>

										</div>
										<div id="cartSuccess2"></div>

										<span class="or">OR</span>

										<ul class="sharing-links">
											<li><a target="_blank" href="#" class="link-email-friend"
												title="Email to a Friend">
											</a></li>
											<li>
												<a href="#" target="_blank"
												title="Share on Facebook" class="link-facebook">
											</a>
										</li>
										<li>
											<a href="#" target="_blank"
											title="Share on Twitter" class="link-twitter">
										</a>
									</li>
									<li>
										<a href="#" target="_blank"
										title="Share on Twitter" class="link-instagram">
									</a>
								</li>

							</ul>
						</div>
					</div>
				</div>

				<div class="clear"></div>
				<div class="short-description">

					<div>
						<h3>Short Description</h3> 
						<span id="shortDescription"></span> 
					</div> 
				</div>
			</div>
			<div class="clearer"></div>

		</div>


	</div>

</div>






</div>
</div>
</div>