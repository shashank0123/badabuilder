 <script type="text/javascript">
   <!-- Banner slider js -->
   APPTHA(document).ready(function($){
    $("#apptha_bannerslider").owlCarousel({
        navigation : true,
        slideSpeed : 500,
        singleItem:true,
        stopOnHover:true,
        autoPlay:true,
        responsiveBaseWidth:window,
        nav: true,
        navigationText: ["<span class='icon-new icon-new-arrow-left7'></span>","<span class='icon-new icon-new-arrow-right7'></span>"]
    });
    $(window).on("load", function() {
        $("#mian-silder-placeholder").hide();
    });
});
</script>

<script>
    jQuery(document).ready(function(){
        jQuery('.megaNav > ul > li').mouseover(function(){
            jQuery('.megaNav > ul > li').removeClass('active1');
            jQuery(this).addClass('active1');
        });
        jQuery('.subNav > ul > li').mouseover(function(){
            jQuery('.subNav > ul > li').removeClass('active2');
            jQuery(this).addClass('active2');
        });
        jQuery('.fullCont').mouseleave(function(){
            jQuery('.subNav > ul > li').removeClass('active2');
            jQuery('.subNav > ul > li:first-child').addClass('active2');
        });
        jQuery('.skip-links > ul > li').mouseover(function(){
            jQuery('.skip-links > ul > li').removeClass('skip-active');
            jQuery(this).addClass('skip-active');
        });
        jQuery('.page-header-container').mouseleave(function(){
            jQuery('.skip-links > ul > li').removeClass('skip-active');
        });

        @if(!empty(session()->get('cartMessage')))
        Swal( <?php echo session()->get('cartMessage')?> );
            @endif

    });
</script>
<script type="text/javascript">
    // document.observe('click', function(e, el) {
    //     if ( ! e.target.descendantOf('header-minicart')) {
    //         if($('header-cart').hasClassName('skip-active')){
    //             $('header-cart').removeClassName('skip-active');
    //         }
    //     }
    // });
    jQuery(function() {
        jQuery(window).scroll(function() {
            if(jQuery(this).scrollTop() > 300) {
                jQuery('#back_top').fadeIn();
            } else {
                jQuery('#back_top').fadeOut();
            }
        });
        jQuery('#back_top').click(function() {
            jQuery('body,html').animate({scrollTop:0},500);
        });
    });
    
    jQuery('.b-lazy').parent().addClass('lli');
    setTimeout(function(){
        var bLazy = new Blazy({
            breakpoints: []
            , success: function(element){
                setTimeout(function(){
                    var parent = element.parentNode;
                    parent.className = parent.className.replace(/\bloading\b/,'');
                }, 200);
            }
        });
    },1000);
    jQuery('body').on('click', '.bx-next,.bx-prev, .brandSlider .slider', function() {
        setTimeout(function(){
            var bLazy = new Blazy({
                breakpoints: []
                , success: function(element){
                    setTimeout(function(){
                        var parent = element.parentNode;
                        parent.className = parent.className.replace(/\bloading\b/,'');
                    }, 200);
                }
            });
        },1000);
    });
</script> 







<script type="text/javascript">
            //quick view function pop up
            $jQno = jQuery.noConflict();
            $jQno(function () {
                $jQno('a[rel*=facebox]').facebox();
            });
            
        </script>
        <script>
            jQuery(document).ready(function () {
                jQuery(".mobileNav").click(function () {
                    if (jQuery(".mobileNav span").hasClass("icon")) {
                        jQuery(".mobileNav span").removeClass('icon').addClass('close-menu');
                    } else {
                        jQuery(".mobileNav span").removeClass('close-menu').addClass('icon');
                    }
                });
            });
        </script>

       <!--  
        <script type="text/javascript">
         //<![CDATA[
         thiscartpopup.afterInit();
         Event.observe(window, "resize", function() {
         	thiscartpopup.positionAll();
         });
         //]]>
     </script>
 -->


            </script>
     <script type="text/javascript">
         //<![CDATA[
         thiscartpopup = new cartpopup();
         thiscartpopup.product = true;
         thiscartpopup.backurl = "http://www.ripplemart.com/building-materials/cement.html";
         thiscartpopup.backname = "Cement";
         thiscartpopup.imageurl = "https://www.ripplemart.com/media/catalog/product/cache/1/small_image/135x/9df78eab33525d08d6e5fb8d27136e95/K/C/KCP_OPC_53_Grade_Cement-min.jpg";
         thiscartpopup.productname = "KCP OPC 53 Grade Cement";
         thiscartpopup.showpopup = false;
         thiscartpopup.emptycart = true;
         thiscartpopup.ajaxenabled = true;
         thiscartpopup.slidespeed = 0.3;
         thiscartpopup.deleteurls = [];
         thiscartpopup.updateurl = "https://www.ripplemart.com/checkout/cart/updatePost/";
         thiscartpopup.showonadd = true;
         thiscartpopup.autoclose = false;
         thiscartpopup.autoclosetime = false;
         thiscartpopup.clickopen = false;
         thiscartpopup.formkey = "bfoMK4kbRxPCc4v5";
         //]]>
     </script>
     <script type="text/javascript">
         //<![CDATA[
         var socialLoginFrm = new VarienForm('form_login', true);
         socialLoginFrm.submit = function () {
         	if (this.validator.validate()) {
         		var form = this.form;
         		doSociallogin(form.id, form.action, 'formSuccess', 'progress_image_login');
         	}
         }.bind(socialLoginFrm);
         var socialRegisFrm = new VarienForm('social_frm_register', true);
         socialRegisFrm.submit = function () {
         	if (this.validator.validate()) {
         		var form = this.form;
         		doSociallogin(form.id, form.action, 'register_error', 'progress_image_register');
         	}
         }.bind(socialRegisFrm);
         var socialforgetFrm = new VarienForm('forget_password_form', true);
         socialforgetFrm.submit = function () {
         	if (this.validator.validate()) {
         		var form = this.form;
         		doSociallogin(form.id, form.action, 'forget_password_error', 'progress_image_forgot');
         	} else {
         		if (typeof window.innerHeight != 'undefined') {
         			var newpostop = Math.round(document.body.offsetTop + ((window.innerHeight - $('header_logo_Div').getHeight())) / 2);
         			var newposleft = Math.round(document.body.offsetLeft + ((window.innerWidth - $('header_logo_Div').getWidth())) / 2);
         			$('header_logo_Div').setStyle({top: newpostop + 'px', left: newposleft + 'px'});
         		} else {
         			var newpostop = Math.round(document.body.offsetTop + ((document.documentElement.offsetHeight - $('header_logo_Div').getHeight())) / 2);
         			var newposleft = Math.round(document.body.offsetLeft + ((document.documentElement.offsetWidth - $('header_logo_Div').getWidth())) / 2);
         			$('header_logo_Div').setStyle({top: newpostop + 'px', left: newposleft + 'px'});
         		}
         	}
         }.bind(socialforgetFrm);
         var socialTwitFrm = new VarienForm('social_tiw_login', true);
         socialTwitFrm.submit = function () {
         	if (this.validator.validate()) {
         		var form = this.form;
         		doSociallogin(form.id, form.action, 'twitter_error', 'progress_image_twitter');
         	}
         }.bind(socialTwitFrm);
         //]]>
     </script>



     <script type="text/javascript">
     	function fblogin() {
     		FB.login(function (response) {
     			FB.getLoginStatus(function (response) {
     				if (response.status === 'connected') {
     					document.getElementById('progress_image_facebooklogin').style.display = "block";
     					login();
     				}
     			});
     		}, {scope: 'email'});
     		return false;
     	}
     	window.fbAsyncInit = function () {
     		FB.init({appId: '1661394007494965', status: true, cookie: true, xfbml: true});
     		/* All the events registered */
     		FB.Event.subscribe('auth.login', function (response) {
         		// do something with response
         		// login();
         	});
     	};
     	
     	function login() {
     		FB.api('/me', function (response) {
     			var fb = $('main_hidden').value;
     			if (fb == 1) {
     				window.location.href = 'https://www.ripplemart.com/sociallogin/index/fblogin?fb=1';
     			} else {
     				window.location.href = 'https://www.ripplemart.com/sociallogin/index/fblogin';
     			}
     		});
     	}
     	$('form_login').observe('keypress', socialkeypressHandler);
     	function socialkeypressHandler(event) {
     		var key = event.which || event.keyCode;
     		switch (key) {
     			default:
     			break;
     			case Event.KEY_RETURN:
     			$('slogin').click();
     			break;
     		}
     	}
     	$('social_popup_main').observe('keypress', socialsignupkeypressHandler);
     	function socialsignupkeypressHandler(event) {
     		var key = event.which || event.keyCode;
     		switch (key) {
     			default:
     			break;
     			case Event.KEY_RETURN:
     			$('sregister').click();
     			break;
     		}
     	}
     	function googlepost() {
     		var fb = $('main_hidden').value;
     		if (fb == 1) {
     			javascript:location.href = 'http://www.ripplemart.com/sociallogin/index/googlepost?fb=1/';
     		} else {
     			javascript:location.href = 'http://www.ripplemart.com/sociallogin/index/googlepost/';
     		}
     	}
     	function yahoopost() {
     		var fb = $('main_hidden').value;
     		if (fb == 1) {
     			javascript:location.href = 'http://www.ripplemart.com/sociallogin/index/yahoopost?fb=1/';
     		} else {
     			javascript:location.href = 'http://www.ripplemart.com/sociallogin/index/yahoopost/';
     		}
     	}

     </script>
     <script>
     	jQuery('#forget_password').on('input', function (e) {
     		jQuery('#advice-required-entry-password').remove();
     		jQuery('#advice-required-entry-email').remove();
     	});
     	jQuery('#forget_password').keypress(function (e) {
     		var key = e.which;
         	if (key == 13) // the enter key code
         	{
         		jQuery('#advice-required-entry-password').remove();
         		jQuery('#advice-required-entry-email').remove();
         	}
         });
     	jQuery('#email').keypress(function (e) {
     		var key = e.which;
         	if (key == 13) // the enter key code
         	{
         		jQuery('#advice-required-entry-forget_password').remove();
         	}
         });
     	jQuery('#password').keypress(function (e) {
     		var key = e.which;
         	if (key == 13) // the enter key code
         	{
         		jQuery('#advice-required-entry-forget_password').remove();
         	}
         });
     </script>
     <script type="text/javascript">
     	jQuery(document).ready(function () {
     		jQuery('#sme-register').click(function () {
     			jQuery('#register_type').val('1');
     			jQuery('#header_logo_Div').addClass('sme-form');
     			jQuery('.sme-company').show();
     			jQuery('.sme-banner-image').show();
     			jQuery('.login-banner-image').hide();
     		});
     		jQuery('.closeLink').click(function () {
     			jQuery('#header_logo_Div').removeClass('sme-form');
     			jQuery('.sme-company').hide();
     			jQuery('.sme-banner-image').hide();
     			jQuery('.login-banner-image').show();
     		});
     	});
     </script>

     <script type="text/javascript">
//<![CDATA[
var discountForm = new VarienForm('discount-coupon-form');
discountForm.submit = function (isRemove) {
    if (isRemove) {
        $('coupon_code').removeClassName('required-entry');
        $('remove-coupone').value = "1";
    } else {
        $('coupon_code').addClassName('required-entry');
        $('remove-coupone').value = "0";
    }
    return VarienForm.prototype.submit.bind(discountForm)();
}
//]]>
</script>

<script type="text/javascript">
        //<![CDATA[
        var coShippingMethodForm = new VarienForm('shipping-zip-form');
        var countriesWithOptionalZip = ["HK","IE","MO","PA"];

        coShippingMethodForm.submit = function () {
            var country = $F('country');
            var optionalZip = false;

            for (i=0; i < countriesWithOptionalZip.length; i++) {
                if (countriesWithOptionalZip[i] == country) {
                    optionalZip = true;
                }
            }
            if (optionalZip) {
                $('postcode').removeClassName('required-entry');
            }
            else {
                $('postcode').addClassName('required-entry');
            }
            return VarienForm.prototype.submit.bind(coShippingMethodForm)();
        }
        //]]>
    </script>

   

 <script>
    jQuery(document).ready(function(){
        jQuery(".block-compare-block").mouseover(function(){
            jQuery(".block-compare-block-add").show();

        });
        jQuery(".block-compare-block").mouseout(function(){
            jQuery(".block-compare-block-add").hide();

        });
    });
</script>

<script type="text/javascript">
    $mp = jQuery.noConflict();
    $mp(document).ready(function(){
        $mp('.slider-carousel').bxSlider({
            slideWidth: 300,
            minSlides: 2,
            maxSlides: 4,
            moveSlides: 1,
            slideMargin: 10
        });
    });
</script>

<script>
   jQuery(document).ready(function(){
    jQuery(".requestCall").click(function(){
        jQuery("#backgroundpopup-call").show();
        jQuery("#callback_information").show();
    });
});
</script>


<script type="text/javascript">
   $mp = jQuery.noConflict();
   $mp(document).ready(function () {
                     // Checking whether desktop or not
                     if (window.screen.width >= 625) {
                       $mp('.portfolioSlider .products-grid').bxSlider({
                           infiniteLoop: false,
                           hideControlOnEnd: true,
                           minSlides: 1,
                     // maxSlides: 8,
                     moveSlides: 1,
                     slideWidth: 275,
                     slideMargin: 14,
                     responsive: true,
                     touchEnabled: true
                 });
                       $mp('.brandSlider .products-grid').bxSlider({
                           infiniteLoop: false,
                           hideControlOnEnd: true,
                           minSlides: 1,
                           maxSlides: 5,
                           moveSlides: 1,
                           slideWidth: 200,
                           slideMargin: 15,
                           responsive: true,
                           touchEnabled: true,
                           auto: true,
                           onSlideNext: function (currentSlideHtmlObject, oldIndex, newIndex) {
                               var contNew = newIndex;
                               $mp('.brandSlider .slider').removeClass('active');
                               $mp(".auto-slide-" + contNew).addClass('active');
                               var bsID = $mp(".auto-slide-" + contNew).attr("id");
                               var bsIDCont = '#' + bsID + 'Cont';
                               if (!$mp(bsIDCont).is(':visible')) {
                                   $mp('.brandSlider .cont').slideUp();
                                   $mp(bsIDCont).slideDown();
                               }
                               var bLazy = new Blazy({
                                   breakpoints: []
                                   , success: function (element) {
                                       setTimeout(function () {
                                           var parent = element.parentNode;
                                           parent.className = parent.className.replace(/\bloading\b/, '');
                                       }, 200);
                                   }
                               });
                           },
                           onSlidePrev: function (currentSlideHtmlObject, oldIndex, newIndex) {
                               var contNew = newIndex;
                               $mp('.brandSlider .slider').removeClass('active');
                               $mp(".auto-slide-" + contNew).addClass('active');
                               var bsID = $mp(".auto-slide-" + contNew).attr("id");
                               var bsIDCont = '#' + bsID + 'Cont';
                               if (!$mp(bsIDCont).is(':visible')) {
                                   $mp('.brandSlider .cont').slideUp();
                                   $mp(bsIDCont).slideDown();
                               }
                               var bLazy = new Blazy({
                                   breakpoints: []
                                   , success: function (element) {
                                       setTimeout(function () {
                                           var parent = element.parentNode;
                                           parent.className = parent.className.replace(/\bloading\b/, '');
                                       }, 200);
                                   }
                               });
                           }
                       });
                   }else {
                       $mp('.portfolioSlider .products-grid').bxSlider({
                           infiniteLoop: false,
                           hideControlOnEnd: true,
                           minSlides: 1,
                           maxSlides: 8,
                           moveSlides: 1,
                           slideWidth: 128,
                           slideMargin: 13,
                           responsive: true
                       });
                       $mp('.brandSlider .products-grid').bxSlider({
                           infiniteLoop: false,
                           hideControlOnEnd: true,
                           minSlides: 1,
                           maxSlides: 1,
                           moveSlides: 1,
                           slideWidth: 208,
                           slideMargin: 15,
                           responsive: true,
                           auto: true,
                           onSlideNext: function (currentSlideHtmlObject, oldIndex, newIndex) {
                               var contNew = newIndex;
                               $mp('.brandSlider .slider').removeClass('active');
                               $mp(".auto-slide-" + contNew).addClass('active');
                               var bsID = $mp(".auto-slide-" + contNew).attr("id");
                               var bsIDCont = '#' + bsID + 'Cont';
                               if (!$mp(bsIDCont).is(':visible')) {
                                   $mp('.brandSlider .cont').slideUp();
                                   $mp(bsIDCont).slideDown();
                               }
                               var bLazy = new Blazy({
                                   breakpoints: []
                                   , success: function (element) {
                                       setTimeout(function () {
                                           var parent = element.parentNode;
                                           parent.className = parent.className.replace(/\bloading\b/, '');
                                       }, 200);
                                   }
                               });
                           },
                           onSlidePrev: function (currentSlideHtmlObject, oldIndex, newIndex) {
                               var contNew = newIndex;
                               $mp('.brandSlider .slider').removeClass('active');
                               $mp(".auto-slide-" + contNew).addClass('active');
                               var bsID = $mp(".auto-slide-" + contNew).attr("id");
                               var bsIDCont = '#' + bsID + 'Cont';
                               if (!$mp(bsIDCont).is(':visible')) {
                                   $mp('.brandSlider .cont').slideUp();
                                   $mp(bsIDCont).slideDown();
                               }
                               var bLazy = new Blazy({
                                   breakpoints: []
                                   , success: function (element) {
                                       setTimeout(function () {
                                           var parent = element.parentNode;
                                           parent.className = parent.className.replace(/\bloading\b/, '');
                                       }, 200);
                                   }
                               });
                           }});
                   }
                   /*brand slider active*/
                   $mp('.brandSlider .slider:first').addClass('active');
               });
           </script>

           <!-- For Tab Panel -->
           <script type="text/javascript">
               jQuery(document).ready(function(){
                jQuery(".tabsProduct li a").click(function() {

        // Active state for tabsProduct
        jQuery(".tabsProduct li a").removeClass("active");
        jQuery(this).addClass("active");
        
        // Active state for TabsProduct Content
        jQuery(".tab_content_container > .tab_content_active").removeClass("tab_content_active").fadeOut(200);
        jQuery(this.rel).fadeIn(500).addClass("tab_content_active");
        
    }); 

            });

        </script>

        <script type="text/javascript">
           jQuery(document).ready(function(){
            jQuery(".tabsProduct2 li a").click(function() {

        // Active state for tabsProduct
        jQuery(".tabsProduct2 li a").removeClass("active");
        jQuery(this).addClass("active");
        
        // Active state for TabsProduct Content
        jQuery(".tab_content_container > .tab_content_active2").removeClass("tab_content_active2").fadeOut(200);
        jQuery(this.rel).fadeIn(500).addClass("tab_content_active2");
        
    }); 

        });

    </script>


    <script>
        var current_url = "https://www.ripplemart.com/electrical.html";
                                    //<![CDATA[
                                    (function(window,undefined){
                                    // Prepare our Variables
                                    var
                                    History = window.History,
                                    State = History.getState(),
                                    document = window.document,
                                    FILTER = {},
                                    loading_html = '<div id="sticky-referrer" class="browse-vd newvd" style="position: relative;">'
                                    +'<div class="loadingWrapper" style="display: none;">'
                                    +'<div class="loading"></div>'
                                    +'<div class="content">'
                                    +'<img src="https://www.ripplemart.com/skin/frontend/base/default/images/apptha/productfilters/ajax-loader-tr.png" alt="">'
                                    +'<span class="text">Loading...</span>'
                                    +'</div>'
                                    +'</div>'
                                    +'</div>',
                                    isAjax = true;
                                    // Check to see if History.js is enabled for our Browser
                                    if ( !History.enabled ) {
                                        isAjax = false;
                                    }
                                    // Bind to State Change
                                    History.Adapter.bind(window,'statechange',function(){ // Note: We are using statechange instead of popstate
                                    // Log the State
                                    var State = History.getState(); // Note: We are using History.getState() instead of event.state
                                    //History.log('statechange:', State.data, State.title, State.url);
                                    sendAjax(State.url);
                                });
                                    function sendAjax(url) {
                                    //loadingWrapper
                                    new Ajaxreq.Request(url,{
                                        method:'get',
                                        action_content: ['product_list','catalog.leftnav','search_result_list','catalogsearch.leftnav'],
                                        onSuccess: function(transport,json) {
                                            var response = transport.responseJSON;
                                            if(response){
                                                if (product = response['action_content_data']['product_list']) {
                                                    var categoryProducts = $$('div.category-products');
                                                    if(categoryProducts[0]){
                                                        categoryProducts[0].remove();
                                                    }
                                                    var noProduct = $$('p.note-msg');
                                                    if(noProduct[0]){
                                                        noProduct[0].remove();
                                                    }
                                                    var categoryProducts = $$('div.col-main');
                                                    categoryProducts[0].insert(product);
                                                }
                                                if (product = response['action_content_data']['catalog.leftnav']) {
                                                    $('layer_ajax_filter').update(product);
                                                    updateDOM();
                                                }
                                                if (product = response['action_content_data']['search_result_list']) {
                                                    var categoryProducts = $$('div.category-products');
                                                    if(categoryProducts[0]){
                                                        categoryProducts[0].remove();
                                                    }
                                                    var noProduct = $$('p.note-msg');
                                                    if(noProduct[0]){
                                                        noProduct[0].remove();
                                                    }
                                                    var categoryProducts = $$('div.col-main');
                                                    categoryProducts[0].insert(product);
                                                }
                                                if (product = response['action_content_data']['catalogsearch.leftnav']) {
                                                    $('layer_ajax_filter').update(product);
                                                    updateDOM();
                                                }
                                            }
                                        },
                                        onFailure: function() {
                                            alert('Something went wrong...');
                                        },
                                        onCreate: function(){
                                            $$('div.loadingWrapper')[0].show();
                                        },
                                        onComplete: function(){
                                            $$('div.loadingWrapper')[0].hide();
                                        }
                                    });
                                }
                                var filter_array = {"dir":["asc"],"order":["name"]};
                                var title = document.getElementsByTagName("title")[0].innerHTML;
                                var formUrl = function (urlData){
                                    //console.log(urlData);
                                    var _urlArray = []
                                    for(var _item in urlData){
                                        if(urlData[_item].length){
                                            _urlArray.push(_item+'='+urlData[_item].join(','));
                                        }
                                    }
                                    var _urlString = '';
                                    if(_urlArray.length){
                                        _urlString = '?'+_urlArray.join('&');
                                    }
                                    //console.log(_urlString);
                                    var url = current_url+_urlString;
                                    if(!isAjax){
                                        window.location.href = url;
                                        return;
                                    }
                                    History.pushState(_urlArray,title,url);
                                }
                                var updateDOM = function(){
                                    //update the checkbox
                                    var accordion = new Accordion('narrow-by-list', {
                                        startHidden: false,
                                        mutuallyExclusive: false
                                    });
                                    $$('input.filter_options').each(function(input,i){
                                        $(input).observe('click', function(e){
                                            var $this = $(this);
                                            var attributeName = $this.readAttribute('name');
                                            var attributeValue = $this.value;
                                            var attributeArr = filter_array[attributeName];
                                            if(this.checked){
                                                if(attributeArr){
                                                    filter_array[attributeName].push(attributeValue);
                                                }else{
                                                    filter_array[attributeName] = [attributeValue];
                                                }
                                            }else{
                                                if(attributeArr){
                                                    var index = attributeArr.indexOf(attributeValue);
                                                    if (index > -1) {
                                                        filter_array[attributeName].splice(index, 1);
                                                    }
                                                }
                                            }
                                            formUrl(filter_array);
                                        });
                                    });
                                    //update the remove link
                                    $$('div.currently a.btn-remove').each(function(input,i){
                                        $(input).observe('click', function(e){
                                            e.preventDefault();
                                            var $this = $(this);
                                            var attributeName = $this.readAttribute('data-name');
                                            var attributeValue = $this.readAttribute('data-value');
                                            var attributeArr = filter_array[attributeName];
                                            if(attributeArr){
                                                var index = attributeArr.indexOf(attributeValue);
                                                if (index > -1) {
                                                    filter_array[attributeName].splice(index, 1);
                                                }
                                                if(filter_array['prev_cat'] && attributeName == 'cat' ){
                                                    var index = filter_array['prev_cat'].indexOf(attributeValue);
                                                    if (index > -1) {
                                                        filter_array['prev_cat'].splice(index, 1);
                                                    }
                                                    var new_cat_value = filter_array['prev_cat'][filter_array['prev_cat'].length-1];
                                                    if(new_cat_value){
                                                        filter_array[attributeName] = [new_cat_value];
                                                    }
                                                }
                                            }
                                            formUrl(filter_array);
                                        });
                                    });
                                    $$('div.ajaxloader a.ajaxfilter').each(function(input,i){
                                        $(input).observe('click', function(e){
                                            e.preventDefault();
                                            var $this = $(this);
                                            var attributeName = $this.readAttribute('name');
                                            var attributeValue = $this.readAttribute('value');
                                            var attributeArr = filter_array[attributeName];
                                            if(attributeName == 'cat'){
                                                if(filter_array['prev_cat']){
                                                    filter_array['prev_cat'].push(attributeValue);
                                                }else{
                                                    filter_array['prev_cat'] = [attributeValue];
                                                }
                                            }
                                            filter_array[attributeName] = [attributeValue];
                                            formUrl(filter_array);
                                        });
                                    });
                                    //update the clear all link
                                    $$('div.actions a.clearAll').each(function(input,i){
                                        $(input).observe('click', function(e){
                                            e.preventDefault();
                                            if(filter_array.q){
                                                filter_array = {'q':filter_array.q};
                                            }else{
                                                filter_array = {};
                                            }
                                            formUrl(filter_array);
                                        });
                                    });
                                }
                                updateDOM();
                                window.FILTER = {
                                    'filter_array':filter_array,
                                    'formUrl':formUrl
                                };
                                $$('div.main')[0].insert(loading_html);
                            })(window);
                        </script>
                        <script type="text/javascript">
                            jQuery(document).ready(function(){
                                jQuery('.section').removeClass('expanded');
                                jQuery('.toggle').hide();
                                jQuery('.accor').addClass('minus');
                                jQuery('.sub-cat-list').hide();
                            });
                        </script>



                        <script>
     // Add to Cart
     function addToCart(id){
      var CSRF_TOKEN = jQuery('meta[name="csrf-token"]').attr('content');
        // Swal(id);
        var quantity = jQuery('#qty'+id).val();


        jQuery.ajax({
         url: '/add-to-cart',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, quantity: quantity},
         success: function (data) {
           // Swal(data.cartItem);
           jQuery('#cartItem').html(data.cartItem);
           jQuery('#cartItem2').html(data.cartItem);

           jQuery('#cartSuccess').text('Product added to cart successfully');
           jQuery("#cartSuccess").hide().slideDown();
           setTimeout(function(){
              $("#cartSuccess").hide();        
          }, 3000);
       },
       failure: function (data) {
           Swal(data.message);
       }
   });
    }

   // Add to Wishlist
   function addToWishlist(id){
      var CSRF_TOKEN = jQuery('meta[name="csrf-token"]').attr('content');
      userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?> ;

      if(userId == 0){
        Swal('Please Login First');
    }
    else{
        // Swal(id);

        jQuery.ajax({
         url: '/add-to-wishlist',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, userId : userId},
         success: function (data) {
           // Swal(data.wishItem);
           jQuery('#wishItem').html(data.wishItem);
           jQuery('#wishItem2').html(data.wishItem);

           jQuery('#cartSuccess').text('Product added to wishlist');
           jQuery("#cartSuccess").hide().slideDown();
           setTimeout(function(){
              $("#cartSuccess").hide();        
          }, 3000);

       },
       failure: function (data) {
           Swal(data);
       }
   });
    }
}

</script>


<script type="text/javascript">
    function deleteWishlistItem(id){
          var CSRF_TOKEN = jQuery('meta[name="csrf-token"]').attr('content');
          user_id = @if(!empty(Auth::user())){{Auth::user()->id}}@else{{'0'}}@endif;
          // alert(id+" / "+user_id);

          jQuery.ajax({
             url: '/delete-wishlist-item',
             type: 'POST',
             data: {_token: CSRF_TOKEN, id: id, user_id : user_id},
             success: function (data) {
               // alert(data.flag);
               jQuery('#cartItem').html(data.cartItem);
               jQuery('#wishItem').html(data.wishItem);

                jQuery('#div-list'+id).hide();
           },
           failure: function (data) {
               // alert(data.message);
           }
       });
      }
</script>


<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("input:radio[name='tier']").change(function () {
            var _val = jQuery(this).val();
            jQuery('#qty').val(_val);
            var price = jQuery(this).attr('price');
            var savePercent = jQuery(this).attr('save');
            var discount = jQuery(this).attr('discount');
            var oldprice = jQuery(this).attr('oldprice');
            var GST = jQuery(this).attr('gstprice');
            jQuery('.special-price .price').html('₹' + price);
            jQuery('.regular-price .price').html('₹' + price);
            jQuery('.saving_options_discount').html(savePercent + '% Off');
            jQuery('.saving_options_price').html('₹' + discount);
            jQuery('.old-price .price').html('₹' + oldprice);
            if ((jQuery(this).is(":checked"))) {
                jQuery(this).closest('.tier-price-custom').removeClass('tier-price-custom-active').addClass('blockStyle');
            }
            jQuery('.tier-price-custom-active').removeClass('blockStyle');
            jQuery('.tier-price-custom').addClass('tier-price-custom-active');
            jQuery('.price-with-gst').html('₹' + GST);
        });
    });
</script>


<script type="text/javascript">
                jQuery(document).ready(function(){
                    jQuery(".specifications-more").click(function(){
                        jQuery(".collateral-tabs .tab-container").removeClass("current");
                        jQuery("div.product-collateral").addClass("accordion-open");
                        jQuery(".product-collateral ul li").removeClass("current");
                        jQuery(".Specifications-tab").addClass("current");
                        jQuery(".product-collateral ul li").has('span:contains(Specifications)').addClass("current");
                    });
                });
            </script> <!--timer starts-->

            <script>
    $jno = jQuery.noConflict();
    $jno('span.read-more').click(function(){
        $jno(this).prev('p.product_review').css({
            'height' : 'auto',
            'overflow' : 'visible'
        });
        $jno(this).prev('p.product_review').next('span.read-more').css({
            'display' : 'none'
        });
    });
</script>

<script type="text/javascript">
//<![CDATA[
var dataForm = new VarienForm('review-form');
Validation.addAllThese(
    [
    ['rating', 'Please select one of each of the ratings above', function(v) {
        var trs = $('product-review-table').select('tr');
        var inputs;
        var error = 1;
        for( var j=0; j < trs.length; j++ ) {
            var tr = trs[j];
            if( j > 0 ) {
                inputs = tr.select('input');
                for( i in inputs ) {
                    if( inputs[i].checked == true ) {
                        error = 0;
                    }
                }
                if( error == 1 ) {
                    return false;
                } else {
                    error = 1;
                }
            }
        }
        return true;
    }]
    ]
    );
//]]>
</script>

<script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery(".be-the-review").click(function () {
// alert(jQuery(".product-collateral ul li:nth-child(2)").text());
jQuery(".collateral-tabs .tab-container").removeClass("current");
jQuery(".product-collateral ul li").removeClass("current");
jQuery("div.product-collateral").addClass("accordion-open");
jQuery(".Reviews-tab").addClass("current");
jQuery(".product-collateral ul li").has('span:contains(Reviews)').addClass("current");
});
        });
    </script>



    <script type="text/javascript">
//<![CDATA[
jQuery(document).ready(function () {
    jQuery('.increment_qty').click(function () {
        var oldVal = jQuery(this).parent().find("input").val();
        if (parseFloat(oldVal) >= 1) {
            var newVal = parseFloat(oldVal) + 1;
            jQuery(this).parent().find("input").val(newVal);
            var firstTire = jQuery('.tier-0 :input[type="radio"]').val();
            if (firstTire > newVal) {
                jQuery('.tier-price :input[type="radio"]').prop('checked', false);
                jQuery('.tier-price-custom-active').removeClass('blockStyle');
                jQuery('.tier-price-custom').addClass('tier-price-custom-active');
                var savePercent = jQuery('.old-original-price').attr('save');
                var priceold = jQuery('.old-original-price').attr('price');
                var discount = jQuery('.old-original-price').attr('discount');
                var oldprice = jQuery('.old-original-price').attr('oldprice');
                var GST = jQuery('.old-original-price').attr('gstprice');
                jQuery('.special-price .price').html('₹' + priceold);
                jQuery('.regular-price .price').html('₹' + priceold);
                jQuery('.saving_options_discount').html(savePercent + '% Off');
                jQuery('.saving_options_price').html('₹' + discount);
                jQuery('.old-price .price').html('₹' + oldprice);
                jQuery('.price-with-gst').html('₹' + GST);
            }
            jQuery('.tier-price :input[value=' + newVal + ']').closest('.tier-price-custom').removeClass('tier-price-custom-active').addClass('blockStyle');
            var price = jQuery('.tier-price :input[value=' + newVal + ']').attr('price');
            if (typeof price !== "undefined") {
                jQuery('.tier-price :input[type="radio"]').prop('checked', false);
                jQuery('.tier-price :input[value=' + newVal + ']').prop('checked', true);
                jQuery('.tier-price-custom-active').removeClass('blockStyle');
                jQuery('.tier-price-custom').addClass('tier-price-custom-active');
                var savePercent = jQuery('.tier-price :input[value=' + newVal + ']').attr('save');
                var discount = jQuery('.tier-price :input[value=' + newVal + ']').attr('discount');
                var oldprice = jQuery('.tier-price :input[value=' + newVal + ']').attr('oldprice');
                var GST = jQuery('.tier-price :input[value=' + newVal + ']').attr('gstprice');
                jQuery('.special-price .price').html('₹' + price);
                jQuery('.regular-price .price').html('₹' + price);
                jQuery('.saving_options_discount').html(savePercent + '% Off');
                jQuery('.saving_options_price').html('₹' + discount);
                jQuery('.old-price .price').html('₹' + oldprice);
                jQuery('.price-with-gst').html('₹' + GST);
            }
        }
    });
    jQuery('.decrement_qty').click(function () {
        var oldVal = jQuery(this).parent().find("input").val();
        if (parseFloat(oldVal) >= 2) {
            var newVal = parseFloat(oldVal) - 1;
            jQuery(this).parent().find("input").val(newVal);
            var firstTire = jQuery('.tier-0 :input[type="radio"]').val();
            if (firstTire > newVal) {
                jQuery('.tier-price :input[type="radio"]').prop('checked', false);
                jQuery('.tier-price-custom-active').removeClass('blockStyle');
                jQuery('.tier-price-custom').addClass('tier-price-custom-active');
                var savePercent = jQuery('.old-original-price').attr('save');
                var priceold = jQuery('.old-original-price').attr('price');
                var discount = jQuery('.old-original-price').attr('discount');
                var oldprice = jQuery('.old-original-price').attr('oldprice');
                var GST = jQuery('.old-original-price').attr('gstprice');
                jQuery('.special-price .price').html('₹' + priceold);
                jQuery('.regular-price .price').html('₹' + priceold);
                jQuery('.saving_options_discount').html(savePercent + '% Off');
                jQuery('.saving_options_price').html('₹' + discount);
                jQuery('.old-price .price').html('₹' + oldprice);
                jQuery('.price-with-gst').html('₹' + GST);
            }
            var price = jQuery('.tier-price :input[nextqty=' + newVal + ']').attr('price');
            if (typeof price !== "undefined") {
                jQuery('.tier-price :input[type="radio"]').prop('checked', false);
                jQuery('.tier-price :input[nextqty=' + newVal + ']').prop('checked', true);
                jQuery('.tier-price :input[nextqty=' + newVal + ']').closest('.tier-price-custom').removeClass('tier-price-custom-active').addClass('blockStyle');
                jQuery('.tier-price-custom-active').removeClass('blockStyle');
                jQuery('.tier-price-custom').addClass('tier-price-custom-active');
                var savePercent = jQuery('.tier-price :input[nextqty=' + newVal + ']').attr('save');
                var discount = jQuery('.tier-price :input[nextqty=' + newVal + ']').attr('discount');
                var oldprice = jQuery('.tier-price :input[nextqty=' + newVal + ']').attr('oldprice');
                var GST = jQuery('.tier-price :input[nextqty=' + newVal + ']').attr('gstprice');
                jQuery('.special-price .price').html('₹' + price);
                jQuery('.regular-price .price').html('₹' + price);
                jQuery('.saving_options_discount').html(savePercent + '% Off');
                jQuery('.saving_options_price').html('₹' + discount);
                jQuery('.old-price .price').html('₹' + oldprice);
                jQuery('.price-with-gst').html('₹' + GST);
            }
        }
    });
    jQuery('.add-to-cart-buttons').click(function () {
        var qty = jQuery('.custom-qty-upadte').val();
        if ((qty == "0") || (qty == "")) {
            jQuery('.custom-qty-upadte').val(1);
        }
    });
    jQuery(".custom-qty-upadte").change(function () {
        var newVal = parseFloat(jQuery(".custom-qty-upadte").val());
        var firstTire = parseFloat(jQuery('.tier-0 :input[type="radio"]').val());
        if (firstTire > newVal) {
            jQuery('.tier-price :input[type="radio"]').prop('checked', false);
            jQuery('.tier-price-custom-active').removeClass('blockStyle');
            jQuery('.tier-price-custom').addClass('tier-price-custom-active');
            var savePercent = jQuery('.old-original-price').attr('save');
            var priceold = jQuery('.old-original-price').attr('price');
            var discount = jQuery('.old-original-price').attr('discount');
            var oldprice = jQuery('.old-original-price').attr('oldprice');
            var GST = jQuery('.old-original-price').attr('gstprice');
            jQuery('.special-price .price').html('₹' + priceold);
            jQuery('.regular-price .price').html('₹' + priceold);
            jQuery('.saving_options_discount').html(savePercent + '% Off');
            jQuery('.saving_options_price').html('₹' + discount);
            jQuery('.old-price .price').html('₹' + oldprice);
            jQuery('.price-with-gst').html('₹' + GST);
        }
        var price = jQuery('.tier-price :input[value=' + newVal + ']').attr('price');
        if (typeof price !== "undefined") {
            jQuery('.tier-price :input[type="radio"]').prop('checked', false);
            jQuery('.tier-price :input[value=' + newVal + ']').prop('checked', true);
            jQuery('.tier-price :input[value=' + newVal + ']').closest('.tier-price-custom').removeClass('tier-price-custom-active').addClass('blockStyle');
            jQuery('.tier-price-custom-active').removeClass('blockStyle');
            jQuery('.tier-price-custom').addClass('tier-price-custom-active');
            var savePercent = jQuery('.tier-price :input[value=' + newVal + ']').attr('save');
            var discount = jQuery('.tier-price :input[value=' + newVal + ']').attr('discount');
            var oldprice = jQuery('.tier-price :input[value=' + newVal + ']').attr('oldprice');
            var GST = jQuery('.tier-price :input[value=' + newVal + ']').attr('gstprice');
            jQuery('.special-price .price').html('₹' + price);
            jQuery('.regular-price .price').html('₹' + price);
            jQuery('.saving_options_discount').html(savePercent + '% Off');
            jQuery('.saving_options_price').html('₹' + discount);
            jQuery('.old-price .price').html('₹' + oldprice);
            jQuery('.price-with-gst').html('₹' + GST);
        } else {
            var lastTire = parseFloat(jQuery('.tier-price .last-tier').val());
            if (newVal > lastTire) {
                var price = jQuery('.tier-price .last-tier').attr('price');
                if (typeof price !== "undefined") {
                    jQuery('.tier-price :input[type="radio"]').prop('checked', false);
                    jQuery('.tier-price .last-tier').prop('checked', true);
                    jQuery('.tier-price .last-tier').closest('.tier-price-custom').removeClass('tier-price-custom-active').addClass('blockStyle');
                    jQuery('.tier-price-custom-active').removeClass('blockStyle');
                    jQuery('.tier-price-custom').addClass('tier-price-custom-active');
                    var savePercent = jQuery('.tier-price .last-tier').attr('save');
                    var discount = jQuery('.tier-price .last-tier').attr('discount');
                    var oldprice = jQuery('.tier-price .last-tier').attr('oldprice');
                    var GST = jQuery('.tier-price .last-tier').attr('gstprice');
                    jQuery('.special-price .price').html('₹' + price);
                    jQuery('.regular-price .price').html('₹' + price);
                    jQuery('.saving_options_discount').html(savePercent + '% Off');
                    jQuery('.saving_options_price').html('₹' + discount);
                    jQuery('.old-price .price').html('₹' + oldprice);
                    jQuery('.price-with-gst').html('₹' + GST);
                }
            } else {
                var i;
                for (i = newVal; i > 0; i--) {
                    var price = jQuery('.tier-price :input[value=' + i + ']').attr('price');
                    if (typeof price !== "undefined") {
                        jQuery('.tier-price :input[type="radio"]').prop('checked', false);
                        jQuery('.tier-price :input[value=' + i + ']').prop('checked', true);
                        jQuery('.tier-price :input[value=' + i + ']').closest('.tier-price-custom').removeClass('tier-price-custom-active').addClass('blockStyle');
                        jQuery('.tier-price-custom-active').removeClass('blockStyle');
                        jQuery('.tier-price-custom').addClass('tier-price-custom-active');
                        var savePercent = jQuery('.tier-price :input[value=' + i + ']').attr('save');
                        var discount = jQuery('.tier-price :input[value=' + i + ']').attr('discount');
                        var oldprice = jQuery('.tier-price :input[value=' + i + ']').attr('oldprice');
                        var GST = jQuery('.tier-price :input[value=' + i + ']').attr('gstprice');
                        jQuery('.special-price .price').html('₹' + price);
                        jQuery('.regular-price .price').html('₹' + price);
                        jQuery('.saving_options_discount').html(savePercent + '% Off');
                        jQuery('.saving_options_price').html('₹' + discount);
                        jQuery('.old-price .price').html('₹' + oldprice);
                        jQuery('.price-with-gst').html('₹' + GST);
                        break;
                    }
// alert(price);
} // break;
}
}
}).change();
});
//]]>
jQuery('#product_addtocart_form').on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        return false;
    }
});


function quickViewPopUp(){
    jQuery('#quickViewPopUp').show();
}
</script>


<script type="text/javascript">
    
    function showMiniCart(){
        jQuery('#cartpopup').show()
    }

</script>