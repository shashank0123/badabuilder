
@extends('layouts.front')

@section('content')


<div class="container breadcrumbs ">
   <ul>
      <li class="home">
         <a href="{{ url('/') }}" title="Go to Home Page">Home</a>
         <span>/ </span>
      </li>
      <li class="category3134">
         <strong>{{$category ?? ''}}</strong>
      </li>
   </ul>
</div>
<div class="container">
   <div class="row">
      <div class="col-sm-3">



      </div>
      <div class="col-sm-9">

         <div class="row sortDiv">
            <select name="sort" id="sort">
               <option value=""> Sort By </option>
               <option value="low-high"> Name (A-Z) </option>
               <option value="high-low"> Name (Z-A) </option>
               <option value="price"> Price  </option>
               <option value="new"> Latest  </option>
               
            </select>
         </div>



         <div class="container-fluid " id="BestSell">

            @if(!empty($keyword))

            <h3>Searched results for '<i><b>{{$keyword}}</b></i>'</h3>
            @endif

            <div class="tab_content tab_content_active">

               <div class="row">

                  @if(count($searched_products)>0)
                  @php $countP=0 @endphp
                  @foreach($searched_products as $product)
                  @php $countP++ @endphp
                  <div class="col-sm-4">
                     <div class="card">
                        <h4 class="productName">{{$product->name ?? ''}}</h4>
                        <img src="{{asset($product->image1)}}" class="product-image" alt="{{$product->name ?? ''}}">

                        <p class="productPrice">
                           Rs. {{$product->sell_price ?? '0'}}

                           <i class="fa fa-shopping-cart" id="shpping-cart"></i>
                        </p>
                        <p class="quickView">
                           <i class="fa fa-search"></i> Quick View
                           <span class="right"><i class="fa fa-heart-o"></i>Add To Wishlist</span>
                        </p>

                     </div>
                  </div>
                  @endforeach
                  @if($countP == 0)
                  <div style="text-align: center; padding: 50px">No products available</div>
                  @endif
                  @endif



               </div>
            </div>
         </div>


      </div>
   </div>
</div>
@endsection