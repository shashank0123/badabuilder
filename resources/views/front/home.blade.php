@extends('layouts.front')

@section('content')


<div class="col-main">
                 
                     @if(count($banners)>0)
                     <div id="apptha_bannerslider" class="owl-carousel">
                        @foreach($banners as $banner)
                        <div class="item"><a
                           href="{{ asset($banner->image_url) }}"><img
                           src="{{ asset($banner->image_url) }}"
                           alt="" /></a></div>
                           @endforeach

                        </div>
                        @endif

                        @if(count($allCategories)>0)
                        <div class="portfolioSlider">
                           <div class="products-grid">
                              @foreach($allCategories as $category)
                              <div class="slider item">
                                 <div class="product_image_div">
                                    <a href="{{ url('products/'.$category->slug ?? '') }}" title="Building Materials " class="product-image">
                                       <img src="{{ $category->category_img }}" alt="Building Materials " width="128" />
                                    </a>
                                    
                                 </div>
                              </div>
                              @endforeach
                           </div>
                        </div>
                        @endif









                        <!-- Featured, On Sale, Top Rated Products -->


                        <div class="container-fluid">
                         <ul class="tabsProduct">
                          <li><a href="javascript:void(0); return false;" rel="#featuredProducts" class="tabProduct active">Featured</a></li>
                          <li><a href="javascript:void(0); return false;" rel="#onSaleProducts" class="tabProduct">On Sale</a></li>
                          <li><a href="javascript:void(0); return false;" rel="#topRatedProducts" class="tabProduct">Top Rated</a></li>

                       </ul>

                       <div class="tab_content_container" id="firstTab">
                          <div class="tab_content tab_content_active" id="featuredProducts">
                           <div class="container">
                              <div class="row">

                                 @if(count($featured_products)>0)
                                 @php $countP=0 @endphp
                                 @foreach($featured_products as $product)
                                 @php $countP++ @endphp
                                 <div class="col-sm-3">
                                    <div class="card">
                                       <a href="{{ url('product-detail/'.$product->slug) }}">
                                          <h4 class="productName">{{ucfirst($product->name ?? '')}}</h4>
                                       </a>
                                       <a href="{{ url('product-detail/'.$product->slug) }}">
                                          <img src="{{asset($product->image1)}}" class="product-image" alt="{{ucfirst($product->name ?? '')}}">
                                       </a>
                                           <h5 class="mrp2"><strike>₹{{$product->mrp ?? '0'}}</strike></h5>
                                       <p class="productPrice">
                                          ₹{{$product->sell_price ?? '0'}}

                                          <i class="fa fa-shopping-cart" id="shpping-cart" onclick="addToCart({{$product->id}})"></i>
                                       </p>
                                       <p class="quickView">
                                          <i class="fa fa-search" onclick="quickViewPopUp()"> Quick View</i>
                                          <span class="right" onclick="addToWishlist({{$product->id}})"><i class="fa fa-heart-o"></i> Add To Wishlist</span>
                                       </p>
                                    </div>
                                 </div>
                                 @endforeach
                                 @if($countP == 0)
                                 <div style="text-align: center; padding: 50px">No products available</div>
                                 @endif
                                 @endif


                              </div>
                           </div>
                        </div>

                        <div class="tab_content" id="onSaleProducts">
                          <div class="container">
                           <div class="row">

                              @if(count($trending_products)>0)
                              @foreach($trending_products as $product)
                              <div class="col-sm-3">
                                 <div class="card">
                                    <a href="{{ url('product-detail/'.$product->slug) }}">
                                       <h4 class="productName">{{ucfirst($product->name ?? '')}}</h4>
                                    </a>
                                    <a href="{{ url('product-detail/'.$product->slug) }}">
                                       <img src="{{asset($product->image1)}}" class="product-image" alt="{{ucfirst($product->name ?? '')}}">
                                    </a>
                                        <h5 class="mrp2"><strike>₹{{$product->mrp ?? '0'}}</strike></h5>
                                    <p class="productPrice">
                                       ₹{{$product->sell_price ?? '0'}}

                                       <i class="fa fa-shopping-cart" id="shpping-cart" onclick="addToCart({{$product->id}})"></i>
                                    </p>
                                    <p class="quickView">

                                       <i class="fa fa-search" onclick="quickViewPopUp()"> Quick View</i>
                                       <span class="right" onclick="addToWishlist({{$product->id}})"><i class="fa fa-heart-o"></i> Add To Wishlist</span>
                                    </p>

                                 </div>
                              </div>
                              @endforeach
                              @endif


                           </div>
                        </div>
                     </div>

                     <div class="tab_content" id="topRatedProducts">
                       <div class="container">
                        <div class="row">

                           @if(count($top_products)>0)
                           @foreach($top_products as $product)
                           <div class="col-sm-3">
                              <div class="card">
                                 <a href="{{ url('product-detail/'.$product->slug) }}">
                                    <h4 class="productName">{{ucfirst($product->name ?? '')}}</h4>
                                 </a>
                                 <a href="{{ url('product-detail/'.$product->slug) }}">
                                    <img src="{{asset($product->image1)}}" class="product-image" alt="{{ucfirst($product->name ?? '')}}">
                                 </a>
                                     <h5 class="mrp2"><strike>₹{{$product->mrp ?? '0'}}</strike></h5>
                                 <p class="productPrice">
                                    ₹{{$product->sell_price ?? '0'}}

                                    <i class="fa fa-shopping-cart" id="shpping-cart" onclick="addToCart({{$product->id}})"></i>
                                 </p>
                                 <p class="quickView">
                                    <i class="fa fa-search" onclick="quickViewPopUp()"> Quick View</i>
                                    <span class="right" onclick="addToWishlist({{$product->id}})"><i class="fa fa-heart-o"></i>Add To Wishlist</span>
                                 </p>

                              </div>
                           </div>
                           @endforeach
                           @endif


                        </div>
                     </div>
                  </div>


               </div>
            </div>




            <!-- Categorywise Products -->

            @if(!empty($allCategories))

            <div class="container-fluid">
             <ul class="tabsProduct2">
               @php $i=0; @endphp
               @foreach($allCategories as $category)


               <li><a href="javascript:void(0); return false;" rel="#tab{{$category->id}}" class="tabProduct @if($i==0){{'active'}}@endif">{{ucfirst($category->category_name ?? '')}}</a></li>
               @php $i++ @endphp
               @endforeach

               <li class="lastItem"><a href="{{url('products/all')}}" rel="">View All</a></li>


            </ul>

            <div class="tab_content_container allProducts">
               @if(!empty($allProducts))
               @php $j=0 @endphp
               @foreach($allProducts as $all)
               <div class="tab_content @if($j==0){{'tab_content_active2'}}@endif" id="tab{{$all['key']}}">
                  <div class="container">
                     <div class="row">
                        @php $countP=0 @endphp
                        @if(!empty($all['products']))
                        @foreach($all['products'] as $product)
                        <div class="col-sm-2" style="margin-left: 5px">
                           <div class="card">
                              <a href="{{ url('product-detail/'.$product->slug) }}">
                                 <h4 class="productName">{{ucfirst($product->name ?? '')}}</h4>
                              </a>
                              <a href="{{ url('product-detail/'.$product->slug) }}">
                                 <img src="{{asset($product->image1)}}" class="product-image2" alt="{{ucfirst($product->name ?? '')}}">
                              </a>
                                  <h5 class="mrp"><strike>₹{{$product->mrp ?? '0'}}</strike></h5>
                              <p class="productPrice">
                                 ₹{{$product->sell_price ?? '0'}}

                                 <i class="fa fa-shopping-cart" id="shpping-cart" onclick="addToCart({{$product->id}})"></i>
                              </p>
                              <p class="quickView">
                                 &nbsp;&nbsp;&nbsp;<i class="fa fa-search" onclick="quickViewPopUp()"></i>
                                 <span class="right" style="text-align: right;" onclick="addToWishlist({{$product->id}})" ><i class="fa fa-heart-o"></i></span>
                              </p>
                           </div>
                        </div>
                        @php $countP++ @endphp
                        @endforeach
                        @if($countP == 0)
                        <div style="text-align: center; padding: 50px">No products available</div>
                        @endif
                        @endif

                     </div>
                  </div>

               </div>
               @php $j++ @endphp
               @endforeach
               @endif
            </div>
         </div>

         @endif









               <div class="container-fluid " id="BestSell">
                  <h2 class="productHeading">Best Sellers <span class="lastItem"><a href="{{url('products/all')}}" rel="">View All</a></span>
                  </h2>
                  <div class="tab_content tab_content_active" style="width: 100%">

                     <div class="row">

                        @if(count($best_sellers_produts)>0)
                        @php $countP=0 @endphp
                        @foreach($best_sellers_produts as $product)
                        @php $countP++ @endphp
                        <div class="col-sm-3" style="padding-right: 0; margin-left: 5px">
                           <div class="card bestSeller" >
                              <div class="row">

                                 <div class="col-sm-5" style="padding-right: 0">

                                    <!-- <div class="cardLeft"> -->
                                       <a href="{{ url('product-detail/'.$product->slug) }}">
                                          <img src="{{asset($product->image1)}}" class="product-image2" alt="{{ucfirst($product->name ?? '')}}" />
                                       </a>
                                       <!-- </div> -->

                                    </div>

                                    <div class="col-sm-7" style="padding-left: 5px">

                                      <div class="cardRight">
                                       <a href="{{ url('product-detail/'.$product->slug) }}">
                                          <h4 class="productName">{{ucfirst($product->name ?? '')}}</h4>
                                     </a>

                                       <h5 class="mrp"><strike>₹{{$product->mrp ?? '0'}}</strike></h5>
                                     <p class="productPrice">
                                       ₹{{$product->sell_price ?? '0'}}

                                       <i class="fa fa-shopping-cart" id="shpping-cart" onclick="addToCart({{$product->id}})"></i>
                                    </p>
                                    <p class="quickView">
                                       &nbsp;&nbsp;&nbsp;<i class="fa fa-search" onclick="quickViewPopUp()"></i>
                                       <span class="right" style="text-align: right;" onclick="addToWishlist({{$product->id}})"><i class="fa fa-heart-o"></i></span>
                                    </p>

                                 </div>

                              </div>

                           </div>




                        </div>
                     </div>
                     @endforeach
                     @if($countP == 0)
                     <div style="text-align: center; padding: 50px">No products available</div>
                     @endif
                     @endif



                  </div>
               </div>
            </div>









            <!-- For Full Banner -->
            @if(!empty($full_banner))
            <div class="container-fluid">
               <img src="{{$full_banner->image_url ?? ''}}" alt="{{ $full_banner->keyword ?? '' }}" class="fullBannerDiv">
            </div>
            @endif


            <div class="container-fluid " id="RecentProducts">
               <h2 class="productHeading">Recently Viewed  <span class="lastItem"><a href="{{url('products/all')}}" rel="">View All</a></span>
               </h2>
               <div class="tab_content tab_content_active">

                  <div class="row">

                     @if(count($recently_viewed_products)>0)
                     @php $countP=0 @endphp
                     @foreach($recently_viewed_products as $product)
                     @php $countP++ @endphp
                     <div class="col-sm-2" style="margin-left: 5px">
                        <div class="card">
                           <a href="{{ url('product-detail/'.$product->slug) }}">
                              <h4 class="productName">{{ucfirst($product->name ?? '')}}</h4>
                           </a>
                           <a href="{{ url('product-detail/'.$product->slug) }}">
                              <img src="{{asset($product->image1)}}" class="product-image2" alt="{{ucfirst($product->name ?? '')}}">
                           </a>
                               <h5 class="mrp"><strike>₹{{$product->mrp ?? '0'}}</strike></h5>
                           <p class="productPrice">
                              ₹{{$product->sell_price ?? '0'}}

                              <i class="fa fa-shopping-cart" id="shpping-cart" onclick="addToCart({{$product->id}})"></i>
                           </p>
                           <p class="quickView">
                              &nbsp;&nbsp;&nbsp;<i class="fa fa-search" onclick="quickViewPopUp()"></i>
                              <span class="right" style="text-align: right;" onclick="addToWishlist({{$product->id}})"><i class="fa fa-heart-o"></i></span>
                           </p>

                        </div>
                     </div>
                     @endforeach
                     @if($countP == 0)
                     <div style="text-align: center; padding: 50px">No products available</div>
                     @endif
                     @endif



                  </div>
               </div>
            </div>




            @if(count($brands)>0)
            
            <div class="brandSlider" style="border-top: 1px solid #ddd; border-bottom: 1px solid #ddd;">
               <div class="products-grid" style="margin: 20px auto">
                  @foreach($brands as $brand)
                  <div class="slider auto-slide-0" id="bs17">
                     <div class="product_image_div"> <img src="{{ asset($brand->brand_img) }}"
                        alt="{{ $brand->brand_name }}" />
                     </div>
                  </div>
                  @endforeach
               </div>
            </div>

            @endif



            @endsection