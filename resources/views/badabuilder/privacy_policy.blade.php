@extends('layouts.badabuilder')

@section('content')
<style type="text/css">
	.tnc h3 {
		font-size: 22px; margin-bottom: 25px; color: #000; font-weight: bold;
	}

	.tnc ol ,
	.tnc ul {
		list-style-type: none;
	}
</style>
  <section class="headings" >
        <div class="text-heading text-center">
            <div class="container">
                <h1>Privacy Policy</h1>
            </div>
        </div>
    </section>

    <div class="road" >
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="{{ url('/') }}">Home</a><span>»</span><span>Privacy Policy</span>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION HEADINGS -->
<div class="container-fluid tnc">
	<div style="margin: 45px;background-color: #fff; border-radius: 10px;">
		<div style="padding: 25px;">
<h3>Privacy Policy </h3>

<p><b>BADABUILEDR</b> Terms of Use </p>

<p><b>BADABUILEDR</b>  Privacy POLICY </p>

<p><b>BADABUILEDR</b> is focused on keeping up hearty security insurances for its clients. Our Privacy Policy ("Privacy Policy") is intended to assist you with seeing how we gather, use and defend the data you give to us and to help you in settling on educated choices when utilizing our Service. </p>

<p>For reasons for this Agreement, "Administration" alludes to the Company's administration which can be gotten to through our site at www.badabuilder.com in which clients can buy development material and partnered administrations. The expressions "we," "us," and "our" allude to the Company. "You" alludes to you, as a client of Service. By tolerating our Privacy Policy and Terms of Use (discovered here: www.badabuilder.com/terms-and-conditions/), you agree to our assortment, stockpiling, use and divulgence of your own data as depicted in this Privacy Policy. </p>

	<ul>

		<li>

<p><b>I. Data WE COLLECT</b> </p>

<p>We gather "Non-Personal Information" and "Individual Information." Non-Personal Information incorporates data that can't be utilized to actually distinguish you, for example, unknown utilization information, general segment data we may gather, alluding/leave pages and URLs, stage types, inclinations you submit and inclinations that are created dependent on the information you submit and number of snaps. Individual Information incorporates your name, street number, phone number, email address, charge card data, which you submit to us through the enlistment procedure or while putting in a request, buying in to a pamphlet, rounding out a structure, reacting to an overview, and so on at the Site. </p>

<ol>
	
	<li>

<p><b>1. Data gathered through Technology</b> </p>

<p>To enact the Service you might be required to present your Personal Information including your name, email address and area. To utilize the Service from there on, you may not be required to submit further Personal Information. In any case, with an end goal to improve the nature of the Service, we track data gave to us by your program or by our product application when you view or utilize the Service, for example, the site you originated from (known as the "alluding URL"), the sort of program you use, the gadget from which you associated with the Service, the time and date of access, and other data that doesn't actually recognize you. We track this data utilizing treats, or little content documents which incorporate a mysterious one of a kind identifier. Treats are sent to a client's program from our servers and are put away on the client's PC hard drive. Sending a treat to a client's program empowers us to gather Non-Personal data about that client and track the client's inclinations while using our administrations, both on an individual and total premise. For instance, the Company may utilize treats to gather the accompanying data: </p>

<p>Help recollect and process the things in the shopping basket </p>

<p>Comprehend and spare client's inclinations for future visits. </p>

<p>Monitor commercials. </p>

<p>Accumulate total information about site traffic and site communications so as to offer better site encounters and devices later on. We may likewise utilize believed outsider administrations that track this data for our sake. </p>

<p>The Company may utilize both constant and meeting treats; tireless treats stay on your PC after you close your meeting and until you erase them, while meeting treats lapse when you close your program. For instance, we may store a constant treat to: </p>

<p>Customize client's understanding </p>

<p>Improve the site </p>

<p>Improve client care </p>

<p>Control challenges, advancements, or an overview or other site highlight </p>

<p>To process exchanges </p>

<p>To send intermittent messages.</p>
</li> 

<li>
<p><b>2. Data you give us by enlisting to a record </b></p>

<p>Notwithstanding the data gave naturally by your program when you visit the Site, to turn into an endorser of the Service you should make an individual profile. You can make a profile by enrolling with the Service and entering your email address, and making a client name and a secret word. By enlisting, you are approving us to gather, store and utilize your email address as per this Privacy Policy. </p>
</li>
</ol>
</li>

<li>

<p><b>II. HOW WE USE AND SHARE INFORMATION</b></p> 

<p>Individual Information: </p>

<p><b>BADABUILEDR</b> may utilize your own data to: </p>

<p>Oversee this site, <a>www.badabuilder.com</a></p> 

<p>Customize the site for you </p>

<p>Empower your entrance to and utilization of the site administrations </p>

<p>Distribute data about you on the site </p>

<p>Send to you items that you buy</p> 

<p>Flexibly to you benefits that you buy </p>

<p>Send to you explanations and solicitations </p>

<p>Gather installments from you; and </p>

<p>Send you promoting interchanges </p>

<p>Aside from as in any case expressed in this Privacy Policy, we don't sell, exchange, lease or in any case share for promoting purposes your Personal Information with outsiders without your assent or without giving you early notification. This does exclude site facilitating accomplices and different gatherings who help us in working our site, leading our business, or overhauling you, who are given access to your own subtleties for the motivations behind reaching you. Those merchants utilize your Personal Information just at our bearing and as per our Privacy Policy.We may likewise discharge your data when we accept discharge is suitable to follow the law, implement our site strategies, or ensure our own or others' privileges, property, or wellbeing. </p>

<p>When all is said in done, the Personal Information you give to us is utilized to assist us with speaking with you. For instance, we utilize Personal Information to contact clients in light of inquiries, request input from clients, offer specialized help, and educate clients about limited time offers. </p>

<p><b>Non-Personal Information </b></p>

<p>When all is said in done, we use Non-Personal Information to assist us with improving the Service and redo the client experience. We additionally total Non-Personal Information so as to follow inclines and investigate client designs on the Site. This Privacy Policy doesn't confine in any capacity our utilization or revelation of Non-Personal Information and we maintain all authority to utilize and uncover such Non-Personal Information to our accomplices, promoters and other outsiders at our watchfulness. </p>

<p>In the occasion we experience a business exchange, for example, a merger, obtaining by another organization, or offer of all or a part of our advantages, your Personal Information might be among the benefits moved. You recognize and assent that such exchanges may happen and are allowed by this Privacy Policy, and that any acquirer of our benefits may keep on handling your Personal Information as set out in this Privacy Policy. On the off chance that our data rehearses change whenever later on, we will present the strategy changes on the Site with the goal that you may quit the new data rehearses. We propose that you check the Site occasionally on the off chance that you are worried about how your data is utilized. </p>
</li>

<li>

<p><b>III. HOW WE PROTECT INFORMATION </b></p>

<p>We execute safety efforts intended to shield your data from unapproved get to. Your record is secured by your record secret key and we ask you to find a way to guard your own data by not uncovering your secret key and by logging out of your record after each utilization. We further shield your data from potential security breaks by actualizing certain mechanical safety efforts including encryption, firewalls and secure attachment layer innovation. In any case, these measures don't ensure that your data won't be gotten to, unveiled, adjusted or pulverized by break of such firewalls and secure server programming. By utilizing our Service, you recognize that you comprehend and consent to accept these dangers. </p>
</li>

<li>
	<p><b>IV. YOUR RIGHTS REGARDING THE USE OF YOUR PERSONAL INFORMATION </b></p>

<p>You have the privilege whenever to keep us from reaching you for promoting purposes. At the point when we send a limited time correspondence to a client, the client can quit further special interchanges by adhering to the withdraw guidelines gave in each special email. You can likewise demonstrate that you don't wish to get promoting interchanges from us in the "Settings" segment of the Site. It would be ideal if you note that despite the limited time inclinations you show by either withdrawing or quitting in the Settings segment of the Site, we may keep on sending you managerial messages including, for instance, intermittent updates to our Privacy Policy. </p>
</li>


<li>
	<p><b>V. Connections TO OTHER WEBSITES </b></p>

<p>As a major aspect of the Service, we may furnish connections to or similarity with different sites or applications. Be that as it may, we are not liable for the security rehearses utilized by those sites or the data or substance they contain. This Privacy Policy applies exclusively to data gathered by us through the Site and the Service. Subsequently, this Privacy Policy doesn't matter to your utilization of an outsider site got to by choosing a connection on our Site or by means of our Service. To the degree that you access or utilize the Service through or on another site or application, at that point the protection strategy of that other site or application will apply to your entrance or utilization of that site or application. We urge our clients to peruse the security explanations of different sites before continuing to utilize them. </p>
</li>

<li>
<p><b>VI. CHANGES TO OUR PRIVACY POLICY </b></p>

<p>The Company claims all authority to change this strategy and our Terms of Service whenever. We will advise you of critical changes to our Privacy Policy by sending a notification to the essential email address indicated in your record or by setting a conspicuous notification on our site. Huge changes will go live 30 days following such warning. Non-material changes or explanations will produce results right away. You ought to intermittently check the Site and this protection page for refreshes. </p>
</li>

<li>
<p><b>VII. Get in touch with US </b></p>

<p>On the off chance that you have any inquiries with respect to this Privacy Policy or the acts of this Site, if it's not too much trouble contact
</p>
</li>
</ul>
</div>
</div>
</div>
@endsection