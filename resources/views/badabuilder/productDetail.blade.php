@extends('layouts.badabuilder')

@section('content')

<div class="road">
    <div class="container" style="margin-top: 20px">
        <div class="row">
            <div class="col">
                <a href="{{ url('/') }}">Home</a><span>»</span>
                <span><a href="{{ url('products/'.$productCategory->slug ?? '') }}" title="">{{ ucfirst($productCategory->category_name) }}</a></span><span>»</span>
                <span>{{ ucfirst($productDetail->name ?? '') }}</span>
            </div>
        </div>
    </div>
</div>
<!-- END SECTION HEADINGS -->

<!-- START SECTION SERVICE DETAILS -->
<section class="service-details">
    <div class="container-fluid">
        <div class="row" style="background-color: #fff">

            <aside class="col-lg-4 col-md-12">
                <div class="row">
                    <div class="col-sm-12 zoomImage">
                        <img src="{{ asset($productDetail->image1 ?? '') }}" alt="{{$productDetail->name ?? ''}}" class="mainImage" id="pImage">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 productThumbnail">
                        <ul>

                            @if(!empty($productDetail->image1))
                            <li>
                                <input type="hidden" name="image1" id="image1" value="{{ asset($productDetail->image1 ?? '') }}">
                                <img src="{{ asset($productDetail->image1 ?? '') }}" alt="{{$productDetail->name ?? ''}}" class="thumbnail" id="pImage1">
                            </li>
                            @endif 

                            @if(!empty($productDetail->image2))
                            <li>
                                <input type="hidden" name="image2" id="image2" value="{{ asset($productDetail->image2 ?? '') }}">
                                <img src="{{ asset($productDetail->image2 ?? '') }}" alt="{{$productDetail->name ?? ''}}" class="thumbnail" id="pImage1">
                            </li>
                            @endif

                            @if(!empty($productDetail->image3))
                            <li>
                                <input type="hidden" name="image3" id="image3" value="{{ asset($productDetail->image3 ?? '') }}">
                                <img src="{{ asset($productDetail->image3 ?? '') }}" alt="{{$productDetail->name ?? ''}}" class="thumbnail" id="pImage1">
                            </li>
                            @endif

                        </ul>
                    </div>

                </div>
            </aside>

            <div class="col-lg-8 col-md-12 service-info">
                <div class=" container row">
                    <div class="col-md-12">
                        <div class="row productDetail">

                            <h2>{{ ucfirst($productDetail->name ?? '') }}</h2>

                        </div>
                        <div class="row">
                            <ul>
                                <li>
                                    @php  $count = $productReview @endphp
                                    @for($i=0; $i < 5; $i++)
                                    @if($count>0)
                                    <i class="fa fa-star"></i>
                                    @else
                                    <i class="fa fa-star-o"></i>
                                    @endif
                                    
                                    @php  $count-- @endphp
                                    @endfor
                                    
                                </li>
                            </ul>
                            &nbsp;({{$countReview}}) Reviews
                        </div>
                        <div class="service-text">
                            <h4 class="productPrice"> 
                                <strike>₹{{$productDetail->mrp ?? '0'}}</strike>
                                ₹{{$productDetail->sell_price ?? '0'}}
                            </h4>
                            <h5 class="availability"> 
                                Availability: 
                                @if($productDetail->availability == 'yes')
                                <span class="availabilityIn">In Stock</span>
                                @else
                                <span class="availabilityOut">Out Of Stock</span>
                                @endif
                                <input type="hidden" name="stock" id="stock{{$productDetail->id}}" value="{{$productDetail->availability}}">
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="specialists">


                    <h5>Description</h5>
                    <p class="my-4">
                        {{ ucfirst($productDetail->short_descriptions ?? '') }}
                    </p>


                </div>

                <div class="container row" id="productQty">


                    <ul id="productQty">
                        <li>
                            &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-minus" onclick="decrementQty({{$productDetail->id}})"></i>&nbsp;&nbsp;
                        </li>
                        <li>
                            <input type="number" name="quantity" id="quantityPd{{$productDetail->id}}" class="form-control productQuantity" value="1" minlength="1" readonly="readonly">&nbsp;&nbsp;
                        </li>
                        <li>
                            &nbsp;&nbsp;<i class="fa fa-plus" onclick="incrementQty({{$productDetail->id}})"></i>
                        </li>
                    </ul>                    
                </div>
                <div class="container row allButtons">
                    <ul>
                        <li>
                            <button type="button"
                            title="Add to Cart" class="btn btn-primary" onclick="addToCart({{$productDetail->id}})">
                        Add to Cart</button>
                    </li>

                    <li>
                        <button type="submit"
                        title="Buy Now" class="btn btn-primary">
                    Buy Now</button>
                </li>

                <li>
                    <button type="submit"
                    title="Add To Wishlist" class="btn btn-primary" onclick="addToWishlist({{$productDetail->id}})">
                    <i class="fa fa-heart-o" style="color: #fff"></i></button>
                </li>
            </ul>
        </div>
        <div class="socialIcons">
            <ul>
                <li>
                    <i class="fa fa-facebook"></i>
                </li>
                <li>
                    <i class="fa fa-twitter"></i>
                </li>
                <li>
                    <i class="fa fa-instagram"></i>
                </li>
            </ul>
        </div>
        
            
        

    </div>
    <p class="alert alert-success" id="cartSuccess"> Product Added To Cart Successfully.</p>


</div>
</div>
</section>
<!-- END SECTION SERVICE DETAILS -->





<div class="tabPanel-widget">
    <label for="tab-1" tabindex="0"></label>
    <input id="tab-1" type="radio" name="tabs" checked="true" aria-hidden="true">
    <h2>Description</h2>
    <div>
      <p>{{$productDetail->long_descriptions ?? ''}}</p>
  </div>
  <label for="tab-2" tabindex="0"></label>
  <input id="tab-2" type="radio" name="tabs" aria-hidden="true">
  <h2>Specifications</h2>
  <div>
    <table class="table table-bordered" style="width: 500px">
        <tbody>
          @if(!empty($productDetail->product_brand))
          <tr>
            <th class="label">Brand</th>
            <td class="data">{{$productDetail->product_brand}}</td>
        </tr>
        @endif
        @if(!empty($productDetail->product_color))
        <tr>
            <th class="label">Color</th>
            <td class="data">{{$productDetail->product_color ?? ''}}</td>
        </tr>
        @endif
        @if(!empty($productDetail->product_weight))
        <tr>
            <th class="label">Weight</th>
            <td class="data">
                {{$productDetail->product_weight ?? ''}}</td>
            </tr>
            @endif
            @if(!empty($productDetail->product_material))
            <tr>
                <th class="label">Material</th>
                <td class="data">{{$productDetail->product_material ?? ''}}</td>
            </tr>
            @endif
            @if(!empty($productDetail->product_model))
            <tr>
                <th class="label">Model</th>
                <td class="data">{{$productDetail->product_model ?? ''}}</td>
            </tr>
            @endif
            @if(!empty($productDetail->product_model_number))
            <tr>
                <th class="label">Model No</th>
                <td class="data">{{$productDetail->product_model_number ?? ''}}</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>
<label for="tab-3" tabindex="0"></label>
<input id="tab-3" type="radio" name="tabs" aria-hidden="true">
<h2>Reviews</h2>
<div>
    <div class="row">

        <div class="col-sm-7">
            <form action="{{route('give-product-rating')}}" method="post" class="reviewForm">
                @csrf
                <input type="text" name="product_id" hidden value="{{$productDetail->id}}">
                <input type="text" name="name" id="name" placeholder="Your Name Here" required="required" class="form-control" value="@if(!empty(Auth::user())){{ucfirst(Auth::user()->name ?? '')}}@endif">
                <input type="email" name="email" id="email" placeholder="Your Email Here" required="required" class="form-control" value="@if(!empty(Auth::user())){{ucfirst(Auth::user()->email ?? '')}}@endif">
                <textarea name="review" id="review" required="required" class="form-control"></textarea> 

                <p>Rate This Product</p>

                <ul class="ratingStar">
                    <li>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i><br>
                        <input type="radio" name="rating" id="rating" value="1" checked="checked">
                    </li>

                    <li>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i><br>
                        <input type="radio" name="rating" id="rating" value="2">
                    </li>

                    <li>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i><br>
                        <input type="radio" name="rating" id="rating" value="3">
                    </li>

                    <li>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i><br>
                        <input type="radio" name="rating" id="rating" value="4">
                    </li>

                    <li>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i><br>
                        <input type="radio" name="rating" id="rating" value="5">
                    </li>

                </ul>
                <button type="submit" name="saveReview" id="saveReview" class="btn btn-warning">Submit</button>

            </form>
        </div>


        <div class="col-sm-5">
            <h5> {{$countReview}} Reviews</h5>
            @if(count($reviews)>0)
            <ul>
                @php $i = 1 @endphp
                @foreach($reviews as $review)
                <li>
                    <b>{{ $i++ }}. {{$review->email}}</b> ( <i class="fa fa-star"></i> {{$review->rating}}/5 )
                    <p>{{$review->review ?? ''}}</p>
                </li>
                @endforeach
            </ul>
            @endif
        </div>

    </div>
</div>
</div>


@endsection