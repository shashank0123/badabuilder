

@extends('layouts.badabuilder')

@section('content')




<section class="headings">
        <div class="text-heading text-center">
            <div class="container"> 
                <h1>Construction</h1>
            </div>
        </div>
    </section>

    <div class="road">
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="{{ url('/') }}">Home</a><span>»</span><span>Construction</span>
                </div>
            </div>
        </div>
    </div>
    <br><br>
    <!-- END SECTION HEADINGS -->

    <!-- START SECTION CONTACT US -->
    <section class="contact-us">
        <div class="container">
            <div class="row">
              
              <a href="{{ url('products/all') }}"><img src="{{ asset('images/building1.jpg') }}" style="width: 100%; height: auto"></a>
            </div>
        </div>
    </section>
    <!-- END SECTION CONTACT US -->
<br><br>
  

  <div class="container-fluid tnc">
	<div style="margin: 25px;background-color: #fff; border-radius: 10px;">
		<div style="padding: 45px; text-align: justify;">





BADABUILDER provides the facility to have the turnkey contract of constructing a building from the initial stages of construction to the final completion stage of occupancy.

All the building services which ever are required will be conducted with the qualified professionals from RippleMart team along with the services of Designs, Approvals and loans if required.

This turnkey contract gives the freedom to the client in choosing a right product according to his requirement and need for which he will have a feeling that he can enjoy for whole life.

With the turnkey contract, a site engineer from RippleMart is employed at the site from the starting stage of construction to its final stage.

For any other information related to the above matter, please send us your query by filling below form.
<br><br><br>
<form action="{{ url('contact') }}" class="contact-form" name="contactform" method="post" novalidate>

                        <div id="success" class="successform">
                            <p class="alert alert-success font-weight-bold" role="alert">Your message was sent successfully!</p>
                        </div>
                        <div id="error" class="errorform">
                            <p>Something went wrong, try refreshing and submitting the form again.</p>
                        </div>
                        <div class="form-group">
                            <input type="text" required class="form-control input-custom input-full" name="name" placeholder="Full Name" required="required">
                        </div>
                        <div class="form-group">
                            <input type="email" required class="form-control input-custom input-full" name="email" placeholder="Email" required="required">
                        </div>
                        <div class="form-group">
                            <input type="number" required class="form-control input-custom input-full" name="mobile" placeholder="Contact Number" required="required">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control input-custom input-full" name="subject" placeholder="Subject" required="required">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control textarea-custom input-full" id="ccomment" name="message_content" required rows="8" placeholder="Message" required="required"></textarea>
                        </div>
                        <button type="submit" id="submit-contact" class="btn btn-primary btn-lg">Submit</button>
                    </form>

		</div>
	</div>
</div>












@endsection