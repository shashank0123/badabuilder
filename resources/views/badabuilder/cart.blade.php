@extends('layouts.badabuilder')

@section('content')




<div class="road" >
    <div class="container" style="padding: 25px 0">
        <div class="row">
            <div class="col">
                <a href="{{ url('/') }}">Home</a><span>»</span><span>Shopping Cart</span>
            </div>
        </div>
    </div>
</div>
<!-- END SECTION HEADINGS -->

<!-- START SECTION PROJECTS -->
<section class="services">
    <div class="container-fluid" >
        <div class="row" style="padding: 25px">
            <div style="text-align: right; width: 100%">
                <a href="@if(!empty(Auth::user())){{'checkout'}}@else{{'login'}}@endif" name="proceed" class="btn btn-primary">Submit Query</a> &nbsp;&nbsp;&nbsp;
                 <a title="Empty Cart" class="btn btn-primary" 
                    href="{{ url('emptyCart') }}" id="right">
                    <span><span>Empty Cart</span></span></a>
            </div>
            <h2>Shopping Cart</h2>
            <table class="table productTable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th style="width: 200px">Qty</th>
                        <th>Sub Total</th>
                        <th>Action</th>
                    </tr>
                </thead>


                <tbody>
                    @if(count($products)>0)
                    @foreach($products as $product)
                    <tr  id="cart{{$product->id}}">
                        <td>
                            <a href="{{ url('product-detail/'.$product->slug) }}">
                                <img src="{{ asset($product->image1) }}" style="max-height: 75px; max-width: 100% "/>
                            </a>
                        </td>

                        <td>
                            <h5>
                                <a href="{{ url('product-detail/'.$product->slug) }}">
                                    {{ ucfirst($product->name ?? '') }}
                                </a>
                            </h5>
                        </td>

                        <td>
                            ₹{{$product->sell_price ?? '0'}}
                        </td>

                        <td>
                            <ul id="productQty">
                                <li>
                                    <i class="fa fa-minus" onclick="decrement({{$product->id}})"></i>&nbsp;&nbsp;
                                </li>
                                <li>
                                    <input type="number" name="quantity" id="quantityPd{{$product->id}}" class="form-control productQuantity" value="{{$product->quantity ?? '1'}}" minlength="1" readonly="readonly">&nbsp;&nbsp;
                                </li>
                                <li>
                                    &nbsp;&nbsp;<i class="fa fa-plus" onclick="increment({{$product->id}})"></i>
                                </li>
                            </ul>
                        </td>

                        <td>
                            <span id="subTotal{{$product->id}}">₹{{$product->subtotal ?? '0'}}</span>
                        </td>

                        <td >
                            
                          <a onclick="deleteCartProduct({{$product->id}})" class="btn btn-danger"><i class="fa fa-close"></i></a>
                      </td>

                  </tr>
                  @endforeach
                  @endif

                  @if(count($products)==0)
                  <tr>
                    <td colspan='5' style="text-align: center; padding: 45px">
                       No product in your Cart 
                   </td>
               </tr>
               @endif

           </tbody>

           <tfoot>
            <tr  style="margin-top: 25px">
                <td colspan="4" style="width:75%">
                    <a href="{{url('/')}}" name="update_cart_action"  
                    title="Continue Shopping" class="btn btn-primary" id="empty_cart_button">
                    <span><span>Continue Shopping</span></span></a>
                </td>

                <td>
                        Subtotal  <br>
                         GST  <br>
                        <strong>Grand Total</strong>
                    </td>
                    <td >
                        <span class="price" id="total_price">₹{{$total}}.00</span> <br>   
                            <span class="price">₹0.00</span><br>
                        <strong><span class="price" id="grand_total">₹{{$total}}.00</span></strong>
                    </td>
            </tr>
        </tfoot>


    </table>
</div>


   
       
        

</div>
</section>
<!-- END SECTION PROJECTS -->
<br><br>
@endsection