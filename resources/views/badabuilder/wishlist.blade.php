@extends('layouts.badabuilder')

@section('content')




<div class="road" >
    <div class="container" style="padding: 25px 0">
        <div class="row">
            <div class="col">
                <a href="{{ url('/') }}">Home</a><span>»</span><span>Wishlist</span>
            </div>
        </div>
    </div>
</div>
<!-- END SECTION HEADINGS -->
<p class="alert alert-success" id="cartSuccess"></p>
            
<!-- START SECTION PROJECTS -->
<section class="services">
    <div class="container-fluid" >
        <div class="row" style="padding: 25px">
            
            <h2>My Wishlist</h2>
            <table class="table productTable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th style="width: 200px">Qty</th>
                        <th>Stock</th>
                        <th>Action</th>
                    </tr>
                </thead>


                <tbody>
                    @if(count($products)>0)
                    @foreach($products as $product)
                    <tr id="div-list{{$product->id}}">
                        <td>
                            <a href="{{ url('product-detail/'.$product->slug) }}">
                                <img src="{{ asset($product->image1) }}" style="max-height: 75px; max-width: 100% "/>
                            </a>
                        </td>

                        <td>
                            <h5>
                                <a href="{{ url('product-detail/'.$product->slug) }}">
                                    {{ ucfirst($product->name ?? '') }}
                                </a>
                            </h5>
                        </td>

                        <td>
                            ₹{{$product->sell_price ?? '0'}}
                        </td>

                        <td>
                            <ul id="productQty">
                                <li>
                                    <i class="fa fa-minus" onclick="decrementQty({{$product->id}})"></i>&nbsp;&nbsp;
                                </li>
                                <li>
                                    <input type="number" name="quantity" id="quantityPd{{$product->id}}" class="form-control productQuantity" value="1" minlength="1" readonly="readonly">&nbsp;&nbsp;
                                </li>
                                <li>
                                    &nbsp;&nbsp;<i class="fa fa-plus" onclick="incrementQty({{$product->id}})"></i>
                                </li>
                            </ul>
                        </td>

                        <td>
                            @if($product->availability=='yes')
                            <span color="green">In Stock</span>
                            @else
                            <span color="red">Out Of Stock</span>
                            @endif 
                            <input type="hidden" name="stock" id="stock{{$product->id}}" value="{{$product->availability}}"> 
                        </td>

                        <td >
                            <a class="btn btn-primary" href="#" onclick="addToCart({{$product->id}})">
                              <i class="fa fa-cart-plus"></i>
                          </a>
                          <a onclick="deleteWishlistItem({{$product->id}})" class="btn btn-danger"><i class="fa fa-close"></i></a>
                      </td>

                  </tr>
                  @endforeach
                  @endif

                  @if(count($products)==0)
                  <tr>
                    <td colspan='5' style="text-align: center; padding: 45px">
                       No product in your wishlist 
                   </td>
               </tr>
               @endif

           </tbody>

           <tfoot>
            <tr  style="margin-top: 25px">
                <td colspan="50" class="a-right cart-footer-actions">


                    <a href="{{url('/')}}" name="update_cart_action"  
                    title="Continue Shopping" class="btn btn-primary" id="empty_cart_button">
                    <span><span>Continue Shopping</span></span></a>


                    <a title="Empty Wishlist" class="btn btn-primary" 
                    href="{{ url('emptyWishlist') }}" id="right">
                    <span><span>Empty Wishlist</span></span></a>

                </td>
            </tr>
        </tfoot>


    </table>
</div>
</div>
</section>
<!-- END SECTION PROJECTS -->
<br><br>
@endsection