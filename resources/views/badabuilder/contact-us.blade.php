
@extends('layouts.badabuilder')

@section('content')


    <section class="headings">
        <div class="text-heading text-center">
            <div class="container">
                <h1>Contact Us</h1>
            </div>
        </div>
    </section>

    <div class="road">
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="index.html">Home</a><span>»</span><span>Contact Us</span>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION HEADINGS -->

    <!-- START SECTION CONTACT US -->
    <section class="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <h3 class="mb-4">Contact Us</h3>
                    <form action="{{ url('contact') }}" class="contact-form" name="contactform" method="post" novalidate>
                        <div id="success" class="successform">
                            <p class="alert alert-success font-weight-bold" role="alert">Your message was sent successfully!</p>
                        </div>
                        <div id="error" class="errorform">
                            <p>Something went wrong, try refreshing and submitting the form again.</p>
                        </div>
                        <div class="form-group">
                            <input type="text" required class="form-control input-custom input-full" name="name" placeholder="Full Name">
                        </div>
                        <div class="form-group">
                            <input type="email" required class="form-control input-custom input-full" name="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="number" required class="form-control input-custom input-full" name="mobile" placeholder="Contact Number">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control input-custom input-full" name="subject" placeholder="Subject">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control textarea-custom input-full" id="ccomment" name="message_content" required rows="8" placeholder="Message"></textarea>
                        </div>
                        <button type="submit" id="submit-contact" class="btn btn-primary btn-lg">Submit</button>
                    </form>
                </div>
                <div class="col-lg-4 col-md-12 bgc">
                    <div class="call-info">
                        <h3>Contact Details</h3>
                        <p class="mb-5">Please find below contact details and contact us today!</p>
                        <ul>
                            <li>
                                <div class="info">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <p class="in-p"><?php echo $companyInfo->address ?? ''; ?></p>
                                </div>
                            </li>
                            <li>
                                <div class="info">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    <p class="in-p"><?php echo $companyInfo->contact_no ?? '' ?? ''; ?></p>
                                </div>
                            </li>
                            <li>
                                <div class="info">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                    <p class="in-p ti"><?php echo $companyInfo->support_email ?? ''; ?></p>
                                </div>
                            </li>
                           <!--  <li>
                                <div class="info cll">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <p class="in-p ti">8:00 a.m - 9:00 p.m</p>
                                </div>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END SECTION CONTACT US -->
<br><br>
    <!-- START SECTION GOOGLE MAP -->
    <div id="contact-map" class="map-area">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13997.00498985874!2d77.0883702258456!3d28.712036537625778!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d06ac618584fb%3A0x356e84758865f95b!2sSector%204%2C%20Rohini%2C%20Delhi!5e0!3m2!1sen!2sin!4v1580811613376!5m2!1sen!2sin" frameborder="0" style="border:0;width: 100%; height: 450px;" allowfullscreen=""></iframe>
    </div>
    <!-- END SECTION GOOGLE MAP -->

  @endsection
