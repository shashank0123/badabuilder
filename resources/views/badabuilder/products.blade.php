
<body class="inner-pages">
 @extends('layouts.badabuilder')

 @section('content')

<!-- 
    <section class="headings">
        <div class="text-heading text-center">
            <div class="container">
                <h1>{{'Project Single'}}</h1>
            </div>
        </div>
    </section>
  -->

  <style type="text/css">
  /* This line can be removed it was just for display on CodePen: */
.container {
  margin-top: 15px;
}

.slider-labels {
  margin-top: 10px;
}

/* Functional styling;
 * These styles are required for noUiSlider to function.
 * You don't need to change these rules to apply your design.
 */
.noUi-target,.noUi-target * {
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -ms-touch-action: none;
  touch-action: none;
  -ms-user-select: none;
  -moz-user-select: none;
  user-select: none;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}

.noUi-target {
  position: relative;
  direction: ltr;
}

.noUi-base {
  width: 100%;
  height: 100%;
  position: relative;
  z-index: 1;
/* Fix 401 */
}

.noUi-origin {
  position: absolute;
  right: 0;
  top: 0;
  left: 0;
  bottom: 0;
}

.noUi-handle {
  position: relative;
  z-index: 1;
}

.noUi-stacking .noUi-handle {
/* This class is applied to the lower origin when
   its values is > 50%. */
  z-index: 10;
}

.noUi-state-tap .noUi-origin {
  -webkit-transition: left 0.3s,top .3s;
  transition: left 0.3s,top .3s;
}

.noUi-state-drag * {
  cursor: inherit !important;
}

/* Painting and performance;
 * Browsers can paint handles in their own layer.
 */
.noUi-base,.noUi-handle {
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}

/* Slider size and handle placement;
 */
.noUi-horizontal {
  height: 4px;
}

.noUi-horizontal .noUi-handle {
  width: 18px;
  height: 18px;
  border-radius: 50%;
  left: -7px;
  top: -7px;
  background-color: #fab702;
  /*background-color: #345DBB;*/
}

/* Styling;
 */
.noUi-background {
  background: #D6D7D9;
}

.noUi-connect {
  background: #fab702;
  /*background: #345DBB;*/
  -webkit-transition: background 450ms;
  transition: background 450ms;
}

.noUi-origin {
  border-radius: 2px;
}

.noUi-target {
  border-radius: 2px;
}

.noUi-target.noUi-connect {
}

/* Handles and cursors;
 */
.noUi-draggable {
  cursor: w-resize;
}

.noUi-vertical .noUi-draggable {
  cursor: n-resize;
}

.noUi-handle {
  cursor: default;
  -webkit-box-sizing: content-box !important;
  -moz-box-sizing: content-box !important;
  box-sizing: content-box !important;
}

.noUi-handle:active {
  border: 8px solid #345DBB;
  border: 8px solid rgba(53,93,187,0.38);
  -webkit-background-clip: padding-box;
  background-clip: padding-box;
  left: -14px;
  top: -14px;
}

/* Disabled state;
 */
[disabled].noUi-connect,[disabled] .noUi-connect {
  background: #B8B8B8;
}

[disabled].noUi-origin,[disabled] .noUi-handle {
  cursor: not-allowed;
}
</style>
<div class="road">
  <div class="container">
    <div class="row">
      <div class="col">
        <a href="{{ url('/') }}">Home</a><span>»</span><span>{{ ucfirst($category ?? '') }}  @if(!empty($keyword))

          Searched results for '<i><b>{{$keyword}}</b></i>'
        @endif</span>
      </div>
    </div>
  </div>
</div>
<!-- END SECTION HEADINGS -->

<!-- START SECTION PROJECTS SINGLE -->
<section class="project-single">
  <div class="container-fluid">
    <div class="row">

      <aside class="col-lg-3 col-md-12" style="background-color: #fff">
        <div class="widget-project-single">
          <div class="project-news" style="padding: 25px;">
            <h5 class="font-weight-bold">Shop By</h5>
            <ul>
               <li><a onclick="getCategory('all')"><i class="fa fa-caret-right" aria-hidden="true"></i>All</a></li>
              @if(count($categories)>0)
              @foreach($categories as $cat)
              <li><a  onclick="getCategory('{{$cat->slug ?? 'all'}}')"><i class="fa fa-caret-right" aria-hidden="true"></i>{{ ucfirst($cat->category_name ?? '') }}</a></li>
              @endforeach
              @endif

            </ul>
          </div>






<div class="project-news" style="padding: 25px;">
            <h5 class="font-weight-bold">Price Filter</h5>
               <div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div id="slider-range"></div>
    </div>
  </div>
  <div class="row slider-labels">
    <div class="col-xs-6 caption">
      <strong>Min:</strong> <span id="slider-range-value1"></span>
    </div>
    <div class="col-xs-6 text-right caption">
      &nbsp;&nbsp;&nbsp;&nbsp;<strong>Max:</strong> <span id="slider-range-value2"></span>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <form>
        <input type="hidden" name="min-value" value="{{$min}}" id="minPrice">
        <input type="hidden" name="max-value" value="{{$max}}" id="maxPrice">
      </form>
    </div>
  </div>
</div>

<div class="btn btn-primary" onclick="getSearchedFilter()">
  Filter
</div>
          </div>


       




          @if(count($brands)>0)

          <div class="project-news" style="padding: 25px;">
            <h5 class="font-weight-bold">Brands</h5>
            <ul>
              @foreach($brands as $brand)
              <li><a onclick="getBrand('{{$brand->slug ?? 'all'}}')"><i class="fa fa-caret-right" aria-hidden="true"></i>{{ ucfirst($brand->brand_name ?? '') }}</a></li>
              @endforeach
            </ul>
          </div>
          @endif



        </div>
      </aside>

      <div class="col-lg-9 col-md-12 project-info">
        <section class="services">
          <div class="container" style="border-bottom: 1px solid #ddd; margin-bottom: 10px">
            <div class="row">
              <div class="col-sm-9">

                <input type="hidden" name="filterCategory" id="filterCategory" value="{{ $slug ?? 'all' }}">
                <input type="hidden" name="filterBrand" id="filterBrand" value="">
                <input type="hidden" name="filterSort" id="filterSort" value="">
                <input type="hidden" name="filterKeyword" id="filterKeyword" value="{{$keyword ?? ''}}">
                @if(!empty($keyword))

                <h3>Searched results for '<i><b>{{$keyword}}</b></i>'</h3>
                @endif

              </div>
              <div class="col-sm-3">
                <select name="sort" id="sortFilter" onchange="getSort()" class="form-control" style="font-size: 14px; margin-right: 25px; height: 45px">
                 <option value=""> Sort By </option>
                 <option value="low-high"> Name (A-Z) </option>
                 <option value="high-low"> Name (Z-A) </option>
                 <option value="priceAsc"> Price (ASC) </option>
                 <option value="priceDesc"> Price (DESC) </option>
                 <option value="new"> Latest  </option>

               </select>
             </div>
           </div>
         </div>
         <div class="container-fluid" id="allCategories">

          <div class="row mt-50" style="margin-top: 45px; min-height: 350px">
            @if(count($searched_products)>0)
            @foreach($searched_products as $product)
            <div class="col-lg-4 col-xs-12 col-sm-12 col-md-6">
              <div class="item mb-30">
                <div class=service-box>
                  <figure class="img-box">
                    <a href="{{ url('product-detail/'.$product->slug) }}"><img src="{{ asset($product->image1) }}" alt=""></a>
                    <figcaption class="default-overlay-outer">
                      <div class="inner">
                        <div class="content-layer">
                          <a class="this-link btn btn-primary" href="#" onclick="addToCart({{$product->id}})">
                            <i class="fa fa-cart-plus"></i>
                          </a>
                          <a class="this-link btn1 btn-primary" href="#"  onclick="addToWishlist({{$product->id}})">
                            <i class="fa fa-heart-o"></i>
                          </a>
                          <input type="hidden" name="stock" id="stock{{$product->id}}" value="{{$product->availability}}">
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="clearfix service-inner-box">
                    <div class=service-icon-box>
                      <img src="{{asset($product->image1)}}" alt="{{ ucfirst($product->name ?? '') }}">
                    </div>
                    <div class=service-content-box>
                      <h3>
                        <a href="{{ url('product-detail/'.$product->slug) }}">{{ ucfirst($product->name ?? '') }}</a>
                      </h3>
                      <p><strike>₹{{$product->mrp ?? '0'}}</strike> ₹{{$product->sell_price ?? '0'}}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
            @else

            <div style="text-align: center; padding: 45px">
              <h3 style="text-align: center;">No result found.</h3></div>

            @endif

          </div>
        </div>
      </section>
    </div>

  </div>
</div>
</section>
<!-- END SECTION PROJECTS SINGLE -->


@endsection

@section('script')


@endsection