<div class="container-fluid" id="allCategories">

          <div class="row mt-50" style="margin-top: 45px; min-height: 350px">
            @if(count($searched_products)>0)
            @foreach($searched_products as $product)
            <div class="col-lg-4 col-xs-12 col-sm-12 col-md-6">
              <div class="item mb-30">
                <div class=service-box>
                  <figure class="img-box">
                    <a href="{{ url('product-detail/'.$product->slug) }}"><img src="{{ asset($product->image1) }}" alt=""></a>
                    <figcaption class="default-overlay-outer">
                      <div class="inner">
                        <div class="content-layer">
                          <a class="this-link btn btn-primary" href="#" onclick="addToCart({{$product->id}})">
                            <i class="fa fa-cart-plus"></i>
                          </a>
                          <a class="this-link btn1 btn-primary" href="#"  onclick="addToWishlist({{$product->id}})">
                            <i class="fa fa-heart-o"></i>
                          </a>
                          <input type="hidden" name="stock" id="stock{{$product->id}}" value="{{$product->availability}}">
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="clearfix service-inner-box">
                    <div class=service-icon-box>
                      <img src="{{asset($product->image1)}}" alt="{{ ucfirst($product->name ?? '') }}">
                    </div>
                    <div class=service-content-box>
                      <h3>
                        <a href="{{ url('product-detail/'.$product->slug) }}">{{ ucfirst($product->name ?? '') }}</a>
                      </h3>
                      <p><strike>₹{{$product->mrp ?? '0'}}</strike> ₹{{$product->sell_price ?? '0'}}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
            @else

            <div style="text-align: center; padding: 45px">
              <h3 style="text-align: center;">No result found.</h3></div>

            @endif

          </div>
        </div>