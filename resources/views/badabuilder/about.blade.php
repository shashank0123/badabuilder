@extends('layouts.badabuilder')

@section('content')

    <section class="headings" style="background-color: #f5f6ff">
        <div class="text-heading text-center">
            <div class="container">
                <h1>About Us</h1>
            </div>
        </div>
    </section>

    <div class="road" style="background-color: #f5f6ff">
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="index.html">Home</a><span>»</span><span>About Us</span>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION HEADINGS -->

    <!-- START SECTION ABOUT -->
    <section class="who-we-are">
        <div class="container">
            <div class="row">
                <div class="col-md-6 who">
                    <img src="{{ asset('front/images/bg/call.jpg') }}" alt="" style="height: 520px;">
                </div>
                <div class="col-md-6 who-1">
                    <div>
                        <h2 class="text-left mb-4" style="margin-top: 25px;">About <span>BADABUILDER</span></h2>
                    </div>
                    <div class="pftext">
                        <p><b>BADABUILDER</b> is an advancement stage that imbues innovation in the development material gracefully chain to drive acquisition and operational productivity. It presents fundamental knowledge that permits foundation temporary workers to choose from different item choices curated utilizing its restrictive calculation, while getting a charge out of scale profits by collected interest. The stage binds together different partners to offer more prominent flexibly chain control and utilizes restrictive credit evaluation procedure to offer proficient exchange money arrangements. </p>
                        <p>

Foundation firms, by ideals of their income cycle and restricted liquidity from conventional FIs, need to draw in merchants and additionally exchange lenders to secure development material. Wholesalers frequently work in a particular item brand blend, adjusting a restricted geological territory. Select exchange agents, utilize long procedures with disagreeable business terms. This leaves framework firms to draw in with various brokers, making their acquisition procedure operationally burdening and monetarily wasteful.</p>
                    </div>
                    <div class="box bg-2">
                       <!--  <a href="about.html" class="button button--wayra button--border-thick button--text-upper button--size-s">read More</a> -->
                        <img src="images/signature.png" class="ml-5" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END SECTION ABOUT -->

    <!-- START SECTION SERVICES -->
    <section class="services">
        <div class="container">
            <div class="section-title">
                <h3>Our Professional</h3>
                <h2>Services</h2>
            </div>
            <div class="row mt-50">
                @if(count($maincategories)>0)
                @foreach($maincategories as $main)
                <div class="col-lg-4 col-xs-12 col-sm-12 col-md-6">
                    <div class="item mb-30">
                        <div class=service-box>
                            <figure class="img-box">
                                <a href="#"><img src="{{ asset($main->category_img ?? '' ) }}" alt=""></a>
                                <figcaption class="default-overlay-outer">
                                    <div class="inner">
                                        <div class="content-layer"><a class="this-link btn btn-primary" href="{{ url('products/'.$main->slug ?? '') }}">Read more</a> </div>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="clearfix service-inner-box">
                                <div class=service-icon-box><img src="{{ asset($main->category_img ?? '' ) }}" alt=""></div>
                                <div class=service-content-box>
                                    <h3><a href="{{ url('products/'.$main->slug ?? '') }}">{{ucfirst($main->category_name ?? '')}}</a></h3>
                                    <p><?php echo substr($main->category_description ?? '',0,50) ?>...</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
           
            </div>
        </div>
    </section>
    <!-- END SECTION SERVICES -->

    <!-- START SECTION TEAM -->
    <section class="team">
        <div class="container">
            <div class="section-title">
                <h3>Meet</h3>
                <h2>OUR TEAM</h2>
            </div>
            <div class="row team-all">
                <!--Team Block-->
                <div class="team-block col-lg-3 col-md-6 col-xs-12">
                    <div class="inner-box team-details">
                        <div class="image team-head">
                            <a href="our-team.html"><img src="images/team/t-1.png" alt="" /></a>
                            <div class="team-hover">
                                <ul class="team-social">
                                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="our-team.html">Arling Tracy</a></h3>
                            <div class="designation">Acountant</div>
                        </div>
                    </div>
                </div>
                <!--Team Block-->
                <div class="team-block col-lg-3 col-md-6 col-xs-12">
                    <div class="inner-box team-details">
                        <div class="image team-head">
                            <a href="our-team.html"><img src="images/team/t-2.png" alt="" /></a>
                            <div class="team-hover">
                                <ul class="team-social">
                                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="our-team.html">Carls Jhons</a></h3>
                            <div class="designation">Financial Advisor</div>
                        </div>
                    </div>
                </div>
                <!--Team Block-->
                <div class="team-block col-lg-3 col-md-6 col-xs-12 pb-none">
                    <div class="inner-box team-details">
                        <div class="image team-head">
                            <a href="our-team.html"><img src="images/team/t-3.png" alt="" /></a>
                            <div class="team-hover">
                                <ul class="team-social">
                                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="our-team.html">Katy Grace</a></h3>
                            <div class="designation">Team Leader</div>
                        </div>
                    </div>
                </div>
                <!--Team Block-->
                <div class="team-block col-lg-3 col-md-6 col-xs-12 pb-none">
                    <div class="inner-box team-details">
                        <div class="image team-head">
                            <a href="our-team.html"><img src="images/team/t-4.png" alt="" /></a>
                            <div class="team-hover">
                                <ul class="team-social">
                                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h3><a href="our-team.html">Mark Web</a></h3>
                            <div class="designation">Founder &amp; CEO</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END SECTION TEAM -->

    @endsection