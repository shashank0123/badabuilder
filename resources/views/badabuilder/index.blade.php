@extends('layouts.badabuilder')

@section('content')

@if(count($banners)>0)

<!-- START REVOLUTION SLIDER 5.0.7 -->
<div id="rev_slider_home_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
    <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
    <div id="rev_slider_home" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
        <ul>
            @foreach($banners as $banner)
            <!-- SLIDE 1 -->
            <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="{{ asset($banner->image_url) }}" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Make an Impact">
                <!-- MAIN IMAGE -->
                <img src="{{ asset($banner->image_url) }}" alt="" data-bgposition="center center" data-bgfit="contain" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" id="slide-1-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full" data-height="full" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-start="1000" data-basealign="slide" data-responsive_offset="on" style="z-index: 5;background-color:rgba(0, 0, 0, 0.35);border-color:rgba(0, 0, 0, 1.00);">
                </div>
                
               </li>

               @endforeach
           </ul>

           <div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: #fab702;"></div>
       </div>
   </div>
   <!-- END REVOLUTION SLIDER -->
   <!-- END SECTION HEADINGS -->
   @endif

   @if(count($topCategories)>0)
   <!-- START SECTION INFO -->
   <section _ngcontent-bgi-c3="" class="featured-boxes-area">
    <div _ngcontent-bgi-c3="" class="container">
        <div _ngcontent-bgi-c3="" class="featured-boxes-inner">
            <div _ngcontent-bgi-c3="" class="row m-0">
                @foreach($topCategories as $top)
                <div _ngcontent-bgi-c3="" class="col-lg-3 col-sm-6 col-md-6 p-0">
                    <div _ngcontent-bgi-c3="" class="single-featured-box">
                        <div _ngcontent-bgi-c3="" class="icon color-fb7756"><img src="{{ $top->category_img ?? '' }}" width="100%" alt=""></div>
                        <h3 _ngcontent-bgi-c3="">{{ $top->category_name ?? '' }}</h3>
                        <p _ngcontent-bgi-c3="">{{ $top->category_description ?? '' }}</p><a _ngcontent-bgi-c3="" class="read-more-btn" href="{{ url('products/'.$top->slug ?? 'all') }}">View Products</a></div>
                    </div>
                    @endforeach    
                </div>
            </div>
        </div>
    </section>
    <!-- END SECTION INFO -->
    @endif

    @if(count($featured_products)>0)
    
    <!-- START SECTION SERVICES -->
    <section class="services">
        <div class="container">
            <div class="section-title">
                <h3>Featured Products</h3>
                <!-- <h2>Services</h2> -->
            </div>
            <div class="row mt-50">
                @foreach($featured_products as $product)
                <div class="col-lg-4 col-xs-12 col-sm-12 col-md-6">
                    <div class="item mb-30">
                        <div class=service-box>
                            <figure class="img-box">
                                <a href="{{ url('product-detail/'.$product->slug) }}"><img src="{{ asset($product->image1) }}" alt=""></a>
                                <figcaption class="default-overlay-outer">
                                    <div class="inner">
                                        <div class="content-layer">
                                            <a class="this-link btn btn-primary" onclick="addToCart({{$product->id}})">
                                                <i class="fa fa-cart-plus"></i>
                                            </a>
                                            <a class="this-link btn1 btn-primary"   onclick="addToWishlist({{$product->id}})">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <input type="hidden" name="stock" id="stock{{$product->id}}" value="{{$product->availability}}">
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="clearfix service-inner-box">
                                <div class=service-icon-box>
                                    <img src="{{asset($product->image1)}}" alt="{{ ucfirst($product->name ?? '') }}">
                                </div>
                                <div class=service-content-box>
                                    <h3>
                                        <a href="{{ url('product-detail/'.$product->slug) }}">{{ ucfirst($product->name ?? '') }}</a>
                                    </h3>
                                    <p><strike>₹{{$product->mrp ?? '0'}}</strike> ₹{{$product->sell_price ?? '0'}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                
            </div>
        </div>
    </section>
    <!-- END SECTION SERVICES -->
    @endif


    @if(count($trending_products)>0)
    
    <!-- START SECTION SERVICES -->
    <section class="services">
        <div class="container">
            <div class="section-title">
                <h3>Trending Products</h3>
                <!-- <h2>Services</h2> -->
            </div>
            <div class="row mt-50">
                @foreach($trending_products as $product)
                <div class="col-lg-4 col-xs-12 col-sm-12 col-md-6">
                    <div class="item mb-30">
                        <div class=service-box>
                            <figure class="img-box">
                                <a href="{{ url('product-detail/'.$product->slug) }}"><img src="{{ asset($product->image1) }}" alt=""></a>
                                <figcaption class="default-overlay-outer">
                                    <div class="inner">
                                        <div class="content-layer">
                                            <a class="this-link btn btn-primary" href="#" onclick="addToCart({{$product->id}})">
                                                <i class="fa fa-cart-plus"></i>
                                            </a>
                                            <a class="this-link btn1 btn-primary" href="#"  onclick="addToWishlist({{$product->id}})">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <input type="hidden" name="stock" id="stock{{$product->id}}" value="{{$product->availability}}">
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="clearfix service-inner-box">
                                <div class=service-icon-box>
                                    <img src="{{asset($product->image1)}}" alt="{{ ucfirst($product->name ?? '') }}">
                                </div>
                                <div class=service-content-box>
                                    <h3>
                                        <a href="{{ url('product-detail/'.$product->slug) }}">{{ ucfirst($product->name ?? '') }}</a>
                                    </h3>
                                    <p><strike>₹{{$product->mrp ?? '0'}}</strike> ₹{{$product->sell_price ?? '0'}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                
            </div>
        </div>
    </section>
    <!-- END SECTION SERVICES -->
    @endif



    @if(!empty($allProducts))
    @php $j=0 @endphp
    @foreach($allProducts as $all)
    
    <!-- START SECTION SERVICES -->
    <section class="services">
        <div class="container" id="allCategories">
            <div class="section-title">
                <h3>{{ucfirst($all['category']->category_name ?? '')}}</h3>
                <!-- <h2>Services</h2> -->
            </div>
            <div class="row mt-50">
                @php $countP=0 @endphp
                @if(!empty($all['products']))
                @foreach($all['products'] as $product)
                <div class="col-lg-3 col-xs-12 col-sm-12 col-md-4">
                    <div class="item mb-30">
                        <div class=service-box>
                            <figure class="img-box">
                                <a href="{{ url('product-detail/'.$product->slug) }}"><img src="{{ asset($product->image1) }}" alt=""></a>
                                <figcaption class="default-overlay-outer">
                                    <div class="inner">
                                        <div class="content-layer">
                                            <a class="this-link btn btn-primary" href="#" onclick="addToCart({{$product->id}})">
                                                <i class="fa fa-cart-plus"></i>
                                            </a>
                                            <a class="this-link btn1 btn-primary" href="#"  onclick="addToWishlist({{$product->id}})">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <input type="hidden" name="stock" id="stock{{$product->id}}" value="{{$product->availability}}">
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="clearfix service-inner-box">
                                <div class=service-icon-box>
                                    <img src="{{asset($product->image1)}}" alt="{{ ucfirst($product->name ?? '') }}">
                                </div>
                                <div class=service-content-box>
                                    <h3>
                                        <a href="{{ url('product-detail/'.$product->slug) }}">{{ ucfirst($product->name ?? '') }}</a>
                                    </h3>
                                    <p><strike>₹{{$product->mrp ?? '0'}}</strike> ₹{{$product->sell_price ?? '0'}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
                
            </div>
        </div>
    </section>
    <!-- END SECTION SERVICES -->
    @endforeach
    @endif



    <!-- STAR SECTION WHO WE ARE -->
    <!-- <section class="who-we-are">
        <div class="container">
            <div class="section-title mb-4">
                <h3>Who</h3>
                <h2>We Are</h2>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-12 col-xs-12">
                    <div class="who-we-are-content">
                        <p>The best place for elit, sed do eiusmod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et lorna aliquatd minimam, quis nostrud exercitation oris nisi ut aliquip ex ea.</p>
                    </div>
                    <div class="skills">
                        <div class="row">
                            <div class="col-12">
                                <h4 class="mb-4">Our Skills</h4>
                                <h3>Construction</h3>
                                <div id="bar1" class="barfiller">
                                    <div class="tipWrap">
                                        <span class="tip"></span>
                                    </div>
                                    <span class="fill" data-percentage="95"></span>
                                </div>
                                <h3>Civil Engineering</h3>
                                <div id="bar2" class="barfiller">
                                    <div class="tipWrap">
                                        <span class="tip"></span>
                                    </div>
                                    <span class="fill" data-percentage="90"></span>
                                </div>
                                <h3>Architecture Design</h3>
                                <div id="bar3" class="barfiller">
                                    <div class="tipWrap">
                                        <span class="tip"></span>
                                    </div>
                                    <span class="fill" data-percentage="85"></span>
                                </div>
                                <h3>Remodeling</h3>
                                <div id="bar4" class="barfiller">
                                    <div class="tipWrap">
                                        <span class="tip"></span>
                                    </div>
                                    <span class="fill" data-percentage="80"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-xs-12">
                    <div class="wprt-image-video w50">
                        <img alt="image" src="images/bg/bg-video.jpg">
                        <a class="icon-wrap popup-video popup-youtube" href="https://www.youtube.com/watch?v=2xHQqYRcrx4">
                            <i class="fa fa-play"></i>
                        </a>
                        <div class="iq-waves">
                            <div class="waves wave-1"></div>
                            <div class="waves wave-2"></div>
                            <div class="waves wave-3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- END SECTION WHO WE ARE -->    

    <!-- START SECTION PROJECTS -->
    <!-- <section class="project">
        <div class="container-fluid">
            <div class="head-project">
                <div class="section-title">
                    <h3 class="home-1">See Our</h3>
                    <h2>Projects</h2>
                </div>
                <div class="inf-btn pro">
                    <a href="project.html" class="btn btn-pro btn-secondary btn-lg">See All Projects</a>
                </div>
            </div>
            <div class="row portfolio-items">
                <div class="item col-lg-3 col-md-6 col-xs-12 landscapes no-gutters">
                    <div class="project-single">
                        <div class="project-inner">
                            <div class="project-head">
                                <img src="images/projects/g-1.jpg" alt="#">
                            </div>
                            <div class="project-bottom">
                                <h4><a href="project-single.html">Construction Project</a><span class="category">Civil Engineering</span></h4>
                            </div>
                            <div class="button">
                                <a class="img-poppu btn" href="images/projects/g-1.jpg" data-rel="lightcase:myCollection:slideshow"><i class="fa fa-photo"></i></a>
                                <a href="project-single.html" class="btn"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item col-lg-3 col-md-6 col-xs-12 people no-gutters">
                    <div class="project-single">
                        <div class="project-inner">
                            <div class="project-head">
                                <img src="images/projects/g-2.jpg" alt="#">
                            </div>
                            <div class="project-bottom">
                                <h4><a href="project-single.html">Construction Project</a><span class="category">Civil Engineering</span></h4>
                            </div>
                            <div class="button">
                                <a class="img-poppu btn" href="images/projects/g-2.jpg" data-rel="lightcase:myCollection:slideshow"><i class="fa fa-photo"></i></a>
                                <a href="project-single.html" class="btn"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item col-lg-3 col-md-6 col-xs-12 people landscapes no-gutters">
                    <div class="project-single">
                        <div class="project-inner">
                            <div class="project-head">
                                <img src="images/projects/g-3.jpg" alt="#">
                            </div>
                            <div class="project-bottom">
                                <h4><a href="project-single.html">Construction Project</a><span class="category">Civil Engineering</span></h4>
                            </div>
                            <div class="button">
                                <a class="img-poppu btn" href="images/projects/g-3.jpg" data-rel="lightcase:myCollection:slideshow"><i class="fa fa-photo"></i></a>
                                <a href="project-single.html" class="btn"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item col-lg-3 col-md-6 col-xs-12 people landscapes no-gutters">
                    <div class="project-single">
                        <div class="project-inner">
                            <div class="project-head">
                                <img src="images/projects/g-4.jpg" alt="#">
                            </div>
                            <div class="project-bottom">
                                <h4><a href="project-single.html">Construction Project</a><span class="category">Civil Engineering</span></h4>
                            </div>
                            <div class="button">
                                <a class="img-poppu btn" href="images/projects/g-4.jpg" data-rel="lightcase:myCollection:slideshow"><i class="fa fa-photo"></i></a>
                                <a href="project-single.html" class="btn"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- END SECTION PROJECTS -->


    

    <!-- START SECTION PRICING -->
    <!-- <section class="pricing-table">
        <div class="container">
            <div class="section-title">
                <h3>Pricing</h3>
                <h2>Packages</h2>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="plan text-center">
                        <span class="plan-name">Standard <small>Monthly plan</small></span>
                        <p class="plan-price"><sup class="currency">$</sup><strong>99</strong><sub>.99</sub></p>
                        <ul class="list-unstyled">
                            <li>Unlimited Extra Feature</li>
                            <li>1 Month Free Resource</li>
                            <li>100 Domain Hosting</li>
                            <li>SSL Shopping Cart</li>
                            <li>24/7 Tech Support</li>
                            <li>Unlimited Project</li>
                        </ul>
                        <a class="btn btn-secondary btn-lg" href="#.">Sign Up Now</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="plan text-center featured no-mgb yes-mgb">
                        <span class="plan-name">Professional <small>Monthly plan</small></span>
                        <p class="plan-price"><sup class="currency">$</sup><strong>149</strong><sub>.99</sub></p>
                        <ul class="list-unstyled">
                            <li>Unlimited Extra Feature</li>
                            <li>1 Month Free Resource</li>
                            <li>100 Domain Hosting</li>
                            <li>SSL Shopping Cart</li>
                            <li>24/7 Tech Support</li>
                            <li>Unlimited Project</li>
                        </ul>
                        <a class="btn btn-secondary btn-lg" href="#.">Sign Up Now</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <div class="plan text-center no-mgb">
                        <span class="plan-name">Premium <small>Monthly plan</small></span>
                        <p class="plan-price"><sup class="currency">$</sup><strong>399</strong><sub>.99</sub></p>
                        <ul class="list-unstyled">
                            <li>Unlimited Extra Feature</li>
                            <li>1 Month Free Resource</li>
                            <li>100 Domain Hosting</li>
                            <li>SSL Shopping Cart</li>
                            <li>24/7 Tech Support</li>
                            <li>Unlimited Project</li>
                        </ul>
                        <a class="btn btn-secondary btn-lg" href="#.">Sign Up Now</a>
                    </div>
                </div>

            </div>
        </div>
    </section> -->
    <!-- END SECTION PRICING -->

    <!-- START SECTION TESTIMONIALS -->
    <!-- <section class="testimonials">
        <div class="container">
            <div class="section-title">
                <h3>Client</h3>
                <h2>Testimonials</h2>
            </div>
            <div class="owl-carousel style1">
                <div class="test-1">                    
                    <img src="images/testimonials/ts-1.jpg" alt="">
                    <h3 class="mt-3">Lisa Smith</h3>
                    <h6>New York</h6>
                    <ul class="starts text-center">
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                    </ul>
                    <p class="mt-4">Lorem ipsum dolor sit amet, ligula magna at etiam aliquip venenatis. Vitae sit felis donec, suscipit bu tortor felis donec ipsum dolor sit the construction.</p>
                </div>
                <div class="test-1">
                    <img src="images/testimonials/ts-2.jpg" alt="">
                    <h3 class="mt-3">Jhon Morris</h3>
                    <h6>Los Angeles</h6>
                    <ul class="starts text-center">
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star-o"></i>
                        </li>
                    </ul>
                    <p class="mt-4">Lorem ipsum dolor sit amet, ligula magna at etiam aliquip venenatis. Vitae sit felis donec, suscipit bu tortor felis donec ipsum dolor sit the construction.</p>
                </div>
                <div class="test-1">
                    <img src="images/testimonials/ts-3.jpg" alt="">
                    <h3 class="mt-3">Mary Deshaw</h3>
                    <h6>Chicago</h6>
                    <ul class="starts text-center">
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                    </ul>
                    <p class="mt-4">Lorem ipsum dolor sit amet, ligula magna at etiam aliquip venenatis. Vitae sit felis donec, suscipit bu tortor felis donec ipsum dolor sit the construction.</p>
                </div>
                <div class="test-1">
                    <img src="images/testimonials/ts-4.jpg" alt="">
                    <h3 class="mt-3">Gary Steven</h3>
                    <h6>Philadelphia</h6>
                    <ul class="starts text-center">
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star-o"></i>
                        </li>
                    </ul>
                    <p class="mt-4">Lorem ipsum dolor sit amet, ligula magna at etiam aliquip venenatis. Vitae sit felis donec, suscipit bu tortor felis donec ipsum dolor sit the construction.</p>
                </div>
                <div class="test-1">
                    <img src="images/testimonials/ts-5.jpg" alt="">
                    <h3 class="mt-3">Cristy Mayer</h3>
                    <h6>San Francisco</h6>
                    <ul class="starts text-center">
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                    </ul>
                    <p class="mt-4">Lorem ipsum dolor sit amet, ligula magna at etiam aliquip venenatis. Vitae sit felis donec, suscipit bu tortor felis donec ipsum dolor sit the construction.</p>
                </div>
                <div class="test-1">
                    <img src="images/testimonials/ts-6.jpg" alt="">
                    <h3 class="mt-3">Ichiro Tasaka</h3>
                    <h6>Houston</h6>
                    <ul class="starts text-center">
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star"></i>
                        </li>
                        <li><i class="fa fa-star-o"></i>
                        </li>
                    </ul>
                    <p class="mt-4">Lorem ipsum dolor sit amet, ligula magna at etiam aliquip venenatis. Vitae sit felis donec, suscipit bu tortor felis donec ipsum dolor sit the construction.</p>
                </div>
            </div>
        </div>
    </section> -->
    <!-- END SECTION TESTIMONIALS -->

    <!-- START SECTION BLOG -->
    <section class="blog-section">
        <div class="container">
            <div class="section-title">
                <h3>Best Sellers</h3>
                <!-- <h2>News</h2> -->
            </div>
            <div class="news-wrap">
                <div class="row">

                    <div class="col-xl-12 col-md-6 col-xs-12 col-lg-12">
                        <div class="row">
                            @if(count($best_sellers_produts)>0)
                            @php $countP=0 @endphp
                            @foreach($best_sellers_produts as $product)
                            @php $countP++ @endphp
                            <div class="col-sm-6">
                                <div class="news-item news-item-sm ">

                                    <a href="{{ url('product-detail/'.$product->slug) }}" class="news-img-link">
                                        <div class="news-item-img">
                                            <img class="resp-img" src="{{asset($product->image1)}}" alt="{{ucfirst($product->name ?? '')}}">
                                        </div>
                                    </a>
                                    <div class="news-item-text">
                                        <a href="{{ url('product-detail/'.$product->slug) }}"><h3>{{ucfirst($product->name ?? '')}}</h3></a>
                                        <span class="date"><strike>₹{{$product->mrp ?? '0'}}</strike> ₹{{$product->sell_price ?? '0'}}</span>
                                        <div class="news-item-descr">
                                            <p>{{ucfirst($product->short_descriptions ?? '') }}</p>
                                        </div>
                                        <input type="hidden" name="stock" id="stock{{$product->id}}" value="{{$product->availability}}">
                                        <div class="news-item-bottom">
                                            <a href="{{ url('product-detail/'.$product->slug) }}" class="news-link">View Detail...</a>
                                            <ul class="action-list">
                                                <li class="action-item"><i class="fa fa-heart"></i> </li>
                                                <li class="action-item"><i class="fa fa-cart-plus"></i> </li>
                                                <!-- <li class="action-item"><i class="fa fa-share-alt"></i> <span>122</span></li> -->
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @if($countP == 0)
                            <div style="text-align: center; padding: 50px">No products available</div>
                            @endif
                            @endif
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </section>
    <!-- END SECTION BLOG -->


    @if(count($brands)>0)
    <!-- STAR SECTION PARTNERS -->
    <div class="partners">
        <div class="container">
            <div class="owl-carousel style2">
                @foreach($brands as $brand)
                <div class="owl-item" style="width: 140px; margin-right: 60px;"><img src="{{ asset($brand->brand_img) }}" alt="{{ $brand->brand_name }}"></div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- END SECTION PARTNERS -->
    @endif

    

    @endsection