@extends('layouts.badabuilder')

@section('content')
<style type="text/css">
	.tnc h3 {
		font-size: 16px; margin-bottom: 25px; color: #333; font-weight: bold;
	}
	.tnc ol ,
	.tnc ul {
		list-style-type: none;
	}
</style>
<section class="headings" >
	<div class="text-heading text-center">
		<div class="container">
			<h1>Terms & conditions</h1>
		</div>
	</div>
</section>

<div class="road" >
	<div class="container">
		<div class="row">
			<div class="col">
				<a href="{{ url('/') }}">Home</a><span>»</span><span>Terms & conditions</span>
			</div>
		</div>
	</div>
</div>
<!-- END SECTION HEADINGS -->
<div class="container-fluid tnc">
	<div style="margin: 45px;background-color: #fff; border-radius: 10px;">
		<div style="padding: 25px; text-align: justify;">


			<h3>This archive is an electronic record as far as Information Technology Act, 2000 and rules there under as relevant and the corrected arrangement spertaining to electronic records in different rules as altered by the Information Technology Act, 2000. Further,this report is distributed as per the arrangements of Rule 3 (1) of the Information Technology (Intermediaries rules) Rules, 2011 that require distributing the guidelines and guidelines, security strategy and Terms of Use for access or utilization of www.Brick2wall.com website.This electronic record is produced by a PC framework and doesn't require any physical or advanced marks. </h3>

			<p>The area name www.Brick2wall.com (hereinafter alluded to as "Site") is possessed by Brick2wall Technologies Pvt. Ltd., with its enrolled office at 2036/160, Ganeshpura, Trinagar, Delhi – 110035,INDIA (hereinafter alluded to as "Brick2wall"). </p>

			<p>Your utilization of the Website and administrations and devices are administered by the accompanying terms and conditions ("Terms of Use") as appropriate to the Website including the relevant approaches which are joined in this by method of reference. On the off chance that you execute on the Website, you will be dependent upon the approaches that are appropriate to the Website for such exchange. By negligible utilization of the Website, you will contract with Brick2wall Technologies Pvt. Ltd. furthermore, these terms and conditions including the strategies comprise your coupling commitments, with Brick2wall. </p>

			<p>With the end goal of these Terms of Use, any place the unique circumstance so requires "You", "YOUR" or "Client" will mean any normal or lawful individual who has consented to turn into a purchaser on the Website by giving Registration Data while enlisting on the Website as Registered User. Brick2wall permits the client to ride the Website for making buys without enrolling on the Website. The expression "We", "Us", "Our" will mean Brick2wall Technologies Pvt. Ltd. </p>

			<p>At the point when You utilize any of the administrations gave by Us through the Website, including however not restricted to, (for example Item Reviews, Seller Reviews), You will be dependent upon the principles, rules, approaches, terms, and conditions appropriate to such help, and they will be esteemed to be consolidated into this Terms of Use and will be considered as a vital part of this Terms of Use. We save the right, at Our sole attentiveness, to change, alter, include or evacuate parts of these Terms of Use, whenever with no earlier composed notification to You. It is Your duty to audit these Terms of Use occasionally for refreshes/changes. Your proceeded with utilization of the Website following the posting of changes will imply that You acknowledge and consent to the amendments. For whatever length of time that You consent to these Terms of Use, We award You an individual, non-select, non-transferable, constrained benefit to enter and utilize the Website. </p>

			<p>Getting to, BROWSING OR OTHERWISE USING THE SITE INDICATES YOUR AGREEMENT TO ALL THE TERMS AND CONDITIONS UNDER THESE TERMS OF USE, SO PLEASE READ THE TERMS OF USE CAREFULLY BEFORE PROCEEDING. By impliedly or explicitly tolerating these Terms of Use, You likewise acknowledge and consent to be limited by Brick2wall Policies (counting however not constrained to Privacy Policy) as altered now and again. </p>

			<p><b>Participation Eligibility </b></p>

			<p>Utilization of the Website is accessible just to people who can shape legitimately restricting agreements under Indian Contract Act, 1872. People who are "awkward to contract" inside the significance of the Indian Contract Act, 1872 including minors, un-released insolvents and so forth are not qualified to utilize the Website. On the off chance that you are a minor for example younger than 18 years, you will not enroll as a User of the Brick2wall site and will not execute on or utilize the site. As a minor on the off chance that you wish to utilize or execute on site, such use or exchange might be made by your legitimate gatekeeper or guardians on the Website. Brick2wall maintains all authority to end your enrollment and/or decline to give you access to the Website on the off chance that it is brought to Brick2wall's notification or on the off chance that it is found that you are younger than 18 years. </p>

			<p><b>Your Account and Registration Obligations </b></p>

			<p>On the off chance that You utilize the Website, You will be answerable for keeping up the privacy of your Display Name and Password and You will be liable for all exercises that happen under your Display Name and Password. You concur that on the off chance that You give any data that is false, off base, not present or fragmented or We have sensible grounds to speculate that such data is false, off base, not present or deficient, or not as per the this Terms of Use, We will reserve the privilege to inconclusively suspend or end or square access of your participation on the Website and decline to give You access to the Website. </p>

			<p><b>Correspondences </b></p>

			<p>At the point when You utilize the Website or send messages or other information, data or correspondence to us, You concur and comprehend that You are speaking with Us through electronic records and You agree to get interchanges by means of electronic records from Us occasionally and as and when required. We may speak with you by email or by such other method of correspondence, electronic or something else. </p>

			<p><b>Stage for Transaction and Communication </b></p>

			<p>The Website is a stage that Users use to collaborate with each other or with shippers selling their items on the stage, for their exchanges. Brick2wall isn't and can't be a gathering to or control in any way any exchange between the Website's Users. </p>

			<p><b>Henceforward: </b></p>

			<ol>
				<li>
					1. All business/authoritative terms are offered by and consented to among Buyers and Sellers alone. The business/legally binding terms incorporate without constraint value, dispatching costs, installment strategies, installment terms, date, period and method of conveyance, guarantees identified with items and administrations and after deals administrations identified with items and administrations. Brick2wall doesn't have any control or doesn't decide or exhort or in any capacity include itself in the contribution or acknowledgment of such business/legally binding terms between the Buyers and Sellers. 
				</li>

				<li>
				2. Brick2wall doesn't make any portrayal or Warranty as to particulars, (for example, quality, esteem, attractiveness, and so forth) of the items or administrations proposed to be sold or offered to be sold or bought on the Website. Brick2wall doesn't verifiably or expressly bolster or underwrite the deal or acquisition of any items or administrations on the Website. Brick2wall acknowledges no obligation for any blunders or oversights, regardless of whether for the benefit of itself or outsiders. </li>

				<li>
					3. Brick2wall isn't answerable for any non-execution or break of any agreement went into among Buyers and Sellers. Brick2wall can't and doesn't ensure that the concerned Buyers and additionally Sellers will play out any exchange closed on the Website. Brick2wall will not and isn't required to intervene or resolve any contest or difference among Buyers and Sellers. 
				</li>

				<li>
					4. Brick2wall doesn't anytime of time during any exchange among Buyer and Seller on the Website come into or claim any of the items or administrations offered by Seller nor does it anytime gain title to or have any rights or claims over the items or administrations offered by Seller to Buyer. 
				</li>

				<li>
					5. At no time will Brick2wall hold any right, title or enthusiasm over the items nor will Brick2wall have any commitments or liabilities in regard of such agreement went into among Buyers and Sellers. Brick2wall isn't liable for unacceptable or postponed execution of administrations or harms or deferrals because of items which are unavailable, inaccessible or put in a raincheck for. 
				</li>

				<li>
					6. The Website is just a stage that can be used by Users to arrive at a bigger base to purchase and sell items or administrations. Brick2wall is just giving a stage to correspondence and it is concurred that the agreement available to be purchased of any of the items or administrations will be a carefully bipartite agreement between the Seller and the Buyer.At no time will Brick2wall hold any right, title or enthusiasm over the items nor will Brick2wall have any commitments or liabilities in regard of such agreement. 
				</li>

				<li>
					7. You will freely concur upon the way and terms and states of conveyance, installment, protection and so forth with the seller(s) that You execute with. 

					Disclaimer: Pricing on any product(s) as is considered the Website may because of some specialized issue, typographical mistake or item data distributed by dealer might be inaccurately reflected and in such an occasion, vender may drop such order(s).
				</li>

				<li>
					8. You discharge and repay Brick2wall as well as any of its officials and delegates from any cost, harm, risk or other result of any of the activities of the Users of the Website and explicitly defer any cases that you may have for this sake under any appropriate law. Despite its sensible endeavors for that benefit, Brick2wall can't assume liability or control the data gave by different Users which is made accessible on the Website. You may see other User's data as hostile, unsafe, conflicting, off base, or misleading. It would be ideal if you use alert and practice safe exchanging when utilizing the Website.Please note that there could be hazards in managing underage people or individuals acting under affectation. 
				</li>
			</ol>

			<p><b>Charges </b></p>

			<p>Participation on the Website is free for purchasers. Brick2wall doesn't charge any expense for perusing and purchasing on the Website. Brick2wall claims all authority to change its Fee Policy every now and then. Specifically, Brick2wall may at its sole watchfulness present new administrations and adjust a few or the entirety of the current administrations offered on the Website. In such an occasion Brick2wall maintains whatever authority is needed to present charges for the new administrations offered or alter/present expenses for existing administrations, all things considered. Changes to the Fee Policy will be posted on the Website and such changes will naturally become taking effect right now after they are posted on the Website. Except if in any case expressed, all charges will be cited in Indian Rupees. You will be exclusively answerable for consistence of every single pertinent law incorporating those in India for making installments to Brick2wall Technologies Pvt. Ltd. </p>

			<p><b>Utilization of the Website </b></p>

			<p>You concur, embrace and affirm that Your utilization of Website will be carefully administered by the accompanying restricting standards: </p>

			<ol>
				<li>
					1. You will not have, show, transfer, change, distribute, transmit, update or offer any data which: 
					<ol>

						<li>

							a. has a place with someone else and to which You don't reserve any privilege to; 
						</li>
						<li>
							b. is terribly destructive, annoying, profane, abusive, vulgar, obscene, paedophilic, derogatory, intrusive of another's protection, contemptuous, or racially, ethnically offensive, stigmatizing, relating or empowering tax evasion or betting, or in any case unlawful in any way whatever; or unlawfully undermining or unlawfully hassling including yet not restricted to "revolting portrayal of ladies" inside the importance of the Indecent Representation of Women (Prohibition) Act, 1986; 
						</li>
						<li>
							c. is misdirecting in any capacity; 
						</li>
						<li>
							d. is obviously hostile to the online network, for example, explicitly unequivocal substance, or substance that advances foulness, pedophilia, prejudice, bias, scorn or physical damage of any sort against any gathering or person; 
						</li>
						<li>
							e. bugs or promoters provocation of someone else; 
						</li>
						<li>
							f. includes the transmission of "garbage mail", "junk letters", or spontaneous mass mailing or "spamming"; 
						</li>
						<li>
							g. advances criminal operations or direct that is damaging, undermining, revolting, slanderous or offensive; 
						</li>
						<li>
							h. encroaches upon or abuses any outsider's privileges [including, yet not restricted to, protected innovation rights, privileges of security (counting without impediment unapproved revelation of an individual's name, email address, physical location or telephone number) or privileges of exposure; 
						</li>
						<li>
							I. advances an illicit or unapproved duplicate of someone else's copyrighted work (see "Copyright objection" beneath for guidelines on the best way to hold up a grievance about transferred copyrighted material, for example, giving pilfered PC projects or connections to them, giving data to go around produce introduced duplicate secure gadgets, or giving pilfered music or connections to pilfered music records; 
						</li>
						<li>
							j. contains limited or secret key just access pages, or concealed pages or pictures (those not connected to or from another open page); 
						</li>
						<li>
							k. gives material that adventures individuals in a sexual, fierce or in any case unseemly way or requests individual data from anybody; 
						</li>
						<li>
							l. gives instructional data about criminal operations, for example, making or purchasing unlawful weapons, disregarding somebody's security, or giving or making PC infections; 
						</li>
						<li>
							m. contains video, photos, or pictures of someone else (with a minor or a grown-up). 
						</li>
						<li>
							n. attempts to increase unapproved get to or surpasses the extent of approved access to the Website or to profiles, web journals, networks, account data, bulletins,friend demand, or different zones of the Website or requests passwords or individual distinguishing data for business or unlawful purposes from different clients; 
						</li>
						<li>
							o. participates in business exercises and additionally deals without Our earlier composed assent, for example, challenges, sweepstakes, bargain, promoting and fraudulent business models, or the purchasing or selling of "virtual" items identified with the Website. All through this Terms of Use, Brick2wall's earlier composed assent implies a correspondence originating from Brick2wall's Legal Department, explicitly in light of Your solicitation, and explicitly tending to the action or lead for which You look for approval; 
						</li>
						<li>
							p. requests betting or participates in any betting movement which We, in Our sole attentiveness, accept is or could be understood as being unlawful; 
						</li>
						<li>
							q. meddles with another USER's utilization and pleasure in the Website or some other person's User and satisfaction in comparable administrations; 
						</li>
						<li>
							r. alludes to any site or URL that, in Our sole prudence, contains material that is improper for the Website or some other site, contains content that would be precluded or damages the letter or soul of these Terms of Use. 
						</li>
						<li>
							s. hurt minors in any capacity; 
						</li>
						<li>
							t. encroaches any patent, trademark, copyright or other exclusive rights or outsider's competitive innovations or privileges of exposure or security or will not be false or include the offer of fake or taken items; 
						</li>
						<li>
							u. abuses any law until further notice in power; 
						</li>
						<li>
							v. bamboozles or deceives the recipient/clients about the inception of such messages or imparts any data which is horribly hostile or threatening in nature; 
						</li>
						<li>
							w. imitate someone else; 
						</li>
						<li>
							x. contains programming infections or some other PC code, documents or projects intended to intrude on, annihilate or limit the usefulness of any PC asset; or contains any trojan ponies, worms, delayed bombs, cancelbots, easter eggs or other PC programming schedules that may harm, adversely meddle with, decrease estimation of, secretly capture or confiscate any framework, information or individual data; 
						</li>
						<li>
							y. undermines the solidarity, trustworthiness, safeguard, security or power of India, inviting relations with outside states, or open request or makes affectation the commission of any cognizable offense or forestalls examination of any offense or is offending some other country. 
						</li>
						<li>
							z. will not be bogus, erroneous or deceiving; 
						</li>
						<li>
							aa. will not, legitimately or in a roundabout way, offer, endeavor to offer, exchange or endeavor to exchange any thing, the managing of which is disallowed or confined in any way under the arrangements of any material law, rule, guideline or rule for the present in power. 
						</li>
						<li>
							bb. will not make obligation for Us or cause Us to lose (in entire or to some extent) the administrations of Our network access supplier ("ISPs") or different providers; 
						</li>
					</ol>
				</li>
				<li>
					2. You will not utilize any "profound connection", "page-scratch", "robot", "insect" or other programmed gadget, program, calculation or philosophy, or any comparable or equal manual procedure, to get to, procure, duplicate or screen any part of the Website or any Content, or in any capacity imitate or go around the navigational structure or introduction of the Website or any Content, to get or endeavor to get any materials, reports or data through any methods not deliberately made accessible through the Website. We claim Our authority to bar any such action. 
				</li>
				<li>

					3. You will not endeavor to increase unapproved access to any bit or highlight of the Website, or some other frameworks or systems associated with the Website or to any server, PC, arrange, or to any of the administrations offered on or through the Website, by hacking, secret phrase "mining" or some other ill-conceived implies.
				</li>
				<li>
					4. You will not test, sweep or test the weakness of the Website or any system associated with the Website nor break the security or verification quantifies on the Website or any system associated with the Website. You may not invert look-into, follow or try to follow any data on some other User of or guest to Website, or some other client, remembering any record for the Website not possessed by You, to its source, or adventure the Website or any assistance or data made accessible or offered by or through the Website, in any capacity where the intention is to uncover any data, including however not restricted to individual recognizable proof or data, other than Your own data, as accommodated by the Website. 
				</li>
				<li>
					5. You will not make any negative, maligning or abusive statement(s) or comment(s) about Us or the brand name or area name utilized by Us includingthe terms Brick2wall, Brick2wall.com, or in any case participate in any direct or activity that may discolor the picture or notoriety, of Brick2wall orsellers on stage or in any case discolor or weaken any Brick2wall's exchange or administration marks, exchange name and additionally generosity related with such exchange or administration marks,trade name as might be claimed or utilized by us. You concur that You won't make any move that forces a nonsensical or excessively huge burden on theinfrastructure of the Website or Brick2wall's frameworks or systems, or any frameworks or systems associated with Brick2wall. 
				</li>
				<li>
					6. You make a deal to avoid utilizing any gadget, programming or routine to meddle or endeavor to meddle with the best possible working of the Website or any exchange beingconducted on the Website, or with some other individual's utilization of the Website. 
				</li>
				<li>
					7. You may not fashion headers or in any case control identifiers so as to camouflage the cause of any message or transmittal You send to Us on or through the Website or any help offered on or through the Website. You may not imagine that You are, or that You speak to, another person, or imitate some other individual or element. 
				</li>
				<li>
					8. You may not utilize the Website or any substance for any reason that is unlawful or restricted by these Terms of Use, or to request the presentation of any illegalactivity or other action which encroaches the privileges of Brick2wall and/or others. 
				</li>
				<li>
					9. You will consistently guarantee full consistence with the pertinent arrangements of the Information Technology Act, 2000 and runs thereunder as appropriate and as revised every now and then and furthermore all relevant Domestic laws, rules and guidelines (counting the arrangements of any material Exchange Control Laws or Regulations in Force) and International Laws, Foreign Exchange Laws, Statutes, Ordinances and Regulations (counting, however not restricted to Sales Tax/VAT, Income Tax, Octroi, Service Tax, Central Excise, Custom Duty, Local Levies) in regards to Your utilization of Our administration and Your posting, buy, requesting of offers to buy, and offer of items or administrations. You will not take part in any exchange in a thing or administration, which is disallowed by the arrangements of anyapplicable law including trade control laws or guidelines for now in power. 
				</li>
				<li>
					10. Exclusively to empower Us to utilize the data You gracefully Us with, so we are not abusing any rights You may have in Your Information, You consent to give Us a non-selective, around the world, interminable, unalterable, eminence free, sub-licensable (through different levels) option to practice the copyright, exposure, database rights or some other rights You have in Your Information, in any media presently known or not as of now known, regarding Your Information. We will just utilize Yourinformation as per the Terms of Use and Privacy Policy relevant to utilization of the Website. 
				</li>
				<li>
					11. Every now and then, You will be answerable for giving data identifying with the items or administrations proposed to be sold by You. In this association, You embrace that all such data will be exact in all regards. You will not overstate or over underscore the traits of such items or administrations to deceive different Users in any way. 
				</li>
				<li>
					12. You will not take part in publicizing to, or sales of, different Users of the Website to purchase or sell any items or administrations, including, however not constrained to,products or administrations identified with that being shown on the Website or identified with us. You may not transmit any networking letters or spontaneous business or junkemail to different Users by means of the Website. It will be an infringement of these Terms of Use to utilize any data got from the Website so as to irritate, misuse, or damage someone else, or so as to contact, promote to, request, or offer to someone else other than Us without Our earlier unequivocal assent. So as to shield Our Users from such promoting or sales, We maintain all authority to limit the quantity of messages or messages which a client may send to different Users in any 24-hour time frame which We consider suitable in its sole attentiveness. You comprehend that We have the privilege consistently to uncover any data (counting the character of the people giving data or materials on the Website) as important to fulfill any law, guideline or legitimate legislative solicitation. This may incorporate, without restriction, exposure of the data regarding examination of supposed criminal behavior or sales of criminal behavior or in light of a legitimate court request or summon. What's more, We can (and You thusly explicitly approve Us to) uncover any data about You to law implementation or other government authorities, as we, in Our sole carefulness, accept essential or fitting regarding the examination and additionally goals of potential violations, particularly those that may include individual injury. 
				</li>
				<li>
					13. We save the right, yet have no commitment, to screen the materials posted on the Website. Brick2wall will reserve the option to evacuate or alter any substance that in its sole tact disregards, or is affirmed to damage, any appropriate law or either the soul or letter of these Terms of Use. Despite this right, YOU remain exclusively answerable for the substance of the material you post on the site and in your private messages. If it's not too much trouble be exhorted that such Content posted doesn't really reflect Brick2wall sees. In no occasion will Brick2wall expect or have any duty or obligation for any Content posted or for any cases, harms or misfortunes coming about because of utilization of Content and additionally appearance of Content on the Website. You therefore speak to and warrant that You have every single essential right in and to all Content which You give and all data it contains and that such Content will not encroach any exclusive or different privileges of outsiders or contain any offensive, tortious, or in any case unlawful data. 
				</li>
				<li>
					14. Your correspondence or professional interactions with, or support in advancements of, promoters found on or through the Website, including installment and conveyance of related items or administrations, and some other terms, conditions, guarantees or portrayals related with such dealings, are exclusively among You and such publicist. We will not be dependable or at risk for any misfortune or harm of any kind acquired as the consequence of any such dealings or as the aftereffect of the nearness of such sponsors on the Website. 
				</li>
				<li>
					15. It is conceivable that different clients (counting unapproved clients or "programmers") may post or transmit hostile or profane materials on the Website and that You might be automatically presented to such hostile and vulgar materials. It is additionally feasible for others to get individual data about You because of your utilization of the Website, and that the beneficiary may utilize such data to annoy or harm You. We don't affirm of such unapproved use, yet by utilizing theWebsite You recognize and concur that We are not liable for the utilization of any close to home data that You freely reveal or share with others on the Website. It would be ideal if you cautiously select the kind of data that You openly reveal or share with others on the Website. 
				</li>
				<li>
					16. Brick2wall will have all the rights to make vital move and guarantee harms that may happen because of your contribution/support in any capacity all alone or through gathering/s of individuals, purposefully or inadvertently in DoS/DDoS (Distributed Denial of Services). 
				</li>
			</ol>
			<p><b>
				Substance Posted nearby 
			</b></p>

			<ol>
				<li>
					1. All content, illustrations, UIs, visual interfaces, photos, trademarks, logos, sounds, music and work of art (by and large, "Content"), is an outsider client created substance and Brick2wall has no power over such outsider client produced content as Brick2wall is just a go-between for the reasons for this Terms of Use. 
				</li>
				<li>
					2. Aside from as explicitly gave in these Terms of Use, no piece of the Website and no Content might be duplicated, repeated, republished, transferred, posted, freely showed, encoded, deciphered, transmitted or circulated in any capacity (counting "reflecting") to some other PC, server, Website or other vehicle for distribution or dispersion or for any business venture, without Brick2wall's express earlier composed assent. 
				</li>
				<li>
					3. You may utilize data on the items and administrations deliberately made accessible on the Website for downloading, gave that You (1) don't expel anyproprietary notice language in all duplicates of such records, (2) utilize such data just for your own, non-business educational reason and don't duplicate or post such data on any organized PC or communicate it in any media, (3) make no alterations to any such data, and (4) don't make any extra portrayals or guarantees identifying with such reports.
				</li>
			</ol>


		</div>
	</div>
</div>
@endsection