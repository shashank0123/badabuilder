<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href='{{ backpack_url('company-info') }}'><i class='fa fa-info'></i> <span>Company Info</span></a></li>


<li><a href='{{ backpack_url('headline') }}'><i class='fa fa-file'></i> <span>Headline</span></a></li>

<li><a href='{{ backpack_url('brand') }}'><i class='fa fa-file'></i> <span>Brands</span></a></li>
<li><a href='{{ backpack_url('banner') }}'><i class='fa fa-flag'></i> <span>Banners</span></a></li>
<li><a href='{{ backpack_url('category') }}'><i class='fa fa-list-alt'></i> <span>Categories</span></a></li>
<li><a href='{{ backpack_url('product') }}'><i class='fa fa-product-hunt'></i> <span>Products</span></a></li>
<li><a href='{{ backpack_url('review') }}'><i class='fa fa-star'></i> <span>Review</span></a></li>
<li><a href='{{ backpack_url('offer') }}'><i class='fa fa-gift'></i> <span>Offers</span></a></li>
<li><a href='{{ backpack_url('customer') }}'><i class='fa fa-users'></i> <span>Customers</span></a></li>
<li><a href='{{ backpack_url('order') }}'><i class='fa fa-first-order'></i> <span>Orders</span></a></li>

<li><a href='{{ backpack_url('faq') }}'><i class='fa fa-question-circle'></i> <span>Manage FAQ</span></a></li>
<li><a href='{{ backpack_url('newsletter') }}'><i class='fa fa-envelope'></i> <span>Newsletter</span></a></li>
<li><a href='{{ backpack_url('testimonial') }}'><i class='fa fa-quote-left'></i> <span>Testimonial</span></a></li>
<li><a href='{{ backpack_url('blog') }}'><i class='fa fa-file'></i> <span>Blogs</span></a></li>
<!-- <li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li> -->
<!-- <li><a href="{{ url(config('backpack.base.route_prefix').'/page') }}"><i class="fa fa-file-o"></i> <span>Pages</span></a></li> -->

<li><a href='{{ backpack_url('help') }}'><i class='fa fa-ticket'></i> <span>Help</span></a></li>


