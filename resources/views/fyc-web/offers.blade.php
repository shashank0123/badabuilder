@extends('layouts/fyc')

@section('content')

<div class="shop-area pt-90 pb-90">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">

                 <h3>Offers</h3>
                <div id="shop-2" class="tab-pane active">
                    <div class="row">
                        @php $i=1 @endphp
                        @if(!empty($offers))
                        @foreach($offers as $offer)
                        @php $i++ @endphp
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <div class="shop-list-wrap mb-30">
                                <div class="row">
                                    <div class="col-xl-4 col-lg-5 col-md-6 col-sm-6">
                                        <div class="product-list-img">
                                            <a onclick="offerDeatil({{$offer->id}})">
                                                <img src="{{url('/'.$offer->image_url)}}" alt="Product Style">
                                            </a>
                                            {{-- <div class="product-list-quickview">
                                                <a data-toggle="modal" data-target="#exampleModal" title="Quick View" href="#"><i class="la la-plus"></i></a>
                                            </div> --}}
                                        </div>
                                    </div>
                                    <div class="col-xl-8 col-lg-7 col-md-6 col-sm-6">
                                        <div class="shop-list-content">
                                            {{-- <span>Chair</span> --}}
                                            <h4><a onclick="offerDeatil({{$offer->id}})">{{$offer->offer_title}}</a></h4>
                                            {{-- <div class="pro-list-price">
                                                <span>₹40.00</span>
                                                <span class="old-price">₹50.00</span>
                                            </div> --}}
                                            <p>{{$offer->offer_short_description}}</p>
                                            {{-- <div class="product-list-action">
                                                <a title="Wishlist" href="#"><i class="la la-heart-o"></i></a>

                                                <a title="Add To Cart" href="#"><i class="la la-shopping-cart"></i></a>
                                            </div> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                        @endforeach
                        @endif

                        @if($i==1)
                        <div style="text-align: center;padding: 5%; margin: auto; font-size: 24px;">No Offers Available</div>
                        @endif
                        
                    </div>
                </div>
                {{-- <div class="pagination-style text-center">
                    <ul>
                        <li><a class="prev" href="#"><i class="la la-angle-left"></i></a></li>
                        <li><a href="#">01</a></li>
                        <li><a href="#">02</a></li>
                        <li><a class="active" href="#">03</a></li>
                        <li><a href="#">04</a></li>
                        <li><a href="#">05</a></li>
                        <li><a href="#">06</a></li>
                        <li><a class="next" href="#"><i class="la la-angle-right"></i></a></li>
                    </ul>
                </div> --}}
            </div>                       
        </div>
    </div>
</div>

@endsection

@section('script')

<script>

function offerDeatil(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            
$.ajax({
    url: '/offer-detail',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id},
         success: function (data) {
            offer = data.offer;

           // alert(data.wishItem);
           $('#offer-detail').show();
           $('#offer_img').attr('scr','/'+offer.image_url);
           $('#offer_head').text(offer.offer_title);
           $('#offer_desc').text(offer.offer_long_description);

           alert(offer.id); 
           
        },
        failure: function (data) {
           Swal(data.message);
        }
});
}

</script>
@endsection