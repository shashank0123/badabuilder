<?php 
use App\Models\Address;
?>
@extends('layouts.fyc')

@section('content')

<!-- <div class="breadcrumb-area bg-img" style="background-image:url(/fyc-new/images/bg/breadcrumb.jpg);">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <h2>My account page</h2>
            <ul>
                <li>
                    <a href="/homew">Home</a>
                </li>
                <li class="active">My account </li>
            </ul>
        </div>
    </div>
</div> -->

<!-- my account wrapper start -->
<div class="my-account-wrapper pt-20 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
<h3>My Account</h3><br><br>
                <!-- My Account Page Start -->
                <div class="myaccount-page-wrapper">

                    @if(session('message'))
                        <div class="alert alert-success" id="message" style="margin-top: 10px; text-align: center;">
                          {{ session('message') }}
                        </div>
                        @endif

                    <!-- My Account Tab Menu Start -->
                    <div class="row">
                        <div class="col-lg-3 col-md-4">
                            <div class="myaccount-tab-menu nav" role="tablist">
                                <a href="#dashboad" class="active" data-toggle="tab"><i class="fa fa-dashboard"></i>
                                Dashboard</a>
                                <a href="#account-info" data-toggle="tab"><i class="fa fa-user"></i> My Account Details</a>    
                                <a href="#orders" data-toggle="tab"><i class="fa fa-cart-arrow-down"></i> Orders</a>
                                <!-- <a href="#download" data-toggle="tab"><i class="fa fa-cloud-download"></i> Help & Support</a> -->
                                <!-- <a href="#payment-method" data-toggle="tab"><i class="fa fa-credit-card"></i> Payment Method</a> -->
                                <a href="#address-edit" data-toggle="tab"><i class="fa fa-map-marker"></i> Address</a>
                                <a id="account-logout-tab" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();" role="tab" aria-selected="false"><i class="fa fa-sign-out"></i>Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  @csrf
                              </form>

                          </div>
                      </div>
                      <!-- My Account Tab Menu End -->
                      <!-- My Account Tab Content Start -->
                      <div class="col-lg-9 col-md-8">
                        <div class="tab-content" id="myaccountContent">
                            <!-- Single Tab Content Start -->
                            <div class="tab-pane fade show active" id="dashboad" role="tabpanel">
                                <div class="myaccount-content">
                                    <h3>Dashboard</h3>
                                    <div class="welcome">
                                        <p>Hello, <strong>{{ucfirst($user->name)}}</strong> {{-- (If Not <strong>Tuntuni !</strong><a href="/login" class="logout"> Logout</a>) --}}</p>
                                    </div>

                                    <p class="mb-0">From your account dashboard. you can easily check & view your recent orders, manage your shipping and billing addresses and edit your password and account details.</p>
                                </div>
                            </div>
                            <!-- Single Tab Content End -->
                            <!-- Single Tab Content Start -->
                            <div class="tab-pane fade" id="orders" role="tabpanel">
                                <div class="myaccount-content">
                                    <h3>Orders</h3>
                                    <div class="myaccount-table table-responsive text-center">
                                        <table class="table table-bordered">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>ORDER ID</th>
                                                    <th>DATE</th>
                                                    <th>ADDRESS</th>
                                                    <th>PAYMENT MODE</th>
                                                    <th>STATUS</th>
                                                    <th>TOTAL</th>
                                                    <th colspan="2">Action</th>
                                                </tr>
                                            </thead>
                                            @if($orderDetails != Null)
                                            <tbody>
                                                @foreach($orderDetails as $order)
                                                <?php 
                                                $add = Address::where('id',$order->shipping_address)->first(); 
                                                ?>
                                                <tr>
                                                    <td><a class="account-order-id" href="javascript:void(0)">{{$order->id}}</a></td>
                                                    <td>{{explode(' ',$order->created_at)[0]}}</td>
                                                    <td>
                                                        @if($add != Null)
                                                        {{$add->address}}
                                                        @endif
                                                        {{-- {{str_limit($order->address, 20)}} --}}
                                                    </td>
                                                    <td>{{$order->payment_method}}</td>
                                                    <td>{{str_replace('_', ' ', $order->status)}}</td>
                                                    <td>Rs. {{$order->price}} for {{$order->ItemCount}} items</td>
                                                    <td>
                                                      <a href="/ordered-item-details/{{$order->id}}" ><span><u><b>View</b></u></span></a>
                                                  </td>
                                                  <td>
                                                      <a href="/ordered-item-track/{{$order->id}}" ><span><u><b>Track</b></u></span></a>
                                                  </td>
                                              </tr>
                                              @endforeach

                                          </tbody>
                                          @endif
                                      </table>
                                  </div>
                              </div>
                          </div>
                          <!-- Single Tab Content End -->
                          <!-- Single Tab Content Start -->
                          <div class="tab-pane fade" id="download" role="tabpanel">
                            <div class="myaccount-content">
                                <a href="/help-support/create" class="btn btn-primary" style="background-color: #000033; margin-bottom: 20px"><span>Add Query</span></a>
                                <h3>Help & Support</h3>
                                <div class="myaccount-table table-responsive text-center">
                                    @if(!empty($help))
                                    <table class="table table-bordered">
                                        <thead class="thead-light">
                                            <tr>
                                                <th>Token</th>
                                                <th>Subject</th>
                                                <th>Message</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($help as $item)
                                            <tr>
                                              <td><a class="account-help-token" href="javascript:void(0)">{{$item->token}}</a></td>
                                              <td>{{$item->subject}}</td>
                                              <td>{{$item->message}}</td>
                                              <td>{{$item->created_at->format('d-m-Y') }}</td>
                                              <td>{{$item->status}}</td>
                                              <td>
                                                  <a href="/reply?token={{$item->token}}" class="btn btn-primary"><span>Reply</span></a>
                                              </td>
                                          </tr>
                                          @endforeach

                                      </tbody>
                                  </table>
                                  @endif
                              </div>
                          </div>
                      </div>
                      <!-- Single Tab Content End -->
                      <!-- Single Tab Content Start -->
                      <div class="tab-pane fade" id="payment-method" role="tabpanel">
                        <div class="myaccount-content">
                            <h3>Payment Method</h3>
                            <p class="saved-message">You Can't Saved Your Payment Method yet.</p>
                        </div>
                    </div>
                    <!-- Single Tab Content End -->
                    <!-- Single Tab Content Start -->
                    <div class="tab-pane fade" id="address-edit" role="tabpanel">
                        <div class="myaccount-content row">
                            <div class="col-sm-6">
                                <h3>Billing Address</h3>
                                <address>
                                    <p style="color: #000"><strong>{{ucfirst($user->name)}}</strong></p>
                                    <p style="color: #000">@if($address)

                                      {{$address->address ?? ''}}, {{$address->city ?? ''}}, {{$address->state ?? ''}}, {{$address->country ?? ''}} - {{$address->pin_code ?? ''}}<br>
                                      @if(!empty($address->phone))
                                      Contact - {{$address->phone}}
                                      @endif
                                      @else
                                      <span> </span>
                                  @endif</p>
                              </address>
                              {{-- <a href="#" class="check-btn sqr-btn "><i class="fa fa-edit"></i> Edit Address</a> --}}
                          </div>
                          <div class="col-sm-6">
                            <h3>Shipping Address</h3>
                            <address>
                                <!-- <p style="color: #000"><strong>{{$user->name}}</strong></p> -->
                                <p><ul>
                                  @if(!empty($addresses))
                                  @foreach($addresses as $address)
                                  <li> -> 
                                    {{$address->address ?? ''}}, {{$address->city ?? ''}}, {{$address->state ?? ''}}, {{$address->country ?? ''}} - {{$address->pin_code ?? ''}}<br>
                                    @if(!empty($address->phone))
                                    Contact - {{$address->phone}}
                                    @endif
                                </li>

                                @endforeach
                                @else
                                <ul>
                                  <span> </span>
                              @endif</p>
                          </address>
                          {{-- <a href="#" class="check-btn sqr-btn "><i class="fa fa-edit"></i> Edit Address</a> --}}
                      </div>

                  </div>
              </div>
              <!-- Single Tab Content End -->
              <!-- Single Tab Content Start -->
              <div class="tab-pane fade" id="account-info" role="tabpanel">
                <div class="myaccount-content">
                    <h3>Account Details</h3>
                    <div class="account-details-form">
                        <form action="/update-profile" method="POST">

                            <input type="hidden" name="add_id" value="@if(!empty($upd_add->id)){{$upd_add->id}}@else{{'0'}}@endif">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="single-input-item">
                                        <label for="name" class="required">Full Name</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="single-input-item">
                                        <label for="email" class="required">Email Address</label>
                                        <input type="email" id="email" name="email" value="{{$user->email}}" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            {{-- //Address --}}
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="single-input-item">
                                        <label for="address" class="required">Address</label>
                                        <input type="text" class="form-control" name="address" id="address" value="@if(!empty($upd_add->address)){{$upd_add->address}}@endif" />
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="single-input-item">
                                        <label for="state" class="required">State</label>
                                        <input type="text" id="state" name="state" value="@if(!empty($upd_add->state)){{$upd_add->state}}@endif" class="form-control"/>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="single-input-item">
                                        <label for="city" class="required">City</label>
                                        <input type="text" id="city" name="city" value="@if(!empty($upd_add->city)){{$upd_add->city}}@endif" class="form-control"/>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="single-input-item">
                                        <label for="pin_code" class="required">PIN Code</label>
                                        <input type="text" id="pin_code" name="pin_code" value="@if(!empty($upd_add->pin_code)){{$upd_add->pin_code}}@endif" class="form-control"/>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="single-input-item">
                                        <label for="phone" class="required">Phone</label>
                                        <input type="text" id="phone" name="phone" value="@if(!empty($upd_add->phone)){{$upd_add->phone}}@endif" class="form-control"/>
                                    </div>
                                </div>

                                <div class="single-input-item">
                                <button stye="submit" name="edit" class="check-btn sqr-btn ">Update Profile</button>
                            </div>
                            </div>
                        </form>


                        <form action="/update-password" method="POST">
                            <fieldset>
                                <legend>Password change</legend>
                                <div class="single-input-item">
                                    <label for="current-password" class="required">Current Password</label>
                                    <input type="password" class="form-control" id="current-password" name="current-password" />
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="single-input-item">
                                            <label for="new-password" class="required">New Password</label>
                                            <input type="password" class="form-control" name="new-password" id="new-password" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="single-input-item">
                                            <label for="confirm-password" class="required">Confirm Password</label>
                                            <input type="password" class="form-control" name="confirm-password" id="confirm-password" />
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="single-input-item">
                                <button stye="submit" name="edit" class="check-btn sqr-btn ">Save Changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> <!-- Single Tab Content End -->
        </div>
    </div> <!-- My Account Tab Content End -->
</div>
</div> <!-- My Account Page End -->
</div>
</div>
</div>
</div>

@endsection