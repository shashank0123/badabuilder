@extends('layouts.fyc')

@section('content')

<style type="text/css">
 .bg-color { background-color: #000033!important; }

 @media screen and (min-width: 991px)  { 
  #desktop-view{ display: block!important; }
  #mobile-view{ display: none!important; }
}

@media screen and (max-width: 991px)  {
  #desktop-view{ display: none!important; }
  #mobile-view{ display: block!important; }
} 

</style>

<div class="shop-area pt-90 pb-90">
 <div class="container-fluid">
  <div class="row ">
   
   <div class="col-lg-12">
    <div class="shop-bottom-area">
     <div class="tab-content jump">
      <div id="shop-1" class="tab-pane active">
       <div class="row">
        <?php $i=0; ?>
        @if($product !=null)
        @foreach($product as $prod)
        <?php
        $i++;
        $discount = $prod->mrp - $prod->sell_price;

        if($discount>0)
         $discount = ($discount*100)/$prod->mrp;
       ?>
       <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
         <div class="product-wrap product-border-1 product-img-zoom mb-15">
          <div class="product-img" style="height: 300px; ">
           <a href="/product-detail/{{$prod->slug}}"><img src="{{url('/'.$prod->image1)}}" alt="product" style="height: 300px; width: auto"></a>
           <span class="price-dec">-{{$discount}}%</span>
           <div class="product-action-2">
            <a data-toggle="modal" data-target="#exampleModal" title="Quick View" data-myid="{{$prod->id}}"><i class="la la-search"></i></a>
            <a title="Add To Cart" onclick="addToCart({{$prod->id}})"><i class="la la-cart-plus"></i></a>
            <a title="Wishlist" onclick="addToWishlist({{$prod->id}})"><i class="la la-heart-o"></i></a>
          </div>
        </div>
        <div class="product-content product-content-padding">
         <h4><a href="/product-detail/{{$prod->slug}}">{{substr($prod->name,0,18)}}.</a></h4>
         <div class="price-addtocart">
          <div class="product-price">
           <span>₹{{$prod->sell_price}}.00</span>
         </div>
       </div>
     </div>
   </div>
 </div>
 @endforeach
 

 @endif                        
</div>
</div>

@if($i==0)

<div style="text-align: center; font-size: 42px; margin: 100px 0">
  No Result Found
</div>
@endif

</div>
</div>
</div>

</div>
</div>
</div>


@endsection


@section('script')


<script>

  function addToCart(id){
    var quantity = $('#quantity'+id).val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        // alert(id+" / "+quantity );

        $.ajax({
         url: '/add-to-cart',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, quantity: quantity},
         success: function (data) {
           // alert(data.cartItem);
           $('#cartItem').html(data.cartItem)
         },
         failure: function (data) {
           alert(data.message);
         }
       });
      }

      function addToWishlist(id){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?> ;

        if(userId == 0){
          alert('Please Login First');
        }
        else{
        // alert(id);

        $.ajax({
         url: '/add-to-wishlist',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, userId : userId},
         success: function (data) {
           // alert(data.wishItem);
           $('#wishItem').html(data.wishItem);
         },
         failure: function (data) {
           alert(data);
         }
       });
      }
    }
  </script>

  @endsection
