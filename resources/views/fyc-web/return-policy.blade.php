@extends('layouts/fyc')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10 terms">
            <br>
            <h2>Return Policy</h2>
            <br>  
            

            <p>You as a customer can cancel your order anytime up to the cut-off time of the slot for which you have placed an order by calling our customer service. In such a case we will refund any payments already made by you for the order as a Store Credit , which can be used on future purchases . If we suspect any fraudulent transaction by any customer or any transaction which defies the terms & conditions of using the website, we at our sole discretion could cancel such orders. We will maintain a negative list of all fraudulent transactions and customers and would deny access to them or cancel any orders placed by them.</p>
            <br>

            <h4>SHIPPING AND DELIVERY</h4>

            <ul>
                <li>    
                    1. Packages are generally dispatched within 2 days after receipt of payment and are shipped via reputed national couriers with tracking. Whichever shipment choice you make, we will provide you with a link to track your package online.
                </li>
                <li>2. Shipping fees includes handling and packing fees as well as postage costs. Handling fees are fixed, whereas transport fees vary according to total weight of the shipment. We advise you to group your items in one order. We cannot group two distinct orders placed separately, and shipping fees will apply to each of them. Your package will be dispatched at your own risk, but special care is taken to protect fragile objects.
                </li>
                <li>3. Boxes are amply sized and your items are well-protected.</li>
                <li>4. Our courier partners will be able to deliver the shipment to you during working days between Monday through Saturday: 9am to 7pm. Working days exclude public holidays and Sundays. Delivery time is subject to factors beyond our control including unexpected travel delays from our courier partners and transporters due to weather conditions and strikes.</li>
                <li>5. As soon as your package ships, we will email you your package tracking information.</li>
            </ul>

            <br><br><br>

        </div>
        <div class="col-sm-1"></div>
    </div>
</div>

@endsection
