<!doctype html>
<html class="no-js" lang="en">



<head>
    <meta charset="utf-8">

 
  <meta name="keywords" content="HomeGlare-Ecommerce">
  <meta name="Navin Kumar Yadav (navinkumaryadav1997@gmail.com) " content="Frontend Developer">
 
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>HomeGlare-Ecommerce</title>
    <meta name="robots" content="HomeGlare-Ecommerce, Ecommerce" />
    <meta name="description" content="HomeGlare-Ecommerce">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/logo/logo.png">

    <!-- CSS
	============================================ -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/vendor/bootstrap.min.css">
    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="assets/css/vendor/line-awesome.css">
    <link rel="stylesheet" href="assets/css/vendor/themify.css">
    <!-- othres CSS -->
    <link rel="stylesheet" href="assets/css/plugins/animate.css">
    <link rel="stylesheet" href="assets/css/plugins/owl-carousel.css">
    <link rel="stylesheet" href="assets/css/plugins/slick.css">
    <link rel="stylesheet" href="assets/css/plugins/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/plugins/jquery-ui.css">
    <link rel="stylesheet" href="assets/css/style.css">

</head>

<body>
    <div class="main-wrapper">
        <header class="header-area bg-black sticky-bar header-padding-1">
            <div class="main-header-wrap">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-xl-2 col-lg-3 logo-border">
                            <div class="logo ">
                                <a href="home.php"><img src="assets/images/logo/logo.png" alt="logo"></a>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-6 d-flex justify-content-center">
                            <div class="main-menu menu-common-style menu-lh-2 menu-margin-2 menu-white">
                                <nav>
                                    <ul>
                                          <li><a href="home.php">Home</a></li>
                                           <li><a href="about-us.php">About us </a></li>
                                           
                                      <li><a href="product.php">Products</a></li>

                                        <li><a href="contact.php">Contact Us</a></li>
                                        
                                       
                                        <li><a href="offers.php">Offers & Service</a></li>
                                         <li><a href="login.php">Login</a></li>
                                         <li><a href="register.php">Sign Up</a></li>


                                       <!--  <li class="angle-shape"><a href="/productw">Shop </a>
                                            <ul class="mega-menu">
                                                <li><a class="menu-title" href="#">Shop Layout</a>
                                                    <ul>
                                                        <li><a href="/productw">standard style</a></li>
                                                        <li><a href="shop-2.html">standard style 2</a></li>
                                                        <li><a href="shop-2-col.html">shop 2 column</a></li>
                                                        <li><a href="shop-no-sidebar.html">shop no sidebar</a></li>
                                                        <li><a href="shop-fullwide.html">shop fullwide</a></li>

                                                    </ul>
                                                </li>
                                                <li><a class="menu-title" href="#">Shop Layout</a>
                                                    <ul>
                                                        <li><a href="shop-fullwide-no-sidebar.html">fullwide no sidebar </a></li>
                                                        <li><a href="shop-list.html">list style</a></li>
                                                        <li><a href="shop-list-2col.html">list 2 column</a></li>
                                                        <li><a href="shop-list-no-sidebar.html">list no sidebar</a></li>
                                                    </ul>
                                                </li>
                                                <li><a class="menu-title" href="#">Product Details</a>
                                                    <ul>
                                                        <li><a href="product-details.html">standard style</a></li>
                                                        <li><a href="product-details-2.html">standard style 2</a></li>
                                                        <li><a href="product-details-tab1.html">tab style 1</a></li>
                                                        <li><a href="product-details-tab2.html">tab style 2</a></li>
                                                        <li><a href="product-details-tab3.html">tab style 3 </a></li>
                                                    </ul>
                                                </li>
                                                <li><a class="menu-title" href="#">Product Details</a>
                                                    <ul>
                                                        <li><a href="product-details-gallery.html">gallery style </a></li>
                                                        <li><a href="product-details-sticky.html">sticky style</a></li>
                                                        <li><a href="product-details-slider.html">slider style</a></li>
                                                        <li><a href="product-details-affiliate.html">Affiliate style</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                       <li class="angle-shape"><a href="#">Pages</a>
                                            <ul class="submenu">
                                                <li><a href="about-us.html">about us </a></li>
                                                <li><a href="cart.html">cart page </a></li>
                                                <li><a href="checkout.html">checkout </a></li>
                                                <li><a href="compare.html">compare </a></li>
                                                <li><a href="wishlist.html">wishlist </a></li>
                                                <li><a href="my-account.html">my account </a></li>
                                                <li><a href="contact.html">contact us </a></li>
                                                <li><a href="login-register.html">login/register </a></li>
                                            </ul>
                                        </li>


                                         -->

                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-3 header-right-border">
                            <div class="header-right-wrap header-right-white ">
                                <div class="search-wrap common-style mr-45">
                                    <button class="search-active">
                                        <i class="la la-search"></i>
                                    </button>
                                </div>
                                <div class="cart-wrap common-style">
                                    <button class="cart-active">
                                        <i class="la la-shopping-cart"></i>
                                        <sup><p class="cartsup">2</p></sup>
                                       <!--  <span class="count-style count-theme-color">2 Items</span> -->
                                    </button>
                                    <div class="shopping-cart-content">
                                        <div class="shopping-cart-top">
                                            <h4>Your Cart</h4>
                                            <a class="cart-close" href="#"><i class="la la-close"></i></a>
                                        </div>
                                        <ul>
                                            <li class="single-shopping-cart">
                                                <div class="shopping-cart-img">
                                                    <a href="#"><img alt="" src="assets/images/cart/cart-1.jpg"></a>
                                                    <div class="item-close">
                                                        <a href="#"><i class="sli sli-close"></i></a>
                                                    </div>
                                                </div>
                                                <div class="shopping-cart-title">
                                                    <h4><a href="#">Golden Easy Spot Chair.</a></h4>
                                                    <span>₹99.00</span>
                                                </div>
                                                <div class="shopping-cart-delete">
                                                    <a href="#"><i class="la la-trash"></i></a>
                                                </div>
                                            </li>
                                            <li class="single-shopping-cart">
                                                <div class="shopping-cart-img">
                                                    <a href="#"><img alt="" src="assets/images/cart/cart-2.jpg"></a>
                                                    <div class="item-close">
                                                        <a href="#"><i class="sli sli-close"></i></a>
                                                    </div>
                                                </div>
                                                <div class="shopping-cart-title">
                                                    <h4><a href="#">Golden Easy Spot Chair.</a></h4>
                                                    <span>₹99.00</span>
                                                </div>
                                                <div class="shopping-cart-delete">
                                                    <a href="#"><i class="la la-trash"></i></a>
                                                </div>
                                            </li>
                                            <li class="single-shopping-cart">
                                                <div class="shopping-cart-img">
                                                    <a href="#"><img alt="" src="assets/images/cart/cart-3.jpg"></a>
                                                    <div class="item-close">
                                                        <a href="#"><i class="sli sli-close"></i></a>
                                                    </div>
                                                </div>
                                                <div class="shopping-cart-title">
                                                    <h4><a href="#">Golden Easy Spot Chair.</a></h4>
                                                    <span>₹99.00</span>
                                                </div>
                                                <div class="shopping-cart-delete">
                                                    <a href="#"><i class="la la-trash"></i></a>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="shopping-cart-bottom">
                                            <div class="shopping-cart-total">
                                                <h4>Subtotal <span class="shop-total">$290.00</span></h4>
                                            </div>
                                            <div class="shopping-cart-btn btn-hover default-btn text-center">
                                                <a class="black-color" href="checkout.php">Continue to Chackout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- main-search start -->
                <div class="main-search-active">
                    <div class="sidebar-search-icon">
                        <button class="search-close"><span class="la la-close"></span></button>
                    </div>
                    <div class="sidebar-search-input">
                        <form>
                            <div class="form-search">
                                <input id="search" class="input-text" value="" placeholder="Search Now" type="search">
                                <button>
                                    <i class="la la-search"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="header-small-mobile header-mobile-white">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-6">
                            <div class="mobile-logo">
                                <a href="home.php">
                                    <img  height="30px" alt="" src="assets/images/logo/logo.png">
                                </a>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="header-right-wrap">
                                <div class="cart-wrap common-style">
                                    <button class="cart-active">
                                        <i class="la la-shopping-cart"></i>
                                       <!--  <span class="count-style">2 Items</span> -->
                                        <sup><p class="cartsup">2</p></sup>
                                    </button>
                                    <div class="shopping-cart-content">
                                        <div class="shopping-cart-top">
                                            <h4>Your Cart</h4>
                                            <a class="cart-close" href="#"><i class="la la-close"></i></a>
                                        </div>
                                        <ul>
                                            <li class="single-shopping-cart">
                                                <div class="shopping-cart-img">
                                                    <a href="#"><img alt="" src="assets/images/cart/cart-1.jpg"></a>
                                                    <div class="item-close">
                                                        <a href="#"><i class="sli sli-close"></i></a>
                                                    </div>
                                                </div>
                                                <div class="shopping-cart-title">
                                                    <h4><a href="#">Golden Easy Spot Chair.</a></h4>
                                                    <span>₹99.00</span>
                                                </div>
                                                <div class="shopping-cart-delete">
                                                    <a href="#"><i class="la la-trash"></i></a>
                                                </div>
                                            </li>
                                            <li class="single-shopping-cart">
                                                <div class="shopping-cart-img">
                                                    <a href="#"><img alt="" src="assets/images/cart/cart-2.jpg"></a>
                                                    <div class="item-close">
                                                        <a href="#"><i class="sli sli-close"></i></a>
                                                    </div>
                                                </div>
                                                <div class="shopping-cart-title">
                                                    <h4><a href="#">Golden Easy Spot Chair.</a></h4>
                                                    <span>₹99.00</span>
                                                </div>
                                                <div class="shopping-cart-delete">
                                                    <a href="#"><i class="la la-trash"></i></a>
                                                </div>
                                            </li>
                                            <li class="single-shopping-cart">
                                                <div class="shopping-cart-img">
                                                    <a href="#"><img alt="" src="assets/images/cart/cart-3.jpg"></a>
                                                    <div class="item-close">
                                                        <a href="#"><i class="sli sli-close"></i></a>
                                                    </div>
                                                </div>
                                                <div class="shopping-cart-title">
                                                    <h4><a href="#">Golden Easy Spot Chair.</a></h4>
                                                    <span>₹99.00</span>
                                                </div>
                                                <div class="shopping-cart-delete">
                                                    <a href="#"><i class="la la-trash"></i></a>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="shopping-cart-bottom">
                                            <div class="shopping-cart-total">
                                                <h4>Subtotal <span class="shop-total">₹290.00</span></h4>
                                            </div>
                                            <div class="shopping-cart-btn btn-hover default-btn text-center">
                                                <a class="black-color" href="checkout.php">Continue to Chackout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mobile-off-canvas">
                                    <a class="mobile-aside-button" href="#"><i class="la la-navicon la-2x"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="mobile-off-canvas-active">
            <a class="mobile-aside-close"><i class="la la-close"></i></a>
            <div class="header-mobile-aside-wrap">
                <div class="mobile-search">
                    <form class="search-form" action="#">
                        <input type="text" placeholder="Search entire store…">
                        <button class="button-search"><i class="la la-search"></i></button>
                    </form>
                </div>
                <div class="mobile-menu-wrap">
                    <!-- mobile menu start -->
                    <div class="mobile-navigation">
                        <!-- mobile menu navigation start -->
                        <nav>
                            <ul class="mobile-menu">
                                 <li><a href="home.php">Home</a></li>
                                           <li><a href="about-us.php">About us </a></li>

                                           <li><a href="product.php">Products</a></li>

                                        <li><a href="contact.php">Contact Us</a></li>
                                        
                                       
                                        <li><a href="offers.php">Offers & Service</a></li>
                               
                                <!-- <li class="menu-item-has-children "><a href="#">shop</a>
                                    <ul class="dropdown">
                                        <li class="menu-item-has-children"><a href="#">shop layout</a>
                                            <ul class="dropdown">
                                                <li><a href="/productw">standard grid style</a></li>
                                                <li><a href="shop-2.html">standard style 2</a></li>
                                                <li><a href="shop-2-col.html">shop 2 column</a></li>
                                                <li><a href="shop-no-sidebar.html">shop no sidebar</a></li>
                                                <li><a href="shop-fullwide.html">shop fullwide</a></li>
                                                <li><a href="shop-fullwide-no-sidebar.html">fullwide no sidebar </a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item-has-children"><a href="#">shop list layout</a>
                                            <ul class="dropdown">
                                                <li><a href="shop-list.html">list style</a></li>
                                                <li><a href="shop-list-2col.html">list 2 column</a></li>
                                                <li><a href="shop-list-no-sidebar.html">list no sidebar</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item-has-children"><a href="#">product details</a>
                                            <ul class="dropdown">
                                                <li><a href="product-details.html">standard style</a></li>
                                                <li><a href="product-details-2.html">standard style 2</a></li>
                                                <li><a href="product-details-tab1.html">tab style 1</a></li>
                                                <li><a href="product-details-tab2.html">tab style 2</a></li>
                                                <li><a href="product-details-tab3.html">tab style 3 </a></li>
                                                <li><a href="product-details-gallery.html">gallery style </a></li>
                                                <li><a href="product-details-sticky.html">sticky style</a></li>
                                                <li><a href="product-details-slider.html">slider style</a></li>
                                                <li><a href="product-details-affiliate.html">Affiliate style</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><a href="#">Pages</a>
                                    <ul class="dropdown">
                                        <li><a href="about-us.html">about us </a></li>
                                        <li><a href="cart.html">cart page </a></li>
                                        <li><a href="checkout.html">checkout </a></li>
                                        <li><a href="compare.html">compare </a></li>
                                        <li><a href="wishlist.html">wishlist </a></li>
                                        <li><a href="my-account.html">my account </a></li>
                                        <li><a href="contact.html">contact us </a></li>
                                        <li><a href="login-register.html">login/register </a></li>
                                    </ul>
                                </li> -->
                               
                            </ul>
                        </nav>
                        <!-- mobile menu navigation end -->
                    </div>
                    <!-- mobile menu end -->
                </div>
                <div class="mobile-curr-lang-wrap">
                    
                   
                    <div class="single-mobile-curr-lang">
                        <a class="mobile-account-active" href="my-account.php">My Account <i class="la la-angle-down"></i></a>
                        <div class="lang-curr-dropdown account-dropdown-active">
                            <ul>
                                <li><a href="login.php">Login</a></li>
                                         <li><a href="register.php">Sign Up</a></li>
                                <li><a href="my-account.php">My Account</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="mobile-social-wrap">
                    <a class="facebook" href="#"><i class="ti-facebook"></i></a>
                    <a class="twitter" href="#"><i class="ti-twitter-alt"></i></a>
                    <a class="pinterest" href="#"><i class="ti-pinterest"></i></a>
                    <a class="instagram" href="#"><i class="ti-instagram"></i></a>
                    <a class="google" href="#"><i class="ti-google"></i></a>
                </div>
            </div>
        </div>