@extends('layouts.fyc')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10 terms">
			<br>
			<h2>Terms & Conditions</h2>
			<br>
			
			<p>All information displayed, transmitted or carried on <b>FYC Professional</b> is protected by copyright and other intellectual property laws.</p>
			<p>This site is designed, updated and maintained independently by <b>FYC Professional</b>. The content is owned by FYC Professional. You may not modify, publish, transmit, transfer, sell, reproduce, create derivative work from, distribute, repost, perform, display or in any way commercially exploit any of the content.</p>
			<h4>Account & Registration Obligations</h4>
			<p>All shoppers have to register and login for placing orders on the Site. You have to keep your account and registration details current and correct for communications related to your purchases from the site.</p>
			<h4>Pricing</h4>
			<p>All the products listed on the Site will be sold at MRP unless otherwise specified. The prices mentioned at the time of ordering be the prices charged on the date of the delivery. Although prices of most of the products do not fluctuate on a daily basis but some of the commodities and fresh prices do change on a daily basis. In case the prices are higher or lower on the date of delivery additional charges will be collected or refunded as the case may be at the time of the delivery of the order.</p>
			<h4>Governing law and jurisdiction </h4>
			<p>Disclaims all warranties or conditions, whether expressed or implied, (including without limitation implied, warranties or conditions of information and context). We consider ourselves and intend to be subject to the jurisdiction only of the courts of Delhi, India.</p>
			<h4>Modes of payment </h4>
			<p>Payments for the products available on the Site may be made in the following ways:</p>
			<ul>
				<li>>Payments can be made by Credit Cards, Debit Cards, Net Banking,  and reward points.</li>
				<li>>Credit card, Debit Card and Net Banking payment options are instant payment options and recommended to ensure faster processing of your order.</li>
				<li>>Cash On Delivery option is not available.</li>
				<h4>Service Overview</h4>
				<p>As part of the registration process on the Site, fyc professional . may collect the following personally identifiable information about you: Name including first and last name, alternate email address, mobile phone number and contact details, Postal code, Demographic profile (like your age, gender, occupation, education, address etc.) and information about the pages on the site you visit/access, the links you click on the site, the number of times you access the page and any such browsing information.</p>
				<h4>CONTACT INFORMATION:</h4>
				<p><b>Customer Service Desk<br>
					E-mail id: yavicosmetics02@gmail.com<br>
					Phone: 9958598813<br>
				Contact Days: Monday to Saturday (10:00 a.m. to 6:00 p.m.)</b></p>

				<br><br>

			</div>
			<div class="col-sm-1"></div>
		</div>
	</div>
	@endsection