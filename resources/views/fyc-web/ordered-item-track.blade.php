@extends('layouts/fyc')

@section('content')
<style media="screen">
/* order tracker css */
ol.progtrckr {
    margin: 0;
    padding: 0;
    list-style-type none;
}

ol.progtrckr li {
    display: inline-block;
    text-align: center;
    line-height: 3.5em;
}

ol.progtrckr[data-progtrckr-steps="2"] li { width: 49%; }
ol.progtrckr[data-progtrckr-steps="3"] li { width: 33%; }
ol.progtrckr[data-progtrckr-steps="4"] li { width: 24%; }
ol.progtrckr[data-progtrckr-steps="5"] li { width: 19%; }
ol.progtrckr[data-progtrckr-steps="6"] li { width: 16%; }
ol.progtrckr[data-progtrckr-steps="7"] li { width: 14%; }
ol.progtrckr[data-progtrckr-steps="8"] li { width: 12%; }
ol.progtrckr[data-progtrckr-steps="9"] li { width: 11%; }

ol.progtrckr li.progtrckr-done {
    color: black;
    border-bottom: 4px solid yellowgreen;
}
ol.progtrckr li.progtrckr-todo {
    color: silver;
    border-bottom: 4px solid silver;
}

ol.progtrckr li:after {
    content: "\00a0\00a0";
}
ol.progtrckr li:before {
    position: relative;
    bottom: -2.5em;
    float: left;
    left: 50%;
    line-height: 1em;
}
ol.progtrckr li.progtrckr-done:before {
    content: "\2713";
    color: white;
    background-color: yellowgreen;
    height: 2.2em;
    width: 2.2em;
    line-height: 2.2em;
    border: none;
    border-radius: 2.2em;
}
ol.progtrckr li.progtrckr-todo:before {
    content: "\039F";
    color: silver;
    background-color: white;
    font-size: 2.2em;
    bottom: -1.2em;
}
</style>
<br><br>
<br><br>
      <main class="page-content">
        <div class="account-page-area">
          <div class="container">
            <div class="myaccount-orders">
                <h4 class="small-title"> ORDER DETAILS</h4>
                @if($orderItemTrack)
                <div class="row">
                  <div class="col-12" style="font-size:1.5rem">
                    <span class="badge p-2 mx-2 {{$orderItemTrack['order_placed'] == true ? 'badge-success' : 'badge-secondary'}}">
                      <span class="mr-2">Order Placed</span>
                      <i class="fa {{$orderItemTrack['order_placed'] == true ? 'fa-check-circle' : 'fa-circle-thin'}}" aria-hidden="true"></i>
                    </span>
                    <span class="badge p-2 mx-2 {{$orderItemTrack['processing'] == true ? 'badge-success' : 'badge-secondary'}}">
                      <span class="mr-2">Processing</span>
                      <i class="fa {{$orderItemTrack['processing'] == true ? 'fa-check-circle' : 'fa-circle-thin'}}" aria-hidden="true"></i>
                    </span>
                    <span class="badge p-2 mx-2 {{$orderItemTrack['preparing'] == true ? 'badge-success' : 'badge-secondary'}}">
                      <span class="mr-2">Preparing</span>
                      <i class="fa {{$orderItemTrack['preparing'] == true ? 'fa-check-circle' : 'fa-circle-thin'}}" aria-hidden="true"></i>
                    </span>
                    <span class="badge p-2 mx-2 {{$orderItemTrack['shiped'] == true ? 'badge-success' : 'badge-secondary'}}" >
                      <span class="mr-2">Shiped</span>
                      <i class="fa {{$orderItemTrack['shiped'] == true ? 'fa-check-circle' : 'fa-circle-thin'}}" aria-hidden="true"></i>
                    </span>
                    <span class="badge p-2 mx-2 {{$orderItemTrack['delivered'] == true ? 'badge-success' : 'badge-secondary'}}">
                      <span class="mr-2">Delivered</span>
                      <i class="fa {{$orderItemTrack['delivered'] == true ? 'fa-check-circle' : 'fa-circle-thin'}}" aria-hidden="true"></i>
                    </span>
                  </div>
                </div>
                  @endif
            </div>
          </div>
        </div>
      </main>

<br><br>
<br><br>
@endsection
