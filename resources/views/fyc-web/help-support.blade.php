@extends('layouts/fyc')

@section('content')
<div class="breadcrumb-area bg-img" style="background-image:url(/fyc-new/images/bg/breadcrumb.jpg);">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <h2>Help & Support</h2>
            <ul>
                <li>
                    <a href="/homew">Home</a>
                </li>
                <li class="active">Help & Support </li>
            </ul>
        </div>
    </div>
</div>
<br><br>

      <main class="page-content">
        <div class="account-page-area">
          <div class="container">
            <div class="row">
              <div class="col-12">
                <h3 class="contact-page-title text-center mb-2">Help Support</h3>
                <form method="post" action="{{ route('help-support.store') }}">
                     {{ csrf_field() }}
                   <div class="form-group row">
                     <label for="subject" class="col-sm-2 col-form-label">Subject</label>
                     <div class="col-sm-10">
                       <!-- <input type="text" class="form-control" name="subject" id="subject" placeholder=""> -->
                       <select class="custom-select @error('service_name') is-invalid @enderror" name="subject" id="subject"  value="{{ old('subject') }}" required autocomplete="subject" autofocus>
                         <option value="" class="d-none">Select subject</option>
                         <option value="regarding_payment">Regarding Payment</option>
                         <option value="regarding_order">Regarding Order</option>
                         <option value="regarding_cancellation">Regarding Cancellation</option>
                         <option value="other_subject">Other</option>
                       </select>
                       @error('subject')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                     </div>

                   </div>
                   @if($allOrders)
                   <div class="form-group row">
                     <label for="order_id" class="col-sm-2 col-form-label">Order ID</label>
                     <div class="col-sm-10">
                       <!-- <input type="text" class="form-control" name="subject" id="subject" placeholder=""> -->
                       <select class="custom-select @error('order_id') is-invalid @enderror" name="order_id" id="order_id"  value="{{ old('order_id') }}" autocomplete="order_id" autofocus>
                         <option value="" class="d-none">Select Order</option>
                         @foreach($allOrders as $orders)
                         <option value="{{$orders->id}}">{{$orders->id}}</option>
                         @endforeach
                       </select>
                       @error('subject')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                     </div>

                   </div>
                   @endif

                   <div class="form-group row d-none" id="other">
                     <label for="other_subject" class="col-sm-2 col-form-label">Other Subject </label>
                     <div class="col-sm-10">
                       <input type="text" class="form-control" name="other_subject" id="other_subject" placeholder="Other Subject">
                     </div>
                   </div>

                   <div class="form-group row">
                     <label for="document" class="col-sm-2 col-form-label">Document</label>
                     <div class="col-sm-10">
                       <input id="document" type="file" class="form-control-file @error('document') is-invalid @enderror" name="document" value="{{ old('document') }}" autocomplete="document" autofocus>

                        @error('document')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                     </div>
                   </div>

                   <div class="form-group row">
                     <label for="message_content" class="col-sm-2 col-form-label">Your Message</label>
                     <div class="col-sm-10">
                       <textarea rows="4" cols="40" class="form-control @error('message_content') is-invalid @enderror" name="message_content" id="message_content" placeholder="Your Query">{{old('message_content')}}</textarea>
                       @error('message_content')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                     </div>
                   </div>

                  <div class="form-group row">
                    <label for="message_content" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-10">
                      <button type="submit" class="btn btn-primary" style="background-color: #000033">Submit Request</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </main>
@endsection
