@extends('layouts.fyc')

@section('content')

 <!-- <div class="breadcrumb-area bg-img" style="background-image:url('/fyc-new/images/bg/breadcrumb.jpg);">
            <div class="container">
                <div class="breadcrumb-content text-center">
                    <h2>About us page</h2>
                    <ul>
                        <li>
                            <a href="/homew">Home</a>
                        </li>
                        <li class="active">About us </li>
                    </ul>
                </div>
            </div>
        </div> -->
        <div class="about-us-area pt-90 pb-90">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="about-us-img text-center">
                            <a href="#">
                                <img src="{{url('/fyc/beauty.jpg')}}" alt ="">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 align-self-center">
                        <div class="about-us-content">
                            <h2>Welcome To <span>FYC</span> Professionals !</h2>
                            <p>We are here to nurture and nourish the original beauty of individuals with nature.</p>
                            <br>
 <h2>History</h2>
                            <p>In Initial days of brand conceptualization, the biggest question for us was the right market place from which we can reach out important consumers. Well there were lot of potential ways to do it but we found professional beauty industry as the best fit to our research driven, very carefully crafted portfolio of skin therapy.
</p>
<p>
Our Skin Care company was established by Mr NeerajAggrawal and Mrs Neha aggrawal in 2005. Yavi Cosmetics is India’s leading natural cosmetics company. Combining ancient wisdom from the Vedas with 21st century technology, we offer a range of over 200 skin care products for the retail and professional markets. Yavi Cosmetics is more of a sensibly premium, professional skin care range of products. As Our clients demands something close to 100% natural and something which is much more potent that the standard cosmetic regime. Under our brand “FYC” we have a large number of products for professional skin care and routine beauty care products.</p>
                            

                            <br>


<h2>Principles</h2>
<p>The Company works on the principle of :</p>
<ul>
    
<li>• Using only the purest natural extracts and active ingredients in our products.</li>
<li>• Maintaining a high level of quality control and continuous R & D.</li>
<li>• To create a customer friendly transparent policy.</li>
<li>• To offer value for money to our customer.</li>
<li>• To impart training to our employees for continuously improving their performance in respective fields by ensuring total involvement.</li>
</ul>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row section-text" >
            <div class="col-sm-4">
                <h4>Eminence Mission</h4>
<p>Yavi Cosmetics with its devotion to perfection and innovation, goes into the creation of a steady stream of products with outstanding functions and superb textures. We are committed to ENHANCE YOUR WELL-BEING NATURALLY through most effective skin care products and extraordinary service.</p>
            </div>
            <div class="col-sm-4">
                <h4>Products Quality</h4>
<p>Yavi Cosmetics focuses on modernity the development of its products is based on “High-Tech” advanced active cosmetology, based on natural ingredients and special technique, which offer effective and visible results. All the products are bio-compatible (i.e. formulated with substances equivalent to the skin). The quality of products is managed by best professionals mainly from cosmetic and herbal industry with collective experience of more than 20 years.</p>
            </div>
            <div class="col-sm-4">
                <h4>Numbers In 2017</h4>
<p>Since 2005, we are developing our company by stepping up investment in markets with outstandng growth in many states all over India. We started with just a couple of products in 5 cities, small distribution network and handful of salons. Yavi Cosmetics today has a large number of quality products, 50 team members, 65 distributors, 5000 salons &lacs of customers all over India. We want to keep on inspiring them all and become a brand that they truly love and trust.</p>
            </div>
        </div>

       <!--  <div class="team-area pt-60 pb-60">
            <div class="container">
                <div class="section-title-2 text-center">
                    <h2>Team Members</h2>
                    <img src="{{url('/fyc-new/images/icon-img/title-shape.png')}} " alt="icon-img">
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="team-wrapper mb-30">
                            <div class="team-img">
                                <a href="#">
                                    <img src="{{url('/fyc-new/images/team/team-1.jpg')}}" alt=""> 
                                </a>
                                <div class="team-action">
                                    <a class="facebook" href="#">
                                        <i class="ti-facebook"></i>
                                    </a>
                                    <a class="twitter" href="#">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                    <a class="instagram" href="#">
                                        <i class="ti-instagram"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="team-content text-center">
                                <h4>Mr.Mike Banding</h4>
                                <span>Manager </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="team-wrapper mb-30">
                            <div class="team-img">
                                <a href="#">
                                    <img src="{{url('/fyc-new/images/team/team-2.jpg')}}" alt=""> 
                                </a>
                                <div class="team-action">
                                    <a class="facebook" href="#">
                                        <i class="ti-facebook"></i>
                                    </a>
                                    <a class="twitter" href="#">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                    <a class="instagram" href="#">
                                        <i class="ti-instagram"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="team-content text-center">
                                <h4>Mr.Peter Pan</h4>
                                <span>Developer </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="team-wrapper mb-30">
                            <div class="team-img">
                                <a href="#">
                                    <img src="{{url('/fyc-new/images/team/team-3.jpg')}}" alt=""> 
                                </a>
                                <div class="team-action">
                                    <a class="facebook" href="#">
                                        <i class="ti-facebook"></i>
                                    </a>
                                    <a class="twitter" href="#">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                    <a class="instagram" href="#">
                                        <i class="ti-instagram"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="team-content text-center">
                                <h4>Ms.Sophia</h4>
                                <span>Designer </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="team-wrapper mb-30">
                            <div class="team-img">
                                <a href="#">
                                    <img src="{{url('/fyc-new/images/team/team-4.jpg')}}" alt=""> 
                                </a>
                                <div class="team-action">
                                    <a class="facebook" href="#">
                                        <i class="ti-facebook"></i>
                                    </a>
                                    <a class="twitter" href="#">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                    <a class="instagram" href="#">
                                        <i class="ti-instagram"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="team-content text-center">
                                <h4>Mr.John Lee</h4>
                                <span>Chairmen </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         -->
  


        @endsection