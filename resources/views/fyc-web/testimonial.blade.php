<?php
use App\Models\Testimonial;
$testimonial = Testimonial::where('status', 'Active')->get();
?>
<style type="text/css">
	body {
		font-family: "Open Sans", sans-serif;
	}
	h2 {
		color: #333;
		text-align: center;
		text-transform: uppercase;
		font-family: "Roboto", sans-serif;
		font-weight: bold;
		position: relative;
		margin: 25px 0 50px;
	}
	h2::after {
		content: "";
		width: 100px;
		position: absolute;
		margin: 0 auto;
		height: 3px;
		background: #ffdc12;
		left: 0;
		right: 0;
		bottom: -10px;
	}
	.carousel {
		width: 100%;
		margin: 0 auto;
		padding-bottom: 50px;
	}
	.carousel .item {
		color: #999;
		font-size: 14px;
		text-align: center;
		overflow: hidden;
		min-height: 200px;
		margin-bottom: 20px
	}
	.carousel .item a {
		color: #eb7245;
	}
	.carousel .img-box {
		width: 145px;
		height: 145px;
		margin: 0 0 0 auto;
		border-radius: 50%;
	}
	.carousel .img-box img {
		width: 100%;
		height: 100%;
		display: block;
		border-radius: 50%;
	}
	.carousel .testimonial {
		padding: 30px 0 10px;
	}
	.	carousel .overview {
		text-align: center;
		padding-bottom: 5px;
	}
	.carousel .overview b {
		color: #333;
		font-size: 15px;
		text-transform: uppercase;
		display: block;
		padding-bottom: 5px;
	}
	.carousel .star-rating i {
		font-size: 18px;
		color: #ffdc12;
	}
	.carousel .carousel-control {
		width: 30px;
		height: 30px;
		border-radius: 50%;
		background: #999;
		text-shadow: none;
		top: 4px;
	}
	.carousel-control i {
		font-size: 20px;
		margin-right: 2px;
	}
	.carousel-control.left {
		left: 10%;
		position: absolute;
		top:20%;
		
	}

	.carousel-control.right {
		right: 10%;
		position: absolute;
		top:20%;
	}

	@media screen and (max-width: 768px){
	.carousel-control.left {
		left: 5%;	
	}

	.carousel-control.right {
		right: 5%;		
	}
}
	.carousel-control.right i {
		margin-right: -2px;
	}
	.carousel .carousel-indicators {
		bottom: 15px;
	}
	.carousel-indicators li, .carousel-indicators li.active {
		width: 11px;
		height: 11px;
		margin: 1px 5px;
		border-radius: 50%;
	}
	.carousel-indicators li {
		background: #e2e2e2;
		border-color: transparent;
	}
	.carousel-indicators li.active {
		border: none;
		background: #888;
	}
	#test-text { text-align: left; }
</style>

<div class="container">
	<h2>Testimonials</h2>
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Carousel indicators -->
		<ol class="carousel-indicators">
			@if($testimonial)
			@foreach( $testimonial as $index => $content)
			<li data-target="#myCarousel" data-slide-to="{{$index}}" class="{{$index ==0 ? 'active' : ''}}"></li>
			@endforeach
			@endif
		</ol>
		<!-- Wrapper for carousel items -->
		<div class="carousel-inner">
			@if($testimonial)
			@foreach( $testimonial as $index => $content)
			<div class="item carousel-item {{$index ==0 ? 'active' : ''}}">
				<div class="row">
					<div class="col-sm-5 col-xs-12">
						<div class="img-box"><img src="{{url("/")}}/{{$content->image_url}}" alt=""></div>		
					</div>
					<div class="col-sm-7 col-xs-12" id="test-text">
						<p class="testimonial">{{$content->review}}</p>
						<p class="overview"><b>{{$content->client_name}}</b>{{$content->role}}</p>
						<div class="star-rating">
							<ul class="list-inline">
								<li class="list-inline-item"><i class="fa {{$content->rating >= 1 ? 'fa-star' : 'fa-star-o'}}"></i></li>
								<li class="list-inline-item"><i class="fa {{$content->rating >= 2 ? 'fa-star' : 'fa-star-o'}}"></i></li>
								<li class="list-inline-item"><i class="fa {{$content->rating >= 3 ? 'fa-star' : 'fa-star-o'}}"></i></li>
								<li class="list-inline-item"><i class="fa {{$content->rating >= 4 ? 'fa-star' : 'fa-star-o'}}"></i></li>
								<li class="list-inline-item"><i class="fa {{$content->rating >= 5 ? 'fa-star' : 'fa-star-o'}}"></i></li>
							</ul>
						</div>
					</div>
				</div>

			</div>
			@endforeach
			@endif


    <!-- <div class="item carousel-item">
      <div class="img-box"><img src="https://www.tutorialrepublic.com/examples/images/clients/2.jpg" alt=""></div>
      <p class="testimonial">Vestibulum quis quam ut magna consequat faucibus. Pellentesque eget nisi a mi suscipit tincidunt. Utmtc tempus dictum risus. Pellentesque viverra sagittis quam at mattis. Suspendisse potenti. Aliquam sit amet gravida nibh, facilisis gravida odio. Phasellus auctor velit.</p>
      <p class="overview"><b>Antonio Moreno</b>Web Developer at <a href="#">Circle Ltd.</a></p>
      <div class="star-rating">
        <ul class="list-inline">
          <li class="list-inline-item"><i class="fa fa-star"></i></li>
          <li class="list-inline-item"><i class="fa fa-star"></i></li>
          <li class="list-inline-item"><i class="fa fa-star"></i></li>
          <li class="list-inline-item"><i class="fa fa-star"></i></li>
          <li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
        </ul>
      </div>
  </div> -->
</div>
<!-- Carousel controls -->
<a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
	<i class="fa fa-angle-left"></i>
</a>
<a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
	<i class="fa fa-angle-right"></i>
</a>
</div>
</div>
