@extends('layouts/fyc')

@section('content')
<br><br>
      <main class="page-content">
        <div class="account-page-area">
          <div class="container">
            <div class="myaccount-orders">
                <h4 class="small-title"> ORDER DETAILS</h4>
                <div class="table-responsive">
                  @if($orderItemDetails)
                    <table class="table table-bordered table-hover">
                      <thead>
                        <tr>
                            <th>Item Name</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Total Amount</th>
                        </tr>
                      </thead>
                        <tbody>
                          @foreach($orderItemDetails as $order)
                            <tr>
                                <td>{{$order->name}}</td>
                                <td>{{$order->quantity}}</td>
                                <td>Rs. {{$order->price}}</td>
                                <td>Rs. {{$order->quantity * $order->price}}  </td>
                            </tr>
                            @endforeach
                            @if(!empty($order_tran))
                            <tr>
                              <td colspan="2" style="text-align: right;"><b>Payment Method -</b> {{$order_tran->payment_method}}</td>
                              <td colspan="2" style="text-align: right;">@if($order_tran->payment_method == "pay by card")<b>Transaction ID -</b> {{$order_tran->transaction_id}}@endif</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>


                    @endif


                </div>
            </div>
          </div>
        </div>
      </main>

<br><br>
@endsection
