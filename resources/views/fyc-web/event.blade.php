@extends('layouts.fyc')

@section('content')
<div class="container">
<div class="row">
	<div class="col-sm-2"></div>
	<div class="col-sm-8">
  	
<div class="row">
    <div class="col-lg-12 ">
        <div class="card-header" style="background: #eee;margin-top: 40px">
            <h2>Customer Info</h2>
        </div>
    </div>
</div>
<br><br>
     
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Opps!</strong> Something went wrong<br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
     
<form action="{{ url('pay') }}" method="POST" name="laravel_instamojo">
    {{ csrf_field() }}
               
     <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <strong>Name</strong>
                <input type="text" name="name" class="form-control" placeholder="Enter Name" required value="{{$user->name}}">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <strong>Mobile Number</strong>
                <input type="text" name="mobile_number" class="form-control" placeholder="Enter Mobile Number" required value="{{$phone}}">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <strong>Email Id</strong>
                <input type="text" name="email" class="form-control" placeholder="Enter Email id" required value="{{$user->email}}">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <strong>Payable Amount</strong>
                <input type="text" name="amount" class="form-control" placeholder="" readonly="" value="{{$amount}}">
            </div>
        </div>
        <div class="col-md-12">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
     
</form>
</div>
</div>
</div>



@endsection