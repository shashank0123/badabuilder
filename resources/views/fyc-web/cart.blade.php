<?php
use App\Models\Product;
use App\Review;
$bill = 0;
$i=0;
$cnt = 0;
?>

@extends('layouts.fyc')

@section('content')

<div class="cart-main-area pt-85 pb-90">
    <div class="container-fluid">
        <h3 class="cart-page-title">Your cart items</h3>
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-9">
                <form action="#">
                    @if(session()->get('cart') != null)

                    <?php

                    $ids = array();
                    $cat_id = array();
                    $quantities = array();


                    foreach(session()->get('cart') as $data)
                    {
                        $ids[$i] = $data->product_id;
                        $quantities[$i] = $data->quantity;
                        $i++;
                    }
                    ?>
                    <div class="table-content table-responsive cart-table-content">
                        <table>
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Product Name</th>
                                    <th>Until Price</th>
                                    <th>Qty</th>
                                    <th>Subtotal</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @for($j=0 ; $j<$i ; $j++ )
                                <?php
                                $cnt++;
                                $product = Product::where('id',$ids[$j])->first();
                                if($product != null)
                                {
                                    $cat_id[$j] = $product->category_id;
                                }
                                else
                                {
                                    $cat_id[$j] = 0;
                                }
                                ?>
                                <tr id="cart{{$ids[$j]}}">
                                    <td class="product-thumbnail">
                                        <a href="/product-detail/{{$product->slug}}"><img src="{{url('/')}}/{{$product->image1}}" alt="" style="width: 120px; height: 120px"></a>
                                    </td>
                                    <td class="product-name"><a href="/product-detail/{{$product->slug}}">{{strtoupper($product->name)}}</a></td>
                                    <td class="product-price-cart"><span class="amount">₹{{$product->sell_price}}.00</span></td>
                                    <td class="product-quantity">
                                        <div class="">


                                            <i class="ti-minus" style="border: 1px solid #afafaf; padding: 5px 5px 8px; cursor: pointer;" onclick="decrement({{$product->id}})"></i>

                                            <input style="width: 50px; text-align: center;" type="text" name="quantity" id="quantity{{$product->id}}" value="{{$quantities[$j]}}" disabled="disabled ">

                                            <i class="ti-plus" style="border: 1px solid #afafaf; padding: 5px 5px 7px; cursor: pointer;"  onclick="increment({{$product->id}})"></i>

                                        </div>
                                    </td>
                                    <?php
                                    $total = $quantities[$j]*$product->sell_price;
                                    $bill =$bill +  $total;
                                    ?>
                                    <td class="product-subtotal" id="amount{{$product->id}}">₹{{$total}}.00</td>
                                    <td class="product-remove">
                                        <a onclick="deleteCartProduct({{$ids[$j]}})"><i class="la la-close"></i></a>
                                    </td>
                                </tr>
                                @endfor
                            </tbody>
                        </table>
                    </div>
                    @endif
                            
                        </form>
                    </div>
                        <!-- <div class="row"> -->
                           
                           

                         <div class="col-lg-3 col-md-12">
                                <div class="grand-totall">
                                    <div class="title-wrap">
                                        <h4 class="cart-bottom-title section-bg-gary-cart">Cart Total</h4>
                                    </div>
                                    <h5>Sub Total<span id="total_price">₹{{$bill}}.00</span></h5>
                                    {{-- <div class="total-shipping">
                                        <h5>Total shipping</h5>
                                        <ul>
                                            <li><input type="checkbox"> Standard <span>₹20.00</span></li>
                                            <li><input type="checkbox"> Express <span>₹30.00</span></li>
                                        </ul>
                                    </div> --}}
                                    <h4 class="grand-totall-title">Grand Total <span id="grand_total">₹{{$bill}}.00</span></h4>
                                    <a href="<?php if(!empty(Auth::user()->id)){echo '/checkout/'.Auth::user()->id;} else{ echo '/login'; }?>">Proceed to Checkout</a>
                                </div>
                                 <div class="col-lg-12 col-md-12" style="float:right;">
                                <div class="cart-shiping-update-wrapper">
                                    <div class="cart-shiping-update">
                                        <a href="/home" style="border-radius:25px;">Continue Shopping</a>
                                    </div>
                                        {{-- <div class="cart-clear">
                                            <button>Update Shopping Cart</button>
                                            <a href="#">Clear Shopping Cart</a>
                                        </div> --}}
                                    </div>
                                {{-- <div class="cart-tax">
                                    <div class="title-wrap">
                                        <h4 class="cart-bottom-title section-bg-gray">Estimate Shipping And Tax</h4>
                                    </div>
                                    <div class="tax-wrapper">
                                        <p>Enter your destination to get a shipping estimate.</p>
                                        <div class="tax-select-wrapper">
                                            <div class="tax-select">
                                                <label>
                                                    * Country
                                                </label>
                                                <select class="email s-email s-wid">
                                                    <option>India</option>
                                                    <option>Nepal</option>
                                                    <option>Åland Islands</option>
                                                    <option>Afghanistan</option>
                                                    <option>Belgium</option>
                                                </select>
                                            </div>
                                            <div class="tax-select">
                                                <label>
                                                    * Region / State
                                                </label>
                                                <select class="email s-email s-wid">
                                                    <option>Delhi</option>
                                                    <option>Punjab</option>
                                                    <option>U.P</option>
                                                    <option>Bihar</option>
                                                    <option>Himachal Pradesh</option>
                                                </select>
                                            </div>
                                            <div class="tax-select">
                                                <label>
                                                    * Zip/Postal Code
                                                </label>
                                                <input type="text">
                                            </div>
                                            <button class="cart-btn-2" type="submit">Get A Quote</button>
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>



        @endsection


        @section('script')

        <script>


        // var count=<?php echo session()->get('count'); ?>;

        function deleteCartProduct(id){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    // count--;

    $.ajax({
        /* the route pointing to the post function */
        url: '/cart/delete-product/'+id,
        type: 'POST',
        datatype: 'JSON',
        /* send the csrf-token and the input to the controller */
        data: {_token: CSRF_TOKEN, id: id},
        success: function (data) {
    $('#cart'+id).hide();
            $('#cartItem').html(data.cartItem);
            $("#total_price").empty();
            $("#total_price").append("₹" + data.bill);
            $("#grand_total").html("₹" + data.bill);
            if(data.bill <= 0){
              $("#checkoutLink").attr("href", "/");
              $("#checkoutLink").html('Continue Shopping');
          }
      }
  });
}


function decrement(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            
            var action = 'minus' ;
            var quantity = $('#quantity'+id).val();
            if(quantity>1){
                
                $('#quantity'+id).val(--quantity);
                $.ajax({
                   url: '/update-cart',
                   type: 'POST',
                   data: {_token: CSRF_TOKEN, id: id, action: action},
                   success: function (data) {
               
               $("#total_price").empty();
               $("#total_price").append("₹" + data.bill);
               $("#grand_total").html("₹" + data.bill);
               $("#amount"+id).html("₹" + data.total);


           },
           failure: function (data) {
             alert(data.message);
         }
     });
            }
        }

        function increment(id){
        
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            
            var quantity = $('#quantity'+id).val();
            var action = 'plus' ;

            $('#quantity'+id).val(++quantity);
            $.ajax({
             url: '/update-cart',
             type: 'POST',
             data: {_token: CSRF_TOKEN, id: id, action:action},
             success: function (data) {
              
            $("#total_price").empty();
            $("#total_price").append("₹" + data.bill);
            $("#grand_total").html("₹" + data.bill);
            $("#amount"+id).html("₹" + data.total);

           },
           failure: function (data) {
               alert('Something went wrong');
           }
       });
   }

  
</script>

@endsection