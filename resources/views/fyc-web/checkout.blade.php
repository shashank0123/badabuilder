
@extends('layouts.fyc')

@section('content')


    

@if(session('error'))
            <div class="alert alert-success">
                {{ session('error') }}
            </div>
        @endif
      <form action="{{route('order-product')}}" method="post" enctype="multipart/form-data">
        @csrf
<div class="checkout-main-area pt-90 pb-90">
    <div class="container">
    <h3>Checkout</h3>
        @if(Auth::user())
                {{-- <div class="customer-zone mb-20">
                    <p class="cart-page-title">Returning customer? <a class="checkout-click1" href="#">Click here to login</a></p>
                    <div class="checkout-login-info">
                        <p>If you have shopped with us before, please enter your details in the boxes below. If you are a new customer, please proceed to the Billing & Shipping section.</p>
                        <form action="#">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="sin-checkout-login">
                                        <label>Username or email address <span>*</span></label>
                                        <input type="text" name="user-name">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="sin-checkout-login">
                                        <label>Passwords <span>*</span></label>
                                        <input type="password" name="user-password">
                                    </div>
                                </div>
                            </div>
                            <div class="button-remember-wrap">
                                <button class="button" type="submit">Login</button>
                                <div class="checkout-login-toggle-btn">
                                    <input type="checkbox">
                                    <label>Remember me</label>
                                </div>
                            </div>
                            <div class="lost-password">
                                <a href="#">Lost your password?</a>
                            </div>
                        </form>
                        <div class="checkout-login-social">
                            <span>Login with:</span>
                            <ul>
                                <li><a href="#">Facebook</a></li>
                                <li><a href="#">Twitter</a></li>
                                <li><a href="#">Google</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="customer-zone mb-20">
                    <p class="cart-page-title">Have a coupon? <a class="checkout-click3" href="#">Click here to enter your code</a></p>
                    <div class="checkout-login-info3">
                        <form action="#">
                            <input type="text" placeholder="Coupon code">
                            <input type="submit" value="Apply Coupon">
                        </form>
                    </div>
                </div> --}}
                <div class="checkout-wrap pt-30">
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="your-order-area">
                            <div class="billing-info-wrap mr-50">
                               <input type="hidden" name="add_id" value="@if(!empty($address)){{$address->id}}@endif">
                               <h3>Billing Details</h3>
                               <div class="row">
                                    {{-- <div class="col-lg-6 col-md-6">
                                        <div class="billing-info mb-20">
                                            <label>Country<abbr class="required" title="required">*</abbr></label>
                                            <input type="text" class="@error('country') is-invalid @enderror" value="india" disabled name="country">
                                        </div>
                                        @error('country')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="billing-info mb-20">
                                            <label>Last Name <abbr class="required" title="required">*</abbr></label>
                                            <input type="text">
                                        </div>
                                    </div> --}}
                                    <div class="col-lg-12">
                                        <div class="billing-info mb-20">
                                           <label>Country<abbr class="required" title="required">*</abbr></label>
                                           <input type="text" class="input-field @error('country') is-invalid @enderror" value="india" disabled name="country" required>
                                       </div>
                                       @error('country')
                                       <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                    {{-- <div class="col-lg-12">
                                        <div class="billing-select mb-20">
                                            <label>Country <abbr class="required" title="required">*</abbr></label>
                                            <select>
                                                <option>Select a country</option>
                                                <option>Azerbaijan</option>
                                                <option>Bahamas</option>
                                                <option>Bahrain</option>
                                                <option>Bangladesh</option>
                                                <option>Barbados</option>
                                            </select>
                                        </div>
                                    </div> --}}
                                    <div class="col-lg-12">
                                        <div class="billing-info mb-20">
                                            <label>Address <abbr class="required" title="required">*</abbr></label>
                                            <input class="input-field billing-address @error('address') is-invalid @enderror" placeholder="Complete address" name="address" type="text" value="{{ $address ? $address->address: old('address') }}">             
                                        </div>
                                        @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="billing-info mb-20">                          
                                            <label>Town / City <abbr class="required" title="required">*</abbr></label>
                                            <input class="input-field @error('city') is-invalid @enderror" value="{{ $address ? $address->city: old('city') }}" id="city" type="text" name="city" pattern="^[a-zA-Z\s]*$"  onkeyup="checkInputValue(this)" required>
                                            {{-- <input type="text"> --}}
                                        </div>
                                        @error('city')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="billing-info mb-20">
                                            <label>State <abbr class="required" title="required">*</abbr></label>
                                            <input class="input-field @error('state') is-invalid @enderror" value="{{$address ? $address->state: old('state') }}" onkeyup="checkInputValue(this)" pattern="^[a-zA-Z\s]*$"  type="text" id="state" name="state" required>
                                            {{-- <input type="text"> --}}
                                        </div>
                                        @error('state')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="billing-info mb-20">
                                            <label>Postcode / ZIP <abbr class="required" title="required">*</abbr></label>
                                            <input class="input-field @error('pin_code') is-invalid @enderror" value="{{$address ? $address->pin_code: old('pin_code') }}" type="text" pattern="^\d+$" name="pin_code" id="pin_code" onkeyup="checkInputValue(this)" maxlength="6" required />
                                            {{-- <input type="text"> --}}
                                        </div>
                                        @error('pin_code')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="billing-info mb-20">
                                            <label>Phone <abbr class="required" title="required">*</abbr></label>
                                            <input class="input-field @error('phone') is-invalid @enderror" value="{{$address ? $address->phone: old('phone') }}" type="text" pattern="^\d+$" name="phone" id="phone" onkeyup="checkInputValue(this)" maxlength="10" id="phone" />
                                            {{-- <input type="text"> --}}
                                        </div>
                                        @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                {{-- <div class="checkout-account mb-25">
                                    <input class="checkout-toggle2" type="checkbox">
                                    <span>Create an account?</span>
                                </div>
                                <div class="checkout-account-toggle open-toggle2 mb-30">
                                    <label>Email Address</label>
                                    <input placeholder="Password" type="password">
                                </div> --}}
                                <div class="checkout-account mt-25">
                                    <input class="checkout-toggle @error('ship-box') is-invalid @enderror" value="different" id="ship-box" type="checkbox" name="ship_to_different_address">
                                    <span>Ship to a different address?</span>
                                </div>
                                <div class="different-address open-toggle mt-30">
                                    <div class="">
                                        @if(!empty($allAddress))
                                        @foreach($allAddress as $address)
                                        <input type="radio" name="shipped"  value="{{$address->id}}">
                                        {{$address->address}}, {{$address->city}}, {{$address->state}}, {{$address->country}} - {{$address->pin_code}}<br>
                                        Contact - {{$address->phone}}<br><br>   
                                        @endforeach
                                        @endif
                                    </div>

                                    <br><br>

                                    <h3>or New Address</h3>
                                    <div class="row">
                                        {{-- <div class="col-lg-6 col-md-6">
                                            <div class="billing-info mb-20">
                                                <label>First Name</label>
                                                <input type="text">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="billing-info mb-20">
                                                <label>Last Name</label>
                                                <input type="text">
                                            </div>
                                        </div> --}}
                                        {{-- <div class="col-lg-12">
                                            <div class="billing-info mb-20">
                                                <label>Company Name</label>
                                                <input type="text">
                                            </div>
                                        </div> --}}
                                        <div class="col-lg-12">
                                            <div class="billing-select mb-20">
                                                <label>Country </label>
                                                <input class="input-field @error('shipping_country') is-invalid @enderror" value="india" disabled name="shipping_country">
                                            </div>
                                            @error('shipping_country')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="billing-info mb-20">
                                                <label>Address</label>
                                                <input class="input-field @error('shipping_address') is-invalid @enderror"  placeholder="Complete address" type="text"name="shipping_address" >     
                                            </div>
                                            @error('shipping_address')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="billing-info mb-20">
                                                <label>Town / City</label>
                                                <input class="input-field @error('shipping_city') is-invalid @enderror"  id="shipping_city" type="text" name="shipping_city" pattern="^[a-zA-Z\s]*$"  onkeyup="checkInputValue(this)" />              
                                            </div>
                                            @error('shipping_city')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-lg-12 col-md-12">
                                            <div class="billing-info mb-20">
                                                <label>State </label>
                                                <input class="input-field @error('shipping_state') is-invalid @enderror"  onkeyup="checkInputValue(this)" pattern="^[a-zA-Z\s]*$"  type="text" id="shipping_state" name="shipping_state" />
                                            </div>
                                            @error('state')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-lg-12 col-md-12">
                                            <div class="billing-info mb-20">
                                                <label>Postcode / ZIP</label>
                                                <input class="input-field @error('shipping_pin_code') is-invalid @enderror"  type="text" pattern="^\d+$" name="shipping_pin_code" id="shipping_pin_code" onkeyup="checkInputValue(this)" maxlength="6" />
                                            </div>
                                            @error('shipping_pin_code')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-lg-12 col-md-12">
                                            <div class="billing-info mb-20">
                                                <label>Phone</label>
                                                <input class="input-field @error('shipping_phone') is-invalid @enderror"  type="text" pattern="^\d+$" name="shipping_phone" id="shipping_phone" onkeyup="checkInputValue(this)" maxlength="10" />
                                            </div>
                                            @error('shipping_phone')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        
                                    </div>
                                </div>
                                </div>
                                {{-- <div class="additional-info-wrap">
                                    <label>Order notes</label>
                                    <textarea placeholder="Notes about your order, e.g. special notes for delivery. " name="message"></textarea>
                                </div> --}}
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="your-order-area">
                                <h3>Your order</h3>
                                <div class="your-order-wrap gray-bg-4">
                                    <div class="your-order-info-wrap">
                                        <div class="your-order-info">
                                            <ul>
                                                <li>Product <span>Total</span></li>
                                            </ul>
                                        </div>
                                        <div class="your-order-middle">

                                            <ul>
                                                @if(session()->get('cart') != null)
                                        @foreach($productDetails as $product)
                                          <li>{{$product->name}} X {{$product->quantity}} <span>₹{{$product->total_price}}.00 </span></li>
                                          @endforeach
                                        @endif
                                                
                                            </ul>
                                        </div>
                                        <div class="your-order-info order-subtotal">
                                            <ul>
                                                <li>Subtotal <span>₹{{$totalAmount}} </span></li>
                                            </ul>
                                        </div>
                                        {{-- <div class="your-order-info order-shipping">
                                            <ul>
                                                <li>Shipping <p>Enter your full address </p>
                                                </li>
                                            </ul>
                                        </div> --}}
                                        <input type="text" name="totalAmount" hidden value="{{$totalAmount}}">
                                        <div class="your-order-info order-total">
                                            <ul>
                                                <li>Total <span>₹{{$totalAmount}}.00 </span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="payment-method">
                                        <div class="pay-top sin-payment">
                                            <input  type="radio" id="cash_on_delivery" name="payment_method" class="input-radio" value="cash_on_delivery" checked>
                                            
                                            <label for="payment_method_1"> Cash On Delivery </label>
                                            {{-- <div class="payment-box payment_method_bacs">
                                                <p>Make your payment directly into our bank account. Please use your Order ID as the payment reference.</p>
                                            </div> --}}
                                        </div>
                                        <div class="pay-top sin-payment">
                                            <input  type="radio" id="pay_by_card" name="payment_method" value="pay_by_card" class="input-radio">
                                            
                                            <label for="payment-method-2">Pay By Card</label>
                                            <div class="payment-box payment_method_bacs">
                                                <p>Make your payment directly into our bank account. Please use your Order ID as the payment reference.</p>
                                            </div>
                                        </div>
                                        {{-- <div class="pay-top sin-payment">
                                            <input id="payment-method-3" class="input-radio" type="radio" value="cheque" name="payment_method">
                                            <label for="payment-method-3">Cash on delivery </label>
                                            <div class="payment-box payment_method_bacs">
                                                <p>Make your payment directly into our bank account. Please use your Order ID as the payment reference.</p>
                                            </div>
                                        </div>
                                        <div class="pay-top sin-payment sin-payment-3">
                                            <input id="payment-method-4" class="input-radio" type="radio" value="cheque" name="payment_method">
                                            <label for="payment-method-4">PayPal <img alt="" src="/fyc-new/images/icon-img/payment.png"><a href="#">What is PayPal?</a></label>
                                            <div class="payment-box payment_method_bacs">
                                                <p>Make your payment directly into our bank account. Please use your Order ID as the payment reference.</p>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                                <div class="Place-order mt-40">
                                    <a onclick="formSubmit()">Place Order</a>
                                    <input value="Place order" id="checkoutForm" class="btn-submit" type="submit" style="display: none;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>

       </form>


        @endsection


        @section('script')

         <script type="text/javascript">

        /* Start input check regex */
          function checkInputValue(field) {
            var value = field.value;
            var fieldId = field.id;
            var pattern = field.pattern;
            var pattern = new RegExp(field.pattern);
            var re = pattern.test(value);
            // var re = value.match(pattern);
            if(!re) {
                $('#' + fieldId + '_error').removeClass('d-none');
                if (document.getElementById(fieldId + '_error')) {
                  $('#' + fieldId + '_error').removeClass('d-none');
                  $('.btn-submit').prop('disabled', true);
                } else {
                    $('#' + fieldId).after('<small id="' +fieldId+'_error" class="text-danger">This value is not valid</small>');
                    $('.btn-submit').prop('disabled', true);
                }
            } else {
                $('#' + fieldId + '_error').addClass('d-none');
                $('.btn-submit').prop('disabled', false)
            }
          };
        /* End input check regex*/


        function formSubmit(){
            $('#checkoutForm').click();
        }
        </script>


        @endsection