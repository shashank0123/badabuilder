<?php
use App\Models\Category;
use App\Models\Product;
?>
@extends('layouts.fyc')

@section('content')

@if(!empty($slider_banners))
<div class="slider-area">
   <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
         <?php $i=0; $j=0;?>
         @foreach($slider_banners as $banner)
         <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}" class="@if($i==0){{'active'}}@endif"></li>
         <?php $i++; ?>
         @endforeach         
         {{-- <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
         <li data-target="#carouselExampleIndicators" data-slide-to="2"></li> --}}
      </ol>
      <div class="carousel-inner">
         @foreach($slider_banners as $banner)
         <div class="carousel-item @if($j==0){{'active'}}@endif">
            <img class="d-block w-100" src="{{url($banner->image_url)}}" alt="First slide">
         </div>
         <?php $j++; ?>
         @endforeach
         {{-- <div class="carousel-item">
            <img class="d-block w-100" src="/fyc-new/images/slider/slider2.jpg" alt="Second slide">
         </div>
         <div class="carousel-item">
            <img class="d-block w-100" src="/fyc-new/images/slider/slider3.jpg" alt="Third slide">
         </div>
         <div class="carousel-item">
            <img class="d-block w-100" src="/fyc-new/images/slider/D.jpg" alt="Third slide">
         </div> --}}
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
         <span class="carousel-control-prev-icon" aria-hidden="true"></span>
         <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
         <span class="carousel-control-next-icon" aria-hidden="true"></span>
         <span class="sr-only">Next</span>
      </a>
   </div>
</div>
@endif
<style type="text/css">
   .bg-color {
      background-color: #000033!important;
   }
</style>

@if(!empty($medium_banners))
<div class="banner-area pt-20 pb-100">
   <div class="banner-slider-active">
      @foreach($medium_banners as $banner)
      <div class="banner-wrap">
         {{-- <div class="banner-wrap res-white-overly-xs res-white-overly-md res-white-overly-lg res-white-overly-xl"> --}}
            <a href="/offer-detail/{{$banner->id}}"><img src="{{url($banner->image_url)}}" alt="banner"></a>
         {{-- <div class="banner-content-2 banner-position-1">
            <h3>Mystery Shine</h3>
            <p>Best shoes collection for women</p>
         </div> --}}
      </div>
      @endforeach
      {{-- <div class="banner-wrap res-white-overly-xs res-white-overly-md res-white-overly-lg res-white-overly-xl">
         <a href="/product-detailw"><img src="/fyc-new/images/banner/banner-3.png" alt="banner"></a>
         <div class="banner-content-3 banner-position-2">
            <h3>The Rapid Shoes <br>Collection</h3>
         </div>
      </div>
      <div class="banner-wrap res-white-overly-xs res-white-overly-md res-white-overly-lg res-white-overly-xl">
         <a href="#"><img src="/fyc-new/images/banner/banner-4.png" alt="banner"></a>
         <div class="banner-content-3 banner-position-3">
            <h3>Cotton T-Shirt <br>FOr Women</h3>
         </div>
      </div>
      <div class="banner-wrap res-white-overly-xs res-white-overly-md res-white-overly-lg res-white-overly-xl">
         <a href="/product-detailw"><img src="/fyc-new/images/banner/banner-3.png" alt="banner"></a>
         <div class="banner-content-3 banner-position-2">
            <h3>The Rapid Shoes <br>Collection</h3>
         </div>
      </div> --}}
   </div>
</div>
@endif

<div class="product-area pb-100">
   <div class="container-fluid">
      <div class="row">
         <div class="col-lg-3  col-md-12 " style="background-color: #00003314">
            <div class="category-menu-wrap" id="mobile-view">
               <button type="button" class="btn btn-block bg-color  mb-3 btn-lg">
                  <h3 class="showcat">
                     <a href="#">
                        <img class="category-menu-non-stick" src="/fyc-new/images/icon-img/category-menu.png" alt="icon">
                        <img class="category-menu-stick" src="/fyc-new/images/icon-img/category-menu-stick.png" alt="icon">
                        All Category <i class="la la-angle-down"></i>
                     </a>
                  </h3>
               </button>
               <div class="category-menu mobile-category-menu hidecat"style="display: none;" >
                  <nav>
                     <ul>
                        @if(!empty($maincategory))
                        @foreach($maincategory as $cat1)
                        <li class="cr-dropdown">
                           <a href="/product/{{$cat1->slug}}">{{$cat1->category_name}} <span class="la la-angle-down"></span></a>
                           <?php
                           $cate = Category::where('category_id',$cat1->id)->get();
                           ?>
                           @if(!empty($cate))
                           <ul class="cr-menu-desktop-none">
                              @foreach($cate as $cat2)
                              <li class="cr-sub-dropdown sub-style">
                                 <a href="/product/{{$cat1->slug}}">{{$cat2->category_name}} <i class="la la-angle-down"></i></a>

                                 <?php
                                 $sub_cat = Category::where('category_id',$cat2->id)->get();
                                 ?>
                                 @if(!empty($sub_cat))
                                 <ul style="display: none;">
                                    @foreach($sub_cat as $cat3)
                                    <li><a href="/product">{{$cat2->category_name}}</a></li>
                                    @endforeach
                                 </ul>
                                 @endif
                              </li>
                              @endforeach                              
                           </ul>
                           @endif
                        </li>
                        @endforeach
                        @endif                       
                     </ul>
                  </nav>
               </div>
            </div>
            <div class="category-menu-wrap" id="desktop-view">
               <button type="button" class="btn bg-color btn-block btn-lg">
                  <h3 class="showcat">
                     <a href="#">
                        <img class="category-menu-non-stick" src="/fyc-new/images/icon-img/category-menu.png" alt="icon">
                        <img class="category-menu-stick" src="/fyc-new/images/icon-img/category-menu-stick.png" alt="icon">
                        All Category <i class="la la-angle-down"></i>
                     </a>
                  </h3>
               </button>
               <div class="category-menu category-menu-desktop hidecat" >
                  <nav>
                     @if(!empty($maincategory))
                     <ul>
                        @foreach($maincategory as $cat1)
                        <li class="cr-dropdown">
                           <a href="/product/{{$cat1->slug}}">{{$cat1->category_name}} <span class="la la-angle-right"></span></a>
                           <?php
                           $cate = Category::where('category_id',$cat1->id)->get();
                           ?>
                           @if(!empty($cate))
                           <div class="category-menu-dropdown ct-menu-res-height-1">
                              @foreach($cate as $cat2)
                              <div class="single-category-menu ct-menu-mrg-bottom category-menu-border">
                                 <h4><a href="/product/{{$cat2->slug}}">{{$cat2->category_name}}</a></h4>
                                 <?php
                                 $sub_cat = Category::where('category_id',$cat2->id)->get();
                                 ?> 
                                 @if(!empty($sub_cat))
                                 <ul>
                                    @foreach($sub_cat as $cat3)
                                    <li><a href="/product/{{$cat3->slug}}">Laptop Keyboard</a></li>
                                    @endforeach
                                 </ul>
                                 @endif
                              </div>
                              @endforeach                             
                              {{-- <div class="single-category-menu">
                                 <a href="#"><img src="/fyc-new/images/banner/menu-banner-2.png" alt=""></a>
                              </div> --}}
                           </div>
                           @endif
                        </li>
                        @endforeach


                     </ul>
                     @endif
                  </nav>
               </div>
            </div>

            @if(!empty($trending_products))
            <div class="offers-img-sec" id="desktop-view" style="margin-top: 350px; text-align: center;">
               <button class="btn bg-color btn-block text-light mb-3"> Trending Products </button>
               @foreach($trending_products as $product)
               <div class="my-2" id="image-pop">  <a href="/product-detail/{{$product->slug}}" >   
                  <img lt="responsive image" class="img-fluid" style="width: 70%; height: auto" src="{{$product->image1}}"></a>
                  <div class="product-action-side">
                        <a data-toggle="modal" data-target="#exampleModal" title="Quick View" href="#" data-myid="{{$product->id}}"><i class="la la-search"></i></a>
                        <a title="Add To Cart" onclick="addToCart({{$product->id}})"><i class="la la-cart-plus"></i></a>
                        <a title="Wishlist" onclick="addToWishlist({{$product->id}})"><i class="la la-heart-o"></i></a>
                     </div>
               </div>
               @endforeach
            </div>
            @endif

            @if(!empty($offers))
            <div class="offers-img-sec" id="desktop-view" style="margin-top: 80px">
               <button class="btn bg-color btn-block text-light mb-3"> OFFRES </button>
               @foreach($offers as $offer)
               <div class="my-2">      
                  <img lt="responsive image" class="img-fluid" src="{{$offer->image_url}}">
               </div>
               @endforeach
               {{-- <a href="/offers" class="btn btn-primary">View More...</a> --}}
               {{-- <div class="my-2">      
                  <img lt="responsive image" class="img-fluid" src="/fyc-new/images/banner/banner-2.png">
               </div>
               <div class="my-2">      
                  <img lt="responsive image" class="img-fluid" src="/fyc-new/images/banner/banner-4.png">
               </div> --}}
            </div>
            @endif
         </div>
         <div class="col-lg-9 col-md-12">
            <div class="section-title-2 text-center">
               <h2>Featured products</h2>
               <img src="/fyc-new/images/icon-img/title-shape.png" alt="icon-img">
            </div>
            <div class=" box-slider-active owl-carousel">
               @foreach($featured_products as $product)
               <div class="product-wrap product-border-1 product-img-zoom">
                  <div class="product-img">
                     <a href="/product-detail/{{$product->slug}}"><img src="{{url('/')}}/{{$product->image1}}" alt="product" class="img-response"></a>
                     <div class="product-action-2">
                        <a data-toggle="modal" data-target="#exampleModal" title="Quick View" data-myid="{{$product->id}}"><i class="la la-search"></i></a>
                        <a title="Add To Cart" onclick="addToCart({{$product->id}})"><i class="la la-cart-plus"></i></a>
                        <a title="Wishlist" onclick="addToWishlist({{$product->id}})"><i class="la la-heart-o"></i></a>
                     </div>
                  </div>
                  <div class="product-content product-content-padding">
                     <h4><a href="/product-detail/{{$product->slug}}">{{substr($product->name,0,20) }}.</a></h4>
                     <div class="price-addtocart">
                        <div class="product-price">
                           <span>₹{{$product->sell_price}}.00</span>
                        </div>
                     </div>
                  </div>
               </div>
               @endforeach               
            </div>
            <div class="section-title-2 text-center mt-4">
               <h2>New Arrivals</h2>
               <img src="/fyc-new/images/icon-img/title-shape.png" alt="icon-img">
            </div>
            <div class="box-slider-active owl-carousel">
               @if(!empty($new_products))
               @foreach($new_products as $product)
               <div class="product-wrap product-border-1 product-img-zoom">
                  <div class="product-img">
                     <a href="/product-detail/{{$product->slug}}"><img src="{{url('/')}}/{{$product->image1}}" alt="product" class="img-response"></a>
                     <div class="product-action-2">
                        <a data-toggle="modal" data-target="#exampleModal" title="Quick View" data-myid="{{$product->id}}"><i class="la la-search"></i></a>
                        <a title="Add To Cart" onclick="addToCart({{$product->id}})"><i class="la la-cart-plus"></i></a>
                        <a title="Wishlist" onclick="addToWishlist({{$product->id}})"><i class="la la-heart-o"></i></a>
                     </div>
                  </div>
                  <div class="product-content product-content-padding">
                     <h4><a href="/product-detail/{{$product->slug}}">{{substr($product->name,0,20) }}.</a></h4>
                     <div class="price-addtocart">
                        <div class="product-price">
                           <span>₹{{$product->sell_price}}.00</span>
                        </div>
                     </div>
                  </div>
               </div>
               @endforeach
               @endif
            </div>

            @if(!empty($slider_cat))

            @foreach($slider_cat as $category)

            <?php
            $products = [];
            $cate = [];

            $check = Category::where('category_id',$category->id)->where('status','Active')->get();
            if(!empty($check)){
               foreach($check as $cat){
                  $cate[]=$cat->id;

                  $checks = Category::where('category_id',$cat->id)->where('status','Active')->get();

                  if(!empty($checks)){
                     foreach($checks as $ch){
                        $cate[]=$ch->id;
                     }
                  }
               }
            }

            if(!empty($cate)){
               foreach($cate as $cat){
                  $prod = Product::where('category_id',$cat)->where('status','Active')->get();
                  if(!empty($prod)){
                     foreach($prod as $pro){
                        $products[] = $pro;
                     }
                  }
               }
            }
            ?>
            <div class="">
               <div class="section-title-2 my-4 text-center">
                  <h2>{{$category->category_name}}</h2>
                  <img src="/fyc-new/images/icon-img/title-shape.png" alt="icon-img">
               </div>
               <div class=" box-slider-active owl-carousel">
                  @if($products)
                  @foreach($products as $product)
                  <div class="product-wrap product-border-1 product-img-zoom">
                     <div class="product-img">
                        <a href="/product-detail/{{$product->slug}}"><img src="{{url('/')}}/{{$product->image1}}" alt="product" class="img-response"></a>
                        <div class="product-action-2">
                           <a data-toggle="modal" data-target="#exampleModal" title="Quick View" data-myid="{{$product->id}}"><i class="la la-search"></i></a>
                           <a title="Add To Cart" onclick="addToCart({{$product->id}})"><i class="la la-cart-plus"></i></a>
                           <a title="Wishlist" onclick="addToWishlist({{$product->id}})"><i class="la la-heart-o"></i></a>
                        </div>
                     </div>
                     <div class="product-content product-content-padding">
                        <h4><a href="/product-detail/{{$product->slug}}">{{substr($product->name,0,20) }}.</a></h4>
                        <div class="price-addtocart">
                           <div class="product-price">
                              <span>₹{{$product->sell_price}}.00</span>
                           </div>
                        </div>
                     </div>
                  </div>
                  @endforeach               
                  @endif
               </div>
            </div>

            @endforeach


            @endif


         </div>
      </div>




   </div>
</div>


@endsection


@section('script')

<script>
     // Add to Cart
    function addToCart(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        // Swal(id);
        var quantity = 1;

      $.ajax({
         url: '/add-to-cart',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, quantity: quantity},
         success: function (data) {
           // Swal(data.cartItem);
           $('#cartItem').html(data.cartItem);
           $('#cartItem2').html(data.cartItem);
         },
         failure: function (data) {
           Swal(data.message);
         }
      });
   }

   // Add to Wishlist
   function addToWishlist(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?> ;

      if(userId == 0){
        Swal('Please Login First');
      }
      else{
        // Swal(id);

      $.ajax({
         url: '/add-to-wishlist',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, userId : userId},
         success: function (data) {
           // Swal(data.wishItem);
           $('#wishItem').html(data.wishItem);
           $('#wishItem2').html(data.wishItem);
         },
         failure: function (data) {
           Swal(data);
         }
      });
      }
   }
</script>

@endsection