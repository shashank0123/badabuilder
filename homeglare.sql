-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 06, 2019 at 01:21 AM
-- Server version: 5.6.44-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `homeglare`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pin_code` bigint(20) DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ship_to_different_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_pin_code` bigint(20) DEFAULT NULL,
  `shipping_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `user_id`, `address`, `country`, `city`, `state`, `pin_code`, `phone`, `created_at`, `updated_at`, `ship_to_different_address`, `shipping_address`, `shipping_country`, `shipping_city`, `shipping_state`, `shipping_pin_code`, `shipping_phone`) VALUES
(1, 2, 'uttam nagar delhi', NULL, 'uttam nagar', 'delhi', 11455, 6898989898, '2019-09-05 06:09:18', '2019-09-05 06:09:18', NULL, 'uttam nagar delhi', NULL, 'uttam nagar', 'delhi', 11455, '6898989898'),
(2, 5, 'RZ-10', NULL, 'uttam nagar', 'delhi', 110059, 9895529567, '2019-09-06 15:10:15', '2019-09-06 15:10:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `image_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyword` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Deactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `image_url`, `image_type`, `keyword`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'storage/banners/c6dfe9a5e8675524ecd627457b3d9457.png', 'slider', 'homeglare slider banner', 'slider_1', 'Active', '2019-08-29 16:42:38', '2019-08-29 19:50:52'),
(2, 'storage/banners/22f63cccddce0b34dcfa035fd5f62591.png', 'slider', 'homeglare slider banner 2', 'slider_2', 'Active', '2019-08-29 16:53:11', '2019-08-29 19:51:13'),
(3, 'storage/banners/be5f90e91893ef7afb921fcb638e40ba.png', 'slider', 'slider banner 3', 'slider_banner_3', 'Active', '2019-08-29 19:52:06', '2019-08-29 19:52:06'),
(4, 'storage/banners/2627801e63b669af06dc42d18441f1e1.png', 'portrait', 'portrait banner 1', 'portrait_banner_1', 'Active', '2019-08-29 19:53:09', '2019-08-29 19:53:09'),
(5, 'storage/banners/f37b768ad965a93bd09337c14a8a6a5e.png', 'medium', 'washing Machine', 'washing_machinee', 'Active', '2019-09-04 14:04:00', '2019-09-05 12:46:32');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `heading` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cart` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_description` text COLLATE utf8mb4_unicode_ci,
  `featured` char(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Deactive',
  `slug` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `category_description`, `featured`, `status`, `slug`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Kitchen appliances', 'Daily Use', 'yes', 'Active', 'daily_use', '0', '2019-08-28 19:29:31', '2019-08-28 19:29:31'),
(2, 'Decoration', 'Daily Use', 'yes', 'Active', 'Decoration', '0', '2019-08-29 14:19:09', '2019-08-29 14:19:09'),
(3, 'Home Appliances', 'Our home appliances store is designed to allow you to run your home efficiently and smoothly. From air purifiers , water purifiers , room heaters , irons to geysers , you need not venture anyplace else for your daily needs. Shop for home appliances online on Homeglare by filtering within top-selling brands, discounts and bank offers for best results.', 'yes', 'Active', 'home_appliances', '0', '2019-09-04 14:12:44', '2019-09-04 14:12:44'),
(4, 'Fruits', 'Fruits', 'yes', 'Active', 'fruits', '0', '2019-09-05 12:47:34', '2019-09-05 12:47:34');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `support_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_discription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_us` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `privacy_policy` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `return_policy` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `email`, `address`, `contact_no`, `support_email`, `sort_discription`, `about_us`, `privacy_policy`, `return_policy`, `created_at`, `updated_at`) VALUES
(1, 'HomeGlare', 'noreply@homeglare.com', '<p>C-191 sector -1 Bawana industrial area Delhi</p>', '8826361066', 'support@homeglare.com', '<p>We are best at our domain.</p>', '<p>this is about company</p>', '<p>its privacy</p>', '<p>return policy</p>', '2019-09-02 01:36:36', '2019-09-06 13:54:48');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `status`, `created_at`, `updated_at`) VALUES
(1, 'can we take cash on delivery', '<p>Yes!</p>', 'active', '2019-09-02 23:41:50', '2019-09-05 13:03:32'),
(2, 'can i have multiple product at a time', '<p>Yes You can buy multiple prooduct at a time</p>', 'active', '2019-09-02 23:56:06', '2019-09-02 23:58:11');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE `newsletters` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(10) UNSIGNED NOT NULL,
  `offer_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `offer_sub_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `offer_short_description` text COLLATE utf8mb4_unicode_ci,
  `offer_long_description` text COLLATE utf8mb4_unicode_ci,
  `image_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `offer_title`, `offer_sub_title`, `offer_short_description`, `offer_long_description`, `image_url`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Offer', 'Ofeer', 'oFeer', 'Offer', 'offers/1d05f1c8cef5691525c57047372e280a.png', 'Active', '2019-09-05 12:51:10', '2019-09-05 12:51:10');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `date` date DEFAULT NULL,
  `price` int(11) NOT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `encrypted_payment_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `address`, `date`, `price`, `payment_method`, `transaction_id`, `encrypted_payment_id`, `status`, `created_at`, `updated_at`) VALUES
(6, 8, '123    ', NULL, 1200, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-09-05 12:19:33', '2019-09-05 12:19:33'),
(3, 5, 'uttam nagar delhi uttam nagar delhi 11455 ', NULL, 550, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-09-03 19:45:46', '2019-09-03 19:45:46'),
(4, 5, 'uttam nagar delhi uttam nagar delhi 11455 ', NULL, 200, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-09-03 19:51:32', '2019-09-03 19:51:32'),
(5, 5, 'uttam nagar delhi uttam nagar delhi 11455 ', NULL, 200, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-09-03 19:54:34', '2019-09-03 19:54:34'),
(7, 5, 'uttam nagar delhi uttam nagar delhi 11455 ', NULL, 0, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-09-05 14:20:39', '2019-09-05 14:20:39'),
(8, 11, 'sadasd    ', NULL, 700, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-09-05 16:09:54', '2019-09-05 16:09:54'),
(9, 5, 'RZ-10 uttam nagar delhi 110059 ', NULL, 325, 'cash_on_delivery', NULL, NULL, 'order_booked', '2019-09-06 15:10:15', '2019-09-06 15:10:15');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `price` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `item_id`, `quantity`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 1, 400, '2019-08-29 19:47:10', '2019-08-29 19:47:10'),
(2, 2, 3, 2, 450, '2019-08-29 20:37:41', '2019-08-29 20:37:41'),
(3, 3, 10, 1, 200, '2019-09-03 19:45:46', '2019-09-03 19:45:46'),
(4, 3, 11, 1, 350, '2019-09-03 19:45:46', '2019-09-03 19:45:46'),
(5, 4, 8, 1, 200, '2019-09-03 19:51:32', '2019-09-03 19:51:32'),
(6, 5, 8, 1, 200, '2019-09-03 19:54:34', '2019-09-03 19:54:34'),
(7, 6, 7, 4, 1200, '2019-09-05 12:19:33', '2019-09-05 12:19:33'),
(8, 8, 11, 2, 700, '2019-09-05 16:09:54', '2019-09-05 16:09:54'),
(9, 9, 1, 1, 100, '2019-09-06 15:10:15', '2019-09-06 15:10:15'),
(10, 9, 3, 1, 225, '2019-09-06 15:10:15', '2019-09-06 15:10:15');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `extras` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_descriptions` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `long_descriptions` text COLLATE utf8mb4_unicode_ci,
  `mrp` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sell_price` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `availability` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  `product_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_description` text COLLATE utf8mb4_unicode_ci,
  `page_keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trending` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `category_id` bigint(20) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_material` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_quantity` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `short_descriptions`, `long_descriptions`, `mrp`, `sell_price`, `slug`, `image1`, `image2`, `image3`, `product_weight`, `availability`, `product_size`, `product_color`, `page_title`, `page_description`, `page_keywords`, `trending`, `category_id`, `status`, `created_at`, `updated_at`, `product_brand`, `product_material`, `min_quantity`) VALUES
(1, 'Cutter Green', 'Green Cutter', 'Green Cutter', '200', '100', 'Cutter_green', 'storage/products/0090a158c1fe19bf5e91ff3300da5b69.png', 'storage/products/b9c67090c25eeaad21fd86cacda0ed9b.png', 'storage/products/cc1f617935e6bbfe07e38012ac33dba7.png', NULL, 'yes', NULL, 'green', 'Green Cutter', 'Green Cutter', 'Green Cutter', 'yes', 1, 'Active', '2019-08-28 19:38:32', '2019-09-05 12:49:47', NULL, NULL, NULL),
(2, 'Container', 'container', 'Colourful Container', '150', '100', 'container_pink', 'storage/products/a37f52cc45ba47a08d5d2c48b775bbe5.png', 'storage/products/26b3e234963131fb85616b190793fad5.png', 'storage/products/965ddd873e3260efe24a6c6f769aaf4b.png', NULL, 'yes', NULL, 'pink', 'container', 'Colourful container', 'container', 'yes', 1, 'Active', '2019-08-29 13:09:52', '2019-08-29 13:09:52', NULL, NULL, NULL),
(3, 'Shaker', 'Shaker', 'My Shaker', '250', '225', 'shaker_my', 'storage/products/a137a2c67d1b593d71df2388cb9ae8ce.png', 'storage/products/834c6bc8ddfea903f64662c4b886e435.png', 'storage/products/e7a7475505df784ca8ced6af6181a66a.png', NULL, 'yes', NULL, 'blue', 'My Shaker', 'My Shaker', 'My Shaker', 'yes', 1, 'Active', '2019-08-29 13:13:47', '2019-08-29 13:13:47', NULL, NULL, NULL),
(4, 'Sprinkler Set', 'Sprinkler', 'Sprinkler Set', '400', '250', 'Sprinkler_powder', 'storage/products/6f6355ae6bfedf371caec394bfb07bdc.png', 'storage/products/33c65fc5c68ca2aa2dad3cd02591882c.png', 'storage/products/183e9163e4262a484f61aa717614ba30.png', NULL, 'yes', NULL, 'transparent', 'Sprinkler Set', 'Sprinkler Set', 'Sprinkler Set', 'yes', 1, 'Active', '2019-08-29 13:20:17', '2019-08-29 14:44:56', NULL, NULL, NULL),
(5, 'CUP PLATES', 'CUP_PLATES', 'CUP_PLATES', '450', '400', 'CUP_PLATES', 'storage/products/2791c6702cdc663bc1e8983b407434f4.png', 'storage/products/f8dd81fcda86fbd947d969812bdbde9e.png', 'storage/products/b3cf1b2f2a6f0383cab2f24c4719b1fc.png', NULL, 'yes', NULL, 'Golden', 'CUP_PLATES', 'CUP_PLATES', 'CUP_PLATES', 'yes', 1, 'Active', '2019-08-29 13:24:11', '2019-08-29 13:35:06', NULL, NULL, NULL),
(6, 'Serving Spoon', 'Serving Spoon', 'Serving Spoon', '200', '150', 'Serving_Spoon', 'storage/products/0ed436159bfc5272424159e476cad350.png', 'storage/products/a71459e2fea4cfd77357251364cb0a1f.png', 'storage/products/c6d5b324efaa961e2f52f8f8217eb4fe.png', NULL, 'yes', NULL, 'green', 'Serving Spoon', 'Serving Spoon', 'Serving Spoon', 'yes', 1, 'Active', '2019-08-29 13:30:14', '2019-08-29 14:04:46', NULL, NULL, NULL),
(7, 'Coffee Jar', 'Coffee Jar', 'Coffee Jar', '400', '300', 'Coffee_Mug', 'storage/products/e688f8cc7f66ad17ad31d63e73b8b6c1.png', 'storage/products/e688f8cc7f66ad17ad31d63e73b8b6c1.png', 'storage/products/e688f8cc7f66ad17ad31d63e73b8b6c1.png', NULL, 'yes', NULL, 'All Color', 'Coffee Jar', 'Coffee Jar', 'Coffee Jar', 'yes', 1, 'Active', '2019-08-29 13:34:35', '2019-08-29 14:40:59', NULL, NULL, NULL),
(8, 'Designer Cup', 'Designer Cup', 'Designer Cup', '250', '200', 'Designer_Cup', 'storage/products/75252211842e181828189382c00fffd4.png', 'storage/products/7fbd339298e0a95d1c1c8445023f259d.png', 'storage/products/4898f39bdc5811fb2fce2b006886600d.png', NULL, 'yes', NULL, 'Pink', 'Designer Cup', 'Designer Cup', 'Designer Cup', 'yes', 1, 'Active', '2019-08-29 13:38:21', '2019-08-29 14:32:22', NULL, NULL, NULL),
(9, 'Paper Glass', 'Paper Glass', 'Paper Glass', '100', '50', 'Paper _Glass', 'storage/products/d1e3e47cdcda797afa7bf6c8e6e1391b.png', 'storage/products/608a214d6ab6af51322cb09b7303807a.png', 'storage/products/608a214d6ab6af51322cb09b7303807a.png', NULL, 'yes', NULL, 'white', 'Paper Glass', 'Paper Glass', 'Paper Glass', 'yes', 1, 'Active', '2019-08-29 13:48:35', '2019-08-29 14:30:39', NULL, NULL, NULL),
(10, 'Fruit Cutter', 'Fruit Cutter', 'Fruit Cutter', '300', '200', 'Fruit_Cutter', 'storage/products/d5cf5418356927fb60fa4a24cb4d426e.png', 'storage/products/9d5992771ca4ec85e7f9264666d36368.png', 'storage/products/bd346e23c71f19d7fa4ee9c7b0eb2390.png', NULL, 'yes', NULL, 'red', 'Fruit Cutter', 'Fruit Cutter', 'Fruit Cutter', 'yes', 1, 'Active', '2019-08-29 13:51:10', '2019-08-29 14:29:49', NULL, NULL, NULL),
(11, 'Vegetable Cutter', 'Vegetable Cutter', 'Vegetable Cutter', '400', '350', 'Vegetable_Cutter', 'storage/products/ce02f5e04a83f96464e8210c4c163d9f.png', 'storage/products/6933d3128f47f4b10c788e65ae6139e3.png', 'storage/products/355dc359ebf58b88294ac2556198f3f7.png', NULL, 'yes', NULL, 'red', 'Vegetable Cutter', 'Vegetable Cutter', 'Vegetable Cutter', 'yes', 1, 'Active', '2019-08-29 13:58:06', '2019-08-29 14:27:52', NULL, NULL, NULL),
(12, 'Spoon Stand', 'Spoon Stand', 'Spoon Stand', '250', '200', 'Spoon_Stand', 'storage/products/c6052e3c7ea82eaaf55397ec2a4db9f1.png', 'storage/products/e89e501f58177d2815bd0b115b12d188.png', 'storage/products/1cba25399a73f390624edfb86089f047.png', NULL, 'yes', NULL, 'green', 'Spoon Stand', 'Spoon Stand', 'Spoon Stand', 'yes', 1, 'Active', '2019-08-29 14:12:42', '2019-08-29 14:12:42', NULL, NULL, NULL),
(13, 'Glass Pot', 'Glass Pot', 'Glass Pot', '400', '399', 'Glass_Pot', 'storage/products/e4aadff901af9bb442476c8266a94c9c.png', 'storage/products/e980b86df0617856eb8f5ee2cfd02129.png', 'storage/products/43ebe4d3cc68e3f9065ce4ffc4aca06c.png', NULL, 'yes', NULL, 'red', 'Glass Pot', 'Glass Pot', 'Glass Pot', 'yes', 2, 'Active', '2019-08-29 14:21:44', '2019-08-29 14:21:44', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `queries`
--

CREATE TABLE `queries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message_content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `response_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `queries`
--

INSERT INTO `queries` (`id`, `name`, `email`, `subject`, `message_content`, `response_status`, `created_at`, `updated_at`) VALUES
(1, 'ravi', 'ravi@gmail.com', 'avenyee', 'hi', 'awating', '2019-09-03 20:00:24', '2019-09-03 20:00:24'),
(2, 'ravi', 'ravi@gmail.com', 'avenyee', 'hi', 'awating', '2019-09-03 20:02:50', '2019-09-03 20:02:50');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review` text COLLATE utf8mb4_unicode_ci,
  `rating` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `client_name`, `rating`, `review`, `city`, `image_url`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(2, 'sonu', '5', 'this is super one', 'delhi', 'storage/testimonial/8a7675e3e6c1393390de75b23c4956bb.png', NULL, 'Active', '2019-09-03 20:04:14', '2019-09-03 20:04:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `provider`, `provider_id`, `access_type`, `status`) VALUES
(1, 'admin', 'somphpdeveloper@gmail.com', NULL, '$2y$10$mLHpfVV/5eyudckfNyprd.bKXIn1rY5EAwYymuCPpElWK5LGaaPjO', NULL, '2019-08-28 19:28:01', '2019-08-30 16:38:10', 'google', '116599215729127144581', 'admin', 'inactive'),
(2, 'Sonu', 'sonubackstage@gmail.com', NULL, '$2y$10$1.GdAr6BLmYDAg1xYZbEVO45fWx.0QInOcHgQTAfec3d0rn.55nyO', NULL, '2019-08-28 20:24:52', '2019-09-05 12:30:59', 'google', '108199582828179969920', 'admin', 'active'),
(4, 'som', 'som@backstagesupporters.com', NULL, '$2y$10$2ygTPMLs9ciHPaNrgx0V0eSBzjlzkJt4KcwpTKV8q2SRjBwi9OC2O', NULL, '2019-08-29 16:26:55', '2019-08-29 16:26:55', NULL, NULL, 'customer', 'active'),
(5, 'raman', 'raman@gmail.com', NULL, '$2y$10$uwBvNhUaqEWsV8J5VbjSgOd20tPo1YfzXJC4rTPHn4dteEoFr5xEW', NULL, '2019-08-29 19:42:25', '2019-08-29 19:42:25', NULL, NULL, 'customer', 'active'),
(7, 'jyotika setthi', 'jyotika@backstagesupporters.com', NULL, NULL, NULL, '2019-08-30 14:08:07', '2019-08-30 14:08:07', 'google', '113595589106945456232', 'customer', 'active'),
(8, 'vicky', 'v@b', NULL, '$2y$10$S.HgqX.CRe.T/l43aJyDZ.UO06irq07k5ndjeLlV6OEK/Y9ePB/Ne', NULL, '2019-09-05 12:13:01', '2019-09-05 12:13:01', NULL, NULL, 'customer', 'active'),
(9, 'ravi', 'ravi@gmail.com', NULL, '$2y$10$1.GdAr6BLmYDAg1xYZbEVO45fWx.0QInOcHgQTAfec3d0rn.55nyO', NULL, '2019-09-05 12:47:53', '2019-09-05 12:47:53', NULL, NULL, 'customer', 'active'),
(10, 'mohan', 'mohan@gmail.com', NULL, '$2y$10$/Ny7Z2S1NH.y1xj7q3seD.Lc3OrRpbDXiMR.fRZYp6b40Nn5uR3aW', NULL, '2019-09-05 15:14:57', '2019-09-05 15:14:57', NULL, NULL, 'customer', 'active'),
(11, 'radha mohan', 'rmp@gmail.com', NULL, '$2y$10$48H7j1cq8FRWWvkD0mxBVeBr9lMk7OG.o70G7jZXZ/uFVLCPzVd6K', NULL, '2019-09-05 15:15:43', '2019-09-05 15:15:43', NULL, NULL, 'customer', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blogs_slug_unique` (`slug`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `brands_slug_unique` (`slug`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_slug_unique` (`slug`);

--
-- Indexes for table `queries`
--
ALTER TABLE `queries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `queries`
--
ALTER TABLE `queries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
